import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToggleService {
  private opened = true;

  constructor() {
  }

  isOpen() {
    return this.opened;
  }

  setOpen(state: boolean) {
    this.opened = state;
  }
}
