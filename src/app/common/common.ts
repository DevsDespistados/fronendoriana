import * as moment from 'moment';

export function parseObject(obj) {
  if (obj instanceof FormData) {
    return obj;
  }
  const b = {};
  for (const objKey in obj) {
    if (obj[objKey] === null) {
      b[objKey] = null;
    } else if (typeof obj[objKey] === 'object') {
      b[objKey] = parseObject(obj[objKey]);
    } else if (moment(new Date(obj[objKey])).isValid()) {
      b[objKey] = moment(new Date(obj[objKey])).format('YYYY-MM-DD');
    } else {
      b[objKey] = obj[objKey];
    }
  }
  return b;
}

export function ENCODE_DATA_QUERY(data) {
  const ret = [];
  // tslint:disable-next-line:forin
  for (const d in data) {
    ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
  }
  return ret.join('&');
}
