import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {OrianaHomePersonalComponent} from './components/personal/oriana-home-personal/oriana-home-personal.component';
import {ViewPrintComponent} from './components/navigation/view-print/view-print.component';
import {OrianaHomeEquipmentComponent} from './components/equipment/oriana-home-equipment/oriana-home-equipment.component';
import {OrianaHomeProjectComponent} from './components/projects/oriana-home-project/oriana-home-project.component';
import {OperationsHomeProjectComponent} from './components/projects/operations-home-project/operations-home-project.component';
import {OrianaHomeContractsComponent} from './components/contracts/oriana-home-contracts/oriana-home-contracts.component';
import {OrianaHomeItemsContractsComponent} from './components/contracts/oriana-home-items-contracts/oriana-home-items-contracts.component';
import {OrianaHomeFormsComponent} from './components/forms/oriana-home-forms/oriana-home-forms.component';
import {OrianaHomeProviderFormsComponent} from './components/forms/provider/oriana-home-provider-forms/oriana-home-provider-forms.component';
import {OrianaHomeProviderFormPlanerComponent} from './components/forms/provider/oriana-home-provider-form-planer/oriana-home-provider-form-planer.component';
import {OrianaHomePermanentFormPlanerComponent} from './components/forms/permanent/oriana-home-permanent-form-planer/oriana-home-permanent-form-planer.component';
import {OrianaHomePermanenteFormConsolidateComponent} from './components/forms/permanent/oriana-home-permanente-form-consolidate/oriana-home-permanente-form-consolidate.component';
import {OrianaFormsContractsComponent} from './components/forms/provider/oriana-forms-contracts/oriana-forms-contracts.component';
import {PermanentFormsComponent} from './components/forms/permanent/permanent-forms/permanent-forms.component';
import {OrianaClientComponent} from './components/personal/oriana-client/oriana-client.component';
import {OrianaProviderComponent} from './components/personal/oriana-provider/oriana-provider.component';
import {OrianaTransitComponent} from './components/personal/oriana-transit/oriana-transit.component';
import {OrianaEventualComponent} from './components/personal/oriana-eventual/oriana-eventual.component';
import {OrianaPermanentComponent} from './components/personal/oriana-permanent/oriana-permanent.component';
import {OrianaCompanyComponent} from './components/personal/oriana-company/oriana-company.component';
import {TracingProjectComponent} from './components/projects/tracing-project/tracing-project.component';
import {ModificationsProjectComponent} from './components/projects/modifications-project/modifications-project.component';
import {FormsProjectComponent} from './components/projects/forms-project/forms-project.component';
import {OrianaItemsContractsComponent} from './components/contracts/oriana-items-contracts/oriana-items-contracts.component';
import {EventualFormsComponent} from './components/forms/eventual/eventual-forms/eventual-forms.component';
import {OrianaHomeEventualFormConsolidateComponent} from './components/forms/eventual/oriana-home-eventual-form-consolidate/oriana-home-eventual-form-consolidate.component';
import {OrianaHomeEvetualPlanerComponent} from './components/forms/eventual/oriana-home-evetual-planer/oriana-home-evetual-planer.component';
import {EquipmentFormsComponent} from './components/forms/equipment/equipment-forms/equipment-forms.component';
import {OrianaHomeEquipmentFormConsolidateComponent} from './components/forms/equipment/oriana-home-equipment-form-consolidate/oriana-home-equipment-form-consolidate.component';
import {OrianaHomeEquipmentFormPlanerComponent} from './components/forms/equipment/oriana-home-equipment-form-planer/oriana-home-equipment-form-planer.component';
import {ProjectForFormsComponent} from './components/forms/project-for-forms/project-for-forms.component';
import {OrianaLoginComponent} from './components/users/oriana-login/oriana-login.component';
import {LoginGuard} from './guards/login.guard';
import {OrianaHomeAccountsComponent} from './components/accounts/oriana-home-accounts/oriana-home-accounts.component';
import {AccountBanksComponent} from './components/accounts/bank/account-banks/account-banks.component';
import {OrianaHomeBankPeriodComponent} from './components/accounts/bankPeriod/oriana-home-bank-period/oriana-home-bank-period.component';
import {BankCheckComponent} from './components/accounts/check/bank-check/bank-check.component';
import {OrianaHomeCheckTransacctionComponent} from './components/accounts/oriana-home-check-transacction/oriana-home-check-transacction.component';
import {OrianaHomeTransactionComponent} from './components/accounts/transactions/oriana-home-transaction/oriana-home-transaction.component';
import {AllTransactionsComponent} from './components/accounts/transactions/all-transactions/all-transactions.component';
import {PettyCashOfficeComponent} from './components/accounts/pettyCashOffice/petty-cash-office/petty-cash-office.component';
import {OrianaHomePettyCashOfficePeriodComponent} from './components/accounts/pettyCashOfficePeriod/oriana-home-petty-cash-office-period/oriana-home-petty-cash-office-period.component';
import {OrianaHomePettyCashOfficeTransactionComponent} from './components/accounts/oriana-home-petty-cash-office-transaction/oriana-home-petty-cash-office-transaction.component';
import {PettyCashAccountsComponent} from './components/accounts/pettyCash/petty-cash-accounts/petty-cash-accounts.component';
import {OrianaHomePettyCashPeriodComponent} from './components/accounts/pettyCashPeriod/oriana-home-petty-cash-period/oriana-home-petty-cash-period.component';
import {OrianaHomePettyCashTransactionsComponent} from './components/accounts/pettyCashTransactions/oriana-home-petty-cash-transactions/oriana-home-petty-cash-transactions.component';
import {TransferProjectComponent} from './components/accounts/accountProjects/transfer-project/transfer-project.component';
import {OrianaHomeReportsComponent} from './components/reports/oriana-home-reports/oriana-home-reports.component';
import {TemplateAccontsReportsComponent} from './components/reports/template-acconts-reports/template-acconts-reports.component';
import {OrianaHomeGuaranteesComponent} from './components/guarantee/oriana-home-guarantees/oriana-home-guarantees.component';
import {CreditLineComponent} from './components/guarantee/credit-line/credit-line.component';
import {TicketFinancialComponent} from './components/guarantee/ticket-financial/ticket-financial.component';
import {TicketsProjectsComponent} from './components/guarantee/tickets-projects/tickets-projects.component';
import {OrianaHomeLettersComponent} from './components/letters/oriana-home-letters/oriana-home-letters.component';
import {LettersSendComponent} from './components/letters/letters-send/letters-send.component';
import {LettersReceivedComponent} from './components/letters/letters-received/letters-received.component';
import {LettersProjectsComponent} from './components/letters/letters-projects/letters-projects.component';
import {OrianaHomeTracingLettersComponent} from './components/letters/oriana-home-tracing-letters/oriana-home-tracing-letters.component';
import {OrianaHomeOrderComponent} from './components/order/oriana-home-order/oriana-home-order.component';
import {OrderItemsComponent} from './components/order/order-items/order-items.component';
import {OrderPetitionComponent} from './components/order/order-petition/order-petition.component';
import {HomeOrderPetitionItemsComponent} from './components/order/home-order-petition-items/home-order-petition-items.component';
import {OrderQuotationComponent} from './components/order/order-quotation/order-quotation.component';
import {HomeOrderQuotationItemComponent} from './components/order/home-order-quotation-item/home-order-quotation-item.component';
import {HomeOrderQuotationBuyComponent} from './components/order/home-order-quotation-buy/home-order-quotation-buy.component';
import {HomeOrderPetitionTracingComponent} from './components/order/home-order-petition-tracing/home-order-petition-tracing.component';
import {HomePeriodBillingComponent} from './components/facturacion/home-period-billing/home-period-billing.component';
import {OrianaInvoiceComponent} from './components/facturacion/oriana-invoice/oriana-invoice.component';
import {OrianaChecksComponent} from './components/facturacion/oriana-checks/oriana-checks.component';
import {OrianaRenderComponent} from './components/facturacion/oriana-render/oriana-render.component';
import {OrianaHomeInvoicesComponent} from './components/facturacion/oriana-home-invoices/oriana-home-invoices.component';
import {OrianaHomeUsersComponent} from './components/users/oriana-home-users/oriana-home-users.component';
import {HomeUserPermisionComponent} from './components/users/home-user-permision/home-user-permision.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent, canActivate: [LoginGuard]},
  {
    path: 'personal', component: OrianaHomePersonalComponent, canActivate: [LoginGuard], children: [
      {path: 'clients', component: OrianaClientComponent, canActivate: [LoginGuard]},
      {path: 'providers', component: OrianaProviderComponent, canActivate: [LoginGuard]},
      {path: 'transits', component: OrianaTransitComponent, canActivate: [LoginGuard]},
      {path: 'eventual', component: OrianaEventualComponent, canActivate: [LoginGuard]},
      {path: 'permanents', component: OrianaPermanentComponent, canActivate: [LoginGuard]},
      {path: 'companies', component: OrianaCompanyComponent, canActivate: [LoginGuard]},
    ]
  },
  {path: 'ownerEquipment', component: OrianaHomeEquipmentComponent, canActivate: [LoginGuard]},
  {path: 'projects', component: OrianaHomeProjectComponent, canActivate: [LoginGuard]},
  {
    path: 'projects/:id', component: OperationsHomeProjectComponent, canActivate: [LoginGuard], children: [
      {path: 'tracing', component: TracingProjectComponent, canActivate: [LoginGuard]},
      {path: 'modifications', component: ModificationsProjectComponent, canActivate: [LoginGuard]},
      {path: 'forms', component: FormsProjectComponent, canActivate: [LoginGuard]},
    ]
  },
  {path: 'contracts', component: OrianaHomeContractsComponent, canActivate: [LoginGuard]},
  {
    path: 'contracts/:id', component: OrianaHomeItemsContractsComponent, children: [
      {path: 'item/:type', component: OrianaItemsContractsComponent, canActivate: [LoginGuard]},
    ]
  },
  {
    path: 'tickets', component: OrianaHomeGuaranteesComponent, canActivate: [LoginGuard], children: [
      {path: 'creditLine', component: CreditLineComponent, canActivate: [LoginGuard]},
      {path: 'guarantees', component: TicketFinancialComponent, canActivate: [LoginGuard]},
      {path: 'projects', component: TicketsProjectsComponent, canActivate: [LoginGuard]},
    ]
  },
  {
    path: 'letters', component: OrianaHomeLettersComponent, canActivate: [LoginGuard], children: [
      {path: 'send', component: LettersSendComponent, canActivate: [LoginGuard]},
      {path: 'received', component: LettersReceivedComponent, canActivate: [LoginGuard]},
      {path: 'projects', component: LettersProjectsComponent, canActivate: [LoginGuard]},
    ]
  },
  {
    path: 'letters/send/:idLetter', component: OrianaHomeTracingLettersComponent, canActivate: [LoginGuard]
  },
  {
    path: 'letters/received/:idLetter', component: OrianaHomeTracingLettersComponent, canActivate: [LoginGuard]
  },
  {
    path: 'forms', component: OrianaHomeFormsComponent, canActivate: [LoginGuard], children: [
      {path: 'provider', component: OrianaFormsContractsComponent, canActivate: [LoginGuard]},
      {path: 'permanent', component: PermanentFormsComponent, canActivate: [LoginGuard]},
      {path: 'eventual', component: EventualFormsComponent, canActivate: [LoginGuard]},
      {path: 'equipment', component: EquipmentFormsComponent, canActivate: [LoginGuard]},
      {path: 'projects', component: ProjectForFormsComponent, canActivate: [LoginGuard]},
    ]
  },
  {
    path: 'order', component: OrianaHomeOrderComponent, canActivate: [LoginGuard], children: [
      {path: 'items', component: OrderItemsComponent, canActivate: [LoginGuard]},
      {path: 'quotation', component: OrderQuotationComponent, canActivate: [LoginGuard]},
      {path: 'petition', component: OrderPetitionComponent, canActivate: [LoginGuard]},
    ]
  },
  {
    path: 'order/petition/:idOrder', component: HomeOrderPetitionItemsComponent, canActivate: [LoginGuard]
  },
  {
    path: 'order/quotation/:idOrder', component: HomeOrderQuotationItemComponent, canActivate: [LoginGuard]
  },
  {
    path: 'order/quotation/buys/:idOrder', component: HomeOrderQuotationBuyComponent, canActivate: [LoginGuard]
  },
  {
    path: 'order/petition/buys/:idOrder', component: HomeOrderPetitionTracingComponent, canActivate: [LoginGuard]
  },
  {path: 'forms/provider/:idP/contract/:idC', component: OrianaHomeProviderFormsComponent, canActivate: [LoginGuard]},
  {path: 'forms/provider/:idP/contract/:idC/planer/:idPeriod', component: OrianaHomeProviderFormPlanerComponent, canActivate: [LoginGuard]},
  {path: 'forms/permanent/:idP', component: OrianaHomePermanenteFormConsolidateComponent, canActivate: [LoginGuard]},
  {path: 'forms/permanent/:idP/planerPermanent/:idPermanent', component: OrianaHomePermanentFormPlanerComponent, canActivate: [LoginGuard]},
  {path: 'forms/eventual/:idP', component: OrianaHomeEventualFormConsolidateComponent, canActivate: [LoginGuard]},
  {path: 'forms/eventual/:idP/planerEventual/:idEventual', component: OrianaHomeEvetualPlanerComponent, canActivate: [LoginGuard]},
  {path: 'forms/equipment/:idP', component: OrianaHomeEquipmentFormConsolidateComponent, canActivate: [LoginGuard]},
  {path: 'forms/equipment/:idP/planerEquipment/:idEquipment', component: OrianaHomeEquipmentFormPlanerComponent, canActivate: [LoginGuard]},
  {
    path: 'accounts', component: OrianaHomeAccountsComponent, canActivate: [LoginGuard], children: [
      {path: 'banks', component: AccountBanksComponent, canActivate: [LoginGuard]},
      {path: 'pettyCashOffice', component: PettyCashOfficeComponent, canActivate: [LoginGuard]},
      {path: 'pettyCash', component: PettyCashAccountsComponent, canActivate: [LoginGuard]},
      {path: 'projects', component: TransferProjectComponent, canActivate: [LoginGuard]}
    ]
  },
  {path: 'accounts/banks/:idB/periods', component: OrianaHomeBankPeriodComponent, canActivate: [LoginGuard]},
  {path: 'accounts/pettyCashOffice/:idB/periods', component: OrianaHomePettyCashOfficePeriodComponent, canActivate: [LoginGuard]},
  {path: 'accounts/pettyCash/:idB/periods', component: OrianaHomePettyCashPeriodComponent, canActivate: [LoginGuard]},
  {
    path: 'accounts/banks/:idB/periods/:idP/movements', component: OrianaHomeCheckTransacctionComponent, canActivate: [LoginGuard],
    children: [
      {path: 'checks', component: BankCheckComponent, canActivate: [LoginGuard]},
      {path: 'transactions/all', component: AllTransactionsComponent, canActivate: [LoginGuard]},
    ]
  },
  {
    path: 'accounts/banks/:idB/periods/:idP/movements/check/:idC/transactions',
    component: OrianaHomeTransactionComponent, canActivate: [LoginGuard]
  },
  {
    path: 'accounts/pettyCashOffice/:idB/periods/:idP/movements',
    component: OrianaHomePettyCashOfficeTransactionComponent, canActivate: [LoginGuard]
  },
  {
    path: 'accounts/pettyCash/:idB/periods/:idP/movements',
    component: OrianaHomePettyCashTransactionsComponent, canActivate: [LoginGuard]
  },
  {
    path: 'reports', component: OrianaHomeReportsComponent, children: [
      {path: 'accounts', component: TemplateAccontsReportsComponent, canActivate: [LoginGuard]}
    ]
  },
  {path: 'invoice', component: HomePeriodBillingComponent, canActivate: [LoginGuard]},
  {
    path: 'invoice/process/:idPeriod', component: OrianaHomeInvoicesComponent, canActivate: [LoginGuard], children: [
      {path: 'checks', component: OrianaChecksComponent, canActivate: [LoginGuard]},
      {path: 'invoices', component: OrianaInvoiceComponent, canActivate: [LoginGuard]},
      {path: 'render', component: OrianaRenderComponent, canActivate: [LoginGuard]},
    ]
  },
  {path: 'users', component: OrianaHomeUsersComponent, canActivate: [LoginGuard]},
  {path: 'users/:id/permission', component: HomeUserPermisionComponent, canActivate: [LoginGuard]},
  {path: 'print', component: ViewPrintComponent, canActivate: [LoginGuard]},
  {path: 'login', component: OrianaLoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
