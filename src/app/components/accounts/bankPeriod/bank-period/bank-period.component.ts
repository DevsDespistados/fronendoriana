import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TotalForm} from '../../../../model/total-form';
import {ViewContract} from '../../../../model/contract/view-contract';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {Period} from '../../../../model/accounts/period';
import {ViewBankPeriod} from '../../../../model/accounts/view-bank-period';
import {PeriodService} from '../../../../services/accounts/period.service';
import {ModalBankPeriodComponent} from '../modal-bank-period/modal-bank-period.component';
import {_permission} from '../../../../services/permissions';
import {Bank} from '../../../../model/accounts/bank';

@Component({
  selector: 'app-bank-period',
  templateUrl: './bank-period.component.html',
  styleUrls: ['./bank-period.component.css']
})
export class BankPeriodComponent implements OnInit {
  periods: ViewBankPeriod[] = [];
  @Input() idBank;
  @Output() bank = new EventEmitter();
  @Input() isbankPeriod = true;
  total: TotalForm;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'mount',
    'options'
  ];
  textTooltip = ['ABIERTO', 'CERRADO'];
  sortedData: ViewContract[];
  options = [
    new OptionOrianaTableToolbar('ABIERTO', 0),
    new OptionOrianaTableToolbar('CERRADO', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 0;
  period: Period;
  bankSelect: Bank;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private periodService: PeriodService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalBankPeriodComponent, {
      width: '400px',
      data: new DataBankPeriod(this.period)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.period.idBankPeriod) {
          this.progress = true;
          this.periodService.addPeriod(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        } else {
          this.progress = true;
          this.periodService.editAccountPeriod(this.period.idBankPeriod, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(ele: ViewBankPeriod) {
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: ele.bankPeriod}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.periodService.deletePeriod(ele.idBankPeriod)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.periods[pos];
    this.period = new Period(
      p.idBankPeriod,
      p.bankPeriod,
      p.stateBankPeriod,
      p.transactions,
      p.idBank
    );
    this.create();
  }

  getFilter(type: any) {
    this.periodService.getPeriods(this.idBank, type).subscribe(
      res => {
        this.periods = res['periods'];
        this.bankSelect = res['bank'];
        this.bank.emit(res['bank']);
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        this.progress = false;
      });
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'PERIODO', 'MONTO\nPERIODO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PERIODO\nCUENTA: ' +
      this.bankSelect.nameAccountBank.toUpperCase(), head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.periods.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.bankPeriod.toUpperCase());
      d.push(this.printService.moneyData(c.mount_period));
      d.push(c.stateBankPeriod == 1 ? 'CERRADO' : 'ABIERTO');
      data.push(d);
    });
    data.push([
      '',
      '',
      this.printService.moneyData(this.totalPeriod()),
      ''
    ]);
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.periodService.searchPeriod(text).subscribe(
        response => {
          this.periods = response['periods'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.periodService.setPeriod(this.periods[pos].idBankPeriod, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.period = new Period(
      null,
      null,
      false,
      0,
      this.idBank
    );
  }

  getRoutePettyCashOrBank(ele: ViewBankPeriod) {
    if (this.isbankPeriod == true) {
      return '/accounts/banks/' + this.idBank + '/periods/' + ele.idBankPeriod + '/movements/';
    } else {
      return '/accounts/pettyCashOffice/' + this.idBank + '/periods/' + ele.idBankPeriod + '/movements';
    }
  }

  getTooltipText() {
    if (this.isbankPeriod == true) {
      return 'Cheques y Transacciones';
    } else {
      return 'Transacciones';
    }
  }

  totalPeriod() {
    let total = 0;
    this.periods.forEach(period => {
      total += period.mount_period;
    });
    return total;
  }

  getRouterBack() {
    if (this.isbankPeriod == true) {
      return '/accounts/banks';
    } else {
      return '/accounts/pettyCashOffice';
    }
  }

  isDisabled() {
    if (this.isbankPeriod == true) {
      return _permission().ac_b_periods != 1 || !this.bankSelect || this.bankSelect.activeBank == 0;
    } else {
      return _permission().ac_pto_period != 1 || !this.bankSelect || this.bankSelect.activeBank == 0;
    }
  }

  isDisabledCT() {
    if (this.isbankPeriod == true) {
      return _permission().ac_check == 0;
    } else {
      return _permission().ac_pto_transaction == 0;
    }
  }

  isDisabledP(ele: ViewBankPeriod) {
    return ele.mount_period != 0;
  }
}

export class DataBankPeriod {
  constructor(public period: Period) {
  }
}
