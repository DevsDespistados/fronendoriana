import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BankPeriodComponent} from './bank-period.component';

describe('BankPeriodComponent', () => {
  let component: BankPeriodComponent;
  let fixture: ComponentFixture<BankPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BankPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
