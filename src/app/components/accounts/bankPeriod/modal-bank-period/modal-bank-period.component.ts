import {Component, Inject} from '@angular/core';
import {ConfText, month} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {moment} from '../../../../services/GlobalFunctions';
import {Period} from '../../../../model/accounts/period';

@Component({
  selector: 'app-modal-bank-period',
  templateUrl: './modal-bank-period.component.html',
  styleUrls: ['./modal-bank-period.component.css']
})
export class ModalBankPeriodComponent {
  text = ConfText;
  formData: Period;
  period: Period;
  private date = new Date();
  year = this.date.getFullYear();
  month = this.date.getMonth();
  months = month;

  constructor(
    public dialogRef: MatDialogRef<ModalBankPeriodComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.period = this.data.period;
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.formData = this.period;
      this.formData.bankPeriod = moment(new Date(this.year, this.month, 1));
      this.dialogRef.close();
    }
  }

  validData() {
    return this.year == null || this.year <= 0 || this.month == null || this.month < 0;
  }
}
