import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalBankPeriodComponent} from './modal-bank-period.component';

describe('ModalBankPeriodComponent', () => {
  let component: ModalBankPeriodComponent;
  let fixture: ComponentFixture<ModalBankPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalBankPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBankPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
