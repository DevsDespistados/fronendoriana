import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Bank} from '../../../../model/accounts/bank';

@Component({
  selector: 'app-oriana-home-bank-period',
  templateUrl: './oriana-home-bank-period.component.html',
  styleUrls: ['./oriana-home-bank-period.component.css']
})
export class OrianaHomeBankPeriodComponent implements OnInit {

  titles = 'Periodo Banco';
  idBank;
  bank: Bank;

  constructor(private activeRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idBank = res['idB'];
    });
  }

  getNamePeriod() {
    if (this.bank) {
      return 'Banco: ' + this.bank.nameAccountBank;
    } else {
      return 'Banco: ';
    }
  }

  setBank(value: any) {
    this.bank = value;
  }
}
