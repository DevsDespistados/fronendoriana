import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeBankPeriodComponent} from './oriana-home-bank-period.component';

describe('OrianaHomeBankPeriodComponent', () => {
  let component: OrianaHomeBankPeriodComponent;
  let fixture: ComponentFixture<OrianaHomeBankPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeBankPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeBankPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
