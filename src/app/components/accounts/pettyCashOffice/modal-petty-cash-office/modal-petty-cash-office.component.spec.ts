import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPettyCashOfficeComponent} from './modal-petty-cash-office.component';

describe('ModalPettyCashOfficeComponent', () => {
  let component: ModalPettyCashOfficeComponent;
  let fixture: ComponentFixture<ModalPettyCashOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPettyCashOfficeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPettyCashOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
