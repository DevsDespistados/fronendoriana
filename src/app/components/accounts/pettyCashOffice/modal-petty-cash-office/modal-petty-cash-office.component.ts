import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {Bank} from '../../../../model/accounts/bank';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-petty-cash-office',
  templateUrl: './modal-petty-cash-office.component.html',
  styleUrls: ['./modal-petty-cash-office.component.css']
})
export class ModalPettyCashOfficeComponent {
  text = ConfText;
  formData;
  back: Bank;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.back = this.data.bank;
  }

  onNoClick(): void {
    this.formData = null;
    this.dialogRef.close();
  }

  datart() {
    if (!this.valid()) {
      this.formData = this.back;
      this.dialogRef.close();
    }
  }

  valid() {
    return this.back.nameAccountBank == null || this.back.nameAccountBank == '';
  }
}
