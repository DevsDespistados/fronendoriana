import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PettyCashOfficeComponent} from './petty-cash-office.component';

describe('PettyCashOfficeComponent', () => {
  let component: PettyCashOfficeComponent;
  let fixture: ComponentFixture<PettyCashOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PettyCashOfficeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PettyCashOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
