import {Component, OnInit} from '@angular/core';
import {DataBank} from '../../bank/account-banks/account-banks.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {AccountService} from '../../../../services/accounts/account.service';
import {Bank} from '../../../../model/accounts/bank';
import {OwnerEquipment} from '../../../../model/Equipment/owner-equipment';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalPettyCashOfficeComponent} from '../modal-petty-cash-office/modal-petty-cash-office.component';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-petty-cash-office',
  templateUrl: './petty-cash-office.component.html',
  styleUrls: ['./petty-cash-office.component.css']
})
export class PettyCashOfficeComponent implements OnInit {

  listBanks: Bank[] = [];
  total = 0;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'account',
    'balance',
    'options'
  ];
  sortedData: OwnerEquipment[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  bankSelect: Bank = new Bank(null, null, null, true, false);
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private accountService: AccountService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalPettyCashOfficeComponent, {
      width: '400px',
      data: new DataBank(this.bankSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.bankSelect.idBank) {
          this.accountService.addAccountBank(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.accountService.editAccountBank(this.bankSelect.idBank, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.bankSelect = <any>this.listBanks[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.bankSelect.nameAccountBank}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.accountService.deleteBank(this.bankSelect.idBank)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.listBanks[pos];
    this.bankSelect = new Bank(
      p.idBank,
      p.nameAccountBank,
      p.balanceBank,
      p.activeBank,
      p.isPettyCash
    );
    this.create();
  }

  getFilter(type: any) {
    this.accountService.getPettyCashOfficeActives(type).subscribe(
      res => {
        this.listBanks = res['banks'];
        this.total = res['total'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  setState(pos, state): any {
    this.accountService.setStateBank(this.listBanks[pos].idBank, {activeBank: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    // this.progress = true;
    // this.ac.report({projects: this.projects}).subscribe(
    //   res => {
    //     this.printService.setDataPrint(res['data']);
    //     this.progress = false;
    //   }, error1 => {
    //     this.progress = false;
    //     this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
    //   }
    // );
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.accountService.searchPettyCashOffice(text).subscribe(
        res => {
          this.listBanks = res['banks'];
          this.total = res['total'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.bankSelect = new Bank(
      null,
      null,
      null,
      true,
      true
    );
  }

  disabled(ele: Bank) {
    return this.isDisabledP() || ele.balanceBank != 0 || ele.activeBank == 0;
  }

  isDisabledP() {
    return _permission().ac_pto_pettyCashOffice != 1;
  }

  isDisabledO() {
    return _permission().ac_pto_period == 0; /// petty Cash office
  }
}
