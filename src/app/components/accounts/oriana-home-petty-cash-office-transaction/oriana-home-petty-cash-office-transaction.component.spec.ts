import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePettyCashOfficeTransactionComponent} from './oriana-home-petty-cash-office-transaction.component';

describe('OrianaHomePettyCashOfficeTransactionComponent', () => {
  let component: OrianaHomePettyCashOfficeTransactionComponent;
  let fixture: ComponentFixture<OrianaHomePettyCashOfficeTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePettyCashOfficeTransactionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePettyCashOfficeTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
