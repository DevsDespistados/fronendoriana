import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ViewPayCheck} from '../../../model/accounts/view-pay-check';
import {ActivatedRoute} from '@angular/router';
import {ViewBankPeriod} from '../../../model/accounts/view-bank-period';
import {ShortDatePipe} from '../../../pipes/short-date.pipe';

@Component({
  selector: 'app-oriana-home-petty-cash-office-transaction',
  templateUrl: './oriana-home-petty-cash-office-transaction.component.html',
  styleUrls: ['./oriana-home-petty-cash-office-transaction.component.css'],
  providers: [ShortDatePipe]
})
export class OrianaHomePettyCashOfficeTransactionComponent implements OnInit {


  titles = {
    module: 'TRANSACCIONES - CAJA CHICA OFICINA',
  };
  check: ViewPayCheck;
  period: ViewBankPeriod;
  bank;
  idBank;
  idPeriod;
  idCheck;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute,
              private periodPipe: ShortDatePipe) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idBank = res['idB'];
      this.idPeriod = res['idP'];
      this.idCheck = res['idC'];
    });
  }

  form(value: ViewPayCheck) {
    this.check = value;
  }

  getSubTitle() {
    if (this.period) {
      return 'CAJA CHICA OFICINA: ' + this.bank.nameAccountBank + ' / PERIODO: ' + this.periodPipe.transform(this.period.bankPeriod);
    } else {
      return 'CAJA CHICA OFICINA: / PERIODO:';
    }
  }

  updateData(data) {
    this.check = data.check;
    this.period = data.period;
    this.bank = data.bank;
  }
}
