import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {ViewProjects} from '../../../../model/project/view-projects';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectService} from '../../../../services/project/project.service';
import {endDate} from '../../../../services/GlobalFunctions';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {TransactionPettyCash} from '../../../../model/accounts/transaction-petty-cash';
import {PettyCash} from '../../../../model/accounts/petty-cash';
import {ViewPettyCashPeriod} from '../../../../model/accounts/view-petty-cash-period';

@Component({
  selector: 'app-modal-petty-cash-transaction',
  templateUrl: './modal-petty-cash-transaction.component.html',
  styleUrls: ['./modal-petty-cash-transaction.component.css']
})
export class ModalPettyCashTransactionComponent implements OnInit {
  transaction: TransactionPettyCash;
  text = ConfText;
  endDate;
  pettyCash: PettyCash;
  period: ViewPettyCashPeriod;
  projects: ViewProjects[] = [];


  constructor(public dialogRef: MatDialogRef<ModalPettyCashTransactionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectService: ProjectService,
              private snack: MatSnackBar) {
    this.transaction = this.data.transaction;
    this.transaction.amountPettyCashExpense = Math.abs(this.transaction.amountPettyCashExpense);
  }

  ngOnInit(): void {
    this.pettyCash = this.data.pettyCash;
    this.period = this.data.pettyCashPeriod;
    this.endDate = endDate(this.data.pettyCashPeriod.datePeriod);
    this.loadProjects();
  }

  accept() {
    this.dialogRef.close(this.transaction);
  }

  loadProjects() {
    this.projectService.getOpeAccount(1).subscribe(
      res => {
        this.projects = <ViewProjects[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  canceled() {
    this.dialogRef.close();
  }

  valid() {
    return this.transaction.datePettyCashExpense == null
      || this.transaction.descriptionPettyCashExpense == ''
      || this.transaction.descriptionPettyCashExpense == null
      || this.transaction.nroReceiptPettyCashExpense == null
      || this.transaction.nroReceiptPettyCashExpense == ''
      || this.transaction.amountPettyCashExpense <= 0
      || this.transaction.idProject == null;
  }
}
