import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPettyCashTransactionComponent} from './modal-petty-cash-transaction.component';

describe('ModalPettyCashTransactionComponent', () => {
  let component: ModalPettyCashTransactionComponent;
  let fixture: ComponentFixture<ModalPettyCashTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPettyCashTransactionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPettyCashTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
