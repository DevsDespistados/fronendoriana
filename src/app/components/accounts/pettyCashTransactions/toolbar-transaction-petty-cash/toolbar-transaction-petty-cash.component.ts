import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-toolbar-transaction-petty-cash',
  templateUrl: './toolbar-transaction-petty-cash.component.html',
  styleUrls: ['./toolbar-transaction-petty-cash.component.css']
})
export class ToolbarTransactionPettyCashComponent implements OnInit {
  @Input() title: string;
  @Input() onProgress = false;
  @Input() searchTittle = 'Buscar Transacciones';
  @Input() onNew = true;
  @Output() new = new EventEmitter();
  @Output() refresh = new EventEmitter();
  @Output() searching = new EventEmitter();
  tooltip = Tooltips;
  value;
  searchVisible = false;
  expense = [
    new OptionOrianaTableToolbar('Archivo', 1),
    new OptionOrianaTableToolbar('Egreso', 2)
  ];


  constructor(private activeRouter: ActivatedRoute) {
  }

  ngOnInit() {
    this.activeRouter.url.subscribe(
      res => {
        console.log(res);
      }
    );
  }

  clickNew(op) {
    this.new.emit(op);
  }

  clickRefresh() {
    this.refresh.emit(true);
  }

  setSearchVisible() {
    if (this.searchVisible) {
      this.searchVisible = false;
    } else {
      this.searchVisible = true;
    }
  }

  resetSearch() {
    this.value = '';
    this.searchVisible = false;
    this.searching.emit(this.value);
  }

  search() {
    this.searching.emit(this.value);
  }

  isVisible() {
    if (this.value) {
      return this.value.length < 1 ? 'hidden' : 'visible';
    } else {
      return 'hidden';
    }
  }
}
