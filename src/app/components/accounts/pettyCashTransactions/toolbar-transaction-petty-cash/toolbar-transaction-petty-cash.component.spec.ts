import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ToolbarTransactionPettyCashComponent} from './toolbar-transaction-petty-cash.component';

describe('ToolbarTransactionPettyCashComponent', () => {
  let component: ToolbarTransactionPettyCashComponent;
  let fixture: ComponentFixture<ToolbarTransactionPettyCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolbarTransactionPettyCashComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarTransactionPettyCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
