import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewTransactions} from '../../../../model/accounts/view-transactions';
import {TotalForm} from '../../../../model/total-form';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {Bank} from '../../../../model/accounts/bank';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {Period} from '../../../../model/accounts/period';
import {PayCheck} from '../../../../model/accounts/pay-check';
import {ViewProjects} from '../../../../model/project/view-projects';
import {TransactionPettyCash} from '../../../../model/accounts/transaction-petty-cash';
import {PettyCash} from '../../../../model/accounts/petty-cash';
import {ViewPettyCashPeriod} from '../../../../model/accounts/view-petty-cash-period';
import {ModalPettyCashTransactionComponent} from '../modal-petty-cash-transaction/modal-petty-cash-transaction.component';
import {ModalPettyCashTransactionFileComponent} from '../modal-petty-cash-transaction-file/modal-petty-cash-transaction-file.component';
import {DataInspectFile, ModalInspectImportFileComponent, RowFile} from '../modal-inspect-import-file/modal-inspect-import-file.component';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-petty-cash-transactions',
  templateUrl: './petty-cash-transactions.component.html',
  styleUrls: ['./petty-cash-transactions.component.css']
})
export class PettyCashTransactionsComponent implements OnInit {

  @Input() idPeriod;
  @Input() idPettyCash;
  @Output() data = new EventEmitter();
  transactions: TransactionPettyCash[];
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  titulo = ' ';
  displayedColumns = [
    'position',
    'date',
    'project',
    'respaldo',
    'description',
    'import',
    'options'
  ];
  sortedData: ViewTransactions[];
  transactionSelect: TransactionPettyCash;
  tooltip = Tooltips;
  currentOption = 0;
  progress = false;
  _pettyCash: PettyCash;
  _period: ViewPettyCashPeriod;

  constructor(public dialog: MatDialog,
              public snack: MatSnackBar,
              public printService: PrintServiceService,
              public transactionService: TransactionService) {
  }

  ngOnInit() {
    this.load();
  }


  create() {
    const dialogRef = this.dialog.open(ModalPettyCashTransactionComponent, {
      width: '400px',
      data: new DataTransactionPettyCash(this.transactionSelect, this._pettyCash, this._period)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.transactionSelect = result;
        if (!this.transactionSelect.idPettyCashExpense) {
          this.transactionService.addTransactionPettyCash(this.transactionSelect)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.transactionService.editTransactionPettyCash(this.transactionSelect.idPettyCashExpense, result)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  createFile() {
    const dialogRef = this.dialog.open(ModalPettyCashTransactionFileComponent, {
      width: '400px',
      data: new DataTransactionPettyCash(null, this._pettyCash, this._period)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.transactionService.importFile(result, this.idPeriod).subscribe(
          res => {
            this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: res['result']}});
            if (res['list']) {
              this.openInspect(res['list']);
            } else {
              this.load();
            }
          }, error => {
            console.log(error);
          }
        );
      }
    });
  }

  openInspect(data: RowFile[]) {
    const dialogRef = this.dialog.open(ModalInspectImportFileComponent, {
      width: '950px',
      data: new DataInspectFile(data)
    });
    dialogRef.afterClosed().subscribe(result => {
      this.progress = false;
      this.load();
    });
  }

  deleted(pos: number) {
    this.transactionSelect = <any>this.transactions[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.transactionSelect.nroReceiptPettyCashExpense}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.transactionService.deletePettyCashExpense(this.transactionSelect.idPettyCashExpense)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.transactions[pos];
    this.transactionSelect = new TransactionPettyCash(
      p.idPettyCashExpense,
      p.nameProject,
      p.idProject,
      p.datePettyCashExpense,
      p.nroReceiptPettyCashExpense,
      p.amountPettyCashExpense,
      p.descriptionPettyCashExpense,
      p.idPettyCashPeriod,
      p.idPettyCash,
      p.idTransaction
    );
    this.create();
  }

  getFilter(idPettyCash: number, idPeriod: number) {
    this.transactionService.getTransactionPettyCashPeriod(idPettyCash, idPeriod).subscribe(
      res => {
        console.log(res);
        this.transactions = res['expenses'];
        this._pettyCash = res['pettyCash'];
        this._period = res['pettyCashPeriod'];
        this.data.emit({pettyCash: this._pettyCash, period: this._period});
        this.total = <TotalForm>{total: 0};
        console.log(res);
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  setType(type): any {
    if (type == 2) {
      this.create();
    } else {
      this.createFile();
    }
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.idPettyCash, this.idPeriod);
  }

  printReport() {
    // this.progress = true;
    // this.ac.report({projects: this.projects}).subscribe(
    //   res => {
    //     this.printService.setDataPrint(res['data']);
    //     this.progress = false;
    //   }, error1 => {
    //     this.progress = false;
    //     this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
    //   }
    // );
  }

  //
  // printFacturation(id) {
  //   this.transactionService.printFact({idTransaction: id, user: _user()}).subscribe(
  //     res => {
  //       this.printService.setDataPrint(res['data']);
  //     }, error1 => {
  //       this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
  //     }
  //   );
  // }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.transactionService.searchPettyCashTransacction(text, this.idPeriod).subscribe(
        response => {
          this.transactions = response['transaction'];
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.transactionSelect = new TransactionPettyCash(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this.idPeriod,
      this.idPettyCash,
      null
    );
  }

  newDisabled() {
    return this._period ? this.isDisabled() && this._pettyCash.activePettyCash == 1 && this._period.statePettyCashPeriod == 0 : true;
  }

  totalTranssactions() {
    let total = 0;
    this.transactions.forEach(t => {
      total += t.amountPettyCashExpense;
    });
    return total;
  }

  isDisabled() {
    return _permission().ac_pt_transactions != 1;
  }
}

export class DataComplement {
  bank: Bank;
  period: Period;
  check: PayCheck;
  project: ViewProjects;
}

export class DataTransactionPettyCash {
  // bank: Bank;
  // project: ViewProjects;
  // check: ViewPayCheck;
  // period: Period;

  constructor(public transaction: TransactionPettyCash,
              public pettyCash: PettyCash,
              public pettyCashPeriod: ViewPettyCashPeriod) {
  }
}

