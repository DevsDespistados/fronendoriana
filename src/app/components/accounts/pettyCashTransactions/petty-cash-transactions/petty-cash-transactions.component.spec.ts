import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PettyCashTransactionsComponent} from './petty-cash-transactions.component';

describe('PettyCashTransactionsComponent', () => {
  let component: PettyCashTransactionsComponent;
  let fixture: ComponentFixture<PettyCashTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PettyCashTransactionsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PettyCashTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
