import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ViewPettyCashPeriod} from '../../../../model/accounts/view-petty-cash-period';
import {PettyCash} from '../../../../model/accounts/petty-cash';

@Component({
  selector: 'app-oriana-home-petty-cash-transactions',
  templateUrl: './oriana-home-petty-cash-transactions.component.html',
  styleUrls: ['./oriana-home-petty-cash-transactions.component.css']
})
export class OrianaHomePettyCashTransactionsComponent implements OnInit {


  titles = {
    module: 'TRANSACCIONES - PERIODO',
  };
  period: ViewPettyCashPeriod;
  pettyCash: PettyCash;
  idBank;
  idPeriod;
  idCheck;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idBank = res['idB'];
      this.idPeriod = res['idP'];
    });
  }

  form(value: ViewPettyCashPeriod) {
    this.period = value;
  }

  getSubTitle() {
    if (this.pettyCash) {
      return 'Caja Chica: ' + this.pettyCash.nameAccountPettyCash;
    } else {
      return 'Caja Chica: ';
    }
  }

  updateData(data) {
    this.pettyCash = data.pettyCash;
    this.period = data.period;
  }
}
