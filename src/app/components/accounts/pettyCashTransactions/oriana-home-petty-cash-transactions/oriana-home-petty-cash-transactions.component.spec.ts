import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePettyCashTransactionsComponent} from './oriana-home-petty-cash-transactions.component';

describe('OrianaHomePettyCashTransactionsComponent', () => {
  let component: OrianaHomePettyCashTransactionsComponent;
  let fixture: ComponentFixture<OrianaHomePettyCashTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePettyCashTransactionsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePettyCashTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
