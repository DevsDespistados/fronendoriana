import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPettyCashTransactionFileComponent} from './modal-petty-cash-transaction-file.component';

describe('ModalPettyCashTransactionFileComponent', () => {
  let component: ModalPettyCashTransactionFileComponent;
  let fixture: ComponentFixture<ModalPettyCashTransactionFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPettyCashTransactionFileComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPettyCashTransactionFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
