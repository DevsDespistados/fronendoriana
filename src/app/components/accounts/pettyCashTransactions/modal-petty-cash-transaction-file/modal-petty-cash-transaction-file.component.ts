import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {endDate, moment} from '../../../../services/GlobalFunctions';
import {FileInput} from 'ngx-material-file-input';

@Component({
  selector: 'app-modal-petty-cash-transaction-file',
  templateUrl: './modal-petty-cash-transaction-file.component.html',
  styleUrls: ['./modal-petty-cash-transaction-file.component.css']
})
export class ModalPettyCashTransactionFileComponent implements OnInit {
  text = ConfText;
  file: FileInput;
  formData;

  constructor(public dialogRef: MatDialogRef<ModalPettyCashTransactionFileComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snack: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  accept() {
    this.formData = new FormData();
    this.formData.append('dateIni', moment(this.data.pettyCashPeriod.datePeriod));
    this.formData.append('dateEnd', endDate(this.data.pettyCashPeriod.datePeriod));
    this.formData.append('file', this.file.files[0], this.file.fileNames[0]);
    this.dialogRef.close(this.formData);
  }

  canceled() {
    this.file = null;
    this.dialogRef.close();
  }

  valid() {
    return this.file == null;
  }
}

