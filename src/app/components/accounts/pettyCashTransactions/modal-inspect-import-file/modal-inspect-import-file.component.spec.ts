import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalInspectImportFileComponent} from './modal-inspect-import-file.component';

describe('ModalInspectImportFileComponent', () => {
  let component: ModalInspectImportFileComponent;
  let fixture: ComponentFixture<ModalInspectImportFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalInspectImportFileComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInspectImportFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
