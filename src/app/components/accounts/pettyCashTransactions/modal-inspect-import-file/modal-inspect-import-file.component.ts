import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfText} from '../../../../model/configuration/conf-text';

@Component({
  selector: 'app-modal-inspect-import-file',
  templateUrl: './modal-inspect-import-file.component.html',
  styleUrls: ['./modal-inspect-import-file.component.css']
})
export class ModalInspectImportFileComponent implements OnInit {
  rowsImport: RowFile[] = [];
  infoExist;
  text = ConfText;
  columns = [
    'position',
    'date',
    'project',
    'receipt',
    'description',
    'import'
  ];

  constructor(public dialogRef: MatDialogRef<ModalInspectImportFileComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.rowsImport = this.data.rowsImport;
  }

  ngOnInit() {
  }

  showExist(info: ValidRow) {
    this.infoExist = info;
  }

  canceled() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close();
  }

}

export class DataInspectFile {
  constructor(public rowsImport: RowFile[]) {
  }
}

export class RowFile {
  constructor(
    public nro: number,
    public proyecto: string,
    public descripcion: string,
    public fecha: any,
    public importe: number,
    public nrorecivo: string,
    public exist: ValidRow
  ) {
  }
}

export class ValidRow {
  constructor(
    public nro: boolean,
    public proyecto: boolean,
    public descripcion: boolean,
    public fecha: boolean,
    public importe: boolean,
    public nroRecivo: boolean,
  ) {
  }
}
