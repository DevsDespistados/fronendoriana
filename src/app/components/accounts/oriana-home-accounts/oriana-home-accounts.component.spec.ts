import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeAccountsComponent} from './oriana-home-accounts.component';

describe('OrianaHomeAccountsComponent', () => {
  let component: OrianaHomeAccountsComponent;
  let fixture: ComponentFixture<OrianaHomeAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeAccountsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
