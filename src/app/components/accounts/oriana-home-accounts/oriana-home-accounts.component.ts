import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-accounts',
  templateUrl: './oriana-home-accounts.component.html',
  styleUrls: ['./oriana-home-accounts.component.css']
})
export class OrianaHomeAccountsComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'CUENTAS',
  };

  links = [
    {label: 'BANCOS', path: '/accounts/banks', permission: _permission().ac_b_banks > 0},
    {label: 'CAJA CHICA OFICINA', path: '/accounts/pettyCashOffice', permission: _permission().ac_pto_pettyCashOffice > 0},
    {label: 'CAJA CHICA', path: '/accounts/pettyCash', permission: _permission().ac_pt_pettyCash},
    {label: 'TRASPASO', path: '/accounts/projects', permission: _permission().ac_pj_projects > 0},
  ];

  navLinks = [];

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }
}
