import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AccountBanksComponent} from './account-banks.component';

describe('AccountBanksComponent', () => {
  let component: AccountBanksComponent;
  let fixture: ComponentFixture<AccountBanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountBanksComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountBanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
