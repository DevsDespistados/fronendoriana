import {Component, OnInit} from '@angular/core';
import {OwnerEquipment} from '../../../../model/Equipment/owner-equipment';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {Bank} from '../../../../model/accounts/bank';
import {AccountService} from '../../../../services/accounts/account.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalBankComponent} from '../modal-bank/modal-bank.component';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-account-banks',
  templateUrl: './account-banks.component.html',
  styleUrls: ['./account-banks.component.css']
})
export class AccountBanksComponent implements OnInit {
  listBanks: Bank[];
  total = 0;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'account',
    'balance',
    'options'
  ];
  sortedData: OwnerEquipment[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  bankSelect: Bank = new Bank(null, null, null, true, false);
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private accountService: AccountService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalBankComponent, {
      width: '400px',
      data: new DataBank(this.bankSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.bankSelect.idBank) {
          this.accountService.addAccountBank(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.accountService.editAccountBank(this.bankSelect.idBank, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.bankSelect = <any>this.listBanks[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.bankSelect.nameAccountBank}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.accountService.deleteBank(this.bankSelect.idBank)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.listBanks[pos];
    this.bankSelect = new Bank(
      p.idBank,
      p.nameAccountBank,
      p.balanceBank,
      p.activeBank,
      p.isPettyCash
    );
    this.create();
  }

  getFilter(type: any) {
    this.accountService.getBankActives(type).subscribe(
      res => {
        this.listBanks = res['banks'];
        this.total = res['total'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  setState(pos, state): any {
    this.accountService.setStateBank(this.listBanks[pos].idBank, {activeBank: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'CUENTA', 'MONTO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE CUENTAS', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.listBanks.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameAccountBank.toUpperCase());
      d.push(this.printService.moneyData(c.balanceBank));
      d.push(c.activeBank == 1 ? 'DISPONIBLE' : 'NO DIPONIBLE');
      data.push(d);
    });
    data.push([
      '',
      '',
      this.printService.moneyData(this.total),
      ''
    ]);
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.accountService.searchBanks(text).subscribe(
        res => {
          this.listBanks = res['banks'];
          this.total = res['total'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.bankSelect = new Bank(
      null,
      null,
      null,
      true,
      false
    );
  }

  disabled(ele: Bank) {
    return this.isDisabled() || ele.balanceBank != 0 || ele.activeBank == 0;
  }

  isDisabled() {
    return _permission().ac_b_banks != 1;
  }

  disabledP() {
    return _permission().ac_b_periods == 0;
  }
}

export class DataBank {
  constructor(public bank: Bank) {
  }
}
