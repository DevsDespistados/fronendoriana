import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Bank} from '../../../../model/accounts/bank';

@Component({
  selector: 'app-modal-bank',
  templateUrl: './modal-bank.component.html',
  styleUrls: ['./modal-bank.component.css']
})
export class ModalBankComponent {
  text = ConfText;
  formData;
  back: Bank;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.back = this.data.bank;
  }

  onNoClick(): void {
    this.formData = null;
    this.dialogRef.close();
  }

  datart() {
    if (!this.valid()) {
      this.formData = this.back;
      this.dialogRef.close();
    }
  }

  valid() {
    return this.back.nameAccountBank == null || this.back.nameAccountBank == '';
  }
}
