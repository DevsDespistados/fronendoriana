import {Component} from '@angular/core';
import {TransactionsCheckComponent} from '../transactions-check/transactions-check.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {ViewPayCheck} from '../../../../model/accounts/view-pay-check';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {_permission} from '../../../../services/permissions';
import {ReportService} from '../../../../services/report.service';

@Component({
  selector: 'app-transactions-petty-cash-office',
  templateUrl: './transactions-petty-cash-office.component.html',
  styleUrls: ['./transactions-petty-cash-office.component.css']
})
export class TransactionsPettyCashOfficeComponent extends TransactionsCheckComponent {

  constructor(public dialog: MatDialog,
              public snack: MatSnackBar,
              public printService: PrintServiceService,
              public reportService: ReportService,
              public transactionService: TransactionService) {
    super(dialog, snack, printService, reportService, transactionService);
  }

  getFilter(idPeriod: number, idCheck: number) {
    this.transactionService.getTransactionPeriod(idPeriod, idCheck).subscribe(
      res => {
        this.transactions = res['transactions'];
        this._bank = res['bank'];
        this._check = new ViewPayCheck(null, null, null, 9999999999, 'egreso', null, this.idPeriod, 0, 9999999999);
        this._period = res['period'];
        this.data.emit({bank: this._bank, check: this._check, period: this._period});
        this.total = res['total'];
        console.log(res);
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  isDisabled(): boolean {
    return _permission().ac_pto_transaction != 1
      || !this._period
      || !this._bank
      || this._bank.activeBank != 1
      || this._period.stateBankPeriod != 0;
  }
}
