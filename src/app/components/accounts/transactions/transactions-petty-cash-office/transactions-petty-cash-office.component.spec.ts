import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransactionsPettyCashOfficeComponent} from './transactions-petty-cash-office.component';

describe('TransactionsPettyCashOfficeComponent', () => {
  let component: TransactionsPettyCashOfficeComponent;
  let fixture: ComponentFixture<TransactionsPettyCashOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionsPettyCashOfficeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsPettyCashOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
