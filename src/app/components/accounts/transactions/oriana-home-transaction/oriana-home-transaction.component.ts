import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ViewPayCheck} from '../../../../model/accounts/view-pay-check';

@Component({
  selector: 'app-oriana-home-transaction',
  templateUrl: './oriana-home-transaction.component.html',
  styleUrls: ['./oriana-home-transaction.component.css']
})
export class OrianaHomeTransactionComponent implements OnInit {


  titles = {
    module: 'TRANSACCIONES - CHEQUE',
  };
  check: ViewPayCheck;
  period;
  bank;
  idBank;
  idPeriod;
  idCheck;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idBank = res['idB'];
      this.idPeriod = res['idP'];
      this.idCheck = res['idC'];
    });
  }

  form(value: ViewPayCheck) {
    this.check = value;
  }

  getSubTitle() {
    if (this.check) {
      return 'CHEQUE de ' + this.check.type + ' : ' + this.check.numberCheck;
    } else {
      return 'CHEQUE: ';
    }
  }

  updateData(data) {
    console.log(data);
    this.check = data.check;
    this.period = data.period;
    this.bank = data.bank;
  }
}
