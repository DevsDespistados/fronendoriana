import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeTransactionComponent} from './oriana-home-transaction.component';

describe('OrianaHomeTransactionComponent', () => {
  let component: OrianaHomeTransactionComponent;
  let fixture: ComponentFixture<OrianaHomeTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeTransactionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
