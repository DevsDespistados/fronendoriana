import {Component, OnInit} from '@angular/core';
import {ViewTransactions} from '../../../../model/accounts/view-transactions';
import {TotalForm} from '../../../../model/total-form';
import {Transaction} from '../../../../model/accounts/transaction';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {Bank} from '../../../../model/accounts/bank';
import {ViewPayCheck} from '../../../../model/accounts/view-pay-check';
import {ViewBankPeriod} from '../../../../model/accounts/view-bank-period';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {_user} from '../../../../services/permissions';
import {CodeTransaction} from '../../../../model/accounts/ConstTransactions';
import {DirectIncomeComponent} from '../modalsTransactions/direct-income/direct-income.component';
import {ModalClientIncomeComponent} from '../modalsTransactions/clientIncome/modal-client-income/modal-client-income.component';
import {CompanyIncomeComponent} from '../modalsTransactions/company-income/company-income.component';
import {DirectExpenseComponent} from '../modalsTransactions/direct-expense/direct-expense.component';
import {ProviderAdvanceExpenseComponent} from '../modalsTransactions/provider-advance-expense/provider-advance-expense.component';
import {PermanentAdvanceExpenseComponent} from '../modalsTransactions/permanent-advance-expense/permanent-advance-expense.component';
import {EventualAdvanceExpenseComponent} from '../modalsTransactions/eventual-advance-expense/eventual-advance-expense.component';
import {EquipmentAdvanceExpenseComponent} from '../modalsTransactions/equipment-advance-expense/equipment-advance-expense.component';
import {CompanyExpenseComponent} from '../modalsTransactions/company-expense/company-expense.component';
import {ModalTransferAccountsComponent} from '../modalsTransactions/transerAccount/modal-transfer-accounts/modal-transfer-accounts.component';
import {ModalProviderFormAdvanceComponent} from '../modalsTransactions/providerFormExpense/modal-provider-form-advance/modal-provider-form-advance.component';
import {ModalPermanentFormAdvanceComponent} from '../modalsTransactions/permanentFormExpense/modal-permanent-form-advance/modal-permanent-form-advance.component';
import {ModalEventualFormAdvanceComponent} from '../modalsTransactions/eventualFormExpense/modal-eventual-form-advance/modal-eventual-form-advance.component';
import {ModalEquipmentFormAdvanceComponent} from '../modalsTransactions/equipmentFormExpense/modal-equipment-form-advance/modal-equipment-form-advance.component';
import {DataTransaction} from '../transactions-check/transactions-check.component';
import {ActivatedRoute} from '@angular/router';
import {compareDate, compareNumber, compareString} from '../../../../services/GlobalFunctions';
import {ReportService} from '../../../../services/report.service';
import {ReportServiceService} from '../../../../services/accountsReports/report-service.service';

@Component({
  selector: 'app-all-transactions',
  templateUrl: './all-transactions.component.html',
  styleUrls: ['./all-transactions.component.css']
})
export class AllTransactionsComponent implements OnInit {
  idCheck = null;
  idPeriod;
  idBank;
  data; // = new EventEmitter();
  transactions: ViewTransactions[];
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  titulo = 'Todas las transacciones';
  displayedColumns = [
    'position',
    'date',
    'check',
    'project',
    'operation',
    'accountD',
    'category',
    'respaldo',
    'description',
    'import',
    'options'
  ];
  sortedData: ViewTransactions[];
  transactionSelect: Transaction;
  tooltip = Tooltips;
  currentOption = 0;
  progress = false;
  actualType;
  _bank: Bank;
  _check: ViewPayCheck;
  _period: ViewBankPeriod;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private activeRouter: ActivatedRoute,
              private printService: PrintServiceService,
              private reportService: ReportService,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.activeRouter.parent.params.subscribe(res => {
      this.idBank = res['idB'];
      this.idPeriod = res['idP'];
      this.load();
    });
  }


  create() {
    const dialogRef = this.isTypeTransaction(this.actualType);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.transactionSelect = result;
        if (!this.transactionSelect.idTransaction) {
          this.transactionService.newTransaction(this.transactionSelect.typeTransaction, this.transactionSelect)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.transactionService.editTransaction(this.transactionSelect.idTransaction,
            this.transactionSelect.typeTransaction, result)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.transactionSelect = <any>this.transactions[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.transactionSelect.nroReceiptTransaction}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.transactionService.deleteTransaction(this.transactionSelect.idTransaction)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.transactions[pos];
    this.transactionSelect = new Transaction(
      p.idTransaction,
      p.typeTransaction,
      p.dateTransaction,
      p.nroReceiptTransaction,
      p.descriptionTransaction,
      p.amountTransaction,
      p.idCategory_transaction,
      p.idBankPeriod,
      p.idBankExecutor,
      p.idProject,
      p.idCheck,
      p.idActor,
      p.code_transaction
    );
    this.actualType = this.transactionSelect.code_transaction;
    this.create();
  }

  getFilter(idPeriod: number, idCheck: number) {
    this.transactionService.getTransactionPeriod(idPeriod, idCheck).subscribe(
      res => {
        this.transactions = res['transactions'];
        this._bank = res['bank'];
        this._check = res['check'];
        this._period = res['period'];
        // this.data.emit({bank: this._bank, check: this._check, period: this._period});
        this.total = res['total'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
    // this.transactionService.get
  }

  setType(type): any {
    this.actualType = type;
    this.create();
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.idPeriod, this.idCheck);
  }

  printReport() {
    // this.progress = true;
    // const head = ['NRO',
    //   'FECHA',
    //   'NRO CHEQUE',
    //   'PROYECTO',
    //   'OPERACION',
    //   'CUENTA/DESC',
    //   'CATEGORIA',
    //   'NRO RESPALDO',
    //   'DESCRIPCION',
    //   'IMPORTE'];
    // const body = this.getData();
    // const report = this.printService.createSingleTable('TRANSACCIONES', head, body, 'wrap',
    //   {
    //     0: {
    //       halign: 'right',
    //     },
    //     9: {
    //       halign: 'right',
    //     }
    //   });
    this.reportService.printReport('TRANSACTIONS', {
      G_PERIOD_ID: this.idPeriod,
      G_BANK_ID: this.idBank,
    });
  }

  getData() {
    const data = [];
    this.transactions.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(this.printService.dateData(c.dateTransaction).toUpperCase());
      d.push(c.numberCheck);
      d.push(c.nameProject.toUpperCase());
      d.push(c.typeTransaction.toUpperCase());
      d.push(c.nameAccountTransaction.toUpperCase());
      d.push(c.nameCategory.toUpperCase());
      d.push(c.nroReceiptTransaction);
      d.push(c.descriptionTransaction.toUpperCase());
      d.push(this.printService.moneyData(c.amountTransaction));
      data.push(d);
    });
    data.push([
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      'TOTAL',
      this.printService.moneyData(this.total.total),
    ]);
    return data;
  }

  printFacturation(id) {
    this.transactionService.printFact({idTransaction: id, user: _user()}).subscribe(
      (res: any) => {
        this.reportService.printReport('Receipt', {
            ID_TRASACTION: id,
            USER_NAME: res.user.name,
            ACTOR: res.actor.type,
            ACTOR_NAME: res.actor.name,
            AMOUNT_TEXT: res.valor,
            DATE_PARAM: res.date,
          }
        );
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.transactionService.searchTransaction(text).subscribe(
        response => {
          this.transactions = response['transaction'];
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.transactions.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.transactions = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'dateTransaction':
          return compareDate(a.dateTransaction, b.dateTransaction, isAsc);
        case 'numberCheck':
          return compareString(a.numberCheck, b.numberCheck, isAsc);
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'typeTransaction':
          return compareString(a.typeTransaction, b.typeTransaction, isAsc);
        case 'nameAccountTransaction':
          return compareString(a.nameAccountTransaction, b.nameAccountTransaction, isAsc);
        case 'nameCategory':
          return compareString(a.nameCategory, b.nameCategory, isAsc);
        case 'nroReceiptTransaction':
          return compareString(a.nroReceiptTransaction, b.nroReceiptTransaction, isAsc);
        case 'descriptionTransaction':
          return compareString(a.descriptionTransaction, b.descriptionTransaction, isAsc);
        case 'amountTransaction':
          return compareNumber(a.amountTransaction, b.amountTransaction, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.transactionSelect = new Transaction(
      null,
      '',
      null,
      null,
      '',
      null,
      null,
      this.idPeriod,
      this.idBank,
      null,
      this.idCheck,
      null,
      1
    );
  }

  newDisabled() {
    if (this._check) {
      return !(this._check.balance <= 0);
    }
  }

  getTypeTransaction() {
    if (this._check) {
      return this._check.type;
    }
  }

  isTypeTransaction(type) {
    let res = null;
    const t = new DataTransaction(this.transactionSelect);
    t.bank = this._bank;
    t.check = this._check;
    t.period = this._period;
    t.transaction.code_transaction = type;
    switch (type) {
      case CodeTransaction.directIncome: {
        res = this.dialog.open(DirectIncomeComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.clientIncome: {
        res = this.dialog.open(ModalClientIncomeComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.companyIncome: {
        res = this.dialog.open(CompanyIncomeComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.directExpense: {
        res = this.dialog.open(DirectExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.providerExpense: {
        res = this.dialog.open(ProviderAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.permanentExpense: {
        res = this.dialog.open(PermanentAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.eventualExpense: {
        res = this.dialog.open(EventualAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.equipmentExpense: {
        res = this.dialog.open(EquipmentAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.companyExpense: {
        res = this.dialog.open(CompanyExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.transferBank: {
        res = this.dialog.open(ModalTransferAccountsComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.providerFormExpense: {
        res = this.dialog.open(ModalProviderFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.permanentFormExpense: {
        res = this.dialog.open(ModalPermanentFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.eventualFormExpense: {
        res = this.dialog.open(ModalEventualFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.equipmentFormExpense: {
        res = this.dialog.open(ModalEquipmentFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
    }
    return res;
  }
}
