import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EquipmentFormAdvanceComponent} from './equipment-form-advance.component';

describe('EquipmentFormAdvanceComponent', () => {
  let component: EquipmentFormAdvanceComponent;
  let fixture: ComponentFixture<EquipmentFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
