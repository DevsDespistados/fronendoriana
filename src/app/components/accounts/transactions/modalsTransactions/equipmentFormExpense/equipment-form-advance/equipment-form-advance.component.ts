import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {PayCheck} from '../../../../../../model/accounts/pay-check';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {OwnerEquipmentForm} from '../../../../../../model/form/owner-equipment-form';
import {OwnerEquipmentFormService} from '../../../../../../services/form/owner-equipment-form.service';

@Component({
  selector: 'app-equipment-form-advance',
  templateUrl: './equipment-form-advance.component.html',
  styleUrls: ['./equipment-form-advance.component.css']
})
export class EquipmentFormAdvanceComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Input() formSelect: OwnerEquipmentForm;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: PayCheck;
  transaction;
  forms: EquipmentFormForProjects[] = [];
  permanentFormForProjectSelect: EquipmentFormForProjects;

  constructor(private equipmentFormService: OwnerEquipmentFormService) {
  }

  ngOnInit(): void {
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'pago de Planilla Permanente';
    this.getTotalForms(this.formSelect.idOwnerEquipmentForm);
  }

  getTotalForms(idForm) {
    this.equipmentFormService.getEquipmentFormForProjects(idForm).subscribe(
      res => {
        this.forms = res['forms'];
      }
    );
  }

  loadFormSelect(evt) {
    this.permanentFormForProjectSelect = this.forms[evt];
  }

  calcNewBalance() {
    if (this.permanentFormForProjectSelect) {
      return this.permanentFormForProjectSelect.balanceAux - this.transaction.amountTransaction;
    } else {
      return 0;
    }
  }

  accept() {
    this.transaction.idProject = this.permanentFormForProjectSelect.idProject;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.idActor = this.formSelect.idOwnerEquipmentForm;
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    if (this.permanentFormForProjectSelect) {
      return this.transaction.dateTransaction == null
        || this.transaction.descriptionTransaction == ''
        || this.transaction.descriptionTransaction == null
        || this.transaction.nroReceiptTransaction == ''
        || this.transaction.nroReceiptTransaction == null
        || this.transaction.amountTransaction <= 0
        || this.transaction.amountTransaction > this.permanentFormForProjectSelect.balanceAux;
    } else {
      return true;
    }

  }

  pressBack() {
    this.back.emit(true);
  }
}

class EquipmentFormForProjects extends OwnerEquipmentForm {
  constructor(
    public idProject: number,
    public nameProject: string,
    public idOwnerEquipmentForm: number,
    public periodOwnerEquipmentForm: any,
    public activeOwnerEquipmentForm: any,
    public mountPayAux: number,
    public totalAmount: number,
    public bondOwnerEquipmentForm: number,
    public discountOwnerEquipmentForm: number,
    public anticipationOwnerEquipmentForm: number,
    public liquidPayableOwnerEquipmentForm: number,
    public balanceAux: number
  ) {
    super(
      idOwnerEquipmentForm,
      periodOwnerEquipmentForm,
      activeOwnerEquipmentForm,
      mountPayAux,
      totalAmount,
      bondOwnerEquipmentForm,
      discountOwnerEquipmentForm,
      anticipationOwnerEquipmentForm,
      liquidPayableOwnerEquipmentForm,
      balanceAux);
  }

}
