import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TotalForm} from '../../../../../../model/total-form';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {OptionOrianaTableToolbar} from '../../../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {SnackAlertComponent} from '../../../../../snack-msg/snack-alert/snack-alert.component';
import {OwnerEquipmentForm} from '../../../../../../model/form/owner-equipment-form';
import {OwnerEquipmentFormService} from '../../../../../../services/form/owner-equipment-form.service';

@Component({
  selector: 'app-equipment-form-select',
  templateUrl: './equipment-form-select.component.html',
  styleUrls: ['./equipment-form-select.component.css']
})
export class EquipmentFormSelectComponent implements OnInit {
  @Output() formSelect = new EventEmitter();
  @Output() cancel = new EventEmitter();
  equipmentForms: OwnerEquipmentForm[] = [];
  total: TotalForm;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'mountTotal',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  text = ConfText;
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  positionSelect;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private equipmentFormService: OwnerEquipmentFormService) {
  }


  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.equipmentFormService.getHeavyEquipmentForms(type).subscribe(
      res => {
        this.equipmentForms = res['forms'];
        this.total = res['total'];
      },
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      this.equipmentFormService.search(text).subscribe(
        response => {
          this.equipmentForms = response['forms'];
          this.total = response['total'];
        }, error => {
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  sendFormData() {
    this.formSelect.emit(this.equipmentForms[this.positionSelect]);
  }

  validForm() {
    return this.positionSelect == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

  isCheckedDisabled(element: OwnerEquipmentForm) {
    return element.balanceAux == 0;
  }

  getTotalColumn(rr) {
    let total = 0;
    this.equipmentForms.forEach(f => {
      total += f[rr];
    });
    return total;
  }
}
