import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EquipmentFormSelectComponent} from './equipment-form-select.component';

describe('EquipmentFormSelectComponent', () => {
  let component: EquipmentFormSelectComponent;
  let fixture: ComponentFixture<EquipmentFormSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentFormSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentFormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
