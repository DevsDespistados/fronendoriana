import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEquipmentFormAdvanceComponent} from './modal-equipment-form-advance.component';

describe('ModalEquipmentFormAdvanceComponent', () => {
  let component: ModalEquipmentFormAdvanceComponent;
  let fixture: ComponentFixture<ModalEquipmentFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEquipmentFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEquipmentFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
