import {Component, Input, OnInit} from '@angular/core';
import {ViewTransactions} from '../../../../../model/accounts/view-transactions';

@Component({
  selector: 'app-part-forms-project',
  templateUrl: './part-forms-project.component.html',
  styleUrls: ['./part-forms-project.component.css']
})
export class PartFormsProjectComponent implements OnInit {
  displayedColumns = [
    'position',
    'project',
    'date',
    'receipt',
    'account',
    'mount',
    'balance',
    'options'
  ];
  @Input() payParts: ViewTransactions[];
  @Input() projectFormSelect;

  constructor() {
  }

  ngOnInit() {
  }

  deleted(pos) {
  }

}
