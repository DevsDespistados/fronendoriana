import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PartFormsProjectComponent} from './part-forms-project.component';

describe('PartFormsProjectComponent', () => {
  let component: PartFormsProjectComponent;
  let fixture: ComponentFixture<PartFormsProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PartFormsProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartFormsProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
