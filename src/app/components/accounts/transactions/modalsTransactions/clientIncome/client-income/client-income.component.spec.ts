import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ClientIncomeComponent} from './client-income.component';

describe('ClientIncomeComponent', () => {
  let component: ClientIncomeComponent;
  let fixture: ComponentFixture<ClientIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClientIncomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
