import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {PayCheck} from '../../../../../../model/accounts/pay-check';
import {ViewProjects} from '../../../../../../model/project/view-projects';
import {ProjectSpSheet} from '../../../../../../model/project/project-sp-sheet';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';

@Component({
  selector: 'app-client-income',
  templateUrl: './client-income.component.html',
  styleUrls: ['./client-income.component.css']
})
export class ClientIncomeComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Input() formSelect: ProjectSpSheet;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: PayCheck;
  project: ViewProjects;
  transaction;

  constructor() {
  }

  ngOnInit(): void {
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.project = this.dataTransaction.project;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'pago de Planilla ' + this.formSelect.TypeForm;
  }

  accept() {
    this.transaction.typeTransaction = 'ingreso';
    this.transaction.nroReceiptTransaction = this.check.numberCheck;
    this.transaction.idProject = this.project.idProject;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.amountTransaction = this.check.mountCheck;
    this.transaction.idActor = this.formSelect.idProjectForm;
    console.log(this.transaction);
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null;
  }

  pressBack() {
    this.back.emit(true);
  }
}
