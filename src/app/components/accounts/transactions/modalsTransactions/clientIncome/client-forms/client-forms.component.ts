import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Transaction} from '../../../../../../model/accounts/transaction';
import {ProjectSpSheet} from '../../../../../../model/project/project-sp-sheet';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {TotalForms} from '../../../../../projects/forms-project/forms-project.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectSpreadsheetService} from '../../../../../../services/project/project-spreadsheet.service';
import {SnackErrorComponent} from '../../../../../snack-msg/snack-error/snack-error.component';
import {ViewPayCheck} from '../../../../../../model/accounts/view-pay-check';

@Component({
  selector: 'app-client-forms',
  templateUrl: './client-forms.component.html',
  styleUrls: ['./client-forms.component.css']
})
export class ClientFormsComponent implements OnInit {
  @Input() transaction: Transaction;
  @Input() idProject = null;
  @Output() formSelect = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() selectProject = new EventEmitter();
  projectSS: ProjectSpSheet[] = [];
  text = ConfText;
  total: TotalForms = new TotalForms(0, 0, 0);
  positionSelect;
  displayedColumns = [
    'position',
    'type',
    'doc',
    'date',
    'advance',
    'liquid',
    'pay',
    'balance',
    'options'
  ];
  project;
  check: ViewPayCheck;

  // data = new DataComplement();

  constructor(private snack: MatSnackBar,
              private sSheeet: ProjectSpreadsheetService) {
  }

  ngOnInit() {
    if (this.idProject) {
      this.getFilterForm();
    }
  }

  getFilterForm() {
    this.sSheeet.getSpSheetAccount(
      this.idProject,
      this.transaction.idBankExecutor,
      this.transaction.idCheck,
      this.transaction.idBankPeriod
    ).subscribe(
      res => {
        this.projectSS = <ProjectSpSheet[]>res['forms'];
        this.total = res['totals'];
        this.check = res ['check'];
        this.project = res['project'];
      }, error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});

      });
  }

  sendFormData() {
    this.formSelect.emit(this.projectSS[this.positionSelect]);
    this.selectProject.emit(this.project);
  }

  sortData(evet) {
  }

  validForm() {
    return this.positionSelect == null;
  }

  pressBack() {
    this.back.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  isCheckedDisabled(ele: ProjectSpSheet) {
    return ele.balanceAux == 0 || ele.balanceAux < this.check.balance;
  }

  getTotalColumn(rr) {
    let total = 0;
    this.projectSS.forEach(f => {
      total += Number(f[rr]);
    });
    return total;
  }
}

