import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Transaction} from '../../../../../../model/accounts/transaction';

@Component({
  selector: 'app-modal-client-income',
  templateUrl: './modal-client-income.component.html',
  styleUrls: ['./modal-client-income.component.css']
})
export class ModalClientIncomeComponent implements OnInit {

  text = ConfText;
  formData;

  transaction: Transaction;
  formSelect = null;
  idProject;
  selectClass = '';

  constructor(public dialogRef: MatDialogRef<ModalClientIncomeComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transaction = this.data.transaction;
    this.transaction.typeTransaction = 'ingreso';
  }

  ngOnInit(): void {
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  backToForm() {
    this.formSelect = null;
  }

  backToSelect() {
    this.idProject = null;
  }

  sendFormData(): void {
  }

  visibleSelect() {
    if (this.idProject == null && this.formSelect == null && this.transaction.idTransaction == null) {
      return 'visible';
    }
  }

  visibleForm() {
    if (this.formSelect == null && this.idProject != null && this.transaction.idTransaction == null) {
      return 'visible';
    }
  }

  visibleNew() {
    if ((this.idProject != null && this.formSelect != null) || this.transaction.idTransaction != null) {
      return 'visible';
    }
  }

  setIdProject(idProject) {
    this.idProject = idProject;
  }

  setFormSelect(form) {
    this.formSelect = form;
  }

  setProject(data) {
    this.data.project = data;
  }

  accept() {
    this.formData = this.transaction;
    this.dialogRef.close(this.formData);
  }
}
