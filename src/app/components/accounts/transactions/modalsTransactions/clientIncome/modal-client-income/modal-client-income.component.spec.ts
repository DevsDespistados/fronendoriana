import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalClientIncomeComponent} from './modal-client-income.component';

describe('ModalClientIncomeComponent', () => {
  let component: ModalClientIncomeComponent;
  let fixture: ComponentFixture<ModalClientIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalClientIncomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalClientIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
