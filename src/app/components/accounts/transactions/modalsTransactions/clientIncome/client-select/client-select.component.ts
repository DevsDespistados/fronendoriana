import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Client} from '../../../../../../model/personal/client';
import {SnackErrorComponent} from '../../../../../snack-msg/snack-error/snack-error.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Project} from '../../../../../../model/project/project';
import {ClientService} from '../../../../../../services/personal/client.service';
import {ProjectService} from '../../../../../../services/project/project.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-client-select',
  templateUrl: './client-select.component.html',
  styleUrls: ['./client-select.component.css']
})
export class ClientSelectComponent implements OnInit {
  @Output() accept = new EventEmitter();
  @Output() cancel = new EventEmitter();
  clients: Client[] = [];
  projects: Project[] = [];
  text = ConfText;
  idClient: number;
  idProject: number;

  constructor(private snack: MatSnackBar,
              private clientService: ClientService,
              private projectService: ProjectService) {
  }

  ngOnInit() {
    this.loadClients();
  }

  loadClients() {
    this.clientService.getActives(1).subscribe(
      res => {
        this.clients = <Client[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  loadProject(idClient) {
    this.projectService.getProjectOfClient(idClient).subscribe(
      res => {
        this.projects = <Project[]>res['projects'];
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  sendFormData() {
    this.accept.emit(this.idProject);
  }

  validClient() {
    return this.idClient == null || this.idClient <= 0 || this.idProject == null || this.idProject <= 0;
  }

  canceled() {
    this.cancel.emit(true);
  }

}
