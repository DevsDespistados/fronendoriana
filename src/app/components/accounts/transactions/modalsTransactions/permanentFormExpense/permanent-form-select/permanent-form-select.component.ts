import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TotalForm} from '../../../../../../model/total-form';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {OptionOrianaTableToolbar} from '../../../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {SnackAlertComponent} from '../../../../../snack-msg/snack-alert/snack-alert.component';
import {ProviderForm} from '../../../../../../model/form/provider-form';
import {PermanentFormService} from '../../../../../../services/form/permanent-form.service';
import {PermanentForm} from '../../../../../../model/form/permanent-form';

@Component({
  selector: 'app-permanent-form-select',
  templateUrl: './permanent-form-select.component.html',
  styleUrls: ['./permanent-form-select.component.css']
})
export class PermanentFormSelectComponent implements OnInit {
  @Output() formSelect = new EventEmitter();
  @Output() cancel = new EventEmitter();
  permanentForms: PermanentForm[] = [];
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'mountTotal',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  text = ConfText;
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  positionSelect;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private permanentFormService: PermanentFormService) {
  }


  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.permanentFormService.getPermanentForms(type).subscribe(
      res => {
        this.permanentForms = res['forms'];
        this.total = res['total'];
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      this.permanentFormService.search(text).subscribe(
        response => {
          this.permanentForms = response['forms'];
          this.total = response['total'];
        }, error => {
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  sendFormData() {
    this.formSelect.emit(this.permanentForms[this.positionSelect]);
  }

  validForm() {
    return this.positionSelect == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

  isCheckedDisabled(element: ProviderForm) {
    return element.balanceAux == 0;
  }

  getTotalColumn(rr) {
    let total = 0;
    this.permanentForms.forEach(f => {
      total += f[rr];
    });
    return total;
  }
}
