import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PermanentFormSelectComponent} from './permanent-form-select.component';

describe('PermanentFormSelectComponent', () => {
  let component: PermanentFormSelectComponent;
  let fixture: ComponentFixture<PermanentFormSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermanentFormSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanentFormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
