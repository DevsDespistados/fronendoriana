import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPermanentFormAdvanceComponent} from './modal-permanent-form-advance.component';

describe('ModalPermanentFormAdvanceComponent', () => {
  let component: ModalPermanentFormAdvanceComponent;
  let fixture: ComponentFixture<ModalPermanentFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPermanentFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPermanentFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
