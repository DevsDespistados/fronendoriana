import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {PayCheck} from '../../../../../../model/accounts/pay-check';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {PermanentFormService} from '../../../../../../services/form/permanent-form.service';
import {PermanentForm} from '../../../../../../model/form/permanent-form';
import {Transaction} from '../../../../../../model/accounts/transaction';
import {ViewTransactions} from '../../../../../../model/accounts/view-transactions';

@Component({
  selector: 'app-permanent-form-advance',
  templateUrl: './permanent-form-advance.component.html',
  styleUrls: ['./permanent-form-advance.component.css']
})
export class PermanentFormAdvanceComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Input() formSelect: PermanentForm;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: PayCheck;
  transaction: Transaction | ViewTransactions;
  forms: PermanentFormForProjects[] = [];
  permanentFormForProjectSelect: PermanentFormForProjects;

  constructor(private permanentFormService: PermanentFormService) {
  }

  ngOnInit(): void {
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'pago de Planilla Permanente';
    this.getTotalForms(this.transaction.idTransaction ? this.transaction.idActor : this.formSelect.idPermanentForm);
  }

  getTotalForms(idForm) {
    this.permanentFormService.getPermanentFormForProjects(idForm).subscribe(
      res => {
        this.forms = res['forms'];
      }
    );
  }

  loadFormSelect(evt) {
    this.permanentFormForProjectSelect = this.forms[evt];
  }

  calcNewBalance() {
    if (this.permanentFormForProjectSelect) {
      return this.permanentFormForProjectSelect.balanceAux - this.transaction.amountTransaction;
    } else {
      return 0;
    }
  }

  accept() {
    this.transaction.idProject = this.permanentFormForProjectSelect.idProject;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.idActor = this.formSelect.idPermanentForm;
    console.log(this.transaction);
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    if (this.permanentFormForProjectSelect) {
      return this.transaction.dateTransaction == null
        || this.transaction.descriptionTransaction == ''
        || this.transaction.descriptionTransaction == null
        || this.transaction.nroReceiptTransaction == ''
        || this.transaction.nroReceiptTransaction == null
        || this.transaction.amountTransaction <= 0
        || this.transaction.amountTransaction > this.permanentFormForProjectSelect.balanceAux;
    } else {
      return true;
    }

  }

  pressBack() {
    this.back.emit(true);
  }
}

class PermanentFormForProjects extends PermanentForm {
  constructor(
    public idProject: number,
    public nameProject: string,
    public idPermanentForm: number,
    public periodicalPermanentForm: any,
    public discountPermanentForm: number,
    public bondPermanentForm: number,
    public advancePermanentForm: number,
    public liquidPermanentForm: number,
    public statusPermanentForm: any,
    public balanceAux: number,
    public mountPayAux: number,
    public mountTotal: number,
  ) {
    super(
      idPermanentForm,
      periodicalPermanentForm,
      discountPermanentForm,
      bondPermanentForm,
      advancePermanentForm,
      liquidPermanentForm,
      statusPermanentForm,
      balanceAux,
      mountPayAux,
      mountTotal);
  }

}
