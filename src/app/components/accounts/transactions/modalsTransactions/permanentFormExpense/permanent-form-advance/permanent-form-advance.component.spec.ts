import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PermanentFormAdvanceComponent} from './permanent-form-advance.component';

describe('PermanentFormAdvanceComponent', () => {
  let component: PermanentFormAdvanceComponent;
  let fixture: ComponentFixture<PermanentFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermanentFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanentFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
