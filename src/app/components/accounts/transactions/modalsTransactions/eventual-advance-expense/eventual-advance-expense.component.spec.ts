import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventualAdvanceExpenseComponent} from './eventual-advance-expense.component';

describe('EventualAdvanceExpenseComponent', () => {
  let component: EventualAdvanceExpenseComponent;
  let fixture: ComponentFixture<EventualAdvanceExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventualAdvanceExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventualAdvanceExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
