import {Component, Inject, OnInit} from '@angular/core';
import {Transaction} from '../../../../../model/accounts/transaction';
import {ConfText} from '../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../model/accounts/bank';
import {Period} from '../../../../../model/accounts/period';
import {ViewProjects} from '../../../../../model/project/view-projects';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectService} from '../../../../../services/project/project.service';
import {endDate} from '../../../../../services/GlobalFunctions';
import {SnackErrorComponent} from '../../../../snack-msg/snack-error/snack-error.component';
import {CategoryService} from '../../../../../services/accounts/category.service';
import {Category} from '../../../../../model/accounts/category';
import {ViewPayCheck} from '../../../../../model/accounts/view-pay-check';

@Component({
  selector: 'app-direct-expense',
  templateUrl: './direct-expense.component.html',
  styleUrls: ['./direct-expense.component.css']
})
export class DirectExpenseComponent implements OnInit {
  transaction: Transaction;
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: ViewPayCheck;
  projects: ViewProjects[] = [];
  categories: Category[] = [];


  constructor(public dialogRef: MatDialogRef<DirectExpenseComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectService: ProjectService,
              private categoryService: CategoryService,
              private snack: MatSnackBar) {
    this.transaction = this.data.transaction;
    this.transaction.amountTransaction = Math.abs(this.transaction.amountTransaction);
    this.transaction.typeTransaction = 'egreso';
  }

  ngOnInit(): void {
    this.bank = this.data.bank;
    this.period = this.data.period;
    this.check = this.data.check;
    this.endDate = endDate(this.data.period.bankPeriod);
    this.transaction.descriptionTransaction = 'egreso directo';
    this.loadProjects();
    this.loadCategories();
  }

  accept() {
    this.transaction.typeTransaction = 'egreso';
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.idActor = null;
    this.dialogRef.close(this.transaction);
  }

  loadProjects() {
    this.projectService.getOpeAccount(1).subscribe(
      res => {
        this.projects = <ViewProjects[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  loadCategories() {
    this.categoryService.category().subscribe(
      res => {
        this.categories = res['category'];
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  canceled() {
    this.dialogRef.close();
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.nroReceiptTransaction == ''
      || this.transaction.amountTransaction <= 0
      || this.transaction.amountTransaction > this.check.balance
      || this.transaction.idProject == null
      || this.transaction.idCategory_transaction == null;
  }
}
