import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DirectExpenseComponent} from './direct-expense.component';

describe('DirectExpenseComponent', () => {
  let component: DirectExpenseComponent;
  let fixture: ComponentFixture<DirectExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DirectExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
