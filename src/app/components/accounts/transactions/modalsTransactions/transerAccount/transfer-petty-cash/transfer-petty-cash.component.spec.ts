import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransferPettyCashComponent} from './transfer-petty-cash.component';

describe('TransferPettyCashComponent', () => {
  let component: TransferPettyCashComponent;
  let fixture: ComponentFixture<TransferPettyCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransferPettyCashComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferPettyCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
