import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {AccountService} from '../../../../../../services/accounts/account.service';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {ViewPayCheck} from '../../../../../../model/accounts/view-pay-check';
import {CodeTransaction} from '../../../../../../model/accounts/ConstTransactions';

@Component({
  selector: 'app-transfer-petty-cash-office',
  templateUrl: './transfer-petty-cash-office.component.html',
  styleUrls: ['./transfer-petty-cash-office.component.css']
})
export class TransferPettyCashOfficeComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: ViewPayCheck;
  transaction;
  accounts: Bank[] = [];

  constructor(private bankService: AccountService) {
  }

  ngOnInit(): void {
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'transferencia';
    this.loadAccounts();
  }

  accept() {
    this.transaction.typeTransaction = 'transferencia';
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.code_transaction = CodeTransaction.transferBank;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.nroReceiptTransaction == ''
      || this.transaction.amountTransaction <= 0
      || this.transaction.amountTransaction > this.check.balance;
  }

  loadAccounts() {
    this.bankService.getPettyCashOfficeActives(1).subscribe(
      res => {
        this.accounts = <Bank[]>res['banks'];
      }, error1 => {
        console.log(error1);
      }
    );
  }

  pressBack() {
    this.back.emit(true);
  }
}
