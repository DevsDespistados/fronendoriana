import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransferPettyCashOfficeComponent} from './transfer-petty-cash-office.component';

describe('TransferPettyCashOfficeComponent', () => {
  let component: TransferPettyCashOfficeComponent;
  let fixture: ComponentFixture<TransferPettyCashOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransferPettyCashOfficeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferPettyCashOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
