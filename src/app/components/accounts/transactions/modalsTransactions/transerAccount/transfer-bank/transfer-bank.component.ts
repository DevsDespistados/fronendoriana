import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {AccountService} from '../../../../../../services/accounts/account.service';
import {ViewPayCheck} from '../../../../../../model/accounts/view-pay-check';
import {CodeTransaction} from '../../../../../../model/accounts/ConstTransactions';

@Component({
  selector: 'app-transfer-bank',
  templateUrl: './transfer-bank.component.html',
  styleUrls: ['./transfer-bank.component.css']
})
export class TransferBankComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: ViewPayCheck;
  transaction;
  accounts: Bank[] = [];

  constructor(private bankService: AccountService) {
  }

  ngOnInit(): void {
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'transferencia';
    this.loadAccounts(this.bank.idBank);
  }

  accept() {
    this.transaction.typeTransaction = 'transferencia';
    this.transaction.code_transaction = CodeTransaction.transferBank;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.nroReceiptTransaction == ''
      || this.transaction.amountTransaction <= 0
      || this.transaction.amountTransaction > this.check.balance;
  }

  loadAccounts(notId) {
    this.bankService.getBanksNot(notId).subscribe(
      res => {
        this.accounts = <Bank[]>res;
      }, error1 => {
        console.log(error1);
      }
    );
  }

  pressBack() {
    this.back.emit(true);
  }
}
