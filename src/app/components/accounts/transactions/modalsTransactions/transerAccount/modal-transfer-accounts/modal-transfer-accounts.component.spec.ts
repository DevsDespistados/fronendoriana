import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTransferAccountsComponent} from './modal-transfer-accounts.component';

describe('ModalTransferAccountsComponent', () => {
  let component: ModalTransferAccountsComponent;
  let fixture: ComponentFixture<ModalTransferAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTransferAccountsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTransferAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
