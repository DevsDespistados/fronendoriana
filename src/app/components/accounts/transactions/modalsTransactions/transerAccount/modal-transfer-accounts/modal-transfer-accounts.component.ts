import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Transaction} from '../../../../../../model/accounts/transaction';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-transfer-accounts',
  templateUrl: './modal-transfer-accounts.component.html',
  styleUrls: ['./modal-transfer-accounts.component.css']
})
export class ModalTransferAccountsComponent implements OnInit {

  text = ConfText;
  formData;

  transaction: Transaction;
  formSelect = null;
  idProject;
  selectClass = '';

  constructor(public dialogRef: MatDialogRef<ModalTransferAccountsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transaction = this.data.transaction;
    this.transaction.typeTransaction = 'ingreso';
  }

  ngOnInit(): void {
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  backToForm() {
    this.formSelect = null;
  }

  backToSelect() {
    this.idProject = null;
  }

  sendFormData(): void {
  }

  visibleSelect() {
    if (this.idProject == null && this.formSelect == null) {
      return 'visible';
    }
  }

  visibleForm() {
    if (this.formSelect == null && this.idProject != null) {
      return 'visible';
    }
  }

  visibleNew() {
    if (this.idProject != null && this.formSelect != null) {
      return 'visible';
    }
  }

  setIdProject(idProject) {
    this.idProject = idProject;
  }

  setFormSelect(form) {
    this.formSelect = form;
  }

  setProject(data) {
    this.data.project = data;
  }

  resetAccoun() {
    this.data.transaction.idActor = null;
  }

  accept() {
    this.formData = this.transaction;
    this.dialogRef.close(this.formData);
  }
}
