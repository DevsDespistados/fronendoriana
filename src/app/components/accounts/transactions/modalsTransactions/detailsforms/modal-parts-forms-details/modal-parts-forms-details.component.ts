import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Transaction} from '../../../../../../model/accounts/transaction';
import {CodeTransaction} from '../../../../../../model/accounts/ConstTransactions';
import {ConfText} from '../../../../../../model/configuration/conf-text';

@Component({
  selector: 'app-modal-parts-forms-details',
  templateUrl: './modal-parts-forms-details.component.html',
  styleUrls: ['./modal-parts-forms-details.component.css']
})
export class ModalPartsFormsDetailsComponent implements OnInit {
  transactionSelect: Transaction;
  code = CodeTransaction;
  text = ConfText;

  constructor(public dialogRef: MatDialogRef<ModalPartsFormsDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transactionSelect = this.data.transaction;
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
