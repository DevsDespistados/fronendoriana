import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPartsFormsDetailsComponent} from './modal-parts-forms-details.component';

describe('ModalPartsFormsDetailsComponent', () => {
  let component: ModalPartsFormsDetailsComponent;
  let fixture: ComponentFixture<ModalPartsFormsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPartsFormsDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPartsFormsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
