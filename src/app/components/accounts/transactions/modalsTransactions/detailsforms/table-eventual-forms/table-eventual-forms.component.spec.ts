import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TableEventualFormsComponent} from './table-eventual-forms.component';

describe('TableEventualFormsComponent', () => {
  let component: TableEventualFormsComponent;
  let fixture: ComponentFixture<TableEventualFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableEventualFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEventualFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
