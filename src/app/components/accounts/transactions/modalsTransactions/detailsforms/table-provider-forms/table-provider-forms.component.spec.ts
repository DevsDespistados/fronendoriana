import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TableProviderFormsComponent} from './table-provider-forms.component';

describe('TableProviderFormsComponent', () => {
  let component: TableProviderFormsComponent;
  let fixture: ComponentFixture<TableProviderFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableProviderFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableProviderFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
