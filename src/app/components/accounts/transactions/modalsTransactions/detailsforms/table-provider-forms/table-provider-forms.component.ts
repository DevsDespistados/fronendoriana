import {Component, Input, OnInit} from '@angular/core';
import {ViewTransactions} from '../../../../../../model/accounts/view-transactions';
import {TotalForm} from '../../../../../../model/total-form';
import {TransactionService} from '../../../../../../services/accounts/transaction.service';
import {ProviderForm} from '../../../../../../model/form/provider-form';
import {ViewPayCheck} from '../../../../../../model/accounts/view-pay-check';

@Component({
  selector: 'app-table-provider-forms',
  templateUrl: './table-provider-forms.component.html',
  styleUrls: ['./table-provider-forms.component.css']
})
export class TableProviderFormsComponent implements OnInit {
  @Input() transactionSelect: ViewTransactions;
  payParts: ViewTransactions[] = [];
  viewProviderForm: ProviderForm;
  check: ViewPayCheck;
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  displayedColumns = [
    'position',
    'date',
    'check',
    'project',
    'operation',
    'accountD',
    'category',
    'respaldo',
    'description',
    'import',
    // 'balance'
  ];

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.transactionService.getTransactionPartForm(this.transactionSelect.idActor,
      this.transactionSelect.code_transaction).subscribe(
      res => {
        this.payParts = res['transactions'];
        this.viewProviderForm = res['form'];
        this.check = res['check'];
      }
    );
  }

  getTotalColumn(rr) {
    let total = 0;
    this.payParts.forEach(f => {
      total += Number(f[rr]);
    });
    return total;
  }
}
