import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TableProjectsFormsComponent} from './table-projects-forms.component';

describe('TableProjectsFormsComponent', () => {
  let component: TableProjectsFormsComponent;
  let fixture: ComponentFixture<TableProjectsFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableProjectsFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableProjectsFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
