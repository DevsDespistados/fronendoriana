import {Component, Input, OnInit} from '@angular/core';
import {ViewTransactions} from '../../../../../../model/accounts/view-transactions';
import {TransactionService} from '../../../../../../services/accounts/transaction.service';

@Component({
  selector: 'app-table-projects-forms',
  templateUrl: './table-projects-forms.component.html',
  styleUrls: ['./table-projects-forms.component.css']
})
export class TableProjectsFormsComponent implements OnInit {
  payParts: ViewTransactions[];
  @Input() transactionSelect: ViewTransactions;

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit() {
    // this.providerFormSelect = new ProviderForm(
    //   form.idProviderForm,
    //   form.periodProviderForm,
    //   form.advanceProviderForm,
    //   form.discountProviderForm,
    //   form.anticipationProviderForm,
    //   form.liquidPayableProviderForm,
    //   form.activeProviderForm,
    //   form.idProvider_providers,
    //   form.balanceAux,
    //   form.mountPayAux);
    this.transactionService.getTransactionPartForm(this.transactionSelect.idActor,
      this.transactionSelect.code_transaction).subscribe(
      res => {
        this.payParts = res['transactions'];
      }
    );
  }

}
