import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TablePermanentFormsComponent} from './table-permanent-forms.component';

describe('TablePermanentFormsComponent', () => {
  let component: TablePermanentFormsComponent;
  let fixture: ComponentFixture<TablePermanentFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TablePermanentFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablePermanentFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
