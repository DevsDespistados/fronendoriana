import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EquipmentAdvanceExpenseComponent} from './equipment-advance-expense.component';

describe('EquipmentAdvanceExpenseComponent', () => {
  let component: EquipmentAdvanceExpenseComponent;
  let fixture: ComponentFixture<EquipmentAdvanceExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentAdvanceExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentAdvanceExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
