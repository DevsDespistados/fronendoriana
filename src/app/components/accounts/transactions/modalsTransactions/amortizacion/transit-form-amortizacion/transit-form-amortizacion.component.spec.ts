import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransitFormAmortizacionComponent} from './transit-form-amortizacion.component';

describe('TransitFormAmortizacionComponent', () => {
  let component: TransitFormAmortizacionComponent;
  let fixture: ComponentFixture<TransitFormAmortizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransitFormAmortizacionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitFormAmortizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
