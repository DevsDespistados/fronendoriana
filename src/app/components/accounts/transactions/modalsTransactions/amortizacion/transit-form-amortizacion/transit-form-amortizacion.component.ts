import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Transaction} from '../../../../../../model/accounts/transaction';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {ViewPayCheck} from '../../../../../../model/accounts/view-pay-check';
import {Transit} from '../../../../../../model/personal/transit';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {TransactionAmortization} from '../transit-for-amortizacion-select/transit-for-amortizacion-select.component';
import {CodeTransaction} from '../../../../../../model/accounts/ConstTransactions';

@Component({
  selector: 'app-transit-form-amortizacion',
  templateUrl: './transit-form-amortizacion.component.html',
  styleUrls: ['./transit-form-amortizacion.component.css']
})
export class TransitFormAmortizacionComponent implements OnInit {
  @Input() data: DataTransaction;
  @Input() trasit: Transit;
  @Input() transactionSelect: TransactionAmortization;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  transaction: Transaction;
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: ViewPayCheck;

  constructor() {
  }

  ngOnInit(): void {
    this.transaction = this.data.transaction;
    this.bank = this.data.bank;
    this.period = this.data.period;
    this.check = this.data.check;
    this.endDate = endDate(this.data.period.bankPeriod);
    // this.transaction = this.data.transaction;
    this.transaction.amountTransaction = Math.abs(this.transaction.amountTransaction);
    this.transactionSelect.amountTransaction = Math.abs(this.transactionSelect.amountTransaction);
    this.transactionSelect.balance = Math.abs(this.transactionSelect.balance);
  }

  accept() {
    if (this.check) {
      if (this.check.type == 'ingreso') {
        this.transaction.typeTransaction = 'ingreso';
        this.transaction.nroReceiptTransaction = this.check.numberCheck;
        this.transaction.idCheck = this.check.idPayCheck;
        this.transaction.amountTransaction = this.check.mountCheck;
      }
    } else {
      this.transaction.typeTransaction = 'ingreso';
      this.transaction.code_transaction = CodeTransaction.transitAmortizationIncome;
    }
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.idActor = this.transactionSelect.idTransaction;
    this.clickAccept.emit(this.transaction);
  }

  canceled() {
    this.cancel.emit(true);
  }

  pressBack() {
    this.back.emit(true);
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.nroReceiptTransaction == '';
  }

  validT() {
    console.log(this.transactionSelect);
    if (this.check) {
      return this.transaction.dateTransaction == null
        || this.transaction.descriptionTransaction == ''
        || this.transaction.descriptionTransaction == null
        || this.transaction.amountTransaction < 0
        || this.transaction.amountTransaction > this.transactionSelect.balance
        || this.transaction.nroReceiptTransaction == null
        || this.transaction.nroReceiptTransaction == ''
        || this.transaction.amountTransaction > this.check.balance;
    } else {
      return this.transaction.dateTransaction == null
        || this.transaction.descriptionTransaction == ''
        || this.transaction.descriptionTransaction == null
        || this.transaction.amountTransaction < 0
        || this.transaction.amountTransaction > this.transactionSelect.balance
        || this.transaction.nroReceiptTransaction == null
        || this.transaction.nroReceiptTransaction == '';
    }
  }

  visible() {
    if (this.check) {
      return this.check.type == 'ingreso';
    } else {
      return false;
    }
  }
}
