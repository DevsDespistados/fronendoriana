import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Transit} from '../../../../../../model/personal/transit';
import {TransactionAmortization} from '../transit-for-amortizacion-select/transit-for-amortizacion-select.component';

@Component({
  selector: 'app-modal-amortizacion',
  templateUrl: './modal-amortizacion.component.html',
  styleUrls: ['./modal-amortizacion.component.css']
})
export class ModalAmortizacionComponent implements OnInit {

  text = ConfText;
  formData: TransactionAmortization;
  transaction: any;
  formSelect: TransactionAmortization;
  idTransit;
  transitSelect: Transit;
  selectClass = '';

  constructor(public dialogRef: MatDialogRef<ModalAmortizacionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transaction = this.data.transaction;
    this.transaction.typeTransaction = 'ingreso';
  }

  ngOnInit(): void {
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  backToForm() {
    this.formSelect = null;
  }

  backToSelect() {
    this.idTransit = null;
  }

  sendFormData(): void {
  }

  visibleSelect() {
    if (this.idTransit == null && this.formSelect == null) {
      return 'visible';
    }
  }

  visibleForm() {
    if (this.formSelect == null && this.idTransit != null) {
      return 'visible';
    }
  }

  visibleNew() {
    if (this.idTransit != null && this.formSelect != null) {
      return 'visible';
    }
  }

  setTransit(transit: Transit) {
    this.transitSelect = transit;
    this.idTransit = transit.idTransit;
  }

  setFormSelect(transaction) {
    this.formSelect = transaction;
  }

  accept(transacion) {
    this.formData = transacion;
    this.dialogRef.close(this.formData);
  }
}
