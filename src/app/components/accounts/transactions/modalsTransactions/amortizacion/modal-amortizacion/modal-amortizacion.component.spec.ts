import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalAmortizacionComponent} from './modal-amortizacion.component';

describe('ModalAmortizacionComponent', () => {
  let component: ModalAmortizacionComponent;
  let fixture: ComponentFixture<ModalAmortizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalAmortizacionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAmortizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
