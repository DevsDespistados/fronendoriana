import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackErrorComponent} from '../../../../../snack-msg/snack-error/snack-error.component';
import {Transit} from '../../../../../../model/personal/transit';
import {TransitService} from '../../../../../../services/personal/transit.service';

@Component({
  selector: 'app-transit-select',
  templateUrl: './transit-select.component.html',
  styleUrls: ['./transit-select.component.css']
})
export class TransitSelectComponent implements OnInit {
  @Output() accept = new EventEmitter();
  @Output() cancel = new EventEmitter();
  transit: Transit[] = [];
  text = ConfText;
  pos: number;

  constructor(private snack: MatSnackBar,
              private transitService: TransitService) {
  }

  ngOnInit() {
    this.loadClients();
  }

  loadClients() {
    this.transitService.getActives(1).subscribe(
      res => {
        this.transit = <Transit[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  sendFormData() {
    this.accept.emit(this.transit[this.pos]);
  }

  validClient() {
    return this.pos == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

}
