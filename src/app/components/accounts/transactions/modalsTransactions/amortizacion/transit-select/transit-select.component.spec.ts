import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransitSelectComponent} from './transit-select.component';

describe('TransitSelectComponent', () => {
  let component: TransitSelectComponent;
  let fixture: ComponentFixture<TransitSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransitSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
