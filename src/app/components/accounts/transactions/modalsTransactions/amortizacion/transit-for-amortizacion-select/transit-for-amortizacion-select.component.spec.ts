import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransitForAmortizacionSelectComponent} from './transit-for-amortizacion-select.component';

describe('TransitForAmortizacionSelectComponent', () => {
  let component: TransitForAmortizacionSelectComponent;
  let fixture: ComponentFixture<TransitForAmortizacionSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransitForAmortizacionSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitForAmortizacionSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
