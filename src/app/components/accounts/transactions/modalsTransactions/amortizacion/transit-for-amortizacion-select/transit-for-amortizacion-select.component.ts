import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TotalForm} from '../../../../../../model/total-form';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {ViewContract} from '../../../../../../model/contract/view-contract';
import {OptionOrianaTableToolbar} from '../../../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {TransactionService} from '../../../../../../services/accounts/transaction.service';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {ViewTransactions} from '../../../../../../model/accounts/view-transactions';
import {ViewPayCheck} from '../../../../../../model/accounts/view-pay-check';

@Component({
  selector: 'app-transit-for-amortizacion-select',
  templateUrl: './transit-for-amortizacion-select.component.html',
  styleUrls: ['./transit-for-amortizacion-select.component.css']
})
export class TransitForAmortizacionSelectComponent implements OnInit {
  @Input() transactionSelect: DataTransaction;
  @Input() idTransit: number;
  @Output() formSelect = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() back = new EventEmitter();
  transactions: TransactionAmortization[] = [];
  total: TotalForm;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'date',
    'check',
    'operation',
    'accountD',
    'respaldo',
    'description',
    'import',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  text = ConfText;
  sortedData: ViewContract[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  positionSelect;
  check: ViewPayCheck;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private transactionService: TransactionService) {
  }


  ngOnInit() {
    this.check = this.transactionSelect.check;
    this.load();
  }

  getFilter(type: any) {
    this.transactionService.getAllAmortization(this.transactionSelect.bank.idBank,
      this.transactionSelect.period.idBankPeriod,
      this.transactionSelect.check.idPayCheck,
      this.idTransit).subscribe(
      res => {
        this.transactions = res['transactions'];
        this.total = res['total'];
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      // this.eventualFormService.search(text).subscribe(
      //   response => {
      //     this.eventualForms = response['forms'];
      //     this.total = response['total'];
      //   }, error => {
      //     this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      //   }
      // );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  sendFormData() {
    this.formSelect.emit(this.transactions[this.positionSelect]);
  }

  validForm() {
    return this.positionSelect == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

  pressBack() {
    this.back.emit(true);
  }

  isCheckedDisabled(element) {
    if (this.check) {
      return this.check.balance < element.balance || element.balance == 0;
    } else {
      return element.balance == 0;
    }
  }

  getTotalColumn(rr) {
    let total = 0;
    this.transactions.forEach(f => {
      total += f[rr];
    });
    return total;
  }
}

export class TransactionAmortization extends ViewTransactions {
  constructor(public idTransaction: number,
              public typeTransaction: string,
              public dateTransaction: any,
              public nroReceiptTransaction: string,
              public descriptionTransaction: string,
              public amountTransaction: number,
              public idCategory_transaction: number,
              public nameCategory: string,
              public nameAccountTransaction: string,
              public idBankPeriod: number,
              public idBankExecutor: number,
              public idProject: number,
              public nameProject: string,
              public idCheck: number,
              public numberCheck: string,
              public idActor: number,
              public code_transaction: number,
              public pay: number,
              public balance: number) {
    super(idTransaction,
      typeTransaction,
      dateTransaction,
      nroReceiptTransaction,
      descriptionTransaction,
      amountTransaction,
      idCategory_transaction,
      nameCategory,
      nameAccountTransaction,
      idBankPeriod,
      idBankExecutor,
      idProject,
      nameProject,
      idCheck,
      numberCheck,
      idActor,
      code_transaction);
  }
}

