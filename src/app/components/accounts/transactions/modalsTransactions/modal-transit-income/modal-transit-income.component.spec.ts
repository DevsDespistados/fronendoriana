import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTransitIncomeComponent} from './modal-transit-income.component';

describe('ModalTransitIncomeComponent', () => {
  let component: ModalTransitIncomeComponent;
  let fixture: ComponentFixture<ModalTransitIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTransitIncomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTransitIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
