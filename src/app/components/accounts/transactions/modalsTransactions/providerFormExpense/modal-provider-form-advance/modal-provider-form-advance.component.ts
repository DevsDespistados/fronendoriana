import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Transaction} from '../../../../../../model/accounts/transaction';

@Component({
  selector: 'app-modal-provider-form-advance',
  templateUrl: './modal-provider-form-advance.component.html',
  styleUrls: ['./modal-provider-form-advance.component.css']
})
export class ModalProviderFormAdvanceComponent implements OnInit {
  text = ConfText;
  formData;

  transaction: Transaction;
  contractSlect;
  formSelect;
  selectClass = '';

  constructor(public dialogRef: MatDialogRef<ModalProviderFormAdvanceComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transaction = this.data.transaction;
    this.transaction.typeTransaction = 'egreso';
  }

  ngOnInit() {
  }

  closeDialog() {
    this.formData = null;
    this.contractSlect = null;
    this.dialogRef.close();
  }

  setContract(event) {
    this.contractSlect = event;
  }

  visibleSelect() {
    if (this.contractSlect == null && this.transaction.idTransaction == null) {
      return 'visible';
    }
  }

  visibleForm() {
    if (this.contractSlect != null && this.transaction.idTransaction == null) {
      return 'visible';
    }
  }

  visibleNew() {
    if (this.formSelect != null && this.contractSlect != null || this.transaction.idTransaction != null) {
      return 'visible';
    }
  }

  backContract() {
    this.contractSlect = null;
  }

  backForms() {
    this.formSelect = null;
  }

  setFormSelect(evt) {
    this.formSelect = evt;
  }

  accept() {
    this.formData = this.transaction;
    this.dialogRef.close(this.formData);
  }
}
