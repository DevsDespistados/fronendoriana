import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalProviderFormAdvanceComponent} from './modal-provider-form-advance.component';

describe('ModalProviderFormAdvanceComponent', () => {
  let component: ModalProviderFormAdvanceComponent;
  let fixture: ComponentFixture<ModalProviderFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalProviderFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalProviderFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
