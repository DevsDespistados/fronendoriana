import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderFormSelectComponent} from './provider-form-select.component';

describe('ProviderFormSelectComponent', () => {
  let component: ProviderFormSelectComponent;
  let fixture: ComponentFixture<ProviderFormSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderFormSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
