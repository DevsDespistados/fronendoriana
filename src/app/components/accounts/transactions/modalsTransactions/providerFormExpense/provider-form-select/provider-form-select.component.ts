import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {ProviderForm} from '../../../../../../model/form/provider-form';
import {TotalForm} from '../../../../../../model/total-form';
import {ViewContract} from '../../../../../../model/contract/view-contract';
import {OptionOrianaTableToolbar} from '../../../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../../../model/configuration/Tooltips';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {ProviderFormService} from '../../../../../../services/form/provider-form.service';

@Component({
  selector: 'app-provider-form-select',
  templateUrl: './provider-form-select.component.html',
  styleUrls: ['./provider-form-select.component.css']
})
export class ProviderFormSelectComponent implements OnInit {
  @Input() contract: ViewContract;
  @Output() formSelect = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  forms: ProviderForm[] = [];
  total: TotalForm;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'advance',
    'discount',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  text = ConfText;
  sortedData: ViewContract[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  positionSelect;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private providerFormService: ProviderFormService) {
  }


  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.providerFormService.getProviderForms(this.contract.idProvider, this.contract.idContract, type).subscribe(
      res => {
        this.forms = res['forms'];
        this.total = res['total'];
        this.contract = res['contract'];
      }, error1 => {
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      this.providerFormService.searchProviderForm(text).subscribe(
        response => {
          this.forms = <ProviderForm[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  sendFormData() {
    this.formSelect.emit(this.forms[this.positionSelect]);
  }

  validForm() {
    return this.positionSelect == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

  isCheckedDisabled(element: ProviderForm) {
    return element.balanceAux == 0;
  }

  pressBack() {
    this.back.emit(true);
  }

  getTotalColumn(rr) {
    let total = 0;
    this.forms.forEach(f => {
      total += f[rr];
    });
    return total;
  }
}
