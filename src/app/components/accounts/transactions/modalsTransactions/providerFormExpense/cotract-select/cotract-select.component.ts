import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ContractProviderService} from '../../../../../../services/contract/contract-provider.service';
import {SnackAlertComponent} from '../../../../../snack-msg/snack-alert/snack-alert.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {Tooltips} from '../../../../../../model/configuration/Tooltips';
import {ViewContract} from '../../../../../../model/contract/view-contract';
import {Transaction} from '../../../../../../model/accounts/transaction';

@Component({
  selector: 'app-cotract-select',
  templateUrl: './cotract-select.component.html',
  styleUrls: ['./cotract-select.component.css']
})
export class CotractSelectComponent implements OnInit {
  @Input() transaction: Transaction;
  @Output() contractSelect = new EventEmitter();
  @Output() cancel = new EventEmitter();
  contracts = [];
  titulo = 'Disponibles';
  text = ConfText;
  displayedColumns = [
    'position',
    'contract',
    'project',
    'provider',
    'category',
    'options'
  ];
  // options = [
  //   new OptionOrianaTableToolbar('DISPONIBLES', 1),
  //   new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
  //   new OptionOrianaTableToolbar('TODOS', 2)
  // ];
  tooltip = Tooltips;
  currentOption = 1;
  positionSelect;

  constructor(private snack: MatSnackBar,
              private contractProviderService: ContractProviderService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.contractProviderService.getContractProviderFilter(type).subscribe(
      res => {
        this.contracts = res['contracts'];
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  load() {
    this.getFilter(this.currentOption);
    // this.options.forEach(op => {
    //   if (op.url == this.currentOption) {
    //     this.titulo = op.option;
    //   }
    // });
  }

  sortData(sort: Sort): any {
  }

  search(text: string) {
    if (text !== '') {
      this.contractProviderService.searchContract(text).subscribe(
        response => {
          this.contracts = <ViewContract[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sendFormData() {
    this.contractSelect.emit(this.contracts[this.positionSelect]);
  }

  validForm() {
    return this.positionSelect == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

  isCheckedDisabled(element) {
  }
}
