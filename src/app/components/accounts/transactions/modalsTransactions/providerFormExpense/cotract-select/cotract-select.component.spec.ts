import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CotractSelectComponent} from './cotract-select.component';

describe('CotractSelectComponent', () => {
  let component: CotractSelectComponent;
  let fixture: ComponentFixture<CotractSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CotractSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotractSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
