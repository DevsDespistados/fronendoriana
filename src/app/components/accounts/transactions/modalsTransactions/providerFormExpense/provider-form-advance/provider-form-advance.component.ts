import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {PayCheck} from '../../../../../../model/accounts/pay-check';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {ProviderForm} from '../../../../../../model/form/provider-form';
import {ViewContract} from '../../../../../../model/contract/view-contract';
import {Transaction} from '../../../../../../model/accounts/transaction';

@Component({
  selector: 'app-provider-form-advance',
  templateUrl: './provider-form-advance.component.html',
  styleUrls: ['./provider-form-advance.component.css']
})
export class ProviderFormAdvanceComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Input() formSelect: ProviderForm;
  @Input() contract: ViewContract;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: PayCheck;
  transaction: Transaction;

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.dataTransaction);
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'pago de Planilla Proveedor';
  }

  accept() {
    this.transaction.idProject = this.contract.idProject;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.idActor = this.formSelect.idProviderForm;
    console.log(this.transaction);
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == ''
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.amountTransaction <= 0
      || this.transaction.amountTransaction > this.formSelect.balanceAux;
  }

  pressBack() {
    this.back.emit(true);
  }

  calcBalance() {
    return this.formSelect.balanceAux - this.transaction.amountTransaction;
  }
}
