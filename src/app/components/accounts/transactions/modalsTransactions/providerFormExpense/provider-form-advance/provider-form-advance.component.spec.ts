import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderFormAdvanceComponent} from './provider-form-advance.component';

describe('ProviderFormAdvanceComponent', () => {
  let component: ProviderFormAdvanceComponent;
  let fixture: ComponentFixture<ProviderFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
