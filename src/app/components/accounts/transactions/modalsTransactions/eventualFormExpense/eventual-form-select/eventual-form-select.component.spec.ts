import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventualFormSelectComponent} from './eventual-form-select.component';

describe('EventualFormSelectComponent', () => {
  let component: EventualFormSelectComponent;
  let fixture: ComponentFixture<EventualFormSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventualFormSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventualFormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
