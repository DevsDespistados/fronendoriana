import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ViewContract} from '../../../../../../model/contract/view-contract';
import {ProviderForm} from '../../../../../../model/form/provider-form';
import {TotalForm} from '../../../../../../model/total-form';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {OptionOrianaTableToolbar} from '../../../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {EventualFormService} from '../../../../../../services/form/eventual-form.service';
import {EventualForm} from '../../../../../../model/form/eventual-form';
import {SnackAlertComponent} from '../../../../../snack-msg/snack-alert/snack-alert.component';

@Component({
  selector: 'app-eventual-form-select',
  templateUrl: './eventual-form-select.component.html',
  styleUrls: ['./eventual-form-select.component.css']
})
export class EventualFormSelectComponent implements OnInit {
  @Output() formSelect = new EventEmitter();
  @Output() cancel = new EventEmitter();
  eventualForms: EventualForm[] = [];
  total: TotalForm;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'form',
    'period',
    'mountTotal',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  text = ConfText;
  sortedData: ViewContract[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  positionSelect;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private eventualFormService: EventualFormService) {
  }


  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.eventualFormService.getEventualForms(type).subscribe(
      res => {
        this.eventualForms = res['forms'];
        this.total = res['total'];
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      this.eventualFormService.search(text).subscribe(
        response => {
          this.eventualForms = response['forms'];
          this.total = response['total'];
        }, error => {
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  sendFormData() {
    this.formSelect.emit(this.eventualForms[this.positionSelect]);
  }

  validForm() {
    return this.positionSelect == null;
  }

  canceled() {
    this.cancel.emit(true);
  }

  isCheckedDisabled(element: ProviderForm) {
    return element.balanceAux == 0;
  }

  getTotalColumn(rr) {
    let total = 0;
    this.eventualForms.forEach(f => {
      total += f[rr];
    });
    return total;
  }
}
