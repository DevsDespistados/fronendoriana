import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEventualFormAdvanceComponent} from './modal-eventual-form-advance.component';

describe('ModalEventualFormAdvanceComponent', () => {
  let component: ModalEventualFormAdvanceComponent;
  let fixture: ComponentFixture<ModalEventualFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEventualFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEventualFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
