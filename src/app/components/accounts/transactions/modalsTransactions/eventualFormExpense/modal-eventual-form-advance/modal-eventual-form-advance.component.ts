import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Transaction} from '../../../../../../model/accounts/transaction';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-eventual-form-advance',
  templateUrl: './modal-eventual-form-advance.component.html',
  styleUrls: ['./modal-eventual-form-advance.component.css']
})
export class ModalEventualFormAdvanceComponent implements OnInit {
  text = ConfText;
  formData;

  transaction: Transaction;
  formSelect;
  selectClass = '';

  constructor(public dialogRef: MatDialogRef<ModalEventualFormAdvanceComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transaction = this.data.transaction;
    this.transaction.typeTransaction = 'egreso';
  }

  ngOnInit() {
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  backForms() {
    this.formSelect = null;
  }

  setFormSelect(evt) {
    this.formSelect = evt;
  }

  accept() {
    this.formData = this.transaction;
    this.dialogRef.close(this.formData);
  }
}
