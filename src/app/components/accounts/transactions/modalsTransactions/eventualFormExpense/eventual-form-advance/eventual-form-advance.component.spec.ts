import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventualFormAdvanceComponent} from './eventual-form-advance.component';

describe('EventualFormAdvanceComponent', () => {
  let component: EventualFormAdvanceComponent;
  let fixture: ComponentFixture<EventualFormAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventualFormAdvanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventualFormAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
