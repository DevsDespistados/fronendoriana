import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTransaction} from '../../../transactions-check/transactions-check.component';
import {ConfText} from '../../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../../model/accounts/bank';
import {Period} from '../../../../../../model/accounts/period';
import {PayCheck} from '../../../../../../model/accounts/pay-check';
import {endDate} from '../../../../../../services/GlobalFunctions';
import {EventualForm} from '../../../../../../model/form/eventual-form';
import {EventualFormService} from '../../../../../../services/form/eventual-form.service';

@Component({
  selector: 'app-eventual-form-advance',
  templateUrl: './eventual-form-advance.component.html',
  styleUrls: ['./eventual-form-advance.component.css']
})
export class EventualFormAdvanceComponent implements OnInit {
  @Input() dataTransaction: DataTransaction;
  @Input() formSelect: EventualForm;
  @Output() clickAccept = new EventEmitter();
  @Output() back = new EventEmitter();
  @Output() cancel = new EventEmitter();
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: PayCheck;
  transaction;
  forms: EventualFormForProjects[] = [];
  eventualFormForProjectSelect: EventualFormForProjects;

  constructor(private evetualFormService: EventualFormService) {
  }

  ngOnInit(): void {
    this.transaction = this.dataTransaction.transaction;
    this.bank = this.dataTransaction.bank;
    this.period = this.dataTransaction.period;
    this.check = this.dataTransaction.check;
    this.endDate = endDate(this.dataTransaction.period.bankPeriod);
    this.transaction.descriptionTransaction = 'pago de Planilla Eventual';
    this.getTotalForms(this.formSelect.idEventualForm);
  }

  getTotalForms(idForm) {
    this.evetualFormService.getEventualFormForProjects(idForm).subscribe(
      res => {
        this.forms = res['forms'];
      }
    );
  }

  loadFormSelect(evt) {
    this.eventualFormForProjectSelect = this.forms[evt];
  }

  calcNewBalance() {
    if (this.eventualFormForProjectSelect) {
      return this.eventualFormForProjectSelect.balanceAux - this.transaction.amountTransaction;
    } else {
      return 0;
    }
  }

  accept() {
    this.transaction.idProject = this.eventualFormForProjectSelect.idProject;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.idActor = this.formSelect.idEventualForm;
    console.log(this.transaction);
    this.clickAccept.emit(true);
  }

  canceled() {
    this.cancel.emit(true);
  }

  valid() {
    if (this.eventualFormForProjectSelect) {
      return this.transaction.dateTransaction == null
        || this.transaction.descriptionTransaction == ''
        || this.transaction.descriptionTransaction == null
        || this.transaction.nroReceiptTransaction == ''
        || this.transaction.nroReceiptTransaction == null
        || this.transaction.amountTransaction <= 0
        || this.transaction.amountTransaction > this.eventualFormForProjectSelect.balanceAux;
    } else {
      return true;
    }

  }

  pressBack() {
    this.back.emit(true);
  }
}

class EventualFormForProjects extends EventualForm {
  constructor(
    public idProject: number,
    public idEventualForm: number,
    public nameProject: string,
    public year: number,
    public biweekly: number,
    public discountEventualForm: number,
    public bondEventualForm: number,
    public advanceEventualForm: number,
    public liquidEventualForm: number,
    public statusEventualForm: any,
    public balanceAux: number,
    public mountPayAux: number,
    public mountTotal: number
  ) {
    super(idEventualForm,
      year,
      biweekly,
      discountEventualForm,
      bondEventualForm,
      advanceEventualForm,
      liquidEventualForm,
      statusEventualForm,
      balanceAux,
      mountPayAux,
      mountTotal);
  }

}
