import {Component, Inject, OnInit} from '@angular/core';
import {Transaction} from '../../../../../model/accounts/transaction';
import {ConfText} from '../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../model/accounts/bank';
import {Period} from '../../../../../model/accounts/period';
import {ViewPayCheck} from '../../../../../model/accounts/view-pay-check';
import {ViewProjects} from '../../../../../model/project/view-projects';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectService} from '../../../../../services/project/project.service';
import {endDate} from '../../../../../services/GlobalFunctions';
import {SnackErrorComponent} from '../../../../snack-msg/snack-error/snack-error.component';
import {Permanent} from '../../../../../model/personal/permanent';
import {PermanentService} from '../../../../../services/personal/permanent.service';

@Component({
  selector: 'app-permanent-advance-expense',
  templateUrl: './permanent-advance-expense.component.html',
  styleUrls: ['./permanent-advance-expense.component.css']
})
export class PermanentAdvanceExpenseComponent implements OnInit {
  transaction: Transaction;
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: ViewPayCheck;
  projects: ViewProjects[];
  permanents: Permanent[] = [];

  constructor(public dialogRef: MatDialogRef<PermanentAdvanceExpenseComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectService: ProjectService,
              private permanentService: PermanentService,
              private snack: MatSnackBar) {
    this.transaction = this.data.transaction;
    this.transaction.amountTransaction = Math.abs(this.transaction.amountTransaction);
    this.transaction.typeTransaction = 'egreso';
  }

  ngOnInit(): void {
    this.bank = this.data.bank;
    this.period = this.data.period;
    this.check = this.data.check;
    this.endDate = endDate(this.data.period.bankPeriod);
    this.transaction.descriptionTransaction = 'egreso directo';
    this.loadProjects();
    this.loadContractAll();
  }

  accept() {
    this.transaction.typeTransaction = 'egreso';
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.dialogRef.close(this.transaction);
  }

  loadProjects() {
    this.projectService.getOpeAccount(1).subscribe(
      res => {
        this.projects = <ViewProjects[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  loadContractAll() {
    this.permanentService.getActives(1).subscribe(
      res => {
        this.permanents = <Permanent[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  canceled() {
    this.dialogRef.close();
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.nroReceiptTransaction == ''
      || this.transaction.amountTransaction <= 0
      || this.transaction.amountTransaction > this.check.balance
      || this.transaction.idActor == null
      || this.transaction.idActor < 0;
  }
}
