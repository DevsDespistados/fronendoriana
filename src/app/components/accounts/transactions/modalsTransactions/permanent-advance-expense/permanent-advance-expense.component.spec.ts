import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PermanentAdvanceExpenseComponent} from './permanent-advance-expense.component';

describe('PermanentAdvanceExpenseComponent', () => {
  let component: PermanentAdvanceExpenseComponent;
  let fixture: ComponentFixture<PermanentAdvanceExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermanentAdvanceExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanentAdvanceExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
