import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../model/accounts/bank';
import {Period} from '../../../../../model/accounts/period';
import {PayCheck} from '../../../../../model/accounts/pay-check';
import {ViewProjects} from '../../../../../model/project/view-projects';
import {endDate} from '../../../../../services/GlobalFunctions';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectService} from '../../../../../services/project/project.service';
import {SnackErrorComponent} from '../../../../snack-msg/snack-error/snack-error.component';
import {Transaction} from '../../../../../model/accounts/transaction';

@Component({
  selector: 'app-direct-income',
  templateUrl: './direct-income.component.html',
  styleUrls: ['./direct-income.component.css']
})
export class DirectIncomeComponent implements OnInit {
  transaction: Transaction;
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: PayCheck;
  projects: ViewProjects[];

  constructor(public dialogRef: MatDialogRef<DirectIncomeComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectService: ProjectService,
              private snack: MatSnackBar) {
    this.transaction = this.data.transaction;
    this.transaction.typeTransaction = 'ingreso';
  }

  ngOnInit(): void {
    this.bank = this.data.bank;
    this.period = this.data.period;
    this.check = this.data.check;
    this.endDate = endDate(this.data.period.bankPeriod);
    this.transaction.descriptionTransaction = 'ingreso directo';
    this.loadProjects();
  }

  accept() {
    this.transaction.typeTransaction = 'ingreso';
    this.transaction.nroReceiptTransaction = this.check.numberCheck;
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    // this.transaction.idCheck = this.check.idPayCheck;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.transaction.amountTransaction = this.check.mountCheck;
    this.transaction.idActor = null;
    this.dialogRef.close(this.transaction);
  }

  loadProjects() {
    this.projectService.getOpeAccount(1).subscribe(
      res => {
        this.projects = <ViewProjects[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  canceled() {
    this.dialogRef.close();
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.idProject == null;
  }
}
