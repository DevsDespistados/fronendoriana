import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DirectIncomeComponent} from './direct-income.component';

describe('DirectIncomeComponent', () => {
  let component: DirectIncomeComponent;
  let fixture: ComponentFixture<DirectIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DirectIncomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
