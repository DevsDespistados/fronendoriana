import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CompanyIncomeComponent} from './company-income.component';

describe('CompanyIncomeComponent', () => {
  let component: CompanyIncomeComponent;
  let fixture: ComponentFixture<CompanyIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyIncomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
