import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTransitExpenseComponent} from './modal-transit-expense.component';

describe('ModalTransitExpenseComponent', () => {
  let component: ModalTransitExpenseComponent;
  let fixture: ComponentFixture<ModalTransitExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTransitExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTransitExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
