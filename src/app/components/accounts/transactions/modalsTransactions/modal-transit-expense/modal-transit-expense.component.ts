import {Component, Inject, OnInit} from '@angular/core';
import {Transaction} from '../../../../../model/accounts/transaction';
import {ConfText} from '../../../../../model/configuration/conf-text';
import {Bank} from '../../../../../model/accounts/bank';
import {Period} from '../../../../../model/accounts/period';
import {ViewPayCheck} from '../../../../../model/accounts/view-pay-check';
import {Transit} from '../../../../../model/personal/transit';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ProjectService} from '../../../../../services/project/project.service';
import {TransitService} from '../../../../../services/personal/transit.service';
import {endDate} from '../../../../../services/GlobalFunctions';
import {SnackErrorComponent} from '../../../../snack-msg/snack-error/snack-error.component';

@Component({
  selector: 'app-modal-transit-expense',
  templateUrl: './modal-transit-expense.component.html',
  styleUrls: ['./modal-transit-expense.component.css']
})
export class ModalTransitExpenseComponent implements OnInit {

  transaction: Transaction;
  text = ConfText;
  endDate;
  bank: Bank;
  period: Period;
  check: ViewPayCheck;
  transit: Transit[] = [];

  constructor(public dialogRef: MatDialogRef<ModalTransitExpenseComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectService: ProjectService,
              private transitService: TransitService,
              private snack: MatSnackBar) {
    this.transaction = this.data.transaction;
    this.transaction.amountTransaction = Math.abs(this.transaction.amountTransaction);
  }

  ngOnInit(): void {
    this.bank = this.data.bank;
    this.period = this.data.period;
    this.check = this.data.check;
    this.endDate = endDate(this.data.period.bankPeriod);
    this.loadContractAll();
  }

  accept() {
    this.transaction.typeTransaction = 'egreso';
    this.transaction.idBankPeriod = this.period.idBankPeriod;
    this.transaction.idBankExecutor = this.bank.idBank;
    this.dialogRef.close(this.transaction);
  }

  loadContractAll() {
    this.transitService.getActives(1).subscribe(
      res => {
        this.transit = <Transit[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  canceled() {
    this.dialogRef.close();
  }

  valid() {
    return this.transaction.dateTransaction == null
      || this.transaction.descriptionTransaction == ''
      || this.transaction.descriptionTransaction == null
      || this.transaction.nroReceiptTransaction == null
      || this.transaction.nroReceiptTransaction == ''
      || this.transaction.amountTransaction <= 0
      || this.transaction.amountTransaction > this.check.balance
      || this.transaction.idActor == null
      || this.transaction.idActor < 0;
  }
}
