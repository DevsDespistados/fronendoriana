import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderAdvanceExpenseComponent} from './provider-advance-expense.component';

describe('ProviderAdvanceExpenseComponent', () => {
  let component: ProviderAdvanceExpenseComponent;
  let fixture: ComponentFixture<ProviderAdvanceExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderAdvanceExpenseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAdvanceExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
