import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransactionsCheckComponent} from './transactions-check.component';

describe('TransactionsCheckComponent', () => {
  let component: TransactionsCheckComponent;
  let fixture: ComponentFixture<TransactionsCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionsCheckComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
