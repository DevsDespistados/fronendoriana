import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ViewTransactions} from '../../../../model/accounts/view-transactions';
import {TotalForm} from '../../../../model/total-form';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {Transaction} from '../../../../model/accounts/transaction';
import {_permission, _user} from '../../../../services/permissions';
import {ModalClientIncomeComponent} from '../modalsTransactions/clientIncome/modal-client-income/modal-client-income.component';
import {Period} from '../../../../model/accounts/period';
import {Bank} from '../../../../model/accounts/bank';
import {PayCheck} from '../../../../model/accounts/pay-check';
import {ViewProjects} from '../../../../model/project/view-projects';
import {DirectIncomeComponent} from '../modalsTransactions/direct-income/direct-income.component';
import {ViewPayCheck} from '../../../../model/accounts/view-pay-check';
import {ViewBankPeriod} from '../../../../model/accounts/view-bank-period';
import {CompanyIncomeComponent} from '../modalsTransactions/company-income/company-income.component';
import {DirectExpenseComponent} from '../modalsTransactions/direct-expense/direct-expense.component';
import {ProviderAdvanceExpenseComponent} from '../modalsTransactions/provider-advance-expense/provider-advance-expense.component';
import {PermanentAdvanceExpenseComponent} from '../modalsTransactions/permanent-advance-expense/permanent-advance-expense.component';
import {EventualAdvanceExpenseComponent} from '../modalsTransactions/eventual-advance-expense/eventual-advance-expense.component';
import {EquipmentAdvanceExpenseComponent} from '../modalsTransactions/equipment-advance-expense/equipment-advance-expense.component';
import {CompanyExpenseComponent} from '../modalsTransactions/company-expense/company-expense.component';
import {ModalProviderFormAdvanceComponent} from '../modalsTransactions/providerFormExpense/modal-provider-form-advance/modal-provider-form-advance.component';
import {ModalEventualFormAdvanceComponent} from '../modalsTransactions/eventualFormExpense/modal-eventual-form-advance/modal-eventual-form-advance.component';
import {ModalPermanentFormAdvanceComponent} from '../modalsTransactions/permanentFormExpense/modal-permanent-form-advance/modal-permanent-form-advance.component';
import {ModalEquipmentFormAdvanceComponent} from '../modalsTransactions/equipmentFormExpense/modal-equipment-form-advance/modal-equipment-form-advance.component';
import {ModalTransferAccountsComponent} from '../modalsTransactions/transerAccount/modal-transfer-accounts/modal-transfer-accounts.component';
import {CodeTransaction} from '../../../../model/accounts/ConstTransactions';
import {ModalPartsFormsDetailsComponent} from '../modalsTransactions/detailsforms/modal-parts-forms-details/modal-parts-forms-details.component';
import {ModalTransitExpenseComponent} from '../modalsTransactions/modal-transit-expense/modal-transit-expense.component';
import {ModalTransitIncomeComponent} from '../modalsTransactions/modal-transit-income/modal-transit-income.component';
import {ModalAmortizacionComponent} from '../modalsTransactions/amortizacion/modal-amortizacion/modal-amortizacion.component';
import {ReportService} from '../../../../services/report.service';

@Component({
  selector: 'app-transactions-check',
  templateUrl: './transactions-check.component.html',
  styleUrls: ['./transactions-check.component.css']
})
export class TransactionsCheckComponent implements OnInit {
  @Input() idCheck = null;
  @Input() idPeriod;
  @Input() idBank;
  @Output() data = new EventEmitter();
  transactions: ViewTransactions[];
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  titulo = ' ';
  displayedColumns = [
    'position',
    'date',
    'check',
    'project',
    'operation',
    'accountD',
    'category',
    'respaldo',
    'description',
    'import',
    'options'
  ];
  sortedData: ViewTransactions[];
  transactionSelect: Transaction;
  tooltip = Tooltips;
  currentOption = 0;
  progress = false;
  actualType;
  _bank: Bank;
  _check: ViewPayCheck;
  _period: ViewBankPeriod;

  constructor(public dialog: MatDialog,
              public snack: MatSnackBar,
              public printService: PrintServiceService,
              public reportService: ReportService,
              public transactionService: TransactionService) {
  }

  ngOnInit() {
    this.load();
  }



  create() {
    const dialogRef = this.isTypeTransaction(this.actualType);
    console.log(dialogRef);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.transactionSelect = result;
        if (!this.transactionSelect.idTransaction) {
          this.transactionService.newTransaction(this.transactionSelect.typeTransaction, this.transactionSelect)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.transactionService.editTransaction(this.transactionSelect.idTransaction,
            this.transactionSelect.typeTransaction, result)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.transactionSelect = <any>this.transactions[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.transactionSelect.nroReceiptTransaction}
    }).afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.transactionService.deleteTransaction(this.transactionSelect.idTransaction)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.transactions[pos];
    this.actualType = this.transactionSelect.code_transaction;
    this.transactionSelect = new Transaction(
      p.idTransaction,
      p.typeTransaction,
      p.dateTransaction,
      p.nroReceiptTransaction,
      p.descriptionTransaction,
      p.amountTransaction,
      p.idCategory_transaction,
      p.idBankPeriod,
      p.idBankExecutor,
      p.idProject,
      p.idCheck,
      p.idActor,
      p.code_transaction
    );
    this.actualType = Number(this.transactionSelect.code_transaction);
    this.create();
  }

  getFilter(idPeriod: number, idCheck: number) {
    this.transactionService.getTransactionPeriodCheck(idPeriod, idCheck).subscribe(
      res => {
        this.transactions = res['transactions'];
        this._bank = res['bank'];
        this._check = res['check'];
        this._period = res['period'];
        this.data.emit({bank: this._bank, check: this._check, period: this._period});
        this.total = res['total'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
    // this.transactionService.get
  }

  setType(type): any {
    this.actualType = type;
    this.create();
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.idPeriod, this.idCheck);
  }

  printReport() {
    // this.progress = true;
    // this.ac.report({projects: this.projects}).subscribe(
    //   res => {
    //     this.printService.setDataPrint(res['data']);
    //     this.progress = false;
    //   }, error1 => {
    //     this.progress = false;
    //     this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
    //   }
    // );
  }

  printFacturation(id) {
    this.transactionService.printFact({idTransaction: id, user: _user()}).subscribe(
      (res: any) => {
        this.reportService.printReport('Receipt', {
            ID_TRASACTION: id,
            USER_NAME: res.user.name,
            ACTOR: res.actor.type,
            ACTOR_NAME: res.actor.name,
            AMOUNT_TEXT: res.valor,
            DATE_PARAM: res.date,
          }
        );
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.transactionService.searchTransaction(text).subscribe(
        response => {
          this.transactions = response['transaction'];
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.transactionSelect = new Transaction(
      null,
      '',
      new Date(),
      null,
      '',
      null,
      null,
      this.idPeriod,
      this.idBank,
      null,
      this.idCheck,
      null,
      null
    );
  }

  newDisabled() {
    if (this._check) {
      return this.isDisabled() || (this._check.balance <= 0);
    }
  }

  getTypeTransaction() {
    if (this._check) {
      return this._check.type;
    } else {
      return 'egreso';
    }
  }

  isTypeTransaction(type) {
    let res = null;
    const t = new DataTransaction(this.transactionSelect);
    t.bank = this._bank;
    t.check = this._check;
    t.period = this._period;
    t.transaction.code_transaction = type;
    switch (type) {
      case CodeTransaction.directIncome: {
        res = this.dialog.open(DirectIncomeComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.clientIncome: {
        res = this.dialog.open(ModalClientIncomeComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.companyIncome: {
        res = this.dialog.open(CompanyIncomeComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.directExpense: {
        res = this.dialog.open(DirectExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.providerExpense: {
        res = this.dialog.open(ProviderAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.permanentExpense: {
        res = this.dialog.open(PermanentAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.eventualExpense: {
        res = this.dialog.open(EventualAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.equipmentExpense: {
        res = this.dialog.open(EquipmentAdvanceExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.companyExpense: {
        res = this.dialog.open(CompanyExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.transferBank: {
        res = this.dialog.open(ModalTransferAccountsComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.transitExpense: {
        res = this.dialog.open(ModalTransitExpenseComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.transitIncome: {
        res = this.dialog.open(ModalTransitIncomeComponent, {
          width: '400px',
          data: t
        });
        break;
      }
      case CodeTransaction.providerFormExpense: {
        res = this.dialog.open(ModalProviderFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.permanentFormExpense: {
        res = this.dialog.open(ModalPermanentFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.eventualFormExpense: {
        res = this.dialog.open(ModalEventualFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.equipmentFormExpense: {
        res = this.dialog.open(ModalEquipmentFormAdvanceComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.transitAmortizationIncome: {
        res = this.dialog.open(ModalAmortizacionComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
      case CodeTransaction.transitAmortizationExpense: {
        res = this.dialog.open(ModalAmortizacionComponent, {
          minWidth: '400px',
          minHeight: '150px',
          data: t
        });
        break;
      }
    }
    return res;
  }

  showPart(ele) {
    const dialogRef = this.dialog.open(ModalPartsFormsDetailsComponent, {
      minWidth: '400px',
      minHeight: '150px',
      data: new DataTransaction(ele)
    });
    dialogRef.afterClosed().subscribe(result => {
      // if (result) {
      //   this.transactionSelect = result;
      //   if (!this.transactionSelect.idTransaction) {
      //     this.transactionService.newTransaction(this.transactionSelect.typeTransaction, this.transactionSelect)
      //       .subscribe(
      //         formData => {
      //           this.load();
      //           this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
      //         },
      //         error => {
      //           this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      //         });
      //   } else {
      //     this.transactionService.editTransaction(this.transactionSelect.idTransaction,
      //       this.transactionSelect.typeTransaction, result)
      //       .subscribe(
      //         formData => {
      //           this.load();
      //           this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      //         },
      //         error => {
      //           this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      //         });
      //   }
      // } else {
      //   this.resetSelect();
      // }
    });
  }

  isParted(ele: ViewTransactions) {
    return (ele.code_transaction < 14 || ele.code_transaction > 17) && ele.code_transaction != 2;
  }

  isDisabled() {
    return _permission().ac_b_transactions != 1
      || !this._period
      || !this._bank
      || this._bank.activeBank != 1
      || this._period.stateBankPeriod != 0;
  }
}

export class DataComplement {
  bank: Bank;
  period: Period;
  check: PayCheck;
  project: ViewProjects;
}

export class DataTransaction extends DataComplement {
  bank: Bank;
  project: ViewProjects;
  check: ViewPayCheck;
  period: Period;
  constructor(public transaction: Transaction | ViewTransactions) {
    super();
  }
}

