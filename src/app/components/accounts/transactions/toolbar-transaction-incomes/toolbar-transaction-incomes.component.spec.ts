import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ToolbarTransactionIncomesComponent} from './toolbar-transaction-incomes.component';

describe('ToolbarTransactionIncomesComponent', () => {
  let component: ToolbarTransactionIncomesComponent;
  let fixture: ComponentFixture<ToolbarTransactionIncomesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolbarTransactionIncomesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarTransactionIncomesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
