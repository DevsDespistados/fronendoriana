import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-toolbar-transaction-incomes',
  templateUrl: './toolbar-transaction-incomes.component.html',
  styleUrls: ['./toolbar-transaction-incomes.component.css']
})
export class ToolbarTransactionIncomesComponent implements OnInit {
  @Input() title: string;
  @Input() onProgress = false;
  @Input() searchTittle = 'Buscar Transacciones';
  @Input() onNew = true;
  @Input() typeTransaction: TypeTransaction;
  @Output() new = new EventEmitter();
  @Output() refresh = new EventEmitter();
  @Output() searching = new EventEmitter();
  tooltip = Tooltips;
  value;
  searchVisible = false;
  income = [
    new OptionOrianaTableToolbar('Directo', 1),
    new OptionOrianaTableToolbar('Cliente', 2),
    new OptionOrianaTableToolbar('Empresa', 3),
  ];
  transitIncome = [
    new OptionOrianaTableToolbar('ingreso', 13),
    new OptionOrianaTableToolbar('Amortizacion', 18),
  ];

  direct = new OptionOrianaTableToolbar('Directo', 4);
  menuProvider = new OptionOrianaTableToolbar('Proveedor', null);
  menuPermanent = new OptionOrianaTableToolbar('Permanente', null);
  menuEventual = new OptionOrianaTableToolbar('Eventual', null);
  menuEquipment = new OptionOrianaTableToolbar('Equipo Propio', null);
  company = new OptionOrianaTableToolbar('Otra Empresa', 9);
  transfer = new OptionOrianaTableToolbar('Transferencia', 10);
  menuTransit = new OptionOrianaTableToolbar('Transito', null);
  provider = [
    new OptionOrianaTableToolbar('Anticipo', 5),
    new OptionOrianaTableToolbar('Planilla', 14),
  ];
  permanent = [
    new OptionOrianaTableToolbar('Anticipo', 6),
    new OptionOrianaTableToolbar('Planilla', 15),
  ];
  eventual = [
    new OptionOrianaTableToolbar('Anticipo', 7),
    new OptionOrianaTableToolbar('Planilla', 16),
  ];
  equipment = [
    new OptionOrianaTableToolbar('Anticipo', 8),
    new OptionOrianaTableToolbar('Planilla', 17),
  ];
  transitExpense = [
    new OptionOrianaTableToolbar('egreso', 12),
    new OptionOrianaTableToolbar('Amortizacion', 19),
  ];


  constructor(private activeRouter: ActivatedRoute) {
  }

  ngOnInit() {
    this.activeRouter.url.subscribe(
      res => {
        console.log(res);
      }
    );
  }

  clickNew(op) {
    this.new.emit(op);
  }

  clickRefresh() {
    this.refresh.emit(true);
  }

  setSearchVisible() {
    if (this.searchVisible) {
      this.searchVisible = false;
    } else {
      this.searchVisible = true;
    }
  }

  resetSearch() {
    this.value = '';
    this.searchVisible = false;
    this.searching.emit(this.value);
  }

  search() {
    this.searching.emit(this.value);
  }

  isVisible() {
    if (this.value) {
      return this.value.length < 1 ? 'hidden' : 'visible';
    } else {
      return 'hidden';
    }
  }

  isTypeTransaction() {
    return this.typeTransaction == 'ingreso';
  }

}

export declare type TypeTransaction = 'ingreso' | 'egreso';
