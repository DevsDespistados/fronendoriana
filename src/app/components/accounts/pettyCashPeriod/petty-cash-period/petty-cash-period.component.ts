import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TotalForm} from '../../../../model/total-form';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {PeriodService} from '../../../../services/accounts/period.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ViewPettyCashPeriod} from '../../../../model/accounts/view-petty-cash-period';
import {ModalPettyCashPeriodComponent} from '../modal-petty-cash-period/modal-petty-cash-period.component';
import {_permission} from '../../../../services/permissions';
import {PettyCash} from '../../../../model/accounts/petty-cash';

@Component({
  selector: 'app-petty-cash-period',
  templateUrl: './petty-cash-period.component.html',
  styleUrls: ['./petty-cash-period.component.css']
})
export class PettyCashPeriodComponent implements OnInit {
  @Input() idPettyCash;
  @Output() pettyCash = new EventEmitter();
  periods: ViewPettyCashPeriod[];
  total: TotalForm;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'mount',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  sortedData: ViewPettyCashPeriod[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  period: ViewPettyCashPeriod;
  progress = false;
  routerBack = '/accounts/pettyCash';
  pettyCashSelec: PettyCash;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private periodService: PeriodService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalPettyCashPeriodComponent, {
      width: '400px',
      data: new DataPettyCashPeriod(this.period)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.period.idPettyCashPeriod) {
          this.progress = true;
          this.periodService.addPettyCashPeriod(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        } else {
          this.progress = true;
          this.periodService.editPettyCashPeriod(this.period.idPettyCashPeriod, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.period = <any>this.periods[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.period.nameAccountPettyCash}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.periodService.deletePeriod(this.period.idPettyCashPeriod)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.periods[pos];
    this.period = new ViewPettyCashPeriod(
      p.idPettyCash,
      p.nameAccountPettyCash,
      p.balancePettyCash,
      p.activePettyCash,
      p.idPettyCashPeriod,
      p.datePeriod,
      p.period,
      p.statePettyCashPeriod
    );
    this.create();
  }

  getFilter(type: any) {
    this.periodService.getPettyCashPeriods(this.idPettyCash, type).subscribe(
      res => {
        this.periods = res['periods'];
        this.pettyCashSelec = res['pettyCash'];
        this.pettyCash.emit(res['pettyCash']);
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        this.progress = false;
      });
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }


  printReport() {
    this.progress = true;
    const head = ['NRO', 'PERIODO', 'MONTO\nPERIODO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PERIODO\nCAJA CHICA: ' +
      this.pettyCashSelec.nameAccountPettyCash.toUpperCase(), head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.periods.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.period.toUpperCase());
      d.push(this.printService.moneyData(c.balancePettyCash));
      d.push(c.statePettyCashPeriod == 1 ? 'CERRADO' : 'ABIERTO');
      data.push(d);
    });
    data.push([
      '',
      '',
      this.printService.moneyData(this.totalPeriod()),
      ''
    ]);
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.periodService.searchPeriod(text).subscribe(
        response => {
          this.periods = response['periods'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.periodService.setPeriodPettyCash(this.periods[pos].idPettyCashPeriod, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.period = new ViewPettyCashPeriod(
      this.idPettyCash,
      null,
      null,
      null,
      null,
      null,
      null,
      true
    );
  }

  getRoutePettyCashOrBank(ele: ViewPettyCashPeriod) {
    // if (this.isbankPeriod == true) {
    return '/accounts/pettyCash/' + this.idPettyCash + '/periods/' + ele.idPettyCashPeriod + '/movements';
    // } else {
    //   return '/accounts/pettyCashOffice/' + this.idBank + '/periods/' + ele.idBankPeriod + '/movements';
    // }
  }

  getTooltipText() {
    // if (this.isbankPeriod == true) {
    return 'Transacciones';
    // } else {
    //   return 'Transacciones';
    // }
  }

  totalPeriod() {
    let total = 0;
    if (this.periods) {
      this.periods.forEach(period => {
        total += period.balancePettyCash;
      });
    }
    return total;
  }

  isDisabled() {
    return _permission().ac_pt_periods != 1;
  }
}

export class DataPettyCashPeriod {
  constructor(public period: ViewPettyCashPeriod) {
  }
}
