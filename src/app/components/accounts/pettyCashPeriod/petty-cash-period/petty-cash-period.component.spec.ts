import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PettyCashPeriodComponent} from './petty-cash-period.component';

describe('PettyCashPeriodComponent', () => {
  let component: PettyCashPeriodComponent;
  let fixture: ComponentFixture<PettyCashPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PettyCashPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PettyCashPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
