import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PettyCash} from '../../../../model/accounts/petty-cash';

@Component({
  selector: 'app-oriana-home-petty-cash-period',
  templateUrl: './oriana-home-petty-cash-period.component.html',
  styleUrls: ['./oriana-home-petty-cash-period.component.css']
})
export class OrianaHomePettyCashPeriodComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'Periodo Caja Chica',
  };
  idPettyCash;
  pettyCash: PettyCash;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idPettyCash = res['idB'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  getNamePeriod() {
    if (this.pettyCash) {
      return 'Caja Chica: ' + this.pettyCash.nameAccountPettyCash;
    } else {
      return 'Caja Chica: ';
    }
  }

  setBank(value: any) {
    this.pettyCash = value;
  }
}
