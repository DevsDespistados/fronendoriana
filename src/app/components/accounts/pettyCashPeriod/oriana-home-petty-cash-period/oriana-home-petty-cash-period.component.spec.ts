import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePettyCashPeriodComponent} from './oriana-home-petty-cash-period.component';

describe('OrianaHomePettyCashPeriodComponent', () => {
  let component: OrianaHomePettyCashPeriodComponent;
  let fixture: ComponentFixture<OrianaHomePettyCashPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePettyCashPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePettyCashPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
