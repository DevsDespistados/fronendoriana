import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPettyCashPeriodComponent} from './modal-petty-cash-period.component';

describe('ModalPettyCashPeriodComponent', () => {
  let component: ModalPettyCashPeriodComponent;
  let fixture: ComponentFixture<ModalPettyCashPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPettyCashPeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPettyCashPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
