import {Component, Inject} from '@angular/core';
import {ConfText, month} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {moment} from '../../../../services/GlobalFunctions';
import {PettyCashPeriod} from '../../../../model/accounts/petty-cash-period';

@Component({
  selector: 'app-modal-petty-cash-period',
  templateUrl: './modal-petty-cash-period.component.html',
  styleUrls: ['./modal-petty-cash-period.component.css']
})
export class ModalPettyCashPeriodComponent {
  text = ConfText;
  formData: PettyCashPeriod;
  period: PettyCashPeriod;
  private date = new Date();
  year = this.date.getFullYear();
  month = this.date.getMonth();
  months = month;

  constructor(
    public dialogRef: MatDialogRef<ModalPettyCashPeriodComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.period = this.data.period;
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.formData = this.period;
      this.formData.datePeriod = moment(new Date(this.year, this.month, 1));
      this.dialogRef.close();
    }
  }

  validData() {
    return this.year == null || this.year <= 0 || this.month == null || this.month < 0;
  }
}
