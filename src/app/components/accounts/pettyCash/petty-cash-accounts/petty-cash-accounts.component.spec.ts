import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PettyCashAccountsComponent} from './petty-cash-accounts.component';

describe('PettyCashAccountsComponent', () => {
  let component: PettyCashAccountsComponent;
  let fixture: ComponentFixture<PettyCashAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PettyCashAccountsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PettyCashAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
