import {Component, OnInit} from '@angular/core';
import {OwnerEquipment} from '../../../../model/Equipment/owner-equipment';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {AccountService} from '../../../../services/accounts/account.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {HttpErrorResponse} from '@angular/common/http';
import {PettyCash} from '../../../../model/accounts/petty-cash';
import {ModalPettyCashAccountsComponent} from '../modal-petty-cash-accounts/modal-petty-cash-accounts.component';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-petty-cash-accounts',
  templateUrl: './petty-cash-accounts.component.html',
  styleUrls: ['./petty-cash-accounts.component.css']
})
export class PettyCashAccountsComponent implements OnInit {
  listPettyCash: PettyCash[];
  total = 0;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'account',
    'balance',
    'options'
  ];
  sortedData: OwnerEquipment[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  pettyCashSelect: PettyCash = new PettyCash(null, null, null, true, null);
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private accountService: AccountService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalPettyCashAccountsComponent, {
      width: '400px',
      data: new DataPettyCash(this.pettyCashSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.pettyCashSelect.idPettyCash) {
          this.accountService.addAccountPettyCash(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.accountService.editAccountPettyCash(this.pettyCashSelect.idPettyCash, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.pettyCashSelect = <any>this.listPettyCash[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.pettyCashSelect.nameAccountPettyCash}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.accountService.deletePettyCash(this.pettyCashSelect.idPettyCash)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.listPettyCash[pos];
    this.pettyCashSelect = new PettyCash(
      p.idPettyCash,
      p.nameAccountPettyCash,
      p.balancePettyCash,
      p.activePettyCash,
      p.idUser
    );
    this.create();
  }

  getFilter(type: any) {
    this.accountService.getPettyCash(type).subscribe(
      res => {
        console.log(res);
        this.listPettyCash = res['pettyCash'];
        this.total = res['total'].total;
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  setState(pos, state): any {
    this.accountService.setStatePettyCash(this.listPettyCash[pos].idPettyCash, {activePettyCash: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    // this.progress = true;
    // this.ac.report({projects: this.projects}).subscribe(
    //   res => {
    //     this.printService.setDataPrint(res['data']);
    //     this.progress = false;
    //   }, error1 => {
    //     this.progress = false;
    //     this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
    //   }
    // );
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.accountService.searchPettyCash(text).subscribe(
        res => {
          this.listPettyCash = res['pettyCashs'];
          this.total = res['total'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.pettyCashSelect = new PettyCash(
      null,
      null,
      null,
      true,
      null
    );
  }

  disabled(ele: PettyCash) {
    return this.isDisabled() || ele.balancePettyCash != 0 || ele.activePettyCash == 0;
  }

  isDisabled() {
    return _permission().ac_pt_pettyCash != 1;
  }

  isDisabledP() {
    return _permission().ac_pt_periods == 0;
  }

}

export class DataPettyCash {
  constructor(public pettyCash: PettyCash) {
  }
}
