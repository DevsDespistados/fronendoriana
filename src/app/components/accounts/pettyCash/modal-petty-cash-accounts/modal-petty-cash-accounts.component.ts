import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PettyCash} from '../../../../model/accounts/petty-cash';
import {UserService} from '../../../../services/user/user.service';
import {User} from '../../../../model/user/user';

@Component({
  selector: 'app-modal-petty-cash-accounts',
  templateUrl: './modal-petty-cash-accounts.component.html',
  styleUrls: ['./modal-petty-cash-accounts.component.css']
})
export class ModalPettyCashAccountsComponent implements OnInit {
  text = ConfText;
  formData;
  pettyCash: PettyCash;
  users: User[] = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService) {
    this.pettyCash = this.data.pettyCash;
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(res => {
      this.users = <User[]>res;
    }, error1 => {
      console.log(error1);
    });
  }

  onNoClick(): void {
    this.formData = null;
    this.dialogRef.close();
  }

  datart() {
    if (!this.valid()) {
      this.formData = this.pettyCash;
      this.dialogRef.close();
    }
  }

  valid() {
    return this.pettyCash.idUser == null || this.pettyCash.idPettyCash == 0;
  }
}
