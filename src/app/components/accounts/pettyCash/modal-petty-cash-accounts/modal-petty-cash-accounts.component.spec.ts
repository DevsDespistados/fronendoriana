import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPettyCashAccountsComponent} from './modal-petty-cash-accounts.component';

describe('ModalPettyCashAccountsComponent', () => {
  let component: ModalPettyCashAccountsComponent;
  let fixture: ComponentFixture<ModalPettyCashAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPettyCashAccountsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPettyCashAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
