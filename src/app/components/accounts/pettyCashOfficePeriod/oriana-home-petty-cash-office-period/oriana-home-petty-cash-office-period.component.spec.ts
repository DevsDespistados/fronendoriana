import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePettyCashOfficePeriodComponent} from './oriana-home-petty-cash-office-period.component';

describe('OrianaHomePettyCashOfficePeriodComponent', () => {
  let component: OrianaHomePettyCashOfficePeriodComponent;
  let fixture: ComponentFixture<OrianaHomePettyCashOfficePeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePettyCashOfficePeriodComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePettyCashOfficePeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
