import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Bank} from '../../../../model/accounts/bank';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-oriana-home-petty-cash-office-period',
  templateUrl: './oriana-home-petty-cash-office-period.component.html',
  styleUrls: ['./oriana-home-petty-cash-office-period.component.css']
})
export class OrianaHomePettyCashOfficePeriodComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'Periodo Caja Chica Oficina',
  };
  idBank;
  bank: Bank;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idBank = res['idB'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  getNamePeriod() {
    if (this.bank) {
      return 'Caja Chica Oficina: ' + this.bank.nameAccountBank;
    } else {
      return 'Caja Chica Oficina: ';
    }
  }

  setBank(value: any) {
    this.bank = value;
  }
}
