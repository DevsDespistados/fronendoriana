import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PeriodService} from '../../../services/accounts/period.service';
import {Period} from '../../../model/accounts/period';
import {Bank} from '../../../model/accounts/bank';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-check-transacction',
  templateUrl: './oriana-home-check-transacction.component.html',
  styleUrls: ['./oriana-home-check-transacction.component.css']
})
export class OrianaHomeCheckTransacctionComponent implements OnInit {
  titles = {
    module: 'Cheques y Transacciones',
    subtitle: 'Banco / Periodo: ',
  };
  idBank;
  idPeriod;
  navLinks;
  period: Period;
  bank: Bank;
  constructor(private routerActive: ActivatedRoute,
              private router: Router,
              private periodService: PeriodService) {
  }

  ngOnInit() {
    this.routerActive.params.subscribe(
      params => {
        this.idBank = params['idB'];
        this.idPeriod = params['idP'];
        this.navLinks = [
          {label: 'cheques', path: '/accounts/banks/' + params['idB'] + '/periods/' + params['idP'] + '/movements/checks'},
          {label: 'Transacciones', path: '/accounts/banks/' + params['idB'] + '/periods/' + params['idP'] + '/movements/transactions/all'},
        ];
        this.periodService.getPeriod(this.idPeriod).subscribe(res => {
          this.period = res['period'];
          this.bank = res['bank'];
        }, error1 => {
          console.log(error1);
        });
        this.redirect();
      }
    );

  }

  getSubtitle() {
    if (this.period && this.bank) {
      return 'Banco: ' + this.bank.nameAccountBank + ' / Periodo: ' + this.period.bankPeriod;
    } else {
      return 'Banco: / Periodo: ';
    }
  }

  redirect() {
    if (_permission().ac_b_transactions > 0) {
      this.router.navigate([this.navLinks[0].path]);
    }
  }

}
