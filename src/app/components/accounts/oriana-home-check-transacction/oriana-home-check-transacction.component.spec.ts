import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeCheckTransacctionComponent} from './oriana-home-check-transacction.component';

describe('OrianaHomeCheckTransacctionComponent', () => {
  let component: OrianaHomeCheckTransacctionComponent;
  let fixture: ComponentFixture<OrianaHomeCheckTransacctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeCheckTransacctionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeCheckTransacctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
