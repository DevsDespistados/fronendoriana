import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PayCheck} from '../../../../model/accounts/pay-check';
import {convertToNormatDate, endDate, moment} from '../../../../services/GlobalFunctions';
import {Period} from '../../../../model/accounts/period';

@Component({
  selector: 'app-modal-check',
  templateUrl: './modal-check.component.html',
  styleUrls: ['./modal-check.component.css']
})
export class ModalCheckComponent {
  text = ConfText;
  formData;
  check: PayCheck;
  period: Period;
  typeCheck = ['egreso', 'ingreso'];
  maxDate;
  minDate;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.check = this.data.check;
    this.period = this.data.period;
    this.minDate = convertToNormatDate(this.period.bankPeriod);
    this.maxDate = endDate(convertToNormatDate(this.period.bankPeriod));
  }

  onNoClick(): void {
    this.formData = null;
    this.dialogRef.close();
  }

  datart(file) {
    if (!this.valid()) {
      this.formData = new FormData();
      this.formData.append('numberCheck', this.check.numberCheck);
      this.formData.append('mountCheck', this.check.mountCheck);
      this.formData.append('dateCheck', moment(this.check.dateCheck));
      this.formData.append('type', this.check.type);
      this.formData.append('idBankPeriod_period', this.check.idBankPeriod_period);
      if (file.length > 0) {
        this.formData.append('fileCheck', file[0], file[0].name);
      } else {
        this.formData.append('fileCheck', null);
      }
      this.dialogRef.close();
    }
  }

  valid() {
    return this.check && this.check.numberCheck === null || this.check.numberCheck === ''
      || this.check.dateCheck === null || this.check.dateCheck === ''
      || this.check.mountCheck === null || this.check.mountCheck === 0
      || this.check.type == '';
  }
}
