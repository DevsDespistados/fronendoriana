import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {PayCheck} from '../../../../model/accounts/pay-check';
import {TotalRes} from '../../../projects/forms-project/forms-project.component';
import {PayCheckService} from '../../../../services/accounts/pay-check.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalCheckComponent} from '../modal-check/modal-check.component';
import {Period} from '../../../../model/accounts/period';
import {_permission} from '../../../../services/permissions';
import {ViewPayCheck} from '../../../../model/accounts/view-pay-check';
import {Bank} from '../../../../model/accounts/bank';
import {ReportService} from '../../../../services/report.service';
import {connectableObservableDescriptor} from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-bank-check',
  templateUrl: './bank-check.component.html',
  styleUrls: ['./bank-check.component.css']
})
export class BankCheckComponent implements OnInit {
  payChecks: PayCheck[];
  total: TotalRes = new TotalRes(0, 0, 0, 0, 0, 0);
  titulo = 'pendientes';
  displayedColumns = [
    'position',
    'number',
    'type',
    'date',
    'mount',
    'mountRender',
    'balance',
    'options'
  ];
  sortedData: PayCheck[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('RENDIDOS', 1),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  payCheckSelect: PayCheck;
  tooltip = Tooltips;
  currentOption = 0;
  progress = false;
  idPeriod;
  period: Period;
  idBank: number;
  bankSelect: Bank;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private payCheckService: PayCheckService,
              private routerActive: ActivatedRoute,
              private reportService: ReportService,
              private router: Router) {
    this.routerActive.parent.params.subscribe(
      params => {
        this.idPeriod = params['idP'];
        this.idBank = params['idB'];
      }
    );
  }

  routerBack: string;

  ngOnInit() {
    this.load();
    this.setRouterBack();
  }

  create() {
    const dialogRef = this.dialog.open(ModalCheckComponent, {
      width: '400px',
      data: new DataPayCheck(this.payCheckSelect, this.period)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.payCheckSelect.idPayCheck) {
          this.payCheckService.addPayCheck(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.payCheckService.editCheck(this.payCheckSelect.idPayCheck, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.payCheckSelect = <any>this.payChecks[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.payCheckSelect.numberCheck}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.payCheckService.delete(this.payCheckSelect.idPayCheck)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.payChecks[pos];
    this.payCheckSelect = new PayCheck(
      p.idPayCheck,
      p.numberCheck,
      p.dateCheck,
      p.mountCheck,
      p.type,
      p.fileCheck,
      p.idBankPeriod_period,
      p.mountPay
    );
    this.create();
  }

  getFilter(type: any) {
    this.payCheckService.getPayCheck(type, this.idPeriod).subscribe(
      res => {
        this.payChecks = res['checks'];
        this.total = res['total'];
        this.period = res['period'];
        this.bankSelect = res['bank'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  setState(pos, state): any {
    // this.accountService.setStateBank(this.listBanks[pos].idBank, {activeBank: state})
    //   .subscribe(
    //     res => {
    //       this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
    //       if (this.currentOption !== 3) {
    //         this.load();
    //       }
    //     },
    //     (errorResponse: HttpErrorResponse) => {
    //       if (errorResponse.status === 406) {
    //         this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
    //       } else {
    //         this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
    //       }
    //     }
    //   );
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    // this.progress = true;
    // const head = ['NRO', 'NRO CHEQUE', 'FECHA', 'MONTO', 'MONTO\nRENDIDO', 'SALDO'];
    // const body = this.getData();
    // const report = this.printService.createSingleTable('REPORTE CHEQUES\nPERIODO: ' +
    //   this.period.bankPeriod.toUpperCase(), head, body, 'wrap',
    //   {
    //     0: {
    //       halign: 'right',
    //     },
    //     2: {
    //       halign: 'right',
    //     }
    //   });
    // this.printService.setDataPrint(report);
    const params = {
      G_PERIOD_ID: this.idPeriod,
      G_BANK_ID: this.idBank,
      G_QUERY: 'AND (balance!=0 OR balance=0)',
      G_ESTADO: 'TODOS',
    };
    if (this.currentOption !== 2) {
      params.G_QUERY =  this.currentOption == 0 ? 'AND balance!=0' : 'AND balance=0';
      params.G_ESTADO = this.currentOption == 0 ? 'PENDIENTES' : 'RENDIDOS';
    }
    this.reportService.printReport('CHECK', params);
  }

  getData() {
    const data = [];
    this.payChecks.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.numberCheck);
      d.push(this.printService.dateData(c.dateCheck));
      d.push(this.printService.moneyData(c.mountCheck));
      d.push(this.printService.moneyData(c.mountPay));
      d.push(this.printService.moneyData(c.mountCheck - c.mountPay));
      data.push(d);
    });
    data.push([
      '',
      '',
      '',
      this.printService.moneyData(this.total.financial),
      this.printService.moneyData(this.total.advanceReturn),
      this.printService.moneyData(this.total.balance),
    ]);
    return data;
  }

  search(text: string) {
    // if (text !== '') {
    //   this.progress = true;
    //   this.accountService.searchBanks(text).subscribe(
    //     res => {
    //       this.listBanks = res['banks'];
    //       this.total = res['total'];
    //       this.titulo = ConfText.resultSearch;
    //       this.progress = false;
    //     }
    //   );
    // } else {
    //   this.load();
    // }
  }

  setRouterBack() {
    this.routerActive.parent.params.subscribe(res => {
      this.routerBack = '/accounts/banks/' + res['idB'] + '/periods';
    });
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    console.log(this.currentOption);
    this.load();
  }

  resetSelect() {
    this.payCheckSelect = new PayCheck(
      null,
      null,
      new Date(),
      null,
      'egreso',
      null,
      this.idPeriod,
      0
    );
  }

  isDisabled() {
    return _permission().ac_check != 1
      || !this.period
      || this.period.stateBankPeriod == 1
      || this.bankSelect.activeBank == 0;
  }

  isDisabledC(ele: ViewPayCheck) {
    return this.isDisabled() || ele.mountPay > 0;
  }

  isDisabledT() {
    return _permission().ac_b_transactions == 0;
  }
}

export class DataPayCheck {
  constructor(public check: PayCheck, public period: Period) {
  }
}
