import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTransferProjectComponent} from './modal-transfer-project.component';

describe('ModalTransferProjectComponent', () => {
  let component: ModalTransferProjectComponent;
  let fixture: ComponentFixture<ModalTransferProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTransferProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTransferProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
