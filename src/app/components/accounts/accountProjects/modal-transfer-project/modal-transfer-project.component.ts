import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {TransferProjectService} from '../../../../services/accounts/transfer-project.service';
import {ViewAccountProject} from '../../../../model/accounts/view-account-project';
import {ReceiptAccountProject} from '../../../../model/accounts/receipt-account-project';

@Component({
  selector: 'app-modal-transfer-project',
  templateUrl: './modal-transfer-project.component.html',
  styleUrls: ['./modal-transfer-project.component.css']
})
export class ModalTransferProjectComponent implements OnInit {

  text = ConfText;
  projects: ViewAccountProject[] = [];
  formData;
  transfer = new ReceiptAccountProject(null, null, null, null, null, null, null);

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private transferService: TransferProjectService,
    private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.loadProjects();
  }

  loadProjects() {
    this.transferService.getProjects(1).subscribe(
      res => {
        this.projects = <ViewAccountProject[]>res['projectsAccount'];
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  submit() {
    this.dialogRef.close(this.transfer);
  }

  isDisabledDestine(ele: ViewAccountProject) {
    return this.transfer.idProjectOrigin == ele.idProject;
  }

  isDisabledOrigin(ele: ViewAccountProject) {
    return this.transfer.idProjectDestine == ele.idProject;
  }

  change() {
    this.transfer.idProjectDestine = null;
    this.transfer.idProjectOrigin = null;
  }

  onNoClick() {
    this.transfer = new ReceiptAccountProject(null, null, null, null, null, null, null);
    this.dialogRef.close();
  }

  valid() {
    return this.transfer.idProjectOrigin == null
      || this.transfer.idProjectDestine == null
      || this.transfer.descriptionReceiptProject == ''
      || this.transfer.descriptionReceiptProject == null
      || this.transfer.amountReceiptProject <= 0
      || this.transfer.dateReceiptProject == null
      || this.transfer.numberReceiptProject == null
      || this.transfer.numberReceiptProject == '';
  }

}
