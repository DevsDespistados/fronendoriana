import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TransferProjectComponent} from './transfer-project.component';

describe('TransferProjectComponent', () => {
  let component: TransferProjectComponent;
  let fixture: ComponentFixture<TransferProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransferProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
