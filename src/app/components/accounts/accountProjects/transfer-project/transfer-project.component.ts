import {Component, OnInit} from '@angular/core';
import {OwnerEquipment} from '../../../../model/Equipment/owner-equipment';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {TransferProjectService} from '../../../../services/accounts/transfer-project.service';
import {ReceiptAccountProject} from '../../../../model/accounts/receipt-account-project';
import {ViewAccountProject} from '../../../../model/accounts/view-account-project';
import {Project} from '../../../../model/project/project';
import {ModalTransferProjectComponent} from '../modal-transfer-project/modal-transfer-project.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-transfer-project',
  templateUrl: './transfer-project.component.html',
  styleUrls: ['./transfer-project.component.css']
})
export class TransferProjectComponent implements OnInit {
  viewAccountProject: ViewAccountProject[];
  transfer = new ReceiptAccountProject(null, null, null, null, null, null, null);
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'name',
    'income',
    'expense',
    'options'
  ];
  sortedData: OwnerEquipment[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODOS', 2)
  ];
  tooltip = Tooltips;
  projectOffice: Project = new Project(null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null);
  currentOption = 1;
  total;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private transferService: TransferProjectService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalTransferProjectComponent, {
      width: '600px',
      data: new DataTransferProject(this.transfer)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.transfer.idTransferProject) {
          this.transferService.addTransfer(result)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          // this.transferService.(this.projectSelect.idProject, dialogRef.componentInstance.formData, this.projectSelect.idClient)
          //   .subscribe(
          //     formData => {
          //       this.load();
          //       this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          //     },
          //     error => {
          //       this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          //     });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    // this.projectSelect = <any>this.projects[pos];
    // const dialogRef = this.dialog.open(ModalDeleteComponent, {
    //   width: '400px',
    //   data: {deleted: this.projectSelect.nameProject}
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.progress = true;
    //     this.projectService.deleteProject(this.projectSelect.idProject)
    //       .subscribe(
    //         formData => {
    //           this.load();
    //           this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
    //         },
    //         error => {
    //           this.progress = false;
    //           this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
    //         });
    //   } else {
    //     this.resetSelect();
    //   }
    // });
  }

  edit(pos: number) {
    this.create();
  }

  getFilter(type: any) {
    this.transferService.getProjects(type).subscribe(
      res => {
        this.viewAccountProject = res['projectsAccount'];
        this.total = res['total'];
        this.projectOffice = res['office'];
        this.progress = false;
      }, error1 => {
        this.progress = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  search(text: string) {
    if (text !== '') {
      this.transferService.searchTransfer(text).subscribe(
        response => {
          this.viewAccountProject = <any[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.transfer = new ReceiptAccountProject(null, this.projectOffice.idProject, null, null, null, null, null);
  }

  setStateProject(check1, project) {
    this.transferService.setState({stateAccountProject: check1}, project.idProject).subscribe(
      res => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      }, error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        this.load();
      }
    );
  }

  isDisabled() {
    return _permission().ac_pj_projects != 1;
  }
}

export class DataTransferProject {
  constructor(public transfer: ReceiptAccountProject) {
  }
}
