import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ViewItemOrder} from '../../../model/order/view-item-order';

@Component({
  selector: 'app-modal-order-item',
  templateUrl: './modal-order-item.component.html',
  styleUrls: ['./modal-order-item.component.css']
})
export class ModalOrderItemComponent {
  text = ConfText;
  item: ViewItemOrder;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (this.data.item) {
      this.item = this.data.item;
    } else {
      this.item = new ViewItemOrder(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null);
    }
  }

  onNoClick(): void {
    this.item = null;
    this.dialogRef.close();
  }

  sendData() {
    const t = {
      materialItem: this.item.materialItem,
      unityItem: this.item.unityItem,
      priceUnityItem: this.item.priceUnityItem
    };
    this.dialogRef.close(t);
  }

  valid() {
    return this.item.materialItem == null
      || this.item.unityItem == null;
  }
}
