import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeOrderQuotationItemComponent} from './home-order-quotation-item.component';

describe('HomeOrderQuotationItemComponent', () => {
  let component: HomeOrderQuotationItemComponent;
  let fixture: ComponentFixture<HomeOrderQuotationItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeOrderQuotationItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeOrderQuotationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
