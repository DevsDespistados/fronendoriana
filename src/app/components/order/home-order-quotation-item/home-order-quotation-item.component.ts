import {Component, OnInit} from '@angular/core';
import {ViewOrder} from '../../../model/order/view-order';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-home-order-quatation-item',
  templateUrl: './home-order-quotation-item.component.html',
  styleUrls: ['./home-order-quotation-item.component.css']
})
export class HomeOrderQuotationItemComponent implements OnInit {
  titles = {
    module: 'COTIZAR MATERIAL',
  };
  orderSelect: ViewOrder;

  constructor(private snack: MatSnackBar) {
  }

  ngOnInit() {
    // this.snack.open('Debe eliminar Todas las Compras para Rechazar una Solicitud', 'Cerrar', {
    //   duration: 2000,
    // });
  }

  setOrder(select) {
    this.orderSelect = select;
  }

  getSubtitle() {
    if (this.orderSelect) {
      return 'SOLICITUD: ' + this.orderSelect.numberPeriodOrder;
    } else {
      return 'SOLICITUD: ';
    }
  }
}
