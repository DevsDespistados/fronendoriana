import {Component, OnInit} from '@angular/core';
import {ViewOrder} from '../../../model/order/view-order';

@Component({
  selector: 'app-home-order-petition-items',
  templateUrl: './home-order-petition-items.component.html',
  styleUrls: ['./home-order-petition-items.component.css']
})
export class HomeOrderPetitionItemsComponent implements OnInit {
  titles = {
    module: 'SOLICITUD MATERIAL',
  };
  orderSelect: ViewOrder;

  constructor() {
  }

  ngOnInit() {
  }

  setLetter(select) {
    this.orderSelect = select;
  }

  getSubtitle() {
    if (this.orderSelect) {
      return 'SOLICITUD: ' + this.orderSelect.numberPeriodOrder;
    } else {
      return 'SOLICITUD: ';
    }
  }
}
