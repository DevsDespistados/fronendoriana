import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeOrderPetitionItemsComponent} from './home-order-petition-items.component';

describe('HomeOrderPetitionItemsComponent', () => {
  let component: HomeOrderPetitionItemsComponent;
  let fixture: ComponentFixture<HomeOrderPetitionItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeOrderPetitionItemsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeOrderPetitionItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
