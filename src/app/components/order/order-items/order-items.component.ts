import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {ActivatedRoute} from '@angular/router';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ViewItemOrder} from '../../../model/order/view-item-order';
import {ItemService} from '../../../services/order/item.service';
import {ModalOrderItemComponent} from '../modal-order-item/modal-order-item.component';
import {compareDate, compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {PrintServiceService} from '../../../services/print-service.service';

@Component({
  selector: 'app-order-items',
  templateUrl: './order-items.component.html',
  styleUrls: ['./order-items.component.css']
})
export class OrderItemsComponent implements OnInit {
  current: number;
  idContract: number;
  items: ViewItemOrder[];
  itemSelect: ViewItemOrder;
  displayedColumns = [
    'position',
    'item',
    'unity',
    'price',
    'update',
    'option'];
  sortedData: ViewItemOrder[];
  routerBack = '/contracts';

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private itemService: ItemService,
              private printService: PrintServiceService,
              private activeRouter: ActivatedRoute) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalOrderItemComponent, {
      width: '300px',
      data: new DataOrderItem(this.itemSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.itemSelect.idItem) {
          this.itemService.addItem(result)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.itemService.updateItem(this.itemSelect.idItem, result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.itemSelect = this.items[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.itemSelect.materialItem}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.itemService.deleteItem(this.itemSelect.idItem)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const item = this.items[pos];
    this.itemSelect = new ViewItemOrder(
      item.idOrderItem,
      item.idItem,
      item.materialItem,
      item.unityItem,
      item.priceUnityItem,
      item.submitOrderItem,
      item.amountOrderItem,
      item.totalOrderItem,
      item.balanceOrderItem,
      item.idOrder,
      item.updated_at
    );
    this.create();
  }

  getFilter(type: any) {
    // if (type == 1) {
    this.itemService.allItem().subscribe(
      res => {
        this.items = <ViewItemOrder[]>res;
      }
    );
    // }
  }

  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.getFilter(this.current);
  }

  printReport() {
    const head = ['NRO', 'MATERIAL', 'UNIDAD', 'P/U', 'FECHA'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE MATERIALES', head, body, 'wrap',
      {});
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.items.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.materialItem.toUpperCase());
      d.push(c.unityItem.toUpperCase());
      d.push(this.printService.moneyData(c.priceUnityItem));
      d.push(this.printService.dateData(c.updated_at).toUpperCase());
      data.push(d);
    });
    return data;
  }

  resetSelect() {
    this.itemSelect = new ViewItemOrder(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  search(text: string) {
    if (text !== '') {
      this.itemService.searchItem(text).subscribe(
        response => {
          this.items = <ViewItemOrder[]>response;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.items.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.items = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'materialItem':
          return compareString(a.materialItem, b.materialItem, isAsc);
        case 'unityItem':
          return compareString(a.unityItem, b.unityItem, isAsc);
        case 'priceUnityItem':
          return compareNumber(a.priceUnityItem, b.priceUnityItem, isAsc);
        case 'updated':
          return compareDate(a.updated_at, b.updated_at, isAsc);
        default:
          return 0;
      }
    });
  }

  isDisabled() {
    return _permission().o_order_item != 1;
  }


}

export class DataOrderItem {
  constructor(public item: ViewItemOrder) {
  }
}
