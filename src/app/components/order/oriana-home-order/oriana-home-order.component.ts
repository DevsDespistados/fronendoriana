import {Component, OnInit} from '@angular/core';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-order',
  templateUrl: './oriana-home-order.component.html',
  styleUrls: ['./oriana-home-order.component.css']
})
export class OrianaHomeOrderComponent implements OnInit {

  titles = {
    module: 'PEDIDOS',
  };

  links = [
    {label: 'SOLICITUD Y ENTREGA', path: '/order/petition', permission: _permission().o_order_p},
    {label: 'COTIZACION Y COMPRA', path: '/order/quotation', permission: _permission().o_order_q},
    {label: 'MATERIAL', path: '/order/items', permission: _permission().o_order_item}
  ];

  navLinks = [];

  constructor() {
  }

  ngOnInit() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }
}

