import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeOrderComponent} from './oriana-home-order.component';

describe('OrianaHomeOrderComponent', () => {
  let component: OrianaHomeOrderComponent;
  let fixture: ComponentFixture<OrianaHomeOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeOrderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
