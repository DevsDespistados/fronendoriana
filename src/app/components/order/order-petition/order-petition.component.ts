import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ViewOrder} from '../../../model/order/view-order';
import {OrderService} from '../../../services/order/order.service';
import {ModalOrderPetitionComponent} from '../modal-order-petition/modal-order-petition.component';
import {HttpErrorResponse} from '@angular/common/http';
import {compareDate, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-order-petition',
  templateUrl: './order-petition.component.html',
  styleUrls: ['./order-petition.component.css']
})
export class OrderPetitionComponent implements OnInit {
  orders: ViewOrder[] = [];
  titulo = 'APROBADOS';
  displayedColumns = [
    'position',
    'date',
    'nOrder',
    'project',
    'order',
    'options'
  ];
  textTooltip = ['Rechazado', 'Aprobado', 'Pediente'];
  sortedData: ViewOrder[];
  options = [
    new OptionOrianaTableToolbar('SOLICITUDES PENDIENTES', 1),
    new OptionOrianaTableToolbar('SOLICITUDES TERMINADAS', 2),
    new OptionOrianaTableToolbar('SOLICITUD RECHAZADOS', 3),
    new OptionOrianaTableToolbar('COTIZACION APROBADA', 4),
    new OptionOrianaTableToolbar('COTIZACION RECHAZADA', 5),
    new OptionOrianaTableToolbar('COMPRANDO', 8),
    new OptionOrianaTableToolbar('COMPRA RECHAZADA', 6),
    new OptionOrianaTableToolbar('COMPRA CONCLUIDA', 7),
    new OptionOrianaTableToolbar('TODOS', 9)
  ];
  orderSelect: ViewOrder;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalOrderPetitionComponent, {
      width: '300px',
      data: new DataOrder(this.orderSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.orderSelect.idOrder) {
          this.orderService.addOrder(result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, error => {
              this.load();
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            }
          );
        } else {
          this.orderService.updateOrder(this.orderSelect.idOrder, result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            }, error => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.orderSelect = <any>this.orders[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.orderSelect.numberPeriodOrder}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.orderService.deleteOrder(this.orderSelect.idOrder)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});

            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const f = this.orders[pos];
    this.orderSelect = new ViewOrder(
      f.idOrder,
      f.numberPeriodOrder,
      f.dateOrder,
      f.idProject,
      f.nameProject,
      f.stateOrderPetition,
      f.stateText,
      f.amountTracing
    );
    this.create();
  }

  getFilter(type: any) {
    this.progress = true;
    this.orderService.filterOrder(type, 'order').subscribe(
      response => {
        this.orders = <ViewOrder[]>response;
        this.progress = false;
      }
    );
  }


  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.orderService.optionPetition);
    this.options.forEach(op => {
      if (op.url == this.orderService.optionPetition) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'FECHA', 'NRO PEDIDO', 'PROYECTO', 'ESTADO EN \n COTIZACION'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PEDIDOS SOLICITUD', head, body, 'wrap',
      {});
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.orders.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(this.printService.dateData(c.dateOrder).toUpperCase());
      d.push(c.numberPeriodOrder.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      // d.push(c.stateOrderQuotation == 1 ? 'APROBADO' : c.stateOrderQuotation == 2 ? 'PENDIENTE' : 'RECHAZADO');
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.orderService.searchOrder(text).subscribe(
        response => {
          this.orders = <ViewOrder[]>response;
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.orders.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.orders = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'dateOrder':
          return compareDate(a.dateOrder, b.dateOrder, isAsc);
        case 'numberPeriodOrder':
          return compareString(a.numberPeriodOrder, b.numberPeriodOrder, isAsc);
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.orderService.optionPetition = type;
    this.load();
  }

  resetSelect() {
    this.orderSelect = new ViewOrder(
      null,
      null,
      null,
      null,
      null,
      1,
      'Solicitud en Curso',
      0
    );
  }

  select(order: ViewOrder) {
    this.orderSelect = order;
  }

  update(state, text) {
    this.orderSelect.stateText = text;
    this.orderSelect.stateOrderPetition = state;
    this.orderService.updateStates(this.orderSelect.idOrder, this.orderSelect)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.orderService.optionPetition !== 3) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            // this.orders[pos] = (<ViewOrder>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  getStateQuotaion(ele: ViewOrder) {
    // return ele.stateOrderQuotation == 1 ? 'APROBADO' : ele.stateOrderQuotation == 2 ? 'PENDIENTE' : 'RECHAZADO';
  }



  updateTracing(evt, pos) {
    const p = this.orders[pos];
    this.orderService.updateState(p.idOrder, evt);
    this.orderService.updateStates(p.idOrder, {stateOrderTracing: evt}).subscribe(
      res => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        if (this.orderService.optionPetition !== 3) {
          this.load();
        }
      },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse.status === 406) {
          this.orders[pos] = (<ViewOrder>errorResponse.error);
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        } else {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        }
      }
    );
  }

  orderDisabled(ele: ViewOrder) {
    // return this.isDisabled() || ele.stateOrderBuy == 1 || ele.stateOrderQuotation == 1;
  }

  isDisabled() {
    return _permission().o_order_p != 1;
  }

  petitionDisabled(r) {
    return Number(r.stateOrderPetition) === 7;
  }

  isDisabledP() {
    return _permission().o_order_p_item == 0;
  }

  isDisabledPetition() {
    return Number(this.orderSelect.stateOrderPetition) === 1
      || Number(this.orderSelect.stateOrderPetition) === 3;
  }

  isDisabledTr() {
    if (Number(this.orderSelect.stateOrderPetition) === 4) {
      return false;
    } else if (Number(this.orderSelect.stateOrderPetition) === 6) {
      return false;
    } else if (Number(this.orderSelect.stateOrderPetition) === 7) {
      return false;
    } else if (Number(this.orderSelect.stateOrderPetition) === 8) {
      return false;
    } else {
      return true;
    }
  }

  isEnd() {
    if (Number(this.orderSelect.stateOrderPetition) === 4) {
      return false;
    } else if (Number(this.orderSelect.stateOrderPetition) === 8) {
      return false;
    } else {
      return true;
    }
  }

  isReasume() {
    return Number(this.orderSelect.stateOrderPetition) !== 7;
  }
}

export class DataOrder {
  constructor(public form: ViewOrder) {
  }
}

