import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderPetitionComponent} from './order-petition.component';

describe('OrderPetitionComponent', () => {
  let component: OrderPetitionComponent;
  let fixture: ComponentFixture<OrderPetitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderPetitionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPetitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
