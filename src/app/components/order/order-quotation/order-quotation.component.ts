import {Component, OnInit} from '@angular/core';
import {ViewOrder} from '../../../model/order/view-order';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {OrderService} from '../../../services/order/order.service';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {compareDate, compareString} from '../../../services/GlobalFunctions';
import {HttpErrorResponse} from '@angular/common/http';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-order-quotation',
  templateUrl: './order-quotation.component.html',
  styleUrls: ['./order-quotation.component.css']
})
export class OrderQuotationComponent implements OnInit {
  orders: ViewOrder[] = [];
  titulo = 'APROBADOS';
  displayedColumns = [
    'position',
    'date',
    'nOrder',
    'project',
    'state',
    // 'quotation',
    // 'state',
    'buy'
  ];
  textTooltip = ['Rechazado', 'Aprobado', 'Pediente'];
  sortedData: ViewOrder[];
  orderSelect: ViewOrder;
  progress = false;
  options = [
    new OptionOrianaTableToolbar('SOLICITUDES TERMINADAS', 2),
    // new OptionOrianaTableToolbar('SOLICITUD RECHAZADOS', 3),
    new OptionOrianaTableToolbar('COTIZACION APROBADA', 4),
    new OptionOrianaTableToolbar('COTIZACION RECHAZADA', 5),
    new OptionOrianaTableToolbar('COMPRANDO', 8),
    new OptionOrianaTableToolbar('COMPRA RECHAZADA', 6),
    new OptionOrianaTableToolbar('COMPRA CONCLUIDA', 7),
    new OptionOrianaTableToolbar('TODOS', 9)
  ];

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.progress = true;
    this.orderService.filterOrder(type, 'quotation').subscribe(
      response => {
        this.orders = <ViewOrder[]>response;
        this.progress = false;
      }
    );
  }

  load() {
    this.progress = false;
    this.getFilter(this.orderService.optionQuotation);
    this.resetSelect();
    this.options.forEach(op => {
      if (op.url == this.orderService.optionQuotation) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'FECHA', 'NRO PEDIDO', 'PROYECTO', 'ESTADO EN \n COTIZACION'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PEDIDOS COTIZACION', head, body, 'wrap',
      {});
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.orders.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(this.printService.dateData(c.dateOrder).toUpperCase());
      d.push(c.numberPeriodOrder.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      // d.push(c.stateOrderQuotation == 1 ? 'APROBADO' : c.stateOrderQuotation == 2 ? 'PENDIENTE' : 'RECHAZADO');
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.orderService.searchOrder(text).subscribe(
        response => {
          this.orders = <ViewOrder[]>response;
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.orders.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.orders = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'dateOrder':
          return compareDate(a.dateOrder, b.dateOrder, isAsc);
        case 'numberPeriodOrder':
          return compareString(a.numberPeriodOrder, b.numberPeriodOrder, isAsc);
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        default:
          return 0;
      }
    });
  }

  update(state, text) {
    this.orderSelect.stateText = text;
    this.orderSelect.stateOrderPetition = state;
    this.orderService.updateStates(this.orderSelect.idOrder, this.orderSelect)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.orderService.optionPetition !== 3) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            // this.orders[pos] = (<ViewOrder>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.orderSelect = new ViewOrder(
      null,
      null,
      null,
      null,
      null,
      1,
      'Solicitud en Curso',
      0
    );
  }

  updateTracing(evt, pos) {
    const p = this.orders[pos];
    this.orderService.updateState(p.idOrder, evt);
    this.orderService.updateStates(p.idOrder, {stateOrderTracing: evt}).subscribe(
      res => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        if (this.orderService.optionQuotation !== 3) {
          this.load();
        }
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse.status === 406) {
          this.orders[pos] = (<ViewOrder>errorResponse.error);
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        } else {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        }
      }
    );
  }

  setCurrent(type: any) {
    this.orderService.optionQuotation = type;
    this.load();
  }

  isDisabled() {
    return _permission().o_order_q != 1;
  }

  disablePe() {
    if (this.orderSelect.stateOrderPetition === 2) {
      return false;
    } else if (this.orderSelect.stateOrderPetition === 5) {
      return false;
    } else {
      return true;
    }
  }

  disableBuy() {
    if (this.orderSelect.stateOrderPetition == 4) {
      return false;
    } else if (this.orderSelect.stateOrderPetition == 8) {
      return false;
    } else if (this.orderSelect.stateOrderPetition == 6) {
      return false;
    } else {
      return true;
    }
  }

  isDisabledI() {
    return _permission().o_order_q_item == 0;
  }

  select(order) {
    this.orderSelect = order;
  }

  disabledCo() {
    if (this.orderSelect.stateOrderPetition == 4) {
      return false;
    } else {
      return true;
    }
  }
}
