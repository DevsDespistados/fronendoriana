import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderQuotationComponent} from './order-quotation.component';

describe('OrderQuotationComponent', () => {
  let component: OrderQuotationComponent;
  let fixture: ComponentFixture<OrderQuotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderQuotationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderQuotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
