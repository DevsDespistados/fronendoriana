import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalOrderBuyComponent} from './modal-order-buy.component';

describe('ModalOrderBuyComponent', () => {
  let component: ModalOrderBuyComponent;
  let fixture: ComponentFixture<ModalOrderBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalOrderBuyComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOrderBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
