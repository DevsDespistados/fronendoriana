import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OrderTracingService} from '../../../services/order/order-tracing.service';
import {ViewOrderTracingItem} from '../../../model/order/view-order-tracing-item';

@Component({
  selector: 'app-modal-order-buy',
  templateUrl: './modal-order-buy.component.html',
  styleUrls: ['./modal-order-buy.component.css']
})
export class ModalOrderBuyComponent implements OnInit {
  text = ConfText;
  title = 'Material';
  progress = false;
  displayedColumns: string[] = [
    'position',
    'name',
    'unity',
    'pu',
    'order',
    'preview',
    'submit',
    'request',
    'pay'
  ];
  items: ViewOrderTracingItem[] = [];
  // auxItems: ViewOrderTracingItem[] = [];
  idOrder;
  dateOrder;
  sortedData;
  visible = true;

  constructor(
    public dialogRef: MatDialogRef<ModalOrderBuyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public orderTracingService: OrderTracingService) {
    if (this.data.tracing && this.data.option == 'show') {
      this.dateOrder = this.data.tracing.dateOrderBuy;
      this.visible = false;
    } else if (this.data.tracing && this.data.option == 'edit') {
      this.dateOrder = this.data.tracing.dateOrderBuy;
      this.data.tracing.stateOrderBuy = false;
    }
    this.idOrder = data.idOrder;
  }


  ngOnInit() {
    this.load();
  }


  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close({
      items: this.items,
      date: this.dateOrder,
      stateOrderBuy: 0,
      type: this.data.option
    });
  }

  valid(): boolean {
    return !this.dateOrder || this.itemChangetValid();
  }

  private itemChangetValid(): boolean {
    return this.items.filter(a => {
      return a.submitOrderItem > 0;
    }).length == 0;
  }

  load() {
    this.progress = true;
    if (this.visible && this.data.option == 'new') {
      this.orderTracingService.allOrderTracingItem(this.idOrder).subscribe(
        response => {
          this.items = <ViewOrderTracingItem[]>response;
          // this.auxItems = <ViewOrderTracingItem[]> response;
          this.progress = false;
        }
      );
    } else {
      this.orderTracingService.editOrderTracing(this.idOrder, this.data.tracing.idOrderBuy).subscribe(res => {
        this.items = <ViewOrderTracingItem[]>res;
        // this.auxItems = <ViewOrderTracingItem[]> res;
        this.progress = false;
      });
    }
  }

  calcBalance(ele: ViewOrderTracingItem) {
    ele.balanceOrderItem = ele.amountOrderItem - ele.previewSubmitItem - ele.submitOrderItem;
    ele.amountPayOrderTracing = ele.submitOrderItem * ele.priceUnityItem;
  }

  //
  // search(text: string) {
  //   if (text !== '') {
  //     text = text.toLowerCase();
  //     this.sortedData = [];
  //     this.progress = true;
  //     if (this.items) {
  //       this.items.forEach(t => {
  //         if (searchText(t.materialItem, text)) {
  //           this.sortedData.push(t);
  //         }
  //       });
  //     }
  //     this.items = this.sortedData;
  //     this.progress = false;
  //   } else {
  //     this.items = this.auxItems;
  //     this.progress = false;
  //   }
  // }
  getColumns() {
    if (this.visible) {
      return [
        'position',
        'name',
        'unity',
        'pu',
        'order',
        'preview',
        'submit',
        'request',
        'pay'
      ];
    } else {
      return [
        'position',
        'name',
        'unity',
        'pu',
        'order',
        'submit',
        'pay'
      ];
    }
  }

  isVisible() {
    if (this.data.tracing || !this.visible) {
      return this.data.tracing.stateOrderBuy == 0 || this.data.tracing.stateOrderBuy == 1;
    } else {
      return false;
    }
  }

  closeStatus(status) {
    const dat = this.data.tracing;
    dat.stateOrderBuy = status;
    this.dialogRef.close({
      view: dat
    });
  };
}
