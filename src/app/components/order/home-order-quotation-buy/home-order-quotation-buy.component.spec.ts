import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeOrderQuotationBuyComponent} from './home-order-quotation-buy.component';

describe('HomeOrderQuotationBuyComponent', () => {
  let component: HomeOrderQuotationBuyComponent;
  let fixture: ComponentFixture<HomeOrderQuotationBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeOrderQuotationBuyComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeOrderQuotationBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
