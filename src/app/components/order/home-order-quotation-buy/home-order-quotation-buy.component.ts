import {Component, OnInit} from '@angular/core';
import {ViewOrder} from '../../../model/order/view-order';

@Component({
  selector: 'app-home-order-buy',
  templateUrl: './home-order-quotation-buy.component.html',
  styleUrls: ['./home-order-quotation-buy.component.css']
})
export class HomeOrderQuotationBuyComponent implements OnInit {
  titles = {
    module: 'COMPRA MATERIAL',
  };
  orderSelect: ViewOrder;

  constructor() {
  }

  ngOnInit() {
  }

  setLetter(select) {
    this.orderSelect = select;
  }

  getSubtitle() {
    if (this.orderSelect) {
      return 'SOLICITUD: ' + this.orderSelect.numberPeriodOrder;
    } else {
      return 'SOLICITUD: ';
    }
  }
}
