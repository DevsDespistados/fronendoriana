import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalOrderPetitionComponent} from './modal-order-petition.component';

describe('ModalOrderPetitionComponent', () => {
  let component: ModalOrderPetitionComponent;
  let fixture: ComponentFixture<ModalOrderPetitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalOrderPetitionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOrderPetitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
