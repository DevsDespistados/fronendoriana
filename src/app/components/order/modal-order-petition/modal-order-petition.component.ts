import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ViewOrder} from '../../../model/order/view-order';
import {ViewProjects} from '../../../model/project/view-projects';
import {ProjectService} from '../../../services/project/project.service';

@Component({
  selector: 'app-modal-order-peticion',
  templateUrl: './modal-order-petition.component.html',
  styleUrls: ['./modal-order-petition.component.css']
})
export class ModalOrderPetitionComponent implements OnInit {
  text = ConfText;
  form: ViewOrder;
  projects: ViewProjects[] = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private projectService: ProjectService) {
    this.form = this.data.form;
  }

  ngOnInit(): void {
    this.loadProjects();
  }

  loadProjects() {
    this.projectService.getOpen().subscribe(pjs => {
      this.projects = <ViewProjects[]>pjs;
    });
  }

  closeDialog() {
    this.form = null;
    this.dialogRef.close();
  }

  sendFormData() {
    this.dialogRef.close(this.form);
  }


  validData() {
    return this.form.numberPeriodOrder == null
      || this.form.dateOrder == null
      || this.form.idProject == null;
  }
}
