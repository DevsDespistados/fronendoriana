import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {compareDate, compareNumber, compareString} from '../../../services/GlobalFunctions';
import {ViewItemOrder} from '../../../model/order/view-item-order';
import {OrderService} from '../../../services/order/order.service';
import {ActivatedRoute} from '@angular/router';
import {OrderItemService} from '../../../services/order/order-item.service';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ViewOrder} from '../../../model/order/view-order';
import {_permission} from '../../../services/permissions';
import {PrintServiceService} from '../../../services/print-service.service';

@Component({
  selector: 'app-order-quotation-items',
  templateUrl: './order-quotation-items.component.html',
  styleUrls: ['./order-quotation-items.component.css']
})
export class OrderQuotationItemsComponent implements OnInit {
  @Output() getOrder = new EventEmitter();
  idContract: number;
  items: ViewItemOrder[] = [];
  displayedColumns = [
    'position',
    'item',
    'unity',
    'amount',
    'price',
    'total',
    'date'];
  sortedData: ViewItemOrder[];
  idOrder;
  orderSelect: ViewOrder;
  saveVisible = false;
  progress = false;
  routerBack = '/order/quotation';

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private orderService: OrderService,
              private itemOrderService: OrderItemService,
              private routerActive: ActivatedRoute,
              private printService: PrintServiceService) {
  }

  ngOnInit() {
    this.routerActive.params.subscribe(params => {
      this.idOrder = params['idOrder'];
      this.getFilter(params['idOrder']);
    });
  }

  save() {
    this.itemOrderService.updateOrderItem(this.items, 'quotation').subscribe(
      res => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        this.load();
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse.status === 406) {
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        } else {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        }
      }
    );
  }

  getFilter(type: any) {
    this.progress = true;
    this.orderService.getItems(type).subscribe(
      response => {
        this.items = <ViewItemOrder[]>response['items'];
        this.orderSelect = response['order'];
        this.getOrder.emit(response['order']);
        this.progress = false;
      }
    );
  }

  load() {
    this.saveVisible = false;
    this.getFilter(this.idOrder);
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'MATERIAL', 'UNIDAD', 'CANTIDAD', 'P/U', 'TOTAL', 'FECHA'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PEDIDOS COTIZACION\n SOLICITUD: ' +
      this.orderSelect.numberPeriodOrder.toUpperCase() + ' MATERIAL', head, body, 'wrap',
      {
        3: {
          halign: 'right'
        },
        4: {
          halign: 'right'
        },
        5: {
          halign: 'right'
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.items.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.materialItem.toUpperCase());
      d.push(c.unityItem.toUpperCase());
      d.push(c.amountOrderItem);
      d.push(this.printService.moneyData(c.priceUnityItem));
      d.push(this.printService.dateData(c.updated_at).toUpperCase());
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.orderService.searchOrder(text).subscribe(
        response => {
          this.items = <ViewItemOrder[]>response;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.items.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.items = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'materialItem':
          return compareString(a.materialItem, b.materialItem, isAsc);
        case 'unityItem':
          return compareString(a.unityItem, b.unityItem, isAsc);
        case 'priceUnityItem':
          return compareNumber(a.priceUnityItem, b.priceUnityItem, isAsc);
        case 'amountOrderItem':
          return compareNumber(a.priceUnityItem, b.priceUnityItem, isAsc);
        case 'totalOrderItem':
          return compareNumber(a.totalOrderItem, b.totalOrderItem, isAsc);
        case 'updated_at':
          return compareDate(a.updated_at, b.updated_at, isAsc);
        default:
          return 0;
      }
    });
  }

  calcTotalItem(ele: ViewItemOrder): void {
    this.saveVisible = true;
    ele.totalOrderItem = ele.priceUnityItem * ele.amountOrderItem;
  }

  getTotal() {
    let total = 0;
    this.items.forEach(p => {
      total += p.totalOrderItem;
    });
    return total;
  }

  disabledOrder() {
    if (this.orderSelect) {
      if (this.orderSelect.stateOrderPetition == 2) {
        return false;
      } else if (this.orderSelect.stateOrderPetition == 5) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  isDisabled() {
    return _permission().o_order_q_item != 1;
  }
}
