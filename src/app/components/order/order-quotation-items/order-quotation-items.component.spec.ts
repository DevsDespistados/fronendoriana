import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderQuotationItemsComponent} from './order-quotation-items.component';

describe('OrderQuotationItemsComponent', () => {
  let component: OrderQuotationItemsComponent;
  let fixture: ComponentFixture<OrderQuotationItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderQuotationItemsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderQuotationItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
