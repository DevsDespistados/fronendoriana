import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {OrderService} from '../../../services/order/order.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ViewItemOrder} from '../../../model/order/view-item-order';
import {ActivatedRoute} from '@angular/router';
import {ModalOrderItemComponent} from '../modal-order-item/modal-order-item.component';
import {ModalOrderPetitionItemComponent} from '../modal-order-petition-item/modal-order-petition-item.component';
import {ItemService} from '../../../services/order/item.service';
import {isBoolean} from 'util';
import {DataOrderItem} from '../order-items/order-items.component';
import {OrderItemService} from '../../../services/order/order-item.service';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {ViewOrder} from '../../../model/order/view-order';

@Component({
  selector: 'app-order-item-petition',
  templateUrl: './order-item-petition.component.html',
  styleUrls: ['./order-item-petition.component.css']
})
export class OrderItemPetitionComponent implements OnInit {
  @Output() getOrder = new EventEmitter();
  items: ViewItemOrder[] = [];
  displayedColumns = [
    'position',
    'item',
    'unity',
    'pu',
    'mount',
    'options'
  ];
  textTooltip = ['Rechazado', 'Aprobado', 'Pediente'];
  sortedData: ViewItemOrder[];
  idOrder;
  orderSelect: ViewOrder;
  itemSelect: ViewItemOrder;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private orderService: OrderService,
              private activateRouter: ActivatedRoute,
              private itemService: ItemService,
              private itemOrderService: OrderItemService) {
  }

  ngOnInit() {
    this.activateRouter.params.subscribe(params => {
      this.idOrder = params['idOrder'];
      this.getFilter(params['idOrder']);
    });
  }

  create() {
    const dialogRef = this.dialog.open(ModalOrderPetitionItemComponent, {
      minWidth: '450px',
      data: new DataOrderItems(this.items, this.idOrder)
    });
    dialogRef.componentInstance.load();
    dialogRef.afterClosed().subscribe(result => {
      if (result && !isBoolean(result)) {
        // if (result) {
        console.log(result);
        this.itemOrderService.addRelationOrderItem(this.idOrder, result).subscribe(
          response => {
            this.load();
            this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
          }, error => {
            this.load();
            if (error.status == 406) {
              this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
            } else {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          }
        );
        // }
      } else if (isBoolean(result)) {
        this.createItem();
      } else {
        this.resetSelect();
      }
    });
  }

  createItem() {
    const dial = this.dialog.open(ModalOrderItemComponent, {
      width: '300px',
      data: new DataOrderItem(this.itemSelect)
    });
    dial.afterClosed().subscribe(result => {
      if (result) {
        this.itemService.addItem(result)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
        this.create();
      }
    });
  }

  deleted(pos: number) {
    this.itemSelect = <any>this.items[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.itemSelect.materialItem}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.itemOrderService.deleteRelationOrderItem(this.itemSelect.idOrderItem)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});

            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  getIdsSelected(list: ViewItemOrder[]) {
    const res = [];
    list.forEach(i => {
      if (i.amountOrderItem > 0) {
        res.push({
          idItem: i.idItem,
          amountOrderItem: i.amountOrderItem
        });
      }
    });
    return res;
  }

  edit(pos: number) {
    const f = this.items[pos];
    this.itemSelect = new ViewItemOrder(
      f.idOrderItem,
      f.idItem,
      f.materialItem,
      f.unityItem,
      f.priceUnityItem,
      f.submitOrderItem,
      f.amountOrderItem,
      f.totalOrderItem,
      f.balanceOrderItem,
      f.idOrder,
      f.updated_at
    );
    this.create();
  }

  getFilter(idOrder: any) {
    this.progress = true;
    this.orderService.getItems(idOrder).subscribe(
      response => {
        this.items = <ViewItemOrder[]>response['items'];
        this.orderSelect = response['order'];
        this.getOrder.emit(response['order']);
        this.progress = false;
      }
    );
  }


  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.idOrder);
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'MATERIAL', 'UNIDAD', 'P/U', 'CANTIDAD'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PEDIDOS SOLICITUD\n SOLICITUD: ' +
      this.orderSelect.numberPeriodOrder.toUpperCase() + ' MATERIAL', head, body, 'wrap',
      {
        3: {
          halign: 'right'
        },
        4: {
          halign: 'right'
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.items.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.materialItem.toUpperCase());
      d.push(c.unityItem.toUpperCase());
      d.push(this.printService.moneyData(c.priceUnityItem));
      d.push(c.amountOrderItem);
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.orderService.searchOrder(text).subscribe(
        response => {
          this.items = <ViewItemOrder[]>response;
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.items.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.items = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'materialItem':
          return compareString(a.materialItem, b.materialItem, isAsc);
        case 'unityItem':
          return compareString(a.unityItem, b.unityItem, isAsc);
        case 'priceUnityItem':
          return compareNumber(a.priceUnityItem, b.priceUnityItem, isAsc);
        case 'amountOrderItem':
          return compareNumber(a.amountOrderItem, b.amountOrderItem, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.idOrder = type;
    this.load();
  }

  optionsDisabled() {
  }

  resetSelect() {
    this.itemSelect = new ViewItemOrder(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  disabledOrder() {
    return this.orderSelect ?
      Number(this.orderSelect.stateOrderPetition) === 1
      || Number(this.orderSelect.stateOrderPetition) === 3 : false;
  }
}

export class DataOrderItems {
  constructor(public list: ViewItemOrder[], public idOrder: number) {
  }
}
