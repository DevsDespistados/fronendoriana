import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalOrderPetitionItemComponent} from './modal-order-petition-item.component';

describe('ModalOrderPetitionItemComponent', () => {
  let component: ModalOrderPetitionItemComponent;
  let fixture: ComponentFixture<ModalOrderPetitionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalOrderPetitionItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOrderPetitionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
