import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {ItemService} from '../../../services/order/item.service';
import {ViewItemOrder} from '../../../model/order/view-item-order';
import {searchText} from '../../reports/template-acconts-reports/template-acconts-reports.component';

@Component({
  selector: 'app-modal-order-petition-item',
  templateUrl: './modal-order-petition-item.component.html',
  styleUrls: ['./modal-order-petition-item.component.css']
})
export class ModalOrderPetitionItemComponent implements OnInit {
  text = ConfText;
  titulo = 'Material';
  progress = false;
  displayedColumns: string[] = ['select', 'position', 'name', 'unity', 'price', 'mount'];
  items = new MatTableDataSource<ViewItemOrder>([]);
  selection = new SelectionModel<ViewItemOrder>(true, []);
  idOrder;
  sortedData: ViewItemOrder[];
  auxItems = new MatTableDataSource<ViewItemOrder>([]);

  constructor(
    public dialogRef: MatDialogRef<ModalOrderPetitionItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private itemService: ItemService) {
    this.idOrder = data.idOrder;
  }


  ngOnInit() {
    this.load();
    console.log(this.items);
    console.log(this.selection);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.items.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.items.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: ViewItemOrder): string {
    if (!row) {
      return this.isAllSelected() ? 'select' : 'deselect';
    }
    return this.selection.isSelected(row) ? 'deselect' : 'select';
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close(this.selection.selected);
  }

  valid(): boolean {
    return this.selection.selected.length == 0 || this.isInvalidSelection();
  }

  create() {
    this.dialogRef.close(true);
  }

  load() {
    this.progress = true;
    this.itemService.getOrderItems(this.idOrder).subscribe(i => {
      this.items = new MatTableDataSource<ViewItemOrder>(<ViewItemOrder[]>i);
      this.auxItems = new MatTableDataSource<ViewItemOrder>(<ViewItemOrder[]>i);
      this.loadExist();
      this.progress = false;
    });
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.items.data) {
        this.items.data.forEach(t => {
          if (searchText(t.materialItem, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.items = new MatTableDataSource<ViewItemOrder>(this.sortedData);
      this.progress = false;
    } else {
      this.items = this.auxItems;
      this.progress = false;
    }
  }

  loadExist() {
    this.selection = new SelectionModel<ViewItemOrder>(true, []);
    this.items.data.forEach((item) => {
      if (this.data.list.find(o => {
        return o.idItem == item.idItem;
      })) {
        this.selection.toggle(item);
      }
    });
  }

  isInvalid(row: ViewItemOrder) {
    if (this.selection.selected.find(o => {
      return o.idItem == row.idItem;
    })) {
      if (row.amountOrderItem <= 0 || row.amountOrderItem == null) {
        return {background: '#ff5454'};
      }
    }
  }

  isInvalidSelection() {
    return this.selection.selected.filter(o => {
      return o.amountOrderItem <= 0 || o.amountOrderItem == null;
    }).length > 0;
  }
}
