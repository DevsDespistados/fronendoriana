import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewOrder} from '../../../model/order/view-order';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {OrderService} from '../../../services/order/order.service';
import {ActivatedRoute} from '@angular/router';
import {ItemService} from '../../../services/order/item.service';
import {ConfText} from '../../../model/configuration/conf-text';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {compareNumber} from '../../../services/GlobalFunctions';
import {ViewTracingOrder} from '../../../model/order/view-tracing-order';
import {OrderTracingService} from '../../../services/order/order-tracing.service';
import {ModalOrderBuyComponent} from '../modal-order-buy/modal-order-buy.component';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';

@Component({
  selector: 'app-order-buy',
  templateUrl: './order-buy.component.html',
  styleUrls: ['./order-buy.component.css']
})
export class OrderBuyComponent implements OnInit {
  @Output() getOrder = new EventEmitter();
  @Input() editable = true;
  orderTracings: ViewTracingOrder[] = [];
  displayedColumns = [
    'position',
    'date',
    'amount',
    'state',
    'options'
  ];
  textTooltip = ['Rechazado', 'Aprobado', 'Pediente'];
  sortedData: ViewTracingOrder[];
  idOrder;
  orderSelect: ViewOrder;
  orderTracingSelect: ViewTracingOrder;
  progress = false;
  current = 2;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private orderService: OrderService,
              private activateRouter: ActivatedRoute,
              private itemService: ItemService,
              private orderTracingService: OrderTracingService) {
  }

  ngOnInit() {
    this.activateRouter.params.subscribe(params => {
      this.idOrder = params['idOrder'];
      this.getFilter(params['idOrder'], this.current);
    });
  }
  create() {
    const dialogRef = this.dialog.open(ModalOrderBuyComponent, {
      minWidth: '400px',
      data: new DataOrderTracing(this.orderTracingSelect, this.idOrder, 'new', this.editable)
    });
    this.openDialog(dialogRef);
  }

  openDialog(dialogRef) {
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.type == 'new') {
          this.orderTracingService.addOrderTracingItem(result.items, result.date, this.idOrder).subscribe(
            response => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, error => {
              this.load();
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            }
          );
        } else {
          this.orderTracingService.updateOrderBuy(this.orderTracingSelect.idOrderBuy, result.date, result.items).subscribe(
            res => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, error => {
              this.load();
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.orderTracingSelect = <any>this.orderTracings[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.orderTracingSelect.dateOrderBuy}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.orderTracingService.deleteOrderBuy(this.orderTracingSelect.idOrderBuy, this.idOrder)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});

            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    this.orderTracingSelect = this.orderTracings[pos];
    const dialogRef = this.dialog.open(ModalOrderBuyComponent, {
      minWidth: '400px',
      data: new DataOrderTracing(this.orderTracingSelect, this.idOrder, 'edit', this.editable)
    });
    this.openDialog(dialogRef);
  }

  show(pos: number) {
    const f = this.orderTracings[pos];
    const dialogRef = this.dialog.open(ModalOrderBuyComponent, {
      minWidth: '400px',
      data: new DataOrderTracing(f, this.idOrder, 'show',
        this.editable,
        this.orderSelect)
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.orderTracingService.updateStateOrderBuy(f.idOrderBuy, this.orderSelect.idOrder, f.stateOrderBuy).subscribe(r => {
          this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
          this.load();
        });
      }
    });
  }

  getFilter(idOrder: any, type) {
    this.progress = true;
    this.orderTracingService.allOrderBuy(idOrder, type).subscribe(
      response => {
        this.orderTracings = <ViewTracingOrder[]>response['buys'];
        this.orderSelect = response['order'];
        this.getOrder.emit(response['order']);
        this.progress = false;
      }
    );
  }


  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.idOrder, this.current);
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.orderService.searchOrder(text).subscribe(
        response => {
          this.orderTracings = <ViewTracingOrder[]>response;
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.orderTracings.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.orderTracings = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'priceUnityItem':
          return compareNumber(a.priceUnityItem, b.priceUnityItem, isAsc);
        case 'amountOrderItem':
          return compareNumber(a.amountOrderItem, b.amountOrderItem, isAsc);
        case 'state':
          return compareNumber(a.stateOrderBuy, b.stateOrderBuy, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.idOrder = type;
    this.load();
  }

  resetSelect() {
  }

  disabledOrder() {
    if (this.orderSelect) {
      if (this.editable) {
        return this.orderSelect.stateOrderPetition == 4 || this.orderSelect.stateOrderPetition == 8;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}

export class DataOrderTracing {
  constructor(public tracing: ViewTracingOrder,
              public idOrder: number,
              public option: string,
              public editable?: boolean,
              public order?: ViewOrder) {
  }
}
