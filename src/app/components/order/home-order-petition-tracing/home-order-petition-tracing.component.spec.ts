import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeOrderPetitionTracingComponent} from './home-order-petition-tracing.component';

describe('HomeOrderPetitionTracingComponent', () => {
  let component: HomeOrderPetitionTracingComponent;
  let fixture: ComponentFixture<HomeOrderPetitionTracingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeOrderPetitionTracingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeOrderPetitionTracingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
