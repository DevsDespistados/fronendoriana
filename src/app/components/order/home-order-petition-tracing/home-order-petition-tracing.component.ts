import {Component, OnInit} from '@angular/core';
import {ViewOrder} from '../../../model/order/view-order';

@Component({
  selector: 'app-home-order-petition-tracing',
  templateUrl: './home-order-petition-tracing.component.html',
  styleUrls: ['./home-order-petition-tracing.component.css']
})
export class HomeOrderPetitionTracingComponent implements OnInit {
  titles = {
    module: 'SEGUIMIENTO SOLICITUD',
  };
  orderSelect: ViewOrder;

  constructor() {
  }

  ngOnInit() {
  }

  setLetter(select) {
    this.orderSelect = select;
  }

  getSubtitle() {
    if (this.orderSelect) {
      return 'SOLICITUD: ' + this.orderSelect.numberPeriodOrder;
    } else {
      return 'SOLICITUD: ';
    }
  }
}
