import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {RenderChecks, ViewInvoice} from '../oriana-render/oriana-render.component';
import {Project} from '../../../model/project/project';
import {searchText} from '../../reports/template-acconts-reports/template-acconts-reports.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-modal-view-render-check',
  templateUrl: './modal-view-render-check.component.html',
  styleUrls: ['./modal-view-render-check.component.css']
})
export class ModalViewRenderCheckComponent implements OnInit {
  text = ConfText;
  titulo = 'Facturas';
  progress = false;
  displayedColumns: string[] = [
    'position',
    'nameInvoice',
    'numberInvoice',
    'codeControlInvoice',
    'amountTotalInvoice',
    'project',
    'option'
  ];
  invoices: ViewInvoice[] = [];
  sortedData: ViewInvoice[];
  checkSelect: RenderChecks;
  auxItems: ViewInvoice[] = [];
  total: number;
  projects: Project[] = [];

  constructor(
    public dialogRef: MatDialogRef<ModalViewRenderCheckComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) {
    this.projects = data.project;
    this.invoices = data.invoices;
    this.auxItems = data.invoices;
    this.checkSelect = data.check;
  }


  ngOnInit() {
    this.load();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close();
  }

  create() {
    this.dialogRef.close(true);
  }

  totalSelect() {
    let total = 0;
    this.invoices.forEach(i => {
      total += i.amountTotalInvoice;
    });
    this.total = total;
    return total;
  }

  load() {
  }

  deleted(pos: number) {
    const inv = this.invoices[pos];
    this.dialogRef.close({
      id: inv.idRender,
      message: inv.nameInvoice
    });
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.invoices) {
        this.invoices.forEach(t => {
          if (searchText(t.nameInvoice, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.invoices = this.sortedData;
      this.progress = false;
    } else {
      this.invoices = this.auxItems;
      this.progress = false;
    }
  }

  idDisabled() {
    return _permission().f_fac_render != 1;
  }
}
