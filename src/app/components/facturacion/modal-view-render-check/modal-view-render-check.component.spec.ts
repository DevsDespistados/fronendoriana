import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalViewRenderCheckComponent} from './modal-view-render-check.component';

describe('ModalViewRenderCheckComponent', () => {
  let component: ModalViewRenderCheckComponent;
  let fixture: ComponentFixture<ModalViewRenderCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalViewRenderCheckComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalViewRenderCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
