import {Component, OnInit} from '@angular/core';
import {TotalRes} from '../../projects/forms-project/forms-project.component';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {Period} from '../../../model/accounts/period';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {ActivatedRoute} from '@angular/router';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {InvoiceService} from '../../../services/facturacion/invoice.service';
import {Invoice} from '../../../model/facturacion/invoice';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-invoice',
  templateUrl: './oriana-invoice.component.html',
  styleUrls: ['./oriana-invoice.component.css']
})
export class OrianaInvoiceComponent implements OnInit {
  invoices: Invoice[];
  total: TotalRes = new TotalRes(0, 0, 0, 0, 0, 0);
  titulo = 'pendientes';
  displayedColumns = [
    'position',
    'date',
    'nit',
    'name',
    'numberI',
    'numberA',
    'amountT',
    'amountN',
    'sub',
    'dis',
    'amountC',
    'fiscal',
    'code',
  ];
  sortedData: Invoice[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('RENDIDOS', 1),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  tooltip = Tooltips;
  currentOption = 0;
  progress = false;
  idPeriod;
  period: Period;

  // idBank: number;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private invoiceService: InvoiceService,
              private routerActive: ActivatedRoute) {
    this.routerActive.parent.params.subscribe(
      params => {
        this.idPeriod = params['idPeriod'];
      }
    );
  }

  routerBack: string;

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.invoiceService.allInvoice(type).subscribe(
      response => {
        this.invoices = response['invoice'];
        this.period = response['period'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      });
  }

  load() {
    this.progress = true;
    this.getFilter(this.idPeriod);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  sortData(sort: Sort): any {
  }

  isDisabled() {
    return _permission().f_fac_invoice != 1;
  }
}
