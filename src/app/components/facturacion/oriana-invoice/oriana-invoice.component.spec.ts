import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaInvoiceComponent} from './oriana-invoice.component';

describe('OrianaInvoiceComponent', () => {
  let component: OrianaInvoiceComponent;
  let fixture: ComponentFixture<OrianaInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaInvoiceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
