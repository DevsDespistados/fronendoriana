import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPeriodBillingComponent} from './modal-period-billing.component';

describe('ModalPeriodBillingComponent', () => {
  let component: ModalPeriodBillingComponent;
  let fixture: ComponentFixture<ModalPeriodBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPeriodBillingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPeriodBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
