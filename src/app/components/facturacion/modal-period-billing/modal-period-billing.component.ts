import {Component, Inject} from '@angular/core';
import {ConfText, month} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {moment} from '../../../services/GlobalFunctions';
import {PeriodInvoice} from '../../../model/facturacion/period-invoice';
import {FileInput} from 'ngx-material-file-input';

@Component({
  selector: 'app-modal-period-billing',
  templateUrl: './modal-period-billing.component.html',
  styleUrls: ['./modal-period-billing.component.css']
})
export class ModalPeriodBillingComponent {
  text = ConfText;
  formData: FormData;
  period: PeriodInvoice;
  file: FileInput;
  private date = new Date();
  year = this.date.getFullYear();
  month = this.date.getMonth();
  months = month;

  constructor(
    public dialogRef: MatDialogRef<ModalPeriodBillingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.period = this.data.period;
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    this.formData = new FormData();
    this.formData.append('file', this.file.files[0], this.file.fileNames[0]);
    this.formData.append('billingPeriod', moment(new Date(this.year, this.month, 1)));
    this.dialogRef.close();
  }

  validData() {
    return this.year == null
      || this.year <= 0
      || this.month == null
      || this.month < 0
      || !this.file
      || this.file.files.length == 0;
  }
}
