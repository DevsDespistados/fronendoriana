import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalViewRenderPettyCashCheckComponent} from './modal-view-render-petty-cash-check.component';

describe('ModalViewRenderPettyCashCheckComponent', () => {
  let component: ModalViewRenderPettyCashCheckComponent;
  let fixture: ComponentFixture<ModalViewRenderPettyCashCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalViewRenderPettyCashCheckComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalViewRenderPettyCashCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
