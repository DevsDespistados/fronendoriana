import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {RenderChecks} from '../oriana-render/oriana-render.component';
import {searchText} from '../../reports/template-acconts-reports/template-acconts-reports.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-modal-view-render-petty-cash-check',
  templateUrl: './modal-view-render-petty-cash-check.component.html',
  styleUrls: ['./modal-view-render-petty-cash-check.component.css']
})
export class ModalViewRenderPettyCashCheckComponent implements OnInit {
  text = ConfText;
  titulo = 'Cheques';
  progress = false;
  displayedColumns: string[] = [
    'position',
    'dateCheck',
    'numberCheck',
    'mountCheck',
    'option'
  ];
  checks: RenderChecks[] = [];
  sortedData: RenderChecks[];
  auxItems: RenderChecks[] = [];
  total: number;

  constructor(
    public dialogRef: MatDialogRef<ModalViewRenderPettyCashCheckComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) {
    this.checks = this.data.checks;
    this.auxItems = this.data.checks;
  }


  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  create() {
    this.dialogRef.close(true);
  }

  totalSelect() {
    let total = 0;
    this.checks.forEach(i => {
      total += i.mountCheck;
    });
    this.total = total;
    return total;
  }

  deleted(pos: number) {
    const ch = this.checks[pos];
    this.dialogRef.close({
      id: ch.idRender,
      message: ch.numberCheck
    });
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.checks) {
        this.checks.forEach(t => {
          if (searchText(t.numberCheck, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.checks = this.sortedData;
      this.progress = false;
    } else {
      this.checks = this.auxItems;
      this.progress = false;
    }
  }

  idDisabled() {
    return _permission().f_fac_render != 1;
  }
}
