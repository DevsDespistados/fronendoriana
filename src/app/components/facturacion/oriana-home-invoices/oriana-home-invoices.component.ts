import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-invoices',
  templateUrl: './oriana-home-invoices.component.html',
  styleUrls: ['./oriana-home-invoices.component.css']
})
export class OrianaHomeInvoicesComponent implements OnInit {

  titles = {
    module: 'PEDIDOS',
  };
  idPeriod;
  links = [];
  navLinks = [];

  constructor(private routerActive: ActivatedRoute) {
  }


  ngOnInit() {
    this.routerActive.params.subscribe(params => {
      this.idPeriod = params['idPeriod'];
      this.links = [
        {label: 'Cheques', path: '/invoice/process/' + this.idPeriod + '/checks', permission: _permission().f_fac_check},
        {label: 'Facturas', path: '/invoice/process/' + this.idPeriod + '/invoices', permission: _permission().f_fac_invoice},
        {label: 'Rendir Facturas', path: '/invoice/process/' + this.idPeriod + '/render', permission: _permission().f_fac_render}
      ];
      this.load();
    });
  }


  load() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }
}
