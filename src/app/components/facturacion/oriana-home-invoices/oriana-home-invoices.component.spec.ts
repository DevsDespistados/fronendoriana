import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeInvoicesComponent} from './oriana-home-invoices.component';

describe('OrianaHomeInvoicesComponent', () => {
  let component: OrianaHomeInvoicesComponent;
  let fixture: ComponentFixture<OrianaHomeInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeInvoicesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
