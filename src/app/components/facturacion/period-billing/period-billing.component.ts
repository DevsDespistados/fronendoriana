import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {PeriodInvoice} from '../../../model/facturacion/period-invoice';
import {PeriodInvoiceService} from '../../../services/facturacion/period-invoice.service';
import {ModalPeriodBillingComponent} from '../modal-period-billing/modal-period-billing.component';
import {InvoiceService} from '../../../services/facturacion/invoice.service';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-period-billing',
  templateUrl: './period-billing.component.html',
  styleUrls: ['./period-billing.component.css']
})
export class PeriodBillingComponent implements OnInit {
  periods: PeriodInvoice[] = [];
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  sortedData: PeriodInvoice[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 'open'),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 'close'),
    new OptionOrianaTableToolbar('TODAS', 'all')
  ];
  tooltip = Tooltips;
  currentOption = 'open';
  period: PeriodInvoice;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private periodInvoiceService: PeriodInvoiceService,
              private invoiceService: InvoiceService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalPeriodBillingComponent, {
      width: '300px',
      data: new DataBillingPeriod(this.period)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.period.idBillingPeriod) {
          this.progress = true;
          this.invoiceService.importFile(dialogRef.componentInstance.formData).subscribe(
            response => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, (error: HttpErrorResponse) => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
              this.progress = false;
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.period = <any>this.periods[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: 'Usted Eliminara todo el periodo'}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.periodInvoiceService.deletePeriod(this.period.idBillingPeriod)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.periods[pos];
    this.period = new PeriodInvoice(
      p.idBillingPeriod,
      p.billingPeriod,
      p.stateBilling
    );
    this.create();
  }

  getFilter(type: any) {
    this.periodInvoiceService.getPeriodInvoice(type).subscribe(
      res => {
        this.periods = res['periods'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        this.progress = false;
      });
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.periodInvoiceService.searchPeriods(text).subscribe(
        response => {
          this.periods = response['periods'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.periodInvoiceService.setStatePeriod(this.periods[pos].idBillingPeriod, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 'all') {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.period = new PeriodInvoice(
      null,
      null,
      true,
    );
  }

  isDisabled() {
    return _permission().f_fac_period != 1;
  }

  isDisabledR() {
    return _permission().f_fac_render == 0;
  }

}

export class DataBillingPeriod {
  constructor(public period: PeriodInvoice) {
  }
}
