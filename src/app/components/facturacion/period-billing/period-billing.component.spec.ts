import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PeriodBillingComponent} from './period-billing.component';

describe('PeriodBillingComponent', () => {
  let component: PeriodBillingComponent;
  let fixture: ComponentFixture<PeriodBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PeriodBillingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
