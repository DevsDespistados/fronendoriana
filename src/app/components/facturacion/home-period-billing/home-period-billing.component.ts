import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-period-billing',
  templateUrl: './home-period-billing.component.html',
  styleUrls: ['./home-period-billing.component.css']
})
export class HomePeriodBillingComponent implements OnInit {
  titles = {
    module: 'FACTURAS',
    subtitle: 'PERIODOS'
  };

  constructor() {
  }

  ngOnInit() {
  }

}
