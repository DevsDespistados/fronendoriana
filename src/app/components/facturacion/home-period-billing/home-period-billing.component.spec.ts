import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomePeriodBillingComponent} from './home-period-billing.component';

describe('HomePeriodBillingComponent', () => {
  let component: HomePeriodBillingComponent;
  let fixture: ComponentFixture<HomePeriodBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePeriodBillingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePeriodBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
