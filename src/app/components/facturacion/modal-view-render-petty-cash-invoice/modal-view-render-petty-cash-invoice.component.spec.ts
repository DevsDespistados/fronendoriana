import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalViewRenderPettyCashInvoiceComponent} from './modal-view-render-petty-cash-invoice.component';

describe('ModalViewRenderPettyCashInvoiceComponent', () => {
  let component: ModalViewRenderPettyCashInvoiceComponent;
  let fixture: ComponentFixture<ModalViewRenderPettyCashInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalViewRenderPettyCashInvoiceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalViewRenderPettyCashInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
