import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ViewInvoice} from '../oriana-render/oriana-render.component';
import {searchText} from '../../reports/template-acconts-reports/template-acconts-reports.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-modal-view-render-petty-cash-invoice',
  templateUrl: './modal-view-render-petty-cash-invoice.component.html',
  styleUrls: ['./modal-view-render-petty-cash-invoice.component.css']
})
export class ModalViewRenderPettyCashInvoiceComponent implements OnInit {
  text = ConfText;
  titulo = 'Cheques';
  progress = false;
  displayedColumns: string[] = [
    'position',
    'dateInvoice',
    'numberInvoice',
    'nameInvoice',
    'amountCreditInvoice',
    'option'
  ];
  invoices: ViewInvoice[] = [];
  sortedData: ViewInvoice[];
  auxItems: ViewInvoice[] = [];
  total: number;

  constructor(
    public dialogRef: MatDialogRef<ModalViewRenderPettyCashInvoiceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) {
    this.invoices = this.data.invoices;
    this.auxItems = this.data.invoices;
  }


  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  totalSelect() {
    let total = 0;
    this.invoices.forEach(i => {
      total += i.amountTotalInvoice;
    });
    this.total = total;
    return total;
  }

  deleted(pos: number) {
    const inv = this.invoices[pos];
    this.dialogRef.close({
      id: inv.idRender,
      message: inv.numberInvoice
    });
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.invoices) {
        this.invoices.forEach(t => {
          if (searchText(t.nameInvoice, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.invoices = this.sortedData;
      this.progress = false;
    } else {
      this.invoices = this.auxItems;
      this.progress = false;
    }
  }

  idDisabled() {
    return _permission().f_fac_render != 1;
  }
}
