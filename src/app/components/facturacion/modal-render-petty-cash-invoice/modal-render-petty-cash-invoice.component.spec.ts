import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalRenderPettyCashInvoiceComponent} from './modal-render-petty-cash-invoice.component';

describe('ModalRenderPettyCashInvoiceComponent', () => {
  let component: ModalRenderPettyCashInvoiceComponent;
  let fixture: ComponentFixture<ModalRenderPettyCashInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalRenderPettyCashInvoiceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRenderPettyCashInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
