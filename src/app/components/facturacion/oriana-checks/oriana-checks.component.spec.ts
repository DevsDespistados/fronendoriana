import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaChecksComponent} from './oriana-checks.component';

describe('OrianaChecksComponent', () => {
  let component: OrianaChecksComponent;
  let fixture: ComponentFixture<OrianaChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaChecksComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
