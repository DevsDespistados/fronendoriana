import {Component, OnInit} from '@angular/core';
import {PayCheck} from '../../../model/accounts/pay-check';
import {TotalRes} from '../../projects/forms-project/forms-project.component';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {Period} from '../../../model/accounts/period';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {PayCheckService} from '../../../services/accounts/pay-check.service';
import {ActivatedRoute} from '@angular/router';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-checks',
  templateUrl: './oriana-checks.component.html',
  styleUrls: ['./oriana-checks.component.css']
})
export class OrianaChecksComponent implements OnInit {
  payChecks: PayCheck[];
  total: TotalRes = new TotalRes(0, 0, 0, 0, 0, 0);
  titulo = 'pendientes';
  displayedColumns = [
    'position',
    'number',
    'type',
    'date',
    'mount',
    'mountRender',
    'balance',
  ];
  sortedData: PayCheck[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('RENDIDOS', 1),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  tooltip = Tooltips;
  currentOption = 0;
  progress = false;
  idPeriod;
  period: Period;

  // idBank: number;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private payCheckService: PayCheckService,
              private routerActive: ActivatedRoute) {
    this.routerActive.parent.params.subscribe(
      params => {
        this.idPeriod = params['idPeriod'];
      }
    );
  }

  routerBack: string;

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.payCheckService.allCheck(type).subscribe(
      response => {
        this.payChecks = <PayCheck[]>response['checks'];
        this.period = response['period'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  load() {
    this.progress = true;
    this.getFilter(this.idPeriod);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  sortData(sort: Sort): any {
  }

  isDisabled() {
    return _permission().f_fac_check != 1;
  }
}
