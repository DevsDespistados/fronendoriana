import {Component, OnInit} from '@angular/core';
import {InvoiceService} from '../../../services/facturacion/invoice.service';
import {PayCheckService} from '../../../services/accounts/pay-check.service';
import {Project} from '../../../model/project/project';
import {ProjectService} from '../../../services/project/project.service';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ModalRenderCheckComponent} from '../modal-render-check/modal-render-check.component';
import {ModalRenderPettyCashCheckComponent} from '../modal-render-petty-cash-check/modal-render-petty-cash-check.component';
import {ModalRenderPettyCashInvoiceComponent} from '../modal-render-petty-cash-invoice/modal-render-petty-cash-invoice.component';
import {ModalViewRenderCheckComponent} from '../modal-view-render-check/modal-view-render-check.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ModalViewRenderPettyCashCheckComponent} from '../modal-view-render-petty-cash-check/modal-view-render-petty-cash-check.component';
import {ModalViewRenderPettyCashInvoiceComponent} from '../modal-view-render-petty-cash-invoice/modal-view-render-petty-cash-invoice.component';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-render',
  templateUrl: './oriana-render.component.html',
  styleUrls: ['./oriana-render.component.css']
})
export class OrianaRenderComponent implements OnInit {
  progress = false;
  title = ' ';
  routerBack = '/invoice';
  idPeriod: number;
  pettyCash = {
    totalPettyCash: 0,
    allInvoices: [],
    allChecks: [],
    totalInvoices: 0,
    totalChecks: 0
  };
  projects: Project[] = [];
  period;
  otherRender;
  render: RenderInvoiceCheck[] = [];
  checks: RenderChecks[] = [];
  invoices: ViewInvoice[];
  save = false;
  containers50 = '';

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private invoiceService: InvoiceService,
              private payCheckService: PayCheckService,
              private projectService: ProjectService,
              private routerActivated: ActivatedRoute
  ) {
    this.routerActivated.parent.params.subscribe(
      res => {
        this.idPeriod = res['idPeriod'];
      });
  }

  ngOnInit() {
    this.load();
  }

  load() {
    this.getAllChecks(this.idPeriod);
    this.getAllInvoice(this.idPeriod);
    this.getPettyCash(this.idPeriod);
    this.getProjects();
  }

  setProgress() {
    this.progress = false;
  }

  getAllInvoice(id) {
    this.invoiceService.getInvoiceFormRender(id).subscribe(
      response => {
        this.invoices = <ViewInvoice[]>response['invoice'];
        this.period = response['period'];
      }
    );
  }

  getAllChecks(id) {
    this.payCheckService.getPayCheckRender(id).subscribe(
      res => {
        this.checks = <RenderChecks[]>res['checks'];
        this.render = <RenderInvoiceCheck[]>res['renderInvoices'];
      }
    );
  }

  getPettyCash(id) {
    this.payCheckService.getTotalPettyCash(id).subscribe(
      res => {
        this.pettyCash.totalChecks = res['totalChecks']['totalChecks'];
        this.pettyCash.allChecks = <RenderChecks[]>res['allChecks'];
        this.pettyCash.allInvoices = <ViewInvoice[]>res['allInvoices'];
        this.pettyCash.totalInvoices = res['totalInvoices']['totalInvoices'];
        this.pettyCash.totalPettyCash = res['totalPettyCash']['total'];
        this.otherRender = res['otherRender'];
      }
    );
  }

  getProjects() {
    this.progress = true;
    this.projectService.getOpen().subscribe(
      res => {
        this.projects = <Project[]>res;
        this.progress = false;
      }
    );
  }

  create(check: RenderChecks) {
    const suggest = this.getInvoiceSuggested(check);
    const dialogRef = this.dialog.open(ModalRenderCheckComponent, {
      width: '700px',
      data: new DataRenderCheck(suggest, check, this.projects)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.suggest.forEach(t => {
          if (!this.invoices.includes(t)) {
            this.invoices.push(t);
          }
        });
        this.invoices = diff(this.invoices, result.selected);
        check.invoices = result.selected;
        this.saveAll();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
      }
    });
  }

  viewCheck(check: RenderChecks) {
    const dialogRef = this.dialog.open(ModalViewRenderCheckComponent, {
      width: '700px',
      data: new DataRenderCheck(check.invoices, check, this.projects)
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.deleted(result.id, result.message);
      }
    });
  }

  createRenderPettyCashCheck() {
    const suggest = this.getCheckSuggested();
    const dialogRef = this.dialog.open(ModalRenderPettyCashCheckComponent, {
      width: '400px',
      data: new DataRenderCheckPettyCash(suggest)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.suggest.forEach(t => {
          if (!this.pettyCash.allChecks.includes(t)) {
            this.checks.push(t);
          }
        });
        this.pettyCash.allChecks = result.selected;
        this.checks = diff(this.checks, result.selected);
        this.saveAll();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
      }
    });
  }

  viewPettyCashCheck() {
    const dialogRef = this.dialog.open(ModalViewRenderPettyCashCheckComponent, {
      width: '550px',
      data: new DataRenderCheckPettyCash(this.pettyCash.allChecks)
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.deleted(result.id, result.message);
      }
    });
  }

  createRenderPettyCashInvoice() {
    const dialogRef = this.dialog.open(ModalRenderPettyCashInvoiceComponent, {
      width: '550px',
      data: new DataRenderInvoiceCheckPetty(this.invoices)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.suggest.forEach(t => {
          if (!this.pettyCash.allInvoices.includes(t)) {
            this.invoices.push(t);
          }
        });
        this.pettyCash.allInvoices = result.selected;
        this.invoices = diff(this.invoices, result.selected);
        this.saveAll();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
      }
    });
  }

  viewPettyCashInvoice() {
    const dialogRef = this.dialog.open(ModalViewRenderPettyCashInvoiceComponent, {
      width: '600px',
      data: new DataRenderInvoiceCheckPetty(this.pettyCash.allInvoices)
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.deleted(result.id, result.message);
      }
    });
  }

  deleted(id, message) {
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: message}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.payCheckService.deleteInvoiceOnRender(id).subscribe(
          res => {
            if (res === 'eliminado') {
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            }
            this.load();
          }, error => {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          });
      }
    });
  }

  getInvoiceSuggested(check: RenderChecks) {
    return this.invoices.filter(i => {
      return i.amountTotalInvoice <= check.mountCheck;
    });
  }

  getCheckSuggested() {
    return this.checks.filter(c => {
      return !c.invoices || c.invoices.length == 0;
    });
  }

  saveAll() {
    this.progress = true;
    this.payCheckService.saveRenderInvoice({render: this.getAllRender()}).subscribe(
      res => {
        this.load();
        this.progress = false;
      }
    );
  }

  getAllRender() {
    const s: RenderInvoiceCheck[] = [];
    this.checks.forEach(ch => {
      if (ch.invoices) {
        ch.invoices.forEach(i => {
          s.push(new RenderInvoiceCheck(i.idRender ? i.idRender : null, ch.idPayCheck, i.idInvoice, false, 0, this.idPeriod, i.idProject));
        });
      }
    });
    this.pettyCash.allChecks.forEach(ch => {
      s.push(new RenderInvoiceCheck(ch.idRender ? ch.idRender : null, ch.idPayCheck, null, true, 0, this.idPeriod, null));
    });
    this.pettyCash.allInvoices.forEach(ch => {
      s.push(new RenderInvoiceCheck(ch.idRender ? ch.idRender : null, null, ch.idInvoice, true, 0, this.idPeriod, null));
    });
    return s;
  }

  setClass(evt) {
    evt.offsetWidth < 1000 ? this.containers50 = 'containers50' : this.containers50 = '';
    return this.containers50;
  }

  isDisabled() {
    return _permission().f_fac_render != 1;
  }
}

export class RenderChecks {
  constructor(
    public idRender: number,
    public idPayCheck: number,
    public numberCheck: string,
    public dateCheck: any,
    public mountCheck: number,
    public mountPay: number,
    public fileCheck: string,
    public invoices: ViewInvoice[],
    public idBankPeriod_period: number,
  ) {
  }
}

export class RenderInvoiceCheck {
  constructor(
    public idRender: number,
    public idCheck: number,
    public idInvoice: number,
    public onPettyCash: boolean,
    public mountRender: number,
    public idPeriod: number,
    public idProject: number
  ) {
  }
}

export class ViewInvoice {
  constructor(
    public idRender: number,
    public idCheck: number,
    public idPeriod: number,
    public idProject: number,
    public idInvoice: number,
    public dateInvoice: any,
    public nitInvoice: string,
    public nameInvoice: string,
    public numberInvoice: any,
    public numberAutoInvoice: string,
    public amountTotalInvoice: number,
    public amountNotCreditInvoice: number,
    public subTotalInvoice: number,
    public discountInvoice: number,
    public amountCreditInvoice: number,
    public fiscalCreditInvoice: number,
    public codeControlInvoice: string,
    public typeBuyInvoice: any,
  ) {
  }
}

export class DataRenderCheck {
  constructor(public invoices: ViewInvoice[], public check: RenderChecks, public project: Project[]) {
  }
}

export class DataRenderCheckPettyCash {
  constructor(public checks: RenderChecks[]) {
  }
}

export class DataRenderInvoiceCheckPetty {
  constructor(public invoices: ViewInvoice[]) {
  }
}

export function diff(a, b) {
  return a.filter(function (i) {
    return b.indexOf(i) === -1;
  });
}
