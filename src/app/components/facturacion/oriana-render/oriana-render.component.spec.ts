import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaRenderComponent} from './oriana-render.component';

describe('OrianaRenderComponent', () => {
  let component: OrianaRenderComponent;
  let fixture: ComponentFixture<OrianaRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaRenderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
