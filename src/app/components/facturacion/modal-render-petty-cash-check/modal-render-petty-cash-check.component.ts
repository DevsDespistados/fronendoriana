import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {RenderChecks} from '../oriana-render/oriana-render.component';
import {SelectionModel} from '@angular/cdk/collections';
import {Project} from '../../../model/project/project';
import {searchText} from '../../reports/template-acconts-reports/template-acconts-reports.component';

@Component({
  selector: 'app-modal-render-petty-cash-check',
  templateUrl: './modal-render-petty-cash-check.component.html',
  styleUrls: ['./modal-render-petty-cash-check.component.css']
})
export class ModalRenderPettyCashCheckComponent implements OnInit {
  text = ConfText;
  titulo = 'Cheques';
  progress = false;
  displayedColumns: string[] = [
    'select',
    'position',
    'dateCheck',
    'numberCheck',
    'mountCheck'
  ];
  checks = new MatTableDataSource<RenderChecks>([]);
  selection = new SelectionModel<RenderChecks>(true, []);
  sortedData: RenderChecks[];
  auxItems = new MatTableDataSource<RenderChecks>([]);
  total: number;
  projects: Project[] = [];

  constructor(
    public dialogRef: MatDialogRef<ModalRenderPettyCashCheckComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) {
    this.checks = new MatTableDataSource<RenderChecks>(this.data.checks);
    this.auxItems = new MatTableDataSource<RenderChecks>(this.data.checks);
    this.selection = new SelectionModel<RenderChecks>(true, []);
  }


  ngOnInit() {
    this.load();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.checks.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.checks.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: RenderChecks): string {
    if (!row) {
      return this.isAllSelected() ? 'select' : 'deselect';
    }
    return this.selection.isSelected(row) ? 'deselect' : 'select';
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close({
      selected: this.selection.selected,
      suggest: this.checks.data,
    });
  }

  valid(): boolean {
    return this.selection.selected.length == 0;
  }

  create() {
    this.dialogRef.close(true);
  }

  totalSelect() {
    let total = 0;
    this.selection.selected.forEach(i => {
      total += i.mountCheck;
    });
    this.total = total;
    return total;
  }

  load() {
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.checks.data) {
        this.checks.data.forEach(t => {
          if (searchText(t.numberCheck, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.checks = new MatTableDataSource<RenderChecks>(this.sortedData);
      this.progress = false;
    } else {
      this.checks = this.auxItems;
      this.progress = false;
    }
  }
}
