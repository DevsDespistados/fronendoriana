import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalRenderPettyCashCheckComponent} from './modal-render-petty-cash-check.component';

describe('ModalRenderPettyCashCheckComponent', () => {
  let component: ModalRenderPettyCashCheckComponent;
  let fixture: ComponentFixture<ModalRenderPettyCashCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalRenderPettyCashCheckComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRenderPettyCashCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
