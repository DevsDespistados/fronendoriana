import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalRenderCheckComponent} from './modal-render-check.component';

describe('ModalRenderCheckComponent', () => {
  let component: ModalRenderCheckComponent;
  let fixture: ComponentFixture<ModalRenderCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalRenderCheckComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRenderCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
