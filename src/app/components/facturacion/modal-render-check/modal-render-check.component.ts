import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {RenderChecks, ViewInvoice} from '../oriana-render/oriana-render.component';
import {searchText} from '../../reports/template-acconts-reports/template-acconts-reports.component';
import {Project} from '../../../model/project/project';

@Component({
  selector: 'app-modal-render-check',
  templateUrl: './modal-render-check.component.html',
  styleUrls: ['./modal-render-check.component.css']
})
export class ModalRenderCheckComponent implements OnInit {
  text = ConfText;
  titulo = 'Facturas';
  progress = false;
  displayedColumns: string[] = [
    'select',
    'position',
    'nameInvoice',
    'numberInvoice',
    'codeControlInvoice',
    'amountTotalInvoice',
    'project'
  ];
  invoices = new MatTableDataSource<ViewInvoice>([]);
  selection = new SelectionModel<ViewInvoice>(true, []);
  sortedData: ViewInvoice[];
  checkSelect: RenderChecks;
  auxItems = new MatTableDataSource<ViewInvoice>([]);
  total: number;
  projects: Project[] = [];

  constructor(
    public dialogRef: MatDialogRef<ModalRenderCheckComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) {
    this.projects = data.project;
    this.invoices = new MatTableDataSource<ViewInvoice>(this.data.invoices);
    this.auxItems = new MatTableDataSource<ViewInvoice>(this.data.invoices);
    // this.selection = new SelectionModel<ViewInvoice>(true, this.data.check.invoices);
    this.checkSelect = data.check;
  }


  ngOnInit() {
    this.load();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.invoices.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.invoices.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: ViewInvoice): string {
    if (!row) {
      return this.isAllSelected() ? 'select' : 'deselect';
    }
    return this.selection.isSelected(row) ? 'deselect' : 'select';
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close({
      selected: this.selection.selected,
      suggest: this.invoices.data,
    });
  }

  valid(): boolean {
    return this.selection.selected.length == 0 || this.total > this.checkSelect.mountCheck;
  }

  create() {
    this.dialogRef.close(true);
  }

  totalSelect() {
    let total = 0;
    this.selection.selected.forEach(i => {
      total += i.amountTotalInvoice;
    });
    this.total = total;
    return total;
  }

  load() {
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.invoices.data) {
        this.invoices.data.forEach(t => {
          if (searchText(t.nameInvoice, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.invoices = new MatTableDataSource<ViewInvoice>(this.sortedData);
      this.progress = false;
    } else {
      this.invoices = this.auxItems;
      this.progress = false;
    }
  }
}
