import {Component, Inject} from '@angular/core';
import {ModalPersonalInterface} from '../../personal/modal-personal-interface';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProjectMdf} from '../../../model/project/project-mdf';
import {moment} from '../../../services/GlobalFunctions';
import {FileInput} from 'ngx-material-file-input';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-modal-modification',
  templateUrl: './modal-modification.component.html',
  styleUrls: ['./modal-modification.component.css']
})
export class ModalModificationComponent implements ModalPersonalInterface {

  text = ConfText;
  formData;
  modSelect: ProjectMdf;
  idProject;

  constructor(
    public dialogRef: MatDialogRef<ModalModificationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.modSelect = this.data.modif;
    this.idProject = this.data.idProject;
  }

  closeDialog() {
    console.log(this.modSelect);
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.dialogRef.close(this.modSelect);
    }
  }

  validData() {
    let res = false;
    switch (this.modSelect.TypeModification) {
      case 'OT': {
        if (
          this.modSelect.dateModification === null || this.modSelect.dateModification === '' ||
          this.modSelect.documentModification === null || this.modSelect.documentModification === '' ||
          this.modSelect.descriptionModification === null || this.modSelect.descriptionModification === ''
        ) {
          res = true;
        }
        break;
      }
      default: {
        if (this.modSelect.dateModification === null || this.modSelect.dateModification === '' ||
          this.modSelect.amountModification === null || this.modSelect.amountModification < 0 ||
          this.modSelect.daysModification === null || this.modSelect.daysModification < 0 ||
          this.modSelect.documentModification === null || this.modSelect.documentModification === ''
        ) {
          res = true;
        }
        break;
      }
    }
    return res;
  }

  ver(rr) {
    console.log(rr);
    console.log(this.modSelect);
  }

}
