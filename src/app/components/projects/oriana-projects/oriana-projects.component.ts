import {Component, Input, OnInit} from '@angular/core';
import {OrianaInterface} from '../oriana-interface';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {ViewProjects} from '../../../model/project/view-projects';
import {PrintServiceService} from '../../../services/print-service.service';
import {ProjectService} from '../../../services/project/project.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {Project} from '../../../model/project/project';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ModalProjectComponent} from '../modal-project/modal-project.component';
import {compareDate, compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-projects',
  templateUrl: './oriana-projects.component.html',
  styleUrls: ['./oriana-projects.component.css']
})
export class OrianaProjectsComponent implements OnInit, OrianaInterface {
  @Input() height: number;
  projects: ViewProjects[];
  projectSelect = new Project(
    0,
    '',
    '',
    null,
    null,
    null,
    null,
    '',
    '',
    null,
    0,
    0,
    true,
    true,
    0,
    null,
    null,
    null,
    null,
    0,
    1,
    null,
    null,
    1,
    1
  );
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'contract',
    'name',
    'status',
    'mount',
    'limit',
    'endDate',
    'elapsed',
    'advance',
    'options'
  ];
  sortedData: ViewProjects[];
  options = [
    new OptionOrianaTableToolbar('NI - NO INICIADOS', 'NI'),
    new OptionOrianaTableToolbar('EJ - EN EJECUCION', 'EJ'),
    new OptionOrianaTableToolbar('RP - EN RECEPCION PROVISIONAL', 'RP'),
    new OptionOrianaTableToolbar('RD - EN RECEPCION DEFINITIVA', 'RD'),
    new OptionOrianaTableToolbar('RZ - REZAGADOS', 'RZ'),
    new OptionOrianaTableToolbar('TODOS', 'all')
  ];
  tooltip = Tooltips;
  currentOption = 'all';
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private projectService: ProjectService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalProjectComponent, {
      width: '400px',
      data: new DataProject(this.projectSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.projectSelect.idProject) {
          this.projectService.addProject(result, this.projectSelect.idClient)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          this.projectService.editProject(this.projectSelect.idProject, result, this.projectSelect.idClient)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.projectSelect = <any>this.projects[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.projectSelect.nameProject}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.projectService.deleteProject(this.projectSelect.idProject)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const p = this.projects[pos];
    this.projectSelect = new Project(
      p.idProject,
      p.nroContractProject,
      p.nameProject,
      p.amountInitProject,
      p.amountProject,
      p.limitProject,
      p.limitUpdateProject,
      p.stateProject,
      p.fileProject,
      null,
      p.daysPassed,
      p.physicalAdvanceProject,
      p.activeProject,
      p.activeProjectForForms,
      null,
      p.startDateOP,
      p.dateRP,
      p.limitRPtoRDRP,
      p.dateRP,
      p.idClient,
      p.activeProjectAccount,
      p.descriptionRD,
      p.codeLetterProject,
      p.activeLetterForProject,
      p.activeProjectForTicket
    );
    this.create();
  }

  getFilter(type: any) {
    this.projectService.filterProjectStates(type).subscribe(
      res => {
        this.progress = false;
        this.projects = <ViewProjects[]>res;
      },
      error => {
        this.progress = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.projectService.getProjects({state: this.currentOption}).subscribe(
      res => {
        this.projects = <ViewProjects[]>res;
        this.progress = false;
      },
      error => {
        this.progress = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NRO CONTRATO', 'NOMBRE', 'ESTADO', 'MONTO', 'PLAZO', 'F. FINAL', '% TRANS', '% AVANCE FISICO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PROYECTOS', head, body, null,
      {
        0: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        7: {
          halign: 'right',
        },
        8: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.projects.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nroContractProject.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      d.push(c.stateProject.toUpperCase());
      d.push(this.printService.moneyData(c.amountProject));
      d.push(c.limitUpdateProject);
      d.push(this.printService.dateData(c.endDate).toUpperCase());
      d.push(c.daysPassed);
      d.push(this.printService.moneyData(c.physicalAdvanceProject));
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.projectService.searchProject(text).subscribe(
        response => {
          this.projects = <ViewProjects[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.projects.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.projects = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'nroContractProject':
          return compareString(a.nroContractProject, b.nroContractProject, isAsc);
        case 'statesProject':
          return compareString(a.stateProject, b.stateProject, isAsc);
        case 'limitUpdateProject':
          return compareNumber(a.limitUpdateProject, b.limitUpdateProject, isAsc);
        case 'endDate':
          return compareDate(a.endDate, b.endDate, isAsc);
        case 'amountProject':
          return compareNumber(a.amountProject, b.amountProject, isAsc);
        case 'daysPassed':
          return compareNumber(a.daysPassed, b.daysPassed, isAsc);
        case 'physicalAdvanceProject':
          return compareNumber(a.physicalAdvanceProject, b.physicalAdvanceProject, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  resetSelect() {
    this.projectSelect = new Project(
      null,
      null,
      null,
      null,
      0,
      null,
      null,
      null,
      null,
      null,
      null,
      0,
      true,
      true,
      null,
      null,
      null,
      null,
      null,
      0,
      1,
      '',
      null,
      1,
      1);
  }

  isDisabled() {
    return _permission().pj_project != 1;
  }

  isDisabledTracing() {
    return _permission().pj_tracing == 0;
  }

  getRouter(ele: ViewProjects) {
    if (!this.isDisabledTracing()) {
      return ['/projects/' + ele.idProject + '/tracing'];
    }
  }
}

export class DataProject {
  constructor(public project: Project) {
  }
}
