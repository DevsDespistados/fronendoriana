import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaProjectsComponent} from './oriana-projects.component';

describe('OrianaProjectsComponent', () => {
  let component: OrianaProjectsComponent;
  let fixture: ComponentFixture<OrianaProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaProjectsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
