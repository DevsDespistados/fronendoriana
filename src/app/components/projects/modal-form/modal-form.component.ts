import {Component, Inject} from '@angular/core';
import {ModalPersonalInterface} from '../../personal/modal-personal-interface';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProjectSpSheet} from '../../../model/project/project-sp-sheet';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.css']
})
export class ModalFormComponent implements ModalPersonalInterface {

  text = ConfText;
  spreadSheetSelect: ProjectSpSheet;
  idProject;

  constructor(
    public dialogRef: MatDialogRef<ModalFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.spreadSheetSelect = this.data.form;
    this.idProject = this.data.idProject;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.dialogRef.close(this.spreadSheetSelect);
    }
  }

  validData() {
    let res = false;
    switch (this.spreadSheetSelect.TypeForm) {
      case 'AN': {
        if (this.spreadSheetSelect.dateForm === null || this.spreadSheetSelect.dateForm === '' ||
          this.spreadSheetSelect.documentForm === null || this.spreadSheetSelect.documentForm === '' ||
          this.spreadSheetSelect.liquidPayableForm === null || this.spreadSheetSelect.liquidPayableForm < 0 ||
          this.spreadSheetSelect.descriptionForm === null || this.spreadSheetSelect.descriptionForm === '') {
          res = true;
        }
        break;
      }
      default: {
        if (this.spreadSheetSelect.documentForm === null || this.spreadSheetSelect.documentForm === '' ||
          this.spreadSheetSelect.liquidPayableForm === null || this.spreadSheetSelect.liquidPayableForm < 0 ||
          this.spreadSheetSelect.descriptionForm === null || this.spreadSheetSelect.descriptionForm === '' ||
          this.spreadSheetSelect.advanceDiscountForm === null || this.spreadSheetSelect.advanceDiscountForm < 0 ||
          this.spreadSheetSelect.amountForm === null || this.spreadSheetSelect.amountForm < 0 ||
          this.spreadSheetSelect.tillDateForm === null || this.spreadSheetSelect.tillDateForm === '' ||
          this.spreadSheetSelect.sinceDateForm === null || this.spreadSheetSelect.sinceDateForm === '' ||
          this.spreadSheetSelect.dateForm === null || this.spreadSheetSelect.dateForm === '') {
          return true;
        }
      }
    }
    return res;
  }

  ver() {
    if (this.spreadSheetSelect.TypeForm != 'AN') {
      this.spreadSheetSelect.liquidPayableForm = this.spreadSheetSelect.amountForm - this.spreadSheetSelect.advanceDiscountForm;
    }
  }

}
