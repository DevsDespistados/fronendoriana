import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-home-project',
  templateUrl: './oriana-home-project.component.html',
  styleUrls: ['./oriana-home-project.component.css']
})
export class OrianaHomeProjectComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PROYECTOS'
  };

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

}
