import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeProjectComponent} from './oriana-home-project.component';

describe('OrianaHomeProjectComponent', () => {
  let component: OrianaHomeProjectComponent;
  let fixture: ComponentFixture<OrianaHomeProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
