import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ProjectSpSheet} from '../../../model/project/project-sp-sheet';
import {ProjectSpreadsheetService} from '../../../services/project/project-spreadsheet.service';
import {ModalFormComponent} from '../modal-form/modal-form.component';
import {ActivatedRoute} from '@angular/router';
import {compareDate, compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-forms-project',
  templateUrl: './forms-project.component.html',
  styleUrls: ['./forms-project.component.css']
})
export class FormsProjectComponent implements OnInit {

  idProject;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'type',
    'doc',
    'date',
    'since',
    'till',
    'amount',
    'advance',
    'liquid',
    'pay',
    'balance',
    'options'
  ];
  options = [
    new OptionOrianaTableToolbar('AN - PLANILLA DE ANTICIPO', 'AN'),
    new OptionOrianaTableToolbar('PA - PLANILLA DE AVANCE', 'PA'),
    new OptionOrianaTableToolbar('PC - PLANILLA DE CIERRE', 'PC'),
  ];
  sortedData: ProjectSpSheet[];

  tooltip = Tooltips;
  projectSS: ProjectSpSheet[];
  total: TotalForms = new TotalForms(0, 0, 0);
  totalRes: TotalRes = new TotalRes(0, 0, 0, 0, 0, 0);
  spreadSheetSelect: ProjectSpSheet;
  routeBack = '/projects';

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private sSheeet: ProjectSpreadsheetService,
              private activateRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activateRoute.parent.params.subscribe(params => {
      this.idProject = params['id'];
      this.loadProject();
    });
  }

  createProject(event) {
    this.spreadSheetSelect.TypeForm = this.options[event].url;
    const dialogRef = this.dialog.open(ModalFormComponent, {
      width: '400px',
      maxHeight: '600px',
      data: new DataProjectForm(this.spreadSheetSelect, this.idProject, this.options[event].option)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.spreadSheetSelect.idProjectForm) {
          this.sSheeet.addSpSheet(result)
            .subscribe(
              formData => {
                this.loadProject();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                console.log(error);
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          this.sSheeet.editSpSheet(this.spreadSheetSelect.idProjectForm, result)
            .subscribe(
              formData => {
                this.loadProject();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleteProject(pos: number) {
    this.spreadSheetSelect = <any>this.projectSS[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.spreadSheetSelect.TypeForm}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sSheeet.deleteProjectSpSheet(this.spreadSheetSelect.idProjectForm)
          .subscribe(
            formData => {
              this.loadProject();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  editProject(pos: number) {
    const ss = this.projectSS[pos];
    this.spreadSheetSelect = new ProjectSpSheet(
      ss.idProjectForm,
      ss.TypeForm,
      ss.documentForm,
      ss.dateForm,
      ss.sinceDateForm,
      ss.tillDateForm,
      ss.amountForm,
      ss.advanceDiscountForm,
      ss.liquidPayableForm,
      ss.descriptionForm,
      ss.fileForm,
      ss.mountPayAux,
      ss.balanceAux,
      ss.projects_idProject);
    this.options.forEach((res, index) => {
      if (res.url == this.spreadSheetSelect.TypeForm) {
        this.createProject(index);
        return;
      }
    });
  }

  getFilterProject(type: any) {
    this.sSheeet.getProjectSpSheet(type).subscribe(
      res => {
        this.total = res['totalForm'];
        this.totalRes = res['totals'];
        this.projectSS = <ProjectSpSheet[]>res['forms'];
      }, error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});

      }
    );
  }

  isEmpty(): any {
  }

  loadProject() {
    this.getFilterProject(this.idProject);
    this.resetSelect();
  }

  printReport() {
  }

  searchProject(text: string) {
  }

  sortData(sort: Sort): any {
    const data = this.projectSS.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.projectSS = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'TypeForm':
          return compareString(a.TypeForm, b.TypeForm, isAsc);
        case 'documentForm':
          return compareString(a.documentForm, b.documentForm, isAsc);
        case 'dateForm':
          return compareDate(a.dateForm, b.dateForm, isAsc);
        case 'sinceDateForm':
          return compareDate(a.sinceDateForm, b.sinceDateForm, isAsc);
        case 'tillDateForm':
          return compareDate(a.tillDateForm, b.tillDateForm, isAsc);
        case 'amountForm':
          return compareNumber(a.amountForm, b.amountForm, isAsc);
        case 'advanceDiscountForm':
          return compareNumber(a.advanceDiscountForm, b.advanceDiscountForm, isAsc);
        case 'liquidPayableForm':
          return compareNumber(a.liquidPayableForm, b.liquidPayableForm, isAsc);
        case 'mountPayAux':
          return compareNumber(a.mountPayAux, b.mountPayAux, isAsc);
        case 'balanceAux':
          return compareNumber(a.balanceAux, b.balanceAux, isAsc);
        default:
          return 0;
      }
    });
  }

  switchProject(type: any) {
  }

  resetSelect() {
    this.spreadSheetSelect = new ProjectSpSheet(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this.idProject);
  }

  isDisabled(form: ProjectSpSheet) {
    return form.mountPayAux != 0 || this.isNewDisabled();
  }

  isNewDisabled() {
    return _permission().pj_forms != 1;
  }
}

export class TotalRes {
  constructor(
    public amountUpdate: number,
    public advanceSol: number,
    public balance: number,
    public advancePhysical: number,
    public advanceReturn: number,
    public financial: number) {
  }
}

export class TotalForms {
  constructor(
    public amountForm: number,
    public advanceDiscountForm: number,
    public liquidPayableForm: number) {
  }
}

export class DataProjectForm {
  constructor(public form: ProjectSpSheet, public idProject: any, public title: string) {
  }
}
