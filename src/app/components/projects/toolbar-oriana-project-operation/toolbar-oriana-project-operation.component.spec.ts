import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ToolbarOrianaProjectOperationComponent} from './toolbar-oriana-project-operation.component';

describe('ToolbarOrianaProjectOperationComponent', () => {
  let component: ToolbarOrianaProjectOperationComponent;
  let fixture: ComponentFixture<ToolbarOrianaProjectOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToolbarOrianaProjectOperationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarOrianaProjectOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
