import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';

@Component({
  selector: 'app-toolbar-oriana-project-operation',
  templateUrl: './toolbar-oriana-project-operation.component.html',
  styleUrls: ['./toolbar-oriana-project-operation.component.css']
})
export class ToolbarOrianaProjectOperationComponent implements OnInit {

  @Input() title: string;
  @Input() routerBack = null;
  @Input() options: OptionOrianaTableToolbar[];
  @Input() onNew = true;
  tooltip = Tooltips;
  @Output() new = new EventEmitter();
  @Output() refresh = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  clickNew(op) {
    this.new.emit(op);
  }

  clickRefresh() {
    this.refresh.emit(true);
  }
}
