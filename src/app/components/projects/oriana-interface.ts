import {Sort} from '@angular/material/sort';

export interface OrianaInterface {
  load();

  search(text: string);

  getFilter(type: any);

  printReport();

  create(params?: any);

  edit(pos: number);

  deleted(pos: number);

  // updateStateProject(state: any, pos: number);

  isEmpty(): any;

  sortData(sort: Sort): any;

  setCurrent(type: any);

  // resetSelect();
}
