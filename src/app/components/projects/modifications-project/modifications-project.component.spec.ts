import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModificationsProjectComponent} from './modifications-project.component';

describe('ModificationsProjectComponent', () => {
  let component: ModificationsProjectComponent;
  let fixture: ComponentFixture<ModificationsProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModificationsProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificationsProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
