import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {ProjectMdf} from '../../../model/project/project-mdf';
import {ProjectMdfService} from '../../../services/project/project-mdf.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ModalModificationComponent} from '../modal-modification/modal-modification.component';
import {ActivatedRoute} from '@angular/router';
import {compareDate, compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-modifications-project',
  templateUrl: './modifications-project.component.html',
  styleUrls: ['./modifications-project.component.css']
})
export class ModificationsProjectComponent implements OnInit {
  @Input() height: number;
  idProject;
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'type',
    'doc',
    'date',
    'days',
    'amount',
    'options'
  ];
  options = [
    new OptionOrianaTableToolbar('OT - ORDEN DE TRABAJO', 'OT'),
    new OptionOrianaTableToolbar('OC - ORDEN DE CAMBIO', 'OC'),
    new OptionOrianaTableToolbar('CM - CONTRATO MODIFICATORIO', 'CM'),
  ];
  sortedData: ProjectMdf[];
  tooltip = Tooltips;
  statusMod = {
    mount: null,
    dateIni: '',
    mountUpdate: null,
    limitContract: 0,
    increment: 0,
    dateEnd: ''
  };
  routeBack = '/projects';
  total: TotalModif = new TotalModif(0, 0);
  modSelect = new ProjectMdf(0, '', '', new Date(), null, null, '', null, this.idProject);
  modifications: ProjectMdf[];

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private projectMdfservice: ProjectMdfService,
              private activateRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activateRoute.parent.params.subscribe(params => {
      this.idProject = params['id'];
      this.loadProject();
    });
  }

  createProject(event) {
    this.modSelect.TypeModification = this.options[event].url;
    const dialogRef = this.dialog.open(ModalModificationComponent, {
      width: '400px',
      maxHeight: '600px',
      data: new DataMod(this.modSelect, this.idProject, this.options[event].option)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.modSelect.idProjectModification) {
          this.projectMdfservice.addModification(result)
            .subscribe(
              formData => {
                this.loadProject();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          this.projectMdfservice.editModification(this.modSelect.idProjectModification, result)
            .subscribe(
              formData => {
                this.loadProject();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleteProject(pos: number) {
    this.modSelect = <any>this.modifications[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.modSelect.TypeModification}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.projectMdfservice.deleteProjectMdf(this.modSelect.idProjectModification)
          .subscribe(
            formData => {
              this.loadProject();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  editProject(pos: number) {
    const mp: ProjectMdf = this.modifications[pos];
    this.modSelect = new ProjectMdf(
      mp.idProjectModification,
      mp.TypeModification,
      mp.documentModification,
      mp.dateModification,
      mp.daysModification,
      mp.amountModification,
      mp.descriptionModification,
      mp.fileModification,
      mp.projects_idProject);
    this.options.forEach((res, index) => {
      if (res.url == this.modSelect.TypeModification) {
        this.createProject(index);
        return;
      }
    });
  }

  getFilterProject(type: any) {
    this.projectMdfservice.getProjectMdf(type).subscribe(
      res => {
        this.modifications = res['modifications'];
        this.total = res['total'];
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  isEmpty(): any {
  }

  loadProject() {
    this.resetSelect();
    this.getFilterProject(this.idProject);
  }

  printReport() {
  }

  sortData(sort: Sort): any {
    const data = this.modifications.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.modifications = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'TypeModification':
          return compareString(a.TypeModification, b.TypeModification, isAsc);
        case 'documentModification':
          return compareString(a.documentModification, b.documentModification, isAsc);
        case 'daysModification':
          return compareNumber(a.daysModification, b.daysModification, isAsc);
        case 'dateModification':
          return compareDate(a.dateModification, b.dateModification, isAsc);
        case 'amountModification':
          return compareNumber(a.amountModification, b.amountModification, isAsc);
        default:
          return 0;
      }
    });
  }

  resetSelect() {
    this.modSelect = new ProjectMdf(0, 'OT', '', new Date(), null, null, '', null, this.idProject);
  }

  isDisabled() {
    return _permission().pj_modifications != 1;
  }
}

export class TotalModif {
  constructor(public days: number, public amount: number) {
  }
}

export class DataMod {
  constructor(public modif: ProjectMdf, public idProject: any, public title: string) {
  }
}

