import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ConfText} from '../../../model/configuration/conf-text';
import {ViewProjects} from '../../../model/project/view-projects';
import {moment} from '../../../services/GlobalFunctions';

@Component({
  selector: 'app-modal-tracing',
  templateUrl: './modal-tracing.component.html',
  styleUrls: ['./modal-tracing.component.css']
})
export class ModalTracingComponent {
  formData;
  project: ViewProjects;
  text = ConfText;
  typeModal;

  constructor(
    public dialogRef: MatDialogRef<ModalTracingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.project = data.project;
    this.typeModal = data.type;
    // this.projectData();
  }

  // ngAfterViewChecked(): void {
  //   this.setTypeModal();
  // }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.projectData();
      // this.formData = new FormData();
      // this.formData.append('name_project', this.project.nameProject);
      // this.formData.append('amountInitProject', this.project.amountInitProject);
      // this.formData.append('nroContract_project', this.project.nroContractProject);
      // this.formData.append('limit_project', this.project.limitUpdateProject);
      // if (file.length > 0) {
      //   this.formData.append('file_project', file[0], file[0].name);
      // } else {
      //   this.formData.append('file_project', null);
      // }
      this.dialogRef.close();
    }
  }

  setTypeModal() {
    console.log(this.typeModal);
    // if (this.project.startDateOP == null || this.project.startDateOP == '') {
    //   this.typeModal = 1;
    // } else if (this.project.dateRP == null || this.project.dateRP == '') {
    this.typeModal = 2;
    // } else if (this.project.dateRD ==  null || this.project.dateRD == '') {
    //   this.typeModal = 3;
    // }
  }

  validData() {
    if (this.project) {
      switch (this.typeModal) {
        case 1: {
          if (this.project.startDateOP == '' || this.project.startDateOP == null) {
            return true;
          }
          break;
        }
        case 2: {
          if (this.project.dateRP == '' || this.project.dateRP == null
            || this.project.limitRPtoRDRP == null || this.project.limitRPtoRDRP == 0) {
            return true;
          }
          break;
        }
        case 3: {
          if (this.project.dateRD == '' || this.project.dateRD == null
            || this.project.descriptionRD == '' || this.project.descriptionRD == null) {
            return true;
          }
          break;
        }
        default:
          return false;
      }
    }
  }

  projectData() {
    this.formData = new ViewProjects(
      this.project.idProject,
      this.project.nroContractProject,
      this.project.nameProject,
      this.project.amountInitProject,
      this.project.limitProject,
      this.project.stateProject,
      this.project.fileProject,
      this.project.activeProject,
      this.project.activeProjectForForms,
      this.project.activeProjectAccount,
      moment(this.project.startDateOP),
      moment(this.project.dateRP),
      this.project.limitRPtoRDRP,
      moment(this.project.dateRD),
      this.project.idClient,
      this.project.descriptionRD,
      this.project.codeLetterProject,
      this.project.activeLetterForProject,
      this.project.activeProjectForTicket,
      this.project.daysPassed,
      this.project.physicalAdvanceProject,
      this.project.limitUpdateProject,
      this.project.endDate,
      this.project.amountProject
    );
  }


}
