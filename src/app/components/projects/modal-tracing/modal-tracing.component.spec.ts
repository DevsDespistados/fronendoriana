import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTracingComponent} from './modal-tracing.component';

describe('ModalTracingComponent', () => {
  let component: ModalTracingComponent;
  let fixture: ComponentFixture<ModalTracingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTracingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTracingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
