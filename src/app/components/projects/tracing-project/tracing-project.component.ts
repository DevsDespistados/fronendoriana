import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../services/project/project.service';
import {ViewProjects} from '../../../model/project/view-projects';
import {ConfText} from '../../../model/configuration/conf-text';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalTracingComponent} from '../modal-tracing/modal-tracing.component';
import {ActivatedRoute} from '@angular/router';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-tracing-project',
  templateUrl: './tracing-project.component.html',
  styleUrls: ['./tracing-project.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TracingProjectComponent implements OnInit {
  idProject: number;
  project: ViewProjects;
  orderToProceed = {
    startDate_ofProject: '',
    workedDayOP_project: 0,
    limitUpdateOP_project: 0,
    daysToWorkOP_project: 0,
    finalDateOP_project: '',
    percenttagePassedOP_project: 0
  };
  receptionProvisional = {
    dateRP: '',
    daysRPtoRD: '',
    workedDaysRP_project: 0,
    daysToWorkRP_project: 0,
    expectedDateRP_project: '',
    percentagePassedRP_project: 0
  };
  receptionDefinitive: ProjectTracing;
  typeStatus = 0;
  routeBack = '/projects';

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private projectService: ProjectService,
              private detectionChanges: ChangeDetectorRef,
              private activateRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activateRoute.parent.params.subscribe(params => {
      this.idProject = params.id;
      this.loadProject();
    });
  }

  loadProject() {
    this.projectService.getOneProject(this.idProject).subscribe(
      data => {
        this.project = <ViewProjects>data['project'];
        this.orderToProceed = data['op'];
        this.receptionProvisional = data['rp'];
        this.receptionDefinitive = data['rd'];
        this.setStatusProject(this.project.stateProject);
        this.detectionChanges.detectChanges();
      },
      error => {
        // this.appService.stopLoader();
      });
  }

  setStatusProject(status) {
    switch (status) {
      case 'NI': {
        this.typeStatus = 0;
        break;
      }
      case 'EJ': {
        this.typeStatus = 1;
        break;
      }
      case 'RP': {
        this.typeStatus = 2;
        break;
      }
      case 'RD': {
        this.typeStatus = 3;
        break;
      }
      case 'RZ': {
        this.typeStatus = 4;
        break;
      }
      case 'RZRP': {
        this.typeStatus = 5;
        break;
      }
    }
  }
  saveDateIni() {
    const dialogRef = this.dialog.open(ModalTracingComponent, {
      width: '400px',
      data: new DataProjectTracing(this.project, 1)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        this.projectService.addDateFollowUpProject(this.idProject, 'EJ',
          {date: this.project.startDateOP, days: null}).subscribe(
          ok => {
            this.loadProject();
            this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          },
          error => {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        );
      }
    });
  }

  saveDateRP() {
    const dialogRef = this.dialog.open(ModalTracingComponent, {
      width: '400px',
      data: new DataProjectTracing(this.project, 2)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        this.projectService.addDateFollowUpProject(this.idProject, 'RP',
          {date: this.project.dateRP, days: this.project.limitRPtoRDRP})
          .subscribe(
            ok => {
              this.loadProject();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
      }
    });
  }

  saveDateRD() {
    const dialogRef = this.dialog.open(ModalTracingComponent, {
      width: '400px',
      data: new DataProjectTracing(this.project, 3)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        this.projectService.addDateFollowUpProject(this.idProject, 'RD',
          {date: this.project.dateRD, details: this.project.descriptionRD})
          .subscribe(
            ok => {
              this.saveDescription(this.project);
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
      }
    });
  }

  saveDescription(description) {
    this.projectService.updateDescription(description).subscribe(
      ok => {
        this.loadProject();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      },
      error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  deleteState(state) {
    this.projectService.deleteState(this.idProject, state)
      .subscribe(
        ok => {
          this.loadProject();
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        },
        error => {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        });
    if (state == 'RD') {
      this.projectService.deleteState(this.idProject, state)
        .subscribe(
          ok => {
            this.loadProject();
            this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          },
          error => {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          });
    }
    if (state === 'RP') {
      this.projectService.deleteState(this.idProject, state).subscribe(
        ok => {
          this.loadProject();
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        },
        error => {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        });
    }
  }

  isDisabled() {
    return _permission().pj_tracing != 1;
  }
}

export class DataProjectTracing {
  constructor(public project: ViewProjects, public type: number) {
  }
}

export interface ProjectTracing {
  start_date: string;
  days: number;
  details: string;
}
