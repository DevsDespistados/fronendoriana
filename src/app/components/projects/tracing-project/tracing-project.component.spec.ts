import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TracingProjectComponent} from './tracing-project.component';

describe('TracingProjectComponent', () => {
  let component: TracingProjectComponent;
  let fixture: ComponentFixture<TracingProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TracingProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
