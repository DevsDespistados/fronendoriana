import {Component, Inject, OnInit} from '@angular/core';
import {ModalPersonalInterface} from '../../personal/modal-personal-interface';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Project} from '../../../model/project/project';
import {Client} from '../../../model/personal/client';
import {ClientService} from '../../../services/personal/client.service';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-project',
  templateUrl: './modal-project.component.html',
  styleUrls: ['./modal-project.component.css']
})
export class ModalProjectComponent implements OnInit {

  text = ConfText;
  project: Project;
  clients: Client[] = [];
  formProject: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<ModalProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private clientService: ClientService,
    private fb: FormBuilder,
    private snack: MatSnackBar) {
    this.formProject = this.fb.group({
      nroContractProject: [null, [Validators.required]],
      nameProject: [null, [Validators.required]],
      idClient: [null, [Validators.required]],
      amountInitProject: [null, [Validators.required]],
      limitProject: [null, [Validators.required]],
      fileProject: [null]
    });
  }

  ngOnInit() {
    if (this.data.project) {
      this.project = this.data.project;
      this.formProject.patchValue(this.project);
    }
    this.loadClients();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  sendFormData() {
    if (this.formProject.valid) {
      this.project = {...this.project, ...this.formProject.value};
      this.dialogRef.close(this.project);
    }
  }

  // validData() {
  //   return this.project.nameProject === null || this.project.nameProject === '' ||
  //     this.project.nroContractProject === null || this.project.nroContractProject === '' ||
  //     this.project.limitUpdateProject === null || this.project.limitUpdateProject < 1 ||
  //     this.project.amountInitProject === null || this.project.amountInitProject < 1 ||
  //     this.project.idClient === null || this.project.idClient < 0;
  // }

  loadClients() {
    this.clientService.getActives(1).subscribe(res => {
      this.clients = <Client[]>res;
    }, error => {
      this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
    });
  }
}
