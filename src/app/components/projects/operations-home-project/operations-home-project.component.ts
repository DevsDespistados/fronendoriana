import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from '../../../services/project/project.service';
import {ViewProjects} from '../../../model/project/view-projects';

@Component({
  selector: 'app-operations-home-project',
  templateUrl: './operations-home-project.component.html',
  styleUrls: ['./operations-home-project.component.css']
})
export class OperationsHomeProjectComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'OPERACIONES',
    tracing: 'SEGUIMIENTO',
    modifications: 'MODIFICACIONES',
    forms: 'PLANILLAS'
  };
  idProject;
  project: ViewProjects;
  navLinks = [];

  constructor(private cdRef: ChangeDetectorRef,
              private activateRoute: ActivatedRoute,
              private projectService: ProjectService) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.idProject = params['id'];
      this.navLinks = [
        {label: 'Seguimiento', path: '/projects/' + this.idProject + '/tracing'},
        {label: 'Modificaciones', path: '/projects/' + this.idProject + '/modifications'},
        {label: 'Planillas', path: '/projects/' + this.idProject + '/forms'},
      ];
      this.getProject();
    });
  }

  getHeight(tab) {
    try {
      return tab._elementRef.nativeElement.offsetHeight;
    } catch (e) {
      console.log('error');
    }
  }

  getProject() {
    this.projectService.getOneProject(this.idProject).subscribe(
      res => {
        this.project = <ViewProjects>res['project'];
        this.titles.module.concat(this.project.nameProject);
      }
    );
  }

  getNameProject() {
    if (this.project) {
      return 'Proyecto: ' + this.project.nameProject;
    } else {
      return 'proyecto: ';
    }
  }
}
