import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OperationsHomeProjectComponent} from './operations-home-project.component';

describe('OperationsHomeProjectComponent', () => {
  let component: OperationsHomeProjectComponent;
  let fixture: ComponentFixture<OperationsHomeProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OperationsHomeProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationsHomeProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
