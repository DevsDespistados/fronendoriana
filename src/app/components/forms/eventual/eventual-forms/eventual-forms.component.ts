import {Component, OnInit} from '@angular/core';
import {ViewProjects} from '../../../../model/project/view-projects';
import {TotalForm} from '../../../../model/total-form';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {EventualForm} from '../../../../model/form/eventual-form';
import {Eventual} from '../../../../model/personal/eventual';
import {EventualFormService} from '../../../../services/form/eventual-form.service';
import {ModalEventualFormComponent} from '../modal-eventual-form/modal-eventual-form.component';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {compareBiweekly, compareNumber} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';
import {PeriodEventualPipe} from '../../../../pipes/period-eventual.pipe';
import {ViewEventualForm} from '../../../../model/form/view-eventual-form';

@Component({
  selector: 'app-eventual-forms',
  templateUrl: './eventual-forms.component.html',
  styleUrls: ['./eventual-forms.component.css'],
  providers: [PeriodEventualPipe]
})
export class EventualFormsComponent implements OnInit {
  eventualForms: EventualForm[] = [];
  eventuals: Eventual[];
  projects: ViewProjects[];
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'form',
    'period',
    'mountTotal',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  sortedData: EventualForm[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 0;

  formSelect: EventualForm;
  newForm: EventualForm;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private periodPipe: PeriodEventualPipe,
              private printService: PrintServiceService,
              private transactionService: TransactionService,
              private eventualFormService: EventualFormService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalEventualFormComponent, {
      width: '400px',
      data: new DataEventualForm(this.newForm)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.newForm.idEventualForm) {
          this.progress = true;
          this.eventualFormService.addEventualForm(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.formSelect = <any>this.eventualForms[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.formSelect.biweekly + '-' + this.formSelect.year}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.eventualFormService.deleteEventualForm(this.formSelect.idEventualForm)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
  }

  getFilter(type: any) {
    this.eventualFormService.getEventualForms(type).subscribe(
      res => {
        this.eventualForms = res['forms'];
        this.total = res['total'];
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'PLANILLA', 'PERIODO', 'MONTO\nTOTAL', 'ANTICIPO', 'LIQ.\nPAGABLE', 'PAGADO', 'SALDO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PLANILLA EVENTUAL', head, body, null,
      {
        0: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        6: {
          halign: 'right',
        },
        7: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.eventualForms.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push((c.biweekly + '-' + c.year).toUpperCase());
      d.push((this.periodPipe.transform(c.biweekly, c.year)).toUpperCase());
      d.push(this.printService.moneyData(c.mountTotal));
      d.push(this.printService.moneyData(c.advanceEventualForm));
      d.push(this.printService.moneyData(c.liquidEventualForm));
      d.push(this.printService.moneyData(c.mountPayAux));
      d.push(this.printService.moneyData(c.balanceAux));
      d.push(c.statusEventualForm == 1 ? 'PAGADO' : 'PENDIENTE');
      data.push(d);
    });
    return data;
  }

  printAll(ele: ViewEventualForm) {
    this.eventualFormService.getAllProjectOfEventualForm(ele.idEventualForm).subscribe(re => {
      console.log(re);
    });
  }
  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.eventualFormService.search(text).subscribe(
        response => {
          this.eventualForms = response['forms'];
          this.total = response['total'];
          this.progress = false;
        }, error => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.eventualForms.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.eventualForms = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'biweekly':
          return compareBiweekly(a.year, a.biweekly, b.year, b.biweekly, isAsc);
        case 'period':
          return compareBiweekly(a.year, a.biweekly, b.year, b.biweekly, isAsc);
        case 'discountEventualForm':
          return compareNumber(a.discountEventualForm, b.discountEventualForm, isAsc);
        case 'advanceEventualForm':
          return compareNumber(a.advanceEventualForm, b.advanceEventualForm, isAsc);
        case 'liquidEventualForm':
          return compareNumber(a.liquidEventualForm, b.liquidEventualForm, isAsc);
        case 'mountTotal':
          return compareNumber(a.mountTotal, b.mountTotal, isAsc);
        case 'mountPayAux':
          return compareNumber(a.mountPayAux, b.mountPayAux, isAsc);
        case 'balanceAux':
          return compareNumber(a.balanceAux, b.balanceAux, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.eventualFormService.setState(this.eventualForms[pos].idEventualForm, {status: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.newForm = new EventualForm(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      0,
      null,
      null,
      null
    );
  }

  isDisabled(ele: EventualForm) {
    return this.isDisabledForm() || ele.mountPayAux > 0;
  }

  isDisabledForm() {
    return _permission().f_eventual != 1;
  }

  idDisabledPlan() {
    return _permission().f_eventual_plan == 0;
  }
}

export class DataEventualForm {
  constructor(public form: EventualForm) {
  }
}
