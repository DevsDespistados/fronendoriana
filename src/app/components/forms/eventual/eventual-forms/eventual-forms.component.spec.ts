import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventualFormsComponent} from './eventual-forms.component';

describe('EventualFormsComponent', () => {
  let component: EventualFormsComponent;
  let fixture: ComponentFixture<EventualFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventualFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventualFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
