import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TotalFormWork} from '../../../../model/total-form-work';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {Router} from '@angular/router';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {EventualFormService} from '../../../../services/form/eventual-form.service';
import {ViewEventualForm} from '../../../../model/form/view-eventual-form';
import {Project} from '../../../../model/project/project';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {compareNumber, compareString} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-eventual-form-consolidate',
  templateUrl: './eventual-form-consolidate.component.html',
  styleUrls: ['./eventual-form-consolidate.component.css']
})
export class EventualFormConsolidateComponent implements OnInit {
  @Input() height;
  @Input() idEventualForm;
  @Output() getForm = new EventEmitter();
  titulo = 'Planillas';
  viewEventuaForm: ViewEventualForm[] = [];
  sortedData: ViewEventualForm[];
  select: ViewEventualForm;
  total: TotalFormWork = new TotalFormWork(0,
    0,
    0,
    0,
    0,
    0,
    0,
    0);
  form;
  options = [];
  displayedColumns = [
    'position',
    'name',
    'sun1',
    'mon1',
    'tue1',
    'wed1',
    'thu1',
    'fri1',
    'sat1',
    'sun2',
    'mon2',
    'tue2',
    'wed2',
    'thu2',
    'fri2',
    'sat2',
    'days',
    'salary',
    'basic',
    'discount',
    'bonus',
    'total',
    'advance',
    'liquid',
    'options'
  ];
  currentOption = 0;
  msg = {msg: 'Planilla Vacia'};
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private eventualFormService: EventualFormService,
              private router: Router) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.progress = true;
    this.eventualFormService.getViewConsolidate(type).subscribe(
      res => {
        this.viewEventuaForm = res['forms'];
        this.total = res['total'];
        this.form = res['form'];
        this.loadOptions(res['projects']);
        this.getForm.emit(this.form);
        this.progress = false;
      }
    );
  }

  loadOnProject(idForm, idProject) {
    this.progress = true;
    this.eventualFormService.getEventualFormByProject(idForm, idProject).subscribe(
      res => {
        this.viewEventuaForm = res['forms'];
        this.total = res['total'];
        this.form = res['form'];
        this.progress = false;
      }, error => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        this.progress = false;

      });
  }

  isEmpty(): any {
  }

  load() {
    if (this.currentOption == 0) {
      this.getFilter(this.idEventualForm);
    } else {
      this.loadOnProject(this.idEventualForm, this.currentOption);
    }
    this.getForm.emit(this.form);
  }

  loadOptions(list: Project[]) {
    this.options = [];
    list.forEach(proj => {
      this.options.push(new OptionOrianaTableToolbar(proj.nameProject, proj.idProject));
    });
    this.options.push(new OptionOrianaTableToolbar('TODOS', 0));
  }

  search(text) {
    if (text !== '') {
      this.progress = true;
      this.eventualFormService.searchEventual(this.idEventualForm, text).subscribe(
        response => {
          this.viewEventuaForm = response['forms'];
          this.total = response['total'];
          this.progress = false;
        });
    } else {
      this.load();
    }
  }

  deleted(pos: number) {
    this.select = <any>this.viewEventuaForm[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.select.nameEventual}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.eventualFormService.deleteEventualOfForm(this.select.idEventual, this.select.idEventualForm).subscribe(
          res => {
            if (res === 'deleted_all') {
              this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: this.msg});
              this.redirect();
            }
            if (res === 'deleted') {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            }
          }, error => {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          });
      } else {
        this.resetSelect();
      }
    });
  }

  resetSelect() {
    this.select = null;
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.load();
  }

  sortData(sort: Sort): any {
    const data = this.viewEventuaForm.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.viewEventuaForm = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameEventual':
          return compareString(a.nameEventual, b.nameEventual, isAsc);
        case 'salaryJornal':
          return compareNumber(a.salaryJornal, b.salaryJornal, isAsc);
        case 'mountTotal':
          return compareNumber(a.mountTotal, b.mountTotal, isAsc);
        case 'totalDays':
          return compareNumber(a.totalDays, b.totalDays, isAsc);
        case 'advance':
          return compareNumber(a.advance, b.advance, isAsc);
        case 'basicMount':
          return compareNumber(a.basicMount, b.basicMount, isAsc);
        case 'bonus':
          return compareNumber(a.bonus, b.bonus, isAsc);
        case 'discount':
          return compareNumber(a.discount, b.discount, isAsc);
        case 'liquidPay':
          return compareNumber(a.liquidPay, b.liquidPay, isAsc);
        default:
          return 0;
      }
    });
  }

  isDisabled() {
    if (this.form) {
      return this.idDisabledPlan() || this.form.mountPayAux > 0 || this.form.statusEventualForm == 1;
    } else {
      return true;
    }
  }

  idDisabledPlan() {
    return _permission().f_eventual_plan == 0;
  }

  print() {
    this.progress = true;
    this.eventualFormService.report({idEventualForm: this.idEventualForm}).subscribe(
      res => {
        this.printService.setDataPrint(res['data']);
        this.progress = false;
      }, error1 => {
        this.progress = false;
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  // print() {
  //   this.progress = true;
  //   const head = [
  //     'NRO',
  //     'NOMBRE',
  //     'D',
  //     'L',
  //     'M',
  //     'M',
  //     'J',
  //     'V',
  //     'S',
  //     'D',
  //     'L',
  //     'M',
  //     'M',
  //     'J',
  //     'V',
  //     'S',
  //     'DIAS',
  //     'JORNAL',
  //     'MONTO\nBASICO',
  //     'DESCUENTO',
  //     'BONO',
  //     'MONTO\nTOTAL',
  //     'ANTICIPO',
  //     'LIQ.\nPAGABLE'];
  //   const body = this.getData();
  //   const report = this.printService.createSingleTable('REPORTE PLANILLA CONSOLIDADA EVENTUAL', head, body, null,
  //     {
  //       0: {
  //         halign  : 'right',
  //       },
  //       2: {
  //         halign  : 'right',
  //       },
  //       3: {
  //         halign  : 'right',
  //       },
  //       4: {
  //         halign  : 'right',
  //       },
  //       5: {
  //         halign  : 'right',
  //       },
  //       6: {
  //         halign  : 'right',
  //       },
  //       7: {
  //         halign  : 'right',
  //       },
  //       8: {
  //         halign  : 'right',
  //       },
  //       9: {
  //         halign  : 'right',
  //       },
  //       10: {
  //         halign  : 'right',
  //       },
  //       11: {
  //         halign  : 'right',
  //       },
  //       12: {
  //         halign  : 'right',
  //       },
  //       13: {
  //         halign  : 'right',
  //       },
  //       14: {
  //         halign  : 'right',
  //       },
  //       15: {
  //         halign  : 'right',
  //       },
  //       16: {
  //         halign  : 'right',
  //       },
  //       17: {
  //         halign  : 'right',
  //       },
  //       18: {
  //         halign  : 'right',
  //       },
  //       19: {
  //         halign  : 'right',
  //       },
  //       20: {
  //         halign  : 'right',
  //       },
  //       21: {
  //         halign  : 'right',
  //       },
  //       22: {
  //         halign  : 'right',
  //       },
  //       23: {
  //         halign  : 'right',
  //       },
  //     });
  //   this.printService.setDataPrint(report);
  // }
  // getData() {
  //   const data = [];
  //   this.viewEventuaForm.forEach((c, index) => {
  //     const d = [];
  //     d.push(index + 1);
  //     d.push(c.nameEventual.toUpperCase());
  //     d.push(c.sun1);
  //     d.push(c.mon1);
  //     d.push(c.tue1);
  //     d.push(c.wed1);
  //     d.push(c.thu1);
  //     d.push(c.fri1);
  //     d.push(c.sat1);
  //     d.push(c.sun2);
  //     d.push(c.mon2);
  //     d.push(c.tue2);
  //     d.push(c.wed2);
  //     d.push(c.thu2);
  //     d.push(c.fri2);
  //     d.push(c.sat2);
  //     d.push(this.printService.moneyData(c.totalDays));
  //     d.push(this.printService.moneyData(c.salaryJornal));
  //     d.push(this.printService.moneyData(c.basicMount));
  //     d.push(this.printService.moneyData(c.discount));
  //     d.push(this.printService.moneyData(c.bonus));
  //     d.push(this.printService.moneyData(c.mountTotal));
  //     d.push(this.printService.moneyData(c.advance));
  //     d.push(this.printService.moneyData(c.liquidPay));
  //     data.push(d);
  //   });
  //   data.push(
  //     [ '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       '',
  //       'TOTAL',
  //       this.printService.moneyData(this.total.discount),
  //       this.printService.moneyData(this.total.bonus),
  //       this.printService.moneyData(this.total.mountTotal),
  //       this.printService.moneyData(this.total.advance),
  //       this.printService.moneyData(this.total.liquidPay),
  //     ]);
  //   return data;
  // }

  redirect() {
    setTimeout(() => {
      this.router.navigate(['/forms/eventual']);
    }, 2000);
  }
}
