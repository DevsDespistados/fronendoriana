import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventualFormConsolidateComponent} from './eventual-form-consolidate.component';

describe('EventualFormConsolidateComponent', () => {
  let component: EventualFormConsolidateComponent;
  let fixture: ComponentFixture<EventualFormConsolidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventualFormConsolidateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventualFormConsolidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
