import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TotalFormWork} from '../../../../model/total-form-work';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {DataDescription, ModalDescriptionFormComponent} from '../../modal-description-form/modal-description-form.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {EventualWorkProjects} from '../../../../model/form/eventual-work-projects';
import {EventualFormService} from '../../../../services/form/eventual-form.service';
import {DetailsWorkEventualWorkForm} from '../../../../model/form/details-work-eventual-work-form';
import {EventualFormBankTransactions} from '../../../../model/form/advancesModel/eventual-form-bank-transactions';
import {ModalEventualAdvancesComponent} from '../modal-eventual-advances/modal-eventual-advances.component';
import {CodeTransaction} from '../../../../model/accounts/ConstTransactions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-evetual-form-planer',
  templateUrl: './evetual-form-planer.component.html',
  styleUrls: ['./evetual-form-planer.component.css']
})
export class EvetualFormPlanerComponent implements OnInit {

  @Input() height;
  @Input() idEventualForm;
  @Input() idEventual;
  @Output() getForm = new EventEmitter();
  titulo = 'PLANILLANDO: ';
  viewEventualWork: EventualWorkProjects[];
  detailsForm: DetailsWorkEventualWorkForm;
  listAdvanceEventual: EventualFormBankTransactions[] = [];
  totalWork = new TotalFormWork(0, 0, 0, 0, 0, 0, 0, 0);
  eventualForm;
  eventual;
  displayedColumns = [
    'position',
    'name',
    'sun1',
    'mon1',
    'tue1',
    'wed1',
    'thu1',
    'fri1',
    'sat1',
    'sun2',
    'mon2',
    'tue2',
    'wed2',
    'thu2',
    'fri2',
    'sat2',
    'days',
    'salary',
    'basic',
    'discount',
    'bonus',
    'total',
    'advance',
    'liquid'
  ];
  onSave = false;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private eventualFormService: EventualFormService,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter() {
    this.progress = true;
    this.eventualFormService.getViewEventualOnProjects(this.idEventual, this.idEventualForm).subscribe(
      res => {
        this.viewEventualWork = res['forms'];
        this.totalWork = res['total'];
        this.eventual = res['eventual'];
        this.eventualForm = res['eventualForm'];
        this.getForm.emit(this.eventualForm);
        this.titulo = 'PLANILLANDO: ' + this.eventual.nameEventual;
        this.progress = false;
      }, error1 => {
      });
    this.getDetails(this.idEventual, this.idEventualForm);
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter();
    this.getAdvances(this.idEventual);
    this.onSave = false;
  }

  calc(workSelect) {
    workSelect.totalDays = (workSelect.sun1 + workSelect.mon1 + workSelect.tue1 +
      workSelect.wed1 + workSelect.thu1 + workSelect.fri1 + workSelect.sat1 +
      workSelect.sun2 + workSelect.mon2 + workSelect.tue2 + workSelect.wed2 +
      workSelect.thu2 + workSelect.fri2 + workSelect.sat2) / 8;
    workSelect.basicMount = ((workSelect.totalDays) * workSelect.salaryJornal);
    workSelect.mountTotal = workSelect.basicMount + workSelect.bonus - workSelect.discount;
    workSelect.liquidPay = (workSelect.mountTotal - workSelect.advance);
    this.totalWork = this.calcTotal(this.viewEventualWork);
    this.onSave = true;
  }

  calcTotal(viewPermanent) {
    const total = new TotalFormWork(0, 0, 0, 0, 0, 0, 0, 0);
    viewPermanent.forEach(viewPer => {
      total.discount += viewPer.discount;
      total.mountTotal += viewPer.mountTotal;
      total.advance += viewPer.advance;
      total.liquidPay += viewPer.liquidPay;
      total.bonus += viewPer.bonus;
      total.salaryMount += viewPer.salaryJornal;
      total.basicMount += viewPer.basicMount;
    });
    return total;
  }

  getDetails(id, idForm) {
    this.eventualFormService.getDetail(id, idForm).subscribe(
      res => {
        this.detailsForm = <DetailsWorkEventualWorkForm>res[0];
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  saveDetails() {
    this.eventualFormService.saveDetail(this.detailsForm.idBonusDiscount, this.detailsForm).subscribe(
      ok => {
        console.log('save d');
      },
      error =>
        console.log('error d')
    );
  }

  getAdvances(id) {
    this.transactionService.getAnticipations(id, CodeTransaction.eventualExpense).subscribe(
      res => {
        this.listAdvanceEventual = <EventualFormBankTransactions[]>res;
      }
    );
  }

  openDescription() {
    const dialogRef = this.dialog.open(ModalDescriptionFormComponent, {
      width: '400px',
      data: new DataDescription(this.detailsForm.BonusDetails)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.description) {
        this.onSave = true;
        this.detailsForm.BonusDetails = dialogRef.componentInstance.description;
      } else if (!this.onSave) {
        this.onSave = false;
      }
    });
  }

  open(work) {
    const dialogRef = this.dialog.open(ModalEventualAdvancesComponent, {
      width: '500px',
      data: new DataEventualFormPlaner(work, this.listAdvanceEventual)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.onSave == true && dialogRef.componentInstance.edited == false) {
        this.calc(work);
      } else if (this.onSave == false && dialogRef.componentInstance.edited == false) {
        this.calc(work);
        this.onSave = false;
      } else if (dialogRef.componentInstance.edited == true) {
        this.calc(work);
      }
    });
  }

  save() {
    this.progress = true;
    this.eventualFormService.saveFormWorkEventual({forms: this.viewEventualWork, advances: this.listAdvanceEventual}).subscribe(
      ok => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        this.onSave = false;
        this.progress = false;
      }, error => {
        this.progress = false;
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
    this.saveDetails();
  }

  isDisabled() {
    return this.isDisabledPlan() || this.eventualForm?.mountPayAux > 0 || this.eventualForm?.statusEventualForm == 1;
  }

  isDisabledPlan() {
    return _permission().f_eventual_plan == 0;
  }
}

export class DataEventualFormPlaner {
  constructor(public form: EventualWorkProjects, public advances: EventualFormBankTransactions[]) {
  }
}
