import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EvetualFormPlanerComponent} from './evetual-form-planer.component';

describe('EvetualFormPlanerComponent', () => {
  let component: EvetualFormPlanerComponent;
  let fixture: ComponentFixture<EvetualFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EvetualFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvetualFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
