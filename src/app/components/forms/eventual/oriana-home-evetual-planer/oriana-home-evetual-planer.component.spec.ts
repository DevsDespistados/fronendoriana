import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeEvetualPlanerComponent} from './oriana-home-evetual-planer.component';

describe('OrianaHomeEvetualPlanerComponent', () => {
  let component: OrianaHomeEvetualPlanerComponent;
  let fixture: ComponentFixture<OrianaHomeEvetualPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeEvetualPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeEvetualPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
