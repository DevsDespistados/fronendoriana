import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EventualForm} from '../../../../model/form/eventual-form';
import {PeriodEventualPipe} from '../../../../pipes/period-eventual.pipe';

@Component({
  selector: 'app-oriana-home-evetual-planer',
  templateUrl: './oriana-home-evetual-planer.component.html',
  styleUrls: ['./oriana-home-evetual-planer.component.css'],
  providers: [PeriodEventualPipe]
})
export class OrianaHomeEvetualPlanerComponent implements OnInit, AfterViewChecked {
  titles = {
    module: 'PLANILLAR - PLANILLA EVENTUAL',
  };
  idForm;
  idEventual;
  form: EventualForm;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute,
              private periodPipe: PeriodEventualPipe) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idP'];
      this.idEventual = res['idEventual'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  getNamePeriod() {
    if (this.form) {
      return 'PLANILLA: ' + this.periodPipe.transform(this.form.biweekly, this.form.year);
    } else {
      return 'PLANILLA: ';
    }
  }

  setEventualForm(value: any) {
    this.form = value;
  }
}
