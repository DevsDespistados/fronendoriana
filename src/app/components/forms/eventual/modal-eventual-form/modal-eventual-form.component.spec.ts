import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEventualFormComponent} from './modal-eventual-form.component';

describe('ModalEventualFormComponent', () => {
  let component: ModalEventualFormComponent;
  let fixture: ComponentFixture<ModalEventualFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEventualFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEventualFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
