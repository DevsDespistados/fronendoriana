import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EventualForm} from '../../../../model/form/eventual-form';
import {actualBiwekly} from '../../../../services/GlobalFunctions';

@Component({
  selector: 'app-modal-eventual-form',
  templateUrl: './modal-eventual-form.component.html',
  styleUrls: ['./modal-eventual-form.component.css'],
})
export class ModalEventualFormComponent {
  text = ConfText;
  formData: EventualForm;
  form: EventualForm;
  year = new Date().getFullYear();
  biwekly = actualBiwekly();

  constructor(
    public dialogRef: MatDialogRef<ModalEventualFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.data.form;

  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.formData = this.form;
      this.formData.year = this.year;
      this.formData.biweekly = this.biwekly;
      this.dialogRef.close();
    }
  }


  validData() {
    return this.year == null || this.year <= 0 || this.biwekly == null || this.biwekly <= 0 || this.biwekly > 26;
  }
}
