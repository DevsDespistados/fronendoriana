import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EventualForm} from '../../../../model/form/eventual-form';
import {PeriodEventualPipe} from '../../../../pipes/period-eventual.pipe';

@Component({
  selector: 'app-oriana-home-eventual-form-consolidate',
  templateUrl: './oriana-home-eventual-form-consolidate.component.html',
  styleUrls: ['./oriana-home-eventual-form-consolidate.component.css'],
  providers: [PeriodEventualPipe],
})
export class OrianaHomeEventualFormConsolidateComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLA CONSOLIDADA - PLANILLA EVENTUAL',
  };
  idForm;
  form: EventualForm;
  idContract = 0;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute,
              private periodPipe: PeriodEventualPipe) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idP'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  getNamePeriod() {
    if (this.form) {
      return 'Periodo: ' + this.periodPipe.transform(this.form.biweekly, this.form.year);
    } else {
      return 'Periodo: ';
    }
  }

  setEventualForm(value: any) {
    this.form = value;
  }
}
