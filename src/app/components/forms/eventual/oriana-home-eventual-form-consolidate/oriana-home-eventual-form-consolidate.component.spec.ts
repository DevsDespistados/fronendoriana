import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeEventualFormConsolidateComponent} from './oriana-home-eventual-form-consolidate.component';

describe('OrianaHomeEventualFormConsolidateComponent', () => {
  let component: OrianaHomeEventualFormConsolidateComponent;
  let fixture: ComponentFixture<OrianaHomeEventualFormConsolidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeEventualFormConsolidateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeEventualFormConsolidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
