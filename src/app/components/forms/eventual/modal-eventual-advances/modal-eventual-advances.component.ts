import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EventualFormBankTransactions} from '../../../../model/form/advancesModel/eventual-form-bank-transactions';
import {EventualWorkProjects} from '../../../../model/form/eventual-work-projects';

@Component({
  selector: 'app-modal-eventual-advances',
  templateUrl: './modal-eventual-advances.component.html',
  styleUrls: ['./modal-eventual-advances.component.css']
})
export class ModalEventualAdvancesComponent {
  listAdvanceEventual: EventualFormBankTransactions[];
  eventualWork: EventualWorkProjects;
  displayedColumns = [
    'position',
    'pay',
    'date',
    'description',
    'recib',
    'mount',
  ];
  tooltip = [
    'Pendiente',
    'Pagado'
  ];
  text = ConfText;
  edited = false;

  constructor(public dialogRef: MatDialogRef<ModalEventualAdvancesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.eventualWork = data.form;
    this.listAdvanceEventual = data.advances.slice();
  }

  closeDialog() {
    this.listAdvanceEventual = this.data.advances;
    this.edited = false;
    this.listAdvanceEventual.forEach(ro => {
      if (!this.isDisabled(ro) && ro.paidOut == 1 && ro.id == null && ro.idCheck != 99999) {
        this.addAdvance(ro, 0);
      }
    });
    this.dialogRef.close();
  }

  sendFormData() {
    this.listAdvanceEventual.forEach(ro => {
      if (ro.paidOut == 1 && !this.isDisabled(ro)) {
        ro.idCheck = 99999;
      }
    });
    this.edited = true;
    this.dialogRef.close();
  }

  addAdvance(ad: EventualFormBankTransactions, state) {
    if (state == 1) {
      ad.idProject = this.eventualWork.idProject;
      ad.paidOut = 1;
      ad.idEventualForm = this.eventualWork.idEventualForm;
      ad.idEventualWorkProject = this.eventualWork.idEventualWorkProject;
      this.eventualWork.advance += (ad.amountTransaction * -1);
    } else {
      ad.idProject = null;
      ad.paidOut = 0;
      this.eventualWork.advance -= (ad.amountTransaction * -1);
    }
  }

  isDisabled(list: EventualFormBankTransactions) {
    if (list && this.eventualWork) {
      if (list.paidOut == 0) {
        return false;
      } else {
        return this.eventualWork.idEventualWorkProject != list.idEventualWorkProject;
      }
    }
  }

  getTotalAssig() {
    let total = 0;
    this.listAdvanceEventual.forEach(ad => {
      if (ad.paidOut == 1 && !this.isDisabled(ad)) {
        total += (ad.amountTransaction * -1);
      }
    });
    return total;
  }
}
