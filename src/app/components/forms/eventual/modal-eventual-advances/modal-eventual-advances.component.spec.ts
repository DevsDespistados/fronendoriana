import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEventualAdvancesComponent} from './modal-eventual-advances.component';

describe('ModalEventualAdvancesComponent', () => {
  let component: ModalEventualAdvancesComponent;
  let fixture: ComponentFixture<ModalEventualAdvancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEventualAdvancesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEventualAdvancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
