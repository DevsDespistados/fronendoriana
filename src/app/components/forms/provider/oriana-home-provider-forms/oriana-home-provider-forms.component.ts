import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ContractProviderService} from '../../../../services/contract/contract-provider.service';
import {ViewContract} from '../../../../model/contract/view-contract';

@Component({
  selector: 'app-oriana-home-provider-forms',
  templateUrl: './oriana-home-provider-forms.component.html',
  styleUrls: ['./oriana-home-provider-forms.component.css']
})
export class OrianaHomeProviderFormsComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLAS PROVEEDOR'
  };
  idContract;
  idProvider;
  contract: ViewContract;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute,
              private contracts: ContractProviderService) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(param => {
      this.idContract = param['idC'];
      this.idProvider = param['idP'];
      this.getContract();
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  getContract() {
    this.contracts.getContract(this.idContract).subscribe(
      res => {
        this.contract = <ViewContract>res;
      }
    );
  }

  getNameContract() {
    if (this.contract) {
      return 'Contrato: ' + this.contract.numberContract;
    } else {
      return 'Contrato: ';
    }
  }
}
