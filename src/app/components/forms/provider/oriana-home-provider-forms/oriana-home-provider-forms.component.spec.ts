import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeProviderFormsComponent} from './oriana-home-provider-forms.component';

describe('OrianaHomeProviderFormsComponent', () => {
  let component: OrianaHomeProviderFormsComponent;
  let fixture: ComponentFixture<OrianaHomeProviderFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeProviderFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeProviderFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
