import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProviderForm} from '../../../../model/form/provider-form';

@Component({
  selector: 'app-oriana-home-provider-form-planer',
  templateUrl: './oriana-home-provider-form-planer.component.html',
  styleUrls: ['./oriana-home-provider-form-planer.component.css']
})
export class OrianaHomeProviderFormPlanerComponent implements OnInit, AfterViewChecked {


  titles = {
    module: 'PLANILLAR - PLANILLAS PROVEEDOR',
    advance: 'AVANCE',
    discount: 'DESCUENTO',
    anticipation: 'ANTICIPO'
  };
  idForm;
  form: ProviderForm;
  idContract = 0;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idPeriod'];
      this.idContract = res['idC'];
    });
  }

  getHeight(tab) {
    try {
      return tab._elementRef.nativeElement.offsetHeight;
    } catch (e) {
      console.log('error');
    }
  }

  getForm(event) {
    this.form = event;
  }

  getNamePeriod() {
    if (this.form) {
      return 'Periodo: ' + this.form.periodProviderForm;
    } else {
      return 'Periodo: ';
    }
  }
}
