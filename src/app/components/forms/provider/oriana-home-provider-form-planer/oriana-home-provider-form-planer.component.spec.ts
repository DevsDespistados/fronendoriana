import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeProviderFormPlanerComponent} from './oriana-home-provider-form-planer.component';

describe('OrianaHomeProviderFormPlanerComponent', () => {
  let component: OrianaHomeProviderFormPlanerComponent;
  let fixture: ComponentFixture<OrianaHomeProviderFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeProviderFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeProviderFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
