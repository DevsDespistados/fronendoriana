import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalProviderFormComponent} from './modal-provider-form.component';

describe('ModalProviderFormComponent', () => {
  let component: ModalProviderFormComponent;
  let fixture: ComponentFixture<ModalProviderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalProviderFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalProviderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
