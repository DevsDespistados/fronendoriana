import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderFormsComponent} from './provider-forms.component';

describe('ProviderFormsComponent', () => {
  let component: ProviderFormsComponent;
  let fixture: ComponentFixture<ProviderFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
