import {Component, Input, OnInit} from '@angular/core';
import {OrianaInterface} from '../../../projects/oriana-interface';
import {ViewContract} from '../../../../model/contract/view-contract';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {HttpErrorResponse} from '@angular/common/http';
import {ProviderForm} from '../../../../model/form/provider-form';
import {ProviderFormService} from '../../../../services/form/provider-form.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {ProviderFormO} from '../../../../model/form/provider-form-o';
import {ModalProviderFormComponent} from '../modal-provider-form/modal-provider-form.component';
import {TotalForm} from '../../../../model/total-form';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {compareNumber, compareString} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-provider-forms',
  templateUrl: './provider-forms.component.html',
  styleUrls: ['./provider-forms.component.css']
})
export class ProviderFormsComponent implements OnInit, OrianaInterface {
  @Input() height: number;
  @Input() idContract: number;
  @Input() idProvider: number;
  forms: ProviderForm[];
  total = new TotalForm(0, 0, 0, 0, 0);
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'advance',
    'discount',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  sortedData: ProviderForm[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 0;

  contract: ViewContract;
  formSelect: ProviderForm;
  newForm: ProviderFormO;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private providerFormService: ProviderFormService,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    console.log('sada');
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalProviderFormComponent, {
      width: '400px',
      data: new DataProviderForm(this.newForm)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.newForm.idProviderForm) {
          this.progress = true;
          this.providerFormService.addProviderForm(dialogRef.componentInstance.formData, this.idContract)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.progress = true;
    this.formSelect = <any>this.forms[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.contract.numberContract}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.providerFormService.deleteProviderForm(this.formSelect.idProviderForm)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
  }

  getFilter(type: any) {
    this.providerFormService.getProviderForms(this.idProvider, this.idContract, type).subscribe(
      res => {
        this.forms = res['forms'];
        this.total = res['total'];
        this.contract = res['contract'];
        this.progress = false;
      }, error1 => {
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'PERIODO', 'AVANCE', 'DECUENTO', 'ANTICIPO', 'LIQ. PAGABLE', 'PAGADO', 'SALDO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PLANILLA CONTRATO ' +
      (this.contract.numberContract.toUpperCase()), head, body, null,
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        6: {
          halign: 'right',
        },
        7: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.forms.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.periodProviderForm.toUpperCase());
      d.push(this.printService.moneyData(c.advanceProviderForm));
      d.push(this.printService.moneyData(c.discountProviderForm));
      d.push(this.printService.moneyData(c.anticipationProviderForm));
      d.push(this.printService.moneyData(c.liquidPayableProviderForm));
      d.push(this.printService.moneyData(c.mountPayAux));
      d.push(this.printService.moneyData(c.balanceAux));
      d.push(c.activeProviderForm == 1 ? 'PAGADO' : 'PENDIENTE');
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.providerFormService.searchProviderForm(text).subscribe(
        response => {
          this.forms = <ProviderForm[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.forms.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.forms = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'periodProviderForm':
          return compareString(a.periodProviderForm, b.periodProviderForm, isAsc);
        case 'advanceProviderForm':
          return compareNumber(a.advanceProviderForm, b.advanceProviderForm, isAsc);
        case 'categoryContract':
          return compareNumber(a.discountProviderForm, b.discountProviderForm, isAsc);
        case 'anticipationProviderForm':
          return compareNumber(a.anticipationProviderForm, b.anticipationProviderForm, isAsc);
        case 'liquidPayableProviderForm':
          return compareNumber(a.liquidPayableProviderForm, b.liquidPayableProviderForm, isAsc);
        case 'mountPayAux':
          return compareNumber(a.mountPayAux, b.mountPayAux, isAsc);
        case 'balanceAux':
          return compareNumber(a.balanceAux, b.balanceAux, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.providerFormService.updateState(this.forms[pos].idProviderForm, {activeProviderForm: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.newForm = new ProviderFormO(
      null,
      null,
      0,
      this.idProvider,
      this.idContract
    );
  }

  isDisabled(ele) {
    return this.isDisabledForm() || ele.mountPayAux > 0;
  }

  isDisabledForm() {
    return _permission().f_provider != 1;
  }

  isDisabledPlan() {
    return _permission().f_provider_plan == 0;
  }
}

export class DataProviderForm {
  constructor(public form: ProviderFormO) {
  }

}
