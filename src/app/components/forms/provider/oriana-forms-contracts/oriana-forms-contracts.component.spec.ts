import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaFormsContractsComponent} from './oriana-forms-contracts.component';

describe('OrianaFormsContractsComponent', () => {
  let component: OrianaFormsContractsComponent;
  let fixture: ComponentFixture<OrianaFormsContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaFormsContractsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaFormsContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
