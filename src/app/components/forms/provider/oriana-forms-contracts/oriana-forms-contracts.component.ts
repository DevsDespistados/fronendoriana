import {Component, Input, OnInit} from '@angular/core';
import {ViewContract} from '../../../../model/contract/view-contract';
import {Category} from '../../../../model/accounts/category';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {ContractProviderService} from '../../../../services/contract/contract-provider.service';
import {ConfText} from '../../../../model/configuration/conf-text';
import {compareString} from '../../../../services/GlobalFunctions';

@Component({
  selector: 'app-oriana-forms-contracts',
  templateUrl: './oriana-forms-contracts.component.html',
  styleUrls: ['./oriana-forms-contracts.component.css']
})
export class OrianaFormsContractsComponent implements OnInit {

  @Input() height: number;
  contracts: ViewContract[];
  categories: Category[];
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'contract',
    'project',
    'provider',
    'category',
    'options'
  ];
  sortedData: ViewContract[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODOS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private contractProviderService: ContractProviderService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.contractProviderService.getContractProviderFilter(type).subscribe(
      res => {
        this.contracts = res['contracts'];
        this.categories = res['categories'];
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }


  printReport() {
    this.progress = true;
    const head = ['NRO', 'NRO CONTRATO', 'PROYECTO', 'PROVEEDOR', 'CATEGORIA', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE CONTRATOS', head, body, null,
      {
        0: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.contracts.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.numberContract.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      d.push(c.nameProvider.toUpperCase());
      d.push(c.categoryContract.toUpperCase());
      d.push(c.activeContract == 1 ? 'DISPONIBLE' : 'NO DISPONIBLE');
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.contractProviderService.searchContract(text).subscribe(
        response => {
          this.contracts = <ViewContract[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.contracts.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.contracts = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'nameProvider':
          return compareString(a.nameProvider, b.nameProvider, isAsc);
        case 'categoryContract':
          return compareString(a.categoryContract, b.categoryContract, isAsc);
        case 'numberContract':
          return compareString(a.numberContract, b.numberContract, isAsc);
        default:
          return 0;
      }
    });
  }
}
