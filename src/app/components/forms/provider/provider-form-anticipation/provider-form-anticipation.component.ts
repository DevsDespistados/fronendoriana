import {AfterViewChecked, Component, Input, OnInit} from '@angular/core';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {ProviderFormBankTransaction} from '../../../../model/form/advancesModel/provider-form-bank-transaction';
import {ProviderFormService} from '../../../../services/form/provider-form.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {TotalItemsForms} from '../../../../model/total-items-forms';
import {ProviderForm} from '../../../../model/form/provider-form';
import {CodeTransaction} from '../../../../model/accounts/ConstTransactions';

@Component({
  selector: 'app-provider-form-anticipation',
  templateUrl: './provider-form-anticipation.component.html',
  styleUrls: ['./provider-form-anticipation.component.css']
})
export class ProviderFormAnticipationComponent implements OnInit, AfterViewChecked {
  @Input() height;
  @Input() idContract;
  @Input() idProviderForm;
  @Input() providerForm: ProviderForm;
  titulo = 'Planillas';
  listAdvanceProvider: ProviderFormBankTransaction[];
  total: TotalItemsForms;
  displayedColumns = [
    'position',
    'pay',
    'date',
    'description',
    'reb',
    'mount'
  ];
  onSave = false;
  tooltip = [
    'Pendiente',
    'Pagado'
  ];

  constructor(private snack: MatSnackBar,
              private transactionService: TransactionService,
              private providerFormService: ProviderFormService) {
  }

  ngOnInit() {
    this.load();
  }

  ngAfterViewChecked(): void {
  }

  load() {
    this.transactionService.getAnticipations(this.idContract, CodeTransaction.providerExpense).subscribe(
      res => {
        this.listAdvanceProvider = <ProviderFormBankTransaction[]>res;
      }
    );
  }

  addAdvance(ad: ProviderFormBankTransaction, state) {
    if (state == 1) {
      ad.paidOut = 1;
      ad.idProviderForm = this.idProviderForm;
    } else {
      ad.paidOut = 0;
    }
    console.log(this.listAdvanceProvider);
    this.onSave = true;
  }

  save() {
    this.providerFormService.addDataItem({advances: this.listAdvanceProvider}).subscribe(
      ok => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        this.onSave = false;
      }, error => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  isDisabled(list: ProviderFormBankTransaction) {
    if (list) {
      if (this.providerForm.activeProviderForm == 1 || this.providerForm.mountPayAux > 0) {
        return true;
      } else {
        if (list.idProviderForm != this.providerForm.idProviderForm && list.paidOut == 1) {
          return true;
        } else {
          return false;
        }
      }
    }
  }
}
