import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderFormAnticipationComponent} from './provider-form-anticipation.component';

describe('ProviderFormAnticipationComponent', () => {
  let component: ProviderFormAnticipationComponent;
  let fixture: ComponentFixture<ProviderFormAnticipationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderFormAnticipationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFormAnticipationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
