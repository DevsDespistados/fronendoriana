import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProviderFormPlanerComponent} from './provider-form-planer.component';

describe('ProviderFormPlanerComponent', () => {
  let component: ProviderFormPlanerComponent;
  let fixture: ComponentFixture<ProviderFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProviderFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
