import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ViewProviderFormItem} from '../../../../model/form/view-provider-form-item';
import {PrintServiceService} from '../../../../services/print-service.service';
import {ProviderFormService} from '../../../../services/form/provider-form.service';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {TotalItemsForms} from '../../../../model/total-items-forms';
import {ProviderForm} from '../../../../model/form/provider-form';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-provider-form-planer',
  templateUrl: './provider-form-planer.component.html',
  styleUrls: ['./provider-form-planer.component.css']
})
export class ProviderFormPlanerComponent implements OnInit {
  @Input() height;
  @Input() idProviderForm;
  @Input() type;
  @Output() getForm = new EventEmitter();
  titulo = 'Planillas';
  items: ViewProviderFormItem[];
  itemSelect: ViewProviderFormItem;
  total = new TotalItemsForms(0, 0, 0);
  providerForm: ProviderForm;
  displayedColumns = [
    'position',
    'item',
    'unity',
    'pu',
    'quality',
    'mount'
  ];
  onSave = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private providerFormService: ProviderFormService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.providerFormService.getItemsForm(type, this.idProviderForm).subscribe(
      res => {
        this.items = <ViewProviderFormItem[]>res['items'];
        this.total = res['total'];
        this.getForm.emit(res['form']);
        this.providerForm = <ProviderForm>res['form'];
        this.onSave = false;
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter(this.type);
  }

  processItem() {
    this.itemSelect.item_actualAmount = this.itemSelect.item_price * this.itemSelect.item_actualQuantity;
    this.itemSelect.item_accumulatedAmount = this.itemSelect.item_actualAmount + this.itemSelect.item_previousAmount;
    this.itemSelect.item_accumulatedQuantity = this.itemSelect.item_actualQuantity + this.itemSelect.item_previousQuantity;
    this.onSave = true;
  }

  selectItem(form) {
    this.itemSelect = form;
  }

  save() {
    this.providerFormService.addDataItem(
      {items: this.items, advances: null}).subscribe(
      ok => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        this.onSave = false;
      }, error => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  isDisabled() {
    return this.isDisabledPlaner() || this.providerForm.mountPayAux > 0 || this.providerForm.activeProviderForm == 1;
  }

  isDisabledPlaner() {
    return _permission().f_provider_plan != 1;
  }
}
