import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EquipmentFormPlanerComponent} from './equipment-form-planer.component';

describe('EquipmentFormPlanerComponent', () => {
  let component: EquipmentFormPlanerComponent;
  let fixture: ComponentFixture<EquipmentFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
