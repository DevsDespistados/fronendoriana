import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {EquipmentFormBankTransacions} from '../../../../model/form/advancesModel/equipment-form-bank-transacions';
import {ModalEquipmentAdvancesComponent} from '../modal-equipment-advances/modal-equipment-advances.component';
import {OwnerEquipmentFormService} from '../../../../services/form/owner-equipment-form.service';
import {ViewOwnerEquipmentForm} from '../../../../model/form/view-owner-equipment-form';
import {OwnerEquipment} from '../../../../model/Equipment/owner-equipment';
import {TotalForm} from '../../../../model/total-form';
import {OwnerEquipmentForm} from '../../../../model/form/owner-equipment-form';
import {__await} from 'tslib';
import {CodeTransaction} from '../../../../model/accounts/ConstTransactions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-equipment-form-planer',
  templateUrl: './equipment-form-planer.component.html',
  styleUrls: ['./equipment-form-planer.component.css']
})
export class EquipmentFormPlanerComponent implements OnInit {

  @Input() height;
  @Input() idEquipmentForm;
  @Input() idEquipment;
  @Output() getForm = new EventEmitter();
  titulo = 'PLANILLANDO';
  viewOwnerEquipmentWork: ViewOwnerEquipmentForm[];
  listAdvanceEquipment: EquipmentFormBankTransacions[] = [];
  totalWork = new TotalForm(0, 0, 0, 0, 0);
  equipmentForm: OwnerEquipmentForm;
  equipment: OwnerEquipment;
  displayedColumns = [
    'position',
    'name',
    'time',
    'unity',
    'price',
    'total',
    'advance',
    'liquid'
  ];
  onSave = false;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private ownerEquipmentFormService: OwnerEquipmentFormService,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter() {
    this.progress = true;
    this.ownerEquipmentFormService.getViewHeavyEquipmentOnProjects(this.idEquipment, this.idEquipmentForm).subscribe(
      res => {
        this.viewOwnerEquipmentWork = res['forms'];
        this.totalWork = res['total'];
        this.equipment = res['owner'];
        this.equipmentForm = res['form'];
        this.progress = false;
        this.titulo = 'PLANILLANDO: ' + this.equipment.nameOwnerEquipment;
        this.getForm.emit(this.equipmentForm);
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter();
    this.getAdvances(this.idEquipment);
    this.onSave = false;
  }

  calc(tr: ViewOwnerEquipmentForm) {
    tr.totalAmount = (tr.timeMonth * tr.priceHour);
    tr.liquidPayableMonth = (tr.totalAmount - tr.anticipationMonth);
    this.totalWork = this.calcTotal(this.viewOwnerEquipmentWork);
    this.onSave = true;
  }

  calcTotal(viewPermanent: ViewOwnerEquipmentForm[]) {
    const total = new TotalForm(0, 0, 0, 0, 0);
    viewPermanent.forEach(viewPer => {
      total.discount += viewPer.discountMonth;
      total.total += viewPer.totalAmount;
      total.advance += viewPer.anticipationMonth;
      total.liquid += viewPer.liquidPayableMonth;
      total.bond += viewPer.bondsMonth;
    });
    return total;
  }

  getAdvances(id) {
    this.transactionService.getAnticipations(id, CodeTransaction.equipmentExpense).subscribe(
      res => {
        this.listAdvanceEquipment = <EquipmentFormBankTransacions[]>res;
      }
    );
  }

  open(work) {
    const dialogRef = this.dialog.open(ModalEquipmentAdvancesComponent, {
      width: '500px',
      data: new DataEquipmentFormPlaner(work, this.listAdvanceEquipment)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.onSave == true && dialogRef.componentInstance.edited == false) {
        this.calc(work);
      } else if (this.onSave == false && dialogRef.componentInstance.edited == false) {
        __await(this.calc(work));
        this.onSave = false;
      } else if (dialogRef.componentInstance.edited == true) {
        this.listAdvanceEquipment = dialogRef.componentInstance.listAdvanceEquipment;
        this.calc(work);
      }
    });
  }

  save() {
    this.progress = true;
    this.ownerEquipmentFormService
      .saveFormWorkHeavyEquipment({forms: this.viewOwnerEquipmentWork, advances: this.listAdvanceEquipment}).subscribe(
      res => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        this.onSave = false;
        this.progress = false;
      }, error => {
        this.progress = false;
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      });
  }

  isDisabled() {
    return this.isDisabledPlan() || this.equipmentForm.mountPayAux > 0 || this.equipmentForm.activeOwnerEquipmentForm == 1;
  }

  isDisabledPlan() {
    return _permission().f_ownerEquipment_plan != 1;
  }
}

export class DataEquipmentFormPlaner {
  constructor(public form: ViewOwnerEquipmentForm, public advances: EquipmentFormBankTransacions[]) {
  }
}
