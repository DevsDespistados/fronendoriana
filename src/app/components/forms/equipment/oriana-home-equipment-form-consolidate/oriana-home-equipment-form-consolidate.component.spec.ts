import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeEquipmentFormConsolidateComponent} from './oriana-home-equipment-form-consolidate.component';

describe('OrianaHomeEquipmentFormConsolidateComponent', () => {
  let component: OrianaHomeEquipmentFormConsolidateComponent;
  let fixture: ComponentFixture<OrianaHomeEquipmentFormConsolidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeEquipmentFormConsolidateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeEquipmentFormConsolidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
