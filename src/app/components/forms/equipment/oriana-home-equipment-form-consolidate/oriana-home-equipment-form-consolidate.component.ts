import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OwnerEquipmentForm} from '../../../../model/form/owner-equipment-form';

@Component({
  selector: 'app-oriana-home-equipment-form-consolidate',
  templateUrl: './oriana-home-equipment-form-consolidate.component.html',
  styleUrls: ['./oriana-home-equipment-form-consolidate.component.css']
})
export class OrianaHomeEquipmentFormConsolidateComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLA CONSOLIDADA - PLANILLA EQUIPO',
  };
  idForm;
  form: OwnerEquipmentForm;
  idContract = 0;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idP'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  setForm(e) {
    this.form = e;
    console.log(e);
  }

  getNamePeriod() {
    if (this.form) {
      return 'Periodo: ' + this.form.periodOwnerEquipmentForm;
    } else {
      return 'Periodo: ';
    }
  }
}
