import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EquipmentFormConsolidateComponent} from './equipment-form-consolidate.component';

describe('EquipmentFormConsolidateComponent', () => {
  let component: EquipmentFormConsolidateComponent;
  let fixture: ComponentFixture<EquipmentFormConsolidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentFormConsolidateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentFormConsolidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
