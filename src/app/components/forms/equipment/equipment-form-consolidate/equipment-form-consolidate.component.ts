import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {Router} from '@angular/router';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {ViewOwnerEquipmentForm} from '../../../../model/form/view-owner-equipment-form';
import {OwnerEquipmentFormService} from '../../../../services/form/owner-equipment-form.service';
import {OwnerEquipmentForm} from '../../../../model/form/owner-equipment-form';
import {TotalForm} from '../../../../model/total-form';
import {compareNumber, compareString} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-equipment-form-consolidate',
  templateUrl: './equipment-form-consolidate.component.html',
  styleUrls: ['./equipment-form-consolidate.component.css']
})
export class EquipmentFormConsolidateComponent implements OnInit {
  @Input() height;
  @Input() idEquipmentForm;
  @Output() getForm = new EventEmitter();
  titulo = 'Planillas';
  viewEquipmentForm: ViewOwnerEquipmentForm[] = [];
  sortedData: ViewOwnerEquipmentForm[];
  select: ViewOwnerEquipmentForm;
  total: TotalForm = new TotalForm(0,
    0,
    0,
    0,
    0);
  form;
  displayedColumns = [
    'position',
    'name',
    'time',
    'unity',
    'price',
    'total',
    'advance',
    'liquid',
    'options'
  ];
  msg = {msg: 'Planilla Vacia'};
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private ownerEquipmentService: OwnerEquipmentFormService,
              private router: Router) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.progress = true;
    this.ownerEquipmentService.getViewConsolidate(type).subscribe(
      res => {
        this.viewEquipmentForm = <ViewOwnerEquipmentForm[]>res['forms'];
        this.total = res['total'];
        this.form = <OwnerEquipmentForm>res['form'];
        this.progress = false;
        this.titulo = 'Planilla';
        this.getForm.emit(this.form);
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.getFilter(this.idEquipmentForm);
  }

  search(text) {
    if (text !== '') {
      this.progress = true;
      this.ownerEquipmentService.searchOwnerEquipment(this.idEquipmentForm, text).subscribe(
        response => {
          this.viewEquipmentForm = <ViewOwnerEquipmentForm[]>response['forms'];
          this.total = response['total'];
          this.progress = false;
          this.titulo = ConfText.resultSearch;
        });
    } else {
      this.load();
    }
  }

  deleted(pos: number) {
    this.select = <any>this.viewEquipmentForm[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.select.nameOwnerEquipment}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.ownerEquipmentService.deleteHeavyEquipmentOnForm(this.select.idOwnerEquipment, this.select.idOwnerEquipmentForm).subscribe(
          res => {
            if (res === 'deleted_all') {
              this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: this.msg});
              this.redirect();
            }
            if (res === 'deleted') {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            }
          }, error => {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            this.progress = false;
          });
      } else {
        this.resetSelect();
      }
    });
  }

  print() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'TIEMPO', 'UNIDAD', 'PRECIO', 'MONTO\nTOTAL', 'ANTICIPO', 'LIQ.\nPAGABLE'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PLANILLA CONSOLIDADA EQUIPO \n PERIODO ' + this.form.periodOwnerEquipmentForm.toUpperCase(), head, body, null,
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        6: {
          halign: 'right',
        },
        7: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.viewEquipmentForm.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameOwnerEquipment.toUpperCase());
      d.push(this.printService.moneyData(c.timeMonth));
      d.push(c.unityCost.toUpperCase());
      d.push(this.printService.moneyData(c.priceHour));
      d.push(this.printService.moneyData(c.totalAmount));
      d.push(this.printService.moneyData(c.anticipationMonth));
      d.push(this.printService.moneyData(c.liquidPayableMonth));
      data.push(d);
    });
    data.push([
      '',
      '',
      '',
      '',
      'TOTAL',
      this.printService.moneyData(this.total.total),
      this.printService.moneyData(this.total.advance),
      this.printService.moneyData(this.total.liquid),
    ]);
    return data;
  }

  sortData(sort: Sort): any {
    const data = this.viewEquipmentForm.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.viewEquipmentForm = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameOwnerEquipment':
          return compareString(a.nameOwnerEquipment, b.nameOwnerEquipment, isAsc);
        case 'priceHour':
          return compareNumber(a.priceHour, b.priceHour, isAsc);
        case 'totalAmount':
          return compareNumber(a.totalAmount, b.totalAmount, isAsc);
        case 'timeMonth':
          return compareNumber(a.timeMonth, b.timeMonth, isAsc);
        case 'unityCost':
          return compareString(a.unityCost, b.unityCost, isAsc);
        case 'costOwnerEquipment':
          return compareNumber(a.costOwnerEquipment, b.costOwnerEquipment, isAsc);
        case 'anticipationMonth':
          return compareNumber(a.anticipationMonth, b.anticipationMonth, isAsc);
        case 'bondsMonth':
          return compareNumber(a.bondsMonth, b.bondsMonth, isAsc);
        case 'discountMonth':
          return compareNumber(a.discountMonth, b.discountMonth, isAsc);
        case 'liquidPayableMonth':
          return compareNumber(a.liquidPayableMonth, b.liquidPayableMonth, isAsc);
        default:
          return 0;
      }
    });
  }

  resetSelect() {
    this.select = null;
  }

  isDisabled() {
    return this.isDisabledPlan() || this.form.mountPayAux > 0 || this.form.activeOwnerEquipmentForm == 1;
  }

  isDisabledPlan() {
    return _permission().f_ownerEquipment_plan != 1;
  }

  redirect() {
    setTimeout(() => {
      console.log('Test');
      this.router.navigate(['/forms/equipment']);
    }, 2000);
  }
}
