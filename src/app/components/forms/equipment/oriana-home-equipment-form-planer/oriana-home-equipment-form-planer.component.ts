import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OwnerEquipmentForm} from '../../../../model/form/owner-equipment-form';

@Component({
  selector: 'app-oriana-home-equipment-form-planer',
  templateUrl: './oriana-home-equipment-form-planer.component.html',
  styleUrls: ['./oriana-home-equipment-form-planer.component.css']
})
export class OrianaHomeEquipmentFormPlanerComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLAR - PLANILLA EQUIPO',
  };
  idForm;
  idEquipment;
  private _form: OwnerEquipmentForm;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idP'];
      this.idEquipment = res['idEquipment'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }


  form(value: OwnerEquipmentForm) {
    this._form = value;
  }

  getNamePermanent() {
    if (this._form) {
      return 'PLANILLA: ' + this._form.periodOwnerEquipmentForm;
    } else {
      return 'PLANILLA: ';
    }
  }
}
