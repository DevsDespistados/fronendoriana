import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeEquipmentFormPlanerComponent} from './oriana-home-equipment-form-planer.component';

describe('OrianaHomeEquipmentFormPlanerComponent', () => {
  let component: OrianaHomeEquipmentFormPlanerComponent;
  let fixture: ComponentFixture<OrianaHomeEquipmentFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeEquipmentFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeEquipmentFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
