import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EquipmentFormBankTransacions} from '../../../../model/form/advancesModel/equipment-form-bank-transacions';
import {ViewOwnerEquipmentForm} from '../../../../model/form/view-owner-equipment-form';
import {__await} from 'tslib';

@Component({
  selector: 'app-modal-equipment-advances',
  templateUrl: './modal-equipment-advances.component.html',
  styleUrls: ['./modal-equipment-advances.component.css']
})
export class ModalEquipmentAdvancesComponent {
  listAdvanceEquipment: EquipmentFormBankTransacions[];
  equipmentWork: ViewOwnerEquipmentForm;
  displayedColumns = [
    'position',
    'pay',
    'date',
    'description',
    'recib',
    'mount',
  ];
  tooltip = [
    'Pendiente',
    'Pagado'
  ];
  text = ConfText;
  edited = false;
  valid = false;

  constructor(public dialogRef: MatDialogRef<ModalEquipmentAdvancesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.equipmentWork = this.data.form;
    this.listAdvanceEquipment = this.data.advances.slice();
  }

  closeDialog() {
    this.listAdvanceEquipment = this.data.advances;
    __await(this.listAdvanceEquipment.forEach(ro => {
      if (this.isDisabled(ro) == false && ro.paidOut == 1 && ro.id == null && ro.idCheck != 99999) {
        this.addAdvance(ro, 0);
        this.edited = false;
      }
    }));
    this.dialogRef.close();
  }

  sendFormData() {
    __await(this.listAdvanceEquipment.forEach(ro => {
      if (ro.paidOut == 1 && !this.isDisabled(ro)) {
        ro.idCheck = 99999;
      }
    }));
    this.edited = true;
    this.dialogRef.close();
  }

  addAdvance(ad: EquipmentFormBankTransacions, state) {
    if (state == 1) {
      ad.idProject = this.equipmentWork.idProject;
      ad.paidOut = 1;
      ad.idEquipmentForm = this.equipmentWork.idOwnerEquipmentForm;
      ad.idEquipmentProjectForm = this.equipmentWork.idOwnerEquipmentProjectForm;
      this.equipmentWork.anticipationMonth += (ad.amountTransaction * -1);
    } else {
      ad.idProject = null;
      ad.paidOut = 0;
      this.equipmentWork.anticipationMonth -= (ad.amountTransaction * -1);
    }
    this.valid = true;
  }

  isDisabled(list: EquipmentFormBankTransacions) {
    if (list && this.equipmentWork) {
      if (list.paidOut == 0) {
        return false;
      } else {
        return this.equipmentWork.idOwnerEquipmentProjectForm != list.idEquipmentProjectForm;
      }
    }
  }

  getTotalAssig() {
    let total = 0;
    this.listAdvanceEquipment.forEach(ad => {
      if (ad.paidOut == 1 && !this.isDisabled(ad)) {
        total += (ad.amountTransaction * -1);
      }
    });
    return total;
  }
}

