import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEquipmentAdvancesComponent} from './modal-equipment-advances.component';

describe('ModalEquipmentAdvancesComponent', () => {
  let component: ModalEquipmentAdvancesComponent;
  let fixture: ComponentFixture<ModalEquipmentAdvancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEquipmentAdvancesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEquipmentAdvancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
