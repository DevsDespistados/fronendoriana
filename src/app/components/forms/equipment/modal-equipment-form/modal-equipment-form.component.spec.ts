import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEquipmentFormComponent} from './modal-equipment-form.component';

describe('ModalEquipmentFormComponent', () => {
  let component: ModalEquipmentFormComponent;
  let fixture: ComponentFixture<ModalEquipmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEquipmentFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEquipmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
