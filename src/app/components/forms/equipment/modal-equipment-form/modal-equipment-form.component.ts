import {Component, Inject} from '@angular/core';
import {ConfText, month} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OwnerEquipmentForm} from '../../../../model/form/owner-equipment-form';
import {moment} from '../../../../services/GlobalFunctions';

@Component({
  selector: 'app-modal-equipment-form',
  templateUrl: './modal-equipment-form.component.html',
  styleUrls: ['./modal-equipment-form.component.css']
})
export class ModalEquipmentFormComponent {
  text = ConfText;
  formData: OwnerEquipmentForm;
  form: OwnerEquipmentForm;
  private date = new Date();
  year = this.date.getFullYear();
  month = this.date.getMonth();
  months = month;

  constructor(
    public dialogRef: MatDialogRef<ModalEquipmentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.data.form;

  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.formData = this.form;
      this.formData.periodOwnerEquipmentForm = moment(new Date(this.year, this.month, 1));
      this.dialogRef.close();
    }
  }


  validData() {
    return this.year == null || this.year <= 0 || this.month == null || this.month < 0;
  }
}
