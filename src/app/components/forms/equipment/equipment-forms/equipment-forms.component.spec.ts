import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EquipmentFormsComponent} from './equipment-forms.component';

describe('EquipmentFormsComponent', () => {
  let component: EquipmentFormsComponent;
  let fixture: ComponentFixture<EquipmentFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EquipmentFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
