import {Component, OnInit} from '@angular/core';
import {ViewProjects} from '../../../../model/project/view-projects';
import {TotalForm} from '../../../../model/total-form';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {OwnerEquipmentFormService} from '../../../../services/form/owner-equipment-form.service';
import {OwnerEquipment} from '../../../../model/Equipment/owner-equipment';
import {ModalEquipmentFormComponent} from '../modal-equipment-form/modal-equipment-form.component';
import {OwnerEquipmentForm} from '../../../../model/form/owner-equipment-form';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {compareNumber} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-equipment-forms',
  templateUrl: './equipment-forms.component.html',
  styleUrls: ['./equipment-forms.component.css']
})
export class EquipmentFormsComponent implements OnInit {
  ownerEquipmentForms: OwnerEquipmentForm[] = [];
  ownerEquipments: OwnerEquipment[];
  projects: ViewProjects[];
  total: TotalForm = new TotalForm(0, 0, 0, 0, 0);
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'mountTotal',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  sortedData: OwnerEquipmentForm[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 0;

  formSelect: OwnerEquipmentForm;
  newForm: OwnerEquipmentForm;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private transactionService: TransactionService,
              private ownerEquipmentFormService: OwnerEquipmentFormService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalEquipmentFormComponent, {
      width: '400px',
      data: new DataOwnerEquipmentForm(this.newForm)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.newForm.idOwnerEquipmentForm) {
          this.ownerEquipmentFormService.addHeavyEquipmentForm(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.formSelect = <any>this.ownerEquipmentForms[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.formSelect.periodOwnerEquipmentForm}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.ownerEquipmentFormService.addHeavyEquipmentForm(this.formSelect.idOwnerEquipmentForm)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.progress = false;
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
  }

  getFilter(type: any) {
    this.ownerEquipmentFormService.getHeavyEquipmentForms(type).subscribe(
      res => {
        this.ownerEquipmentForms = res['forms'];
        this.total = res['total'];
        this.progress = false;
      },
    );
  }

  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'PERIODO', 'MONTO\nTOTAL', 'ANTICIPO', 'LIQ.\nPAGABLE', 'PAGADO', 'SALDO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PLANILLA EQUIPO', head, body, null,
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        6: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.ownerEquipmentForms.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.periodOwnerEquipmentForm.toUpperCase());
      d.push(this.printService.moneyData(c.totalAmount));
      d.push(this.printService.moneyData(c.anticipationOwnerEquipmentForm));
      d.push(this.printService.moneyData(c.liquidPayableOwnerEquipmentForm));
      d.push(this.printService.moneyData(c.mountPayAux));
      d.push(this.printService.moneyData(c.balanceAux));
      d.push(c.activeOwnerEquipmentForm == 1 ? 'PAGADO' : 'PENDIENTE');
      data.push(d);
    });
    // data.push([
    //   '',
    //   '',
    //   this.total.total,
    //   this.total.advance,
    //   this.total.liquid,
    //   this.total.bond,
    // ]);
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.ownerEquipmentFormService.search(text).subscribe(
        response => {
          this.ownerEquipmentForms = response['forms'];
          this.total = response['total'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.ownerEquipmentForms.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.ownerEquipmentForms = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'periodOwnerEquipmentForm':
          return compareNumber(a.periodOwnerEquipmentForm, b.periodOwnerEquipmentForm, isAsc);
        case 'discountOwnerEquipmentForm':
          return compareNumber(a.discountOwnerEquipmentForm, b.discountOwnerEquipmentForm, isAsc);
        case 'anticipationOwnerEquipmentForm':
          return compareNumber(a.anticipationOwnerEquipmentForm, b.anticipationOwnerEquipmentForm, isAsc);
        case 'liquidPayableOwnerEquipmentForm':
          return compareNumber(a.liquidPayableOwnerEquipmentForm, b.liquidPayableOwnerEquipmentForm, isAsc);
        case 'totalAmount':
          return compareNumber(a.totalAmount, b.totalAmount, isAsc);
        case 'mountPayAux':
          return compareNumber(a.mountPayAux, b.mountPayAux, isAsc);
        case 'balanceAux':
          return compareNumber(a.balanceAux, b.balanceAux, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.ownerEquipmentFormService.setState(this.ownerEquipmentForms[pos].idOwnerEquipmentForm, {activeOwnerEquipmentForm: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.newForm = new OwnerEquipmentForm(
      null,
      null,
      0,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  isDisabled(ele) {
    return this.isDisabledNew() || ele.mountPayAux > 0;
  }

  isDisabledNew() {
    return _permission().f_ownerEquipment != 1;
  }

  isDisabledPlan() {
    return _permission().f_ownerEquipment_plan == 0;
  }
}

export class DataOwnerEquipmentForm {
  constructor(public form: OwnerEquipmentForm) {
  }
}

