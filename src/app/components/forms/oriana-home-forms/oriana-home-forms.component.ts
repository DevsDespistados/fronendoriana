import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-forms',
  templateUrl: './oriana-home-forms.component.html',
  styleUrls: ['./oriana-home-forms.component.css']
})
export class OrianaHomeFormsComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLAS',
    provider: 'PROVEEDOR',
    permanent: 'PERMANENTE',
    eventual: 'EVENTUAL',
    equipment: 'EQUIPO PROPIO',
    projects: 'PROYECTOS'
  };

  private links = [
    {label: 'PROVEEDOR', path: '/forms/provider', permission: _permission().f_provider},
    {label: 'PERMANENTE', path: '/forms/permanent', permission: _permission().f_permanent},
    {label: 'EVENTUAL', path: '/forms/eventual', permission: _permission().f_eventual},
    {label: 'EQUIPO PROPIO', path: '/forms/equipment', permission: _permission().f_ownerEquipment},
    {label: 'PROYECTOS', path: '/forms/projects', permission: _permission().f_projects},
  ];
  navLinks = [];

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }

  getHeight(tab) {
    try {
      return tab._elementRef.nativeElement.offsetHeight;
    } catch (e) {
      console.log('error');
    }
  }
}
