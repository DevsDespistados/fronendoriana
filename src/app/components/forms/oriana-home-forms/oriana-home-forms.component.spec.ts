import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrianaHomeFormsComponent } from './oriana-home-forms.component';

describe('OrianaHomeFormsComponent', () => {
  let component: OrianaHomeFormsComponent;
  let fixture: ComponentFixture<OrianaHomeFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrianaHomeFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
