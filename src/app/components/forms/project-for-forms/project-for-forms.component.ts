import {Component, OnInit} from '@angular/core';
import {ViewProjects} from '../../../model/project/view-projects';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {ProjectService} from '../../../services/project/project.service';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {HttpErrorResponse} from '@angular/common/http';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-project-for-forms',
  templateUrl: './project-for-forms.component.html',
  styleUrls: ['./project-for-forms.component.css']
})
export class ProjectForFormsComponent implements OnInit {

  projects: ViewProjects[] = [];
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'contract',
    'name',
    'status',
  ];
  sortedData: ViewProjects[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLE', 0),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  tooltip = ['No Disponible', 'Disponible'];
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private projectService: ProjectService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.projectService.getOpeForm(type).subscribe(
      res => {
        this.progress = false;
        this.projects = <ViewProjects[]>res['projects'];
      },
      error => {
        this.progress = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  load() {
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  update(id, state) {
    this.projectService.updateStateForFormProject(id, state).subscribe(
      response => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        if (this.currentOption !== 2) {
          this.load();
        }
      }, (errorResponse: HttpErrorResponse) => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  search(text: string) {
    if (text !== '') {
      this.projectService.searchProject(text).subscribe(
        response => {
          this.projects = <ViewProjects[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.projects.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.projects = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'nroContractProject':
          return compareString(a.nroContractProject, b.nroContractProject, isAsc);
        case 'activeProjectForForms':
          return compareNumber(a.activeProjectForForms, b.activeProjectForForms, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  isDisabled() {
    return _permission().f_projects != 1;
  }
}
