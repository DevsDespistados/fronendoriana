import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProjectForFormsComponent} from './project-for-forms.component';

describe('ProjectForFormsComponent', () => {
  let component: ProjectForFormsComponent;
  let fixture: ComponentFixture<ProjectForFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectForFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectForFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
