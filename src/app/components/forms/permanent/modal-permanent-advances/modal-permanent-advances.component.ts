import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PermanentFormBankTransactions} from '../../../../model/form/advancesModel/permanent-form-bank-transactions';
import {PermanentWorkProjects} from '../../../../model/form/permanent-work-projects';
import {ConfText} from '../../../../model/configuration/conf-text';

@Component({
  selector: 'app-modal-permanent-advances',
  templateUrl: './modal-permanent-advances.component.html',
  styleUrls: ['./modal-permanent-advances.component.css']
})
export class ModalPermanentAdvancesComponent {
  listAdvancePermanent: PermanentFormBankTransactions[];
  permanentWork: PermanentWorkProjects;
  displayedColumns = [
    'position',
    'pay',
    'date',
    'description',
    'recib',
    'mount',
  ];
  tooltip = [
    'Pendiente',
    'Pagado'
  ];
  text = ConfText;
  edited = false;
  valid = false;

  constructor(public dialogRef: MatDialogRef<ModalPermanentAdvancesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.permanentWork = data.form;
    this.listAdvancePermanent = data.advances.slice();
  }

  closeDialog() {
    this.listAdvancePermanent = this.data.advances;
    this.edited = false;
    this.listAdvancePermanent.forEach(ro => {
      if (!this.isDisabled(ro) && ro.paidOut == 1 && ro.id == null && ro.idCheck != 99999) {
        this.addAdvance(ro, 0);
      }
    });
    this.dialogRef.close();
  }

  sendFormData() {
    this.listAdvancePermanent.forEach(ro => {
      if (ro.paidOut == 1 && !this.isDisabled(ro)) {
        ro.idCheck = 99999;
      }
    });
    this.edited = true;
    this.dialogRef.close();
  }

  addAdvance(ad: PermanentFormBankTransactions, state) {
    if (state == 1) {
      ad.idProject = this.permanentWork.idProject;
      ad.paidOut = 1;
      ad.idPermanentForm = this.permanentWork.idPermanentForm;
      ad.idPermanentWorkProject = this.permanentWork.idPermanentWorkProject;
      this.permanentWork.advance += (ad.amountTransaction * -1);
    } else {
      ad.idProject = null;
      ad.paidOut = 0;
      this.permanentWork.advance -= (ad.amountTransaction * -1);
    }
    this.valid = true;
  }

  isDisabled(list: PermanentFormBankTransactions) {
    if (list && this.permanentWork) {
      if (list.paidOut == 0) {
        return false;
      } else {
        return this.permanentWork.idPermanentWorkProject != list.idPermanentWorkProject;
      }
    }
  }

  getTotalAssig() {
    let total = 0;
    this.listAdvancePermanent.forEach(ad => {
      if (ad.paidOut == 1 && !this.isDisabled(ad)) {
        total += (ad.amountTransaction * -1);
      }
    });
    return total;
  }
}
