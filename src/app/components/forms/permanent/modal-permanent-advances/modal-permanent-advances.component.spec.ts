import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPermanentAdvancesComponent} from './modal-permanent-advances.component';

describe('ModalPermanentAdvancesComponent', () => {
  let component: ModalPermanentAdvancesComponent;
  let fixture: ComponentFixture<ModalPermanentAdvancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPermanentAdvancesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPermanentAdvancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
