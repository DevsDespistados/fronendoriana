import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePermanenteFormConsolidateComponent} from './oriana-home-permanente-form-consolidate.component';

describe('OrianaHomePermanenteFormConsolidateComponent', () => {
  let component: OrianaHomePermanenteFormConsolidateComponent;
  let fixture: ComponentFixture<OrianaHomePermanenteFormConsolidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePermanenteFormConsolidateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePermanenteFormConsolidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
