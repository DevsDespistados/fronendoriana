import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {PermanentForm} from '../../../../model/form/permanent-form';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-oriana-home-permanente-form-consolidate',
  templateUrl: './oriana-home-permanente-form-consolidate.component.html',
  styleUrls: ['./oriana-home-permanente-form-consolidate.component.css']
})
export class OrianaHomePermanenteFormConsolidateComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLA CONSOLIDADA - PLANILLA PERMANENTE',
  };
  idForm;
  form: PermanentForm;
  idContract = 0;

  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idP'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

  setForm(e) {
    this.form = e;
    console.log(e);
  }

  getNamePeriod() {
    if (this.form) {
      return 'Periodo: ' + this.form.periodicalPermanentForm;
    } else {
      return 'Periodo: ';
    }
  }
}
