import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePermanentFormPlanerComponent} from './oriana-home-permanent-form-planer.component';

describe('OrianaHomePermanentFormPlanerComponent', () => {
  let component: OrianaHomePermanentFormPlanerComponent;
  let fixture: ComponentFixture<OrianaHomePermanentFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePermanentFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePermanentFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
