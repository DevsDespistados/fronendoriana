import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PermanentForm} from '../../../../model/form/permanent-form';

@Component({
  selector: 'app-oriana-home-permanent-form-planer',
  templateUrl: './oriana-home-permanent-form-planer.component.html',
  styleUrls: ['./oriana-home-permanent-form-planer.component.css']
})
export class OrianaHomePermanentFormPlanerComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PLANILLAR - PLANILLA PERMANENTE',
  };
  idForm;
  idPermanent;
  private _form: PermanentForm;

  // private _permanent: Permanent;
  constructor(private cdRef: ChangeDetectorRef,
              private activeRoute: ActivatedRoute) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.idForm = res['idP'];
      this.idPermanent = res['idPermanent'];
    });
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }


  form(value: PermanentForm) {
    this._form = value;
  }

  getNamePermanent() {
    if (this._form) {
      return 'PLANILLA: ' + this._form.periodicalPermanentForm;
    } else {
      return 'PLANILLA: ';
    }
  }
}
