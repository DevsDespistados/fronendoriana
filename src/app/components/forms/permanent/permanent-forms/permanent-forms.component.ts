import {Component, OnInit} from '@angular/core';
import {OrianaInterface} from '../../../projects/oriana-interface';
import {TotalForm} from '../../../../model/total-form';
import {OptionOrianaTableToolbar} from '../../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {SnackSuccessComponent} from '../../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../../snack-msg/snack-error/snack-error.component';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {HttpErrorResponse} from '@angular/common/http';
import {PermanentForm} from '../../../../model/form/permanent-form';
import {Permanent} from '../../../../model/personal/permanent';
import {ViewProjects} from '../../../../model/project/view-projects';
import {PermanentFormService} from '../../../../services/form/permanent-form.service';
import {ModalPermanentFormComponent} from '../modal-permanent-form/modal-permanent-form.component';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {compareNumber, compareString} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-permanent-forms',
  templateUrl: './permanent-forms.component.html',
  styleUrls: ['./permanent-forms.component.css']
})
export class PermanentFormsComponent implements OnInit, OrianaInterface {
  permanentForms: PermanentForm[];
  permanents: Permanent[];
  projects: ViewProjects[];
  total = new TotalForm(0, 0, 0, 0, 0);
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'period',
    'mountTotal',
    'anticipation',
    'liquidPay',
    'pay',
    'balance',
    'options'
  ];
  textTooltip = ['Pendiente', 'Pagado'];
  sortedData: PermanentForm[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 0),
    new OptionOrianaTableToolbar('PAGADAS', 1),
    new OptionOrianaTableToolbar('TODAS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 0;

  formSelect: PermanentForm;
  newForm: PermanentForm;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private transactionService: TransactionService,
              private permanentFormService: PermanentFormService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalPermanentFormComponent, {
      width: '400px',
      data: {form: this.newForm}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.newForm.idPermanentForm) {
          this.progress = true;
          this.permanentFormService.addPermanentForm(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              (error: HttpErrorResponse) => {
                if (error.status == 406) {
                  this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
                } else {
                  this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                }
                this.progress = false;
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.formSelect = <any>this.permanentForms[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.formSelect.periodicalPermanentForm}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.permanentFormService.deletePermanentForm(this.formSelect.idPermanentForm)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
  }

  getFilter(type: any) {
    this.permanentFormService.getPermanentForms(type).subscribe(
      res => {
        this.permanentForms = res['forms'];
        this.total = res['total'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        this.progress = false;
      });
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'PERIODO', 'MONTO\nTOTAL', 'ANTICIPO', 'LIQ.\nPAGABLE', 'PAGADO', 'SALDO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PLANILLA PERMANENTE', head, body, null,
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        6: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.permanentForms.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.periodicalPermanentForm.toUpperCase());
      d.push(this.printService.moneyData(c.mountTotal));
      d.push(this.printService.moneyData(c.advancePermanentForm));
      d.push(this.printService.moneyData(c.liquidPermanentForm));
      d.push(this.printService.moneyData(c.mountPayAux));
      d.push(this.printService.moneyData(c.balanceAux));
      d.push(c.statusPermanentForm == 1 ? 'PAGADO' : 'PENDIENTE');
      data.push(d);
    });
    // data.push([
    //   '',
    //   '',
    //   this.total.total,
    //   this.total.advance,
    //   this.total.liquid,
    //   this.total.bond,
    // ]);
    return data;
  }
  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.permanentFormService.search(text).subscribe(
        response => {
          this.permanentForms = response['forms'];
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.permanentForms.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.permanentForms = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'periodicalPermanentForm':
          return compareString(a.periodicalPermanentForm, b.periodicalPermanentForm, isAsc);
        case 'discountPermanentForm':
          return compareNumber(a.discountPermanentForm, b.discountPermanentForm, isAsc);
        case 'advancePermanentForm':
          return compareNumber(a.advancePermanentForm, b.advancePermanentForm, isAsc);
        case 'liquidPermanentForm':
          return compareNumber(a.liquidPermanentForm, b.liquidPermanentForm, isAsc);
        case 'mountTotal':
          return compareNumber(a.mountTotal, b.mountTotal, isAsc);
        case 'mountPayAux':
          return compareNumber(a.mountPayAux, b.mountPayAux, isAsc);
        case 'balanceAux':
          return compareNumber(a.balanceAux, b.balanceAux, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.permanentFormService.setState(this.permanentForms[pos].idPermanentForm, {status: state})
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.newForm = new PermanentForm(
      null,
      null,
      null,
      null,
      null,
      null,
      0,
      null,
      null,
      null
    );
  }

  isDisabled(ele) {
    return this.isDisabledForm() || ele.mountPayAux > 0;
  }

  isDisabledForm() {
    return _permission().f_permanent != 1;
  }

  idDisabledPlan() {
    return _permission().f_permanent_plan == 0;
  }
}

export interface DataPermanentForm {
  form: PermanentForm;
}
