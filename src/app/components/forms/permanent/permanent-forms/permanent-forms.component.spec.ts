import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PermanentFormsComponent} from './permanent-forms.component';

describe('PermanentFormsComponent', () => {
  let component: PermanentFormsComponent;
  let fixture: ComponentFixture<PermanentFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermanentFormsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanentFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
