import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PrintServiceService} from '../../../../services/print-service.service';
import {SnackUpdateComponent} from '../../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {PermanentFormService} from '../../../../services/form/permanent-form.service';
import {PermanentWorkProjects} from '../../../../model/form/permanent-work-projects';
import {TotalFormWork} from '../../../../model/total-form-work';
import {DetailsWorkPermanentWorkForm} from '../../../../model/form/details-work-permanent-work-form';
import {PermanentFormBankTransactions} from '../../../../model/form/advancesModel/permanent-form-bank-transactions';
import {ModalPermanentAdvancesComponent} from '../modal-permanent-advances/modal-permanent-advances.component';
import {TransactionService} from '../../../../services/accounts/transaction.service';
import {DataDescription, ModalDescriptionFormComponent} from '../../modal-description-form/modal-description-form.component';
import {CodeTransaction} from '../../../../model/accounts/ConstTransactions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-permanent-form-planer',
  templateUrl: './permanent-form-planer.component.html',
  styleUrls: ['./permanent-form-planer.component.css']
})
export class PermanentFormPlanerComponent implements OnInit {
  @Input() height;
  @Input() idPermanentForm;
  @Input() idPermanent;
  @Output() getForm = new EventEmitter();
  titulo = 'PLANILLANDO';
  viewPermanentWork: PermanentWorkProjects[];
  detailsForm: DetailsWorkPermanentWorkForm;
  listAdvancePermanent: PermanentFormBankTransactions[] = [];
  totalWork = new TotalFormWork(0, 0, 0, 0, 0, 0, 0, 0);
  permanentForm;
  permanent;
  displayedColumns = [
    'position',
    'name',
    'days',
    'month',
    'basic',
    'bonus',
    'discount',
    'total',
    'advances',
    'liquid',
  ];
  onSave = false;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private permanentFormService: PermanentFormService,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter() {
    this.progress = true;
    this.permanentFormService.getViewPermanentOnProjects(this.idPermanent, this.idPermanentForm).subscribe(
      res => {
        this.viewPermanentWork = <PermanentWorkProjects[]>res['form'];
        this.totalWork = res['total'];
        this.permanent = res['permanent'];
        this.permanentForm = res['permanentForm'];
        this.progress = false;
        this.titulo = 'PLANILLANDO: ' + this.permanent.namePermanent;
        this.getForm.emit(this.permanentForm);
      }
    );
    this.getDetails(this.idPermanent, this.idPermanentForm);
  }

  isEmpty(): any {
  }

  load() {
    this.getFilter();
    this.getAdvances(this.idPermanent);
    this.onSave = false;
  }

  calc(tr) {
    tr.basicMount = ((tr.days * tr.salaryMount) / 30);
    tr.mountTotal = tr.basicMount + tr.bonus - tr.discount;
    tr.liquidPay = (tr.mountTotal - tr.advance);
    this.totalWork = this.calcTotal(this.viewPermanentWork);
    this.onSave = true;
  }

  calcTotal(viewPermanent: PermanentWorkProjects[]) {
    const total = new TotalFormWork(0, 0, 0, 0, 0, 0, 0, 0);
    viewPermanent.forEach(viewPer => {
      total.discount += viewPer.discount;
      total.mountTotal += viewPer.mountTotal;
      total.advance += viewPer.advance;
      total.liquidPay += viewPer.liquidPay;
      total.bonus += viewPer.bonus;
      total.salaryMount += viewPer.salaryMount;
      total.basicMount += viewPer.basicMount;
    });
    return total;
  }

  getDetails(id, idForm) {
    this.permanentFormService.getDetail(id, idForm).subscribe(
      res => {
        this.detailsForm = <DetailsWorkPermanentWorkForm>res[0];
      });
  }

  saveDetails() {
    this.permanentFormService.saveDetail(this.detailsForm.idBonusDiscount, this.detailsForm).subscribe(
      ok => {
        console.log('save d');
      },
      error =>
        console.log('error d')
    );
  }

  getAdvances(id) {
    this.transactionService.getAnticipations(id, CodeTransaction.permanentExpense).subscribe(
      res => {
        this.listAdvancePermanent = <PermanentFormBankTransactions[]>res;
      }
    );
  }

  openDescription() {
    const dialogRef = this.dialog.open(ModalDescriptionFormComponent, {
      width: '400px',
      data: new DataDescription(this.detailsForm.BonusDetails)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.description) {
        this.onSave = true;
        this.detailsForm.BonusDetails = dialogRef.componentInstance.description;
      } else if (!this.onSave) {
        this.onSave = false;
      }
    });
  }

  open(work) {
    const dialogRef = this.dialog.open(ModalPermanentAdvancesComponent, {
      width: '500px',
      data: new DataPermanentFormPlaner(work, this.listAdvancePermanent)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.onSave == true && dialogRef.componentInstance.edited == false) {
        this.calc(work);
      } else if (this.onSave == false && dialogRef.componentInstance.edited == false) {
        this.calc(work);
        this.onSave = false;
      } else if (dialogRef.componentInstance.edited == true) {
        this.listAdvancePermanent = dialogRef.componentInstance.listAdvancePermanent;
        this.calc(work);
      }
    });
  }

  save() {
    this.progress = true;
    this.permanentFormService.saveFormWorkPermanent({forms: this.viewPermanentWork, advances: this.listAdvancePermanent}).subscribe(
      ok => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        this.onSave = false;
        this.progress = false;
      }, error => {
        this.progress = false;
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
    this.saveDetails();
  }

  isDisabled() {
    if (this.permanentForm) {
      return this.isDisabledPlan() || this.permanentForm.mountPayAux > 0 || this.permanentForm.statusPermanentForm == 1;
    }
  }

  isDisabledPlan() {
    return _permission().f_permanent_plan == 0;
  }
}

export class DataPermanentFormPlaner {
  constructor(public form: PermanentWorkProjects, public advances: PermanentFormBankTransactions[]) {
  }
}
