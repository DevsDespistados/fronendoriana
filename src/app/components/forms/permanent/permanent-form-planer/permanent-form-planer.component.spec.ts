import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PermanentFormPlanerComponent} from './permanent-form-planer.component';

describe('PermanentFormPlanerComponent', () => {
  let component: PermanentFormPlanerComponent;
  let fixture: ComponentFixture<PermanentFormPlanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermanentFormPlanerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanentFormPlanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
