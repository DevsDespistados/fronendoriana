import {Component, Inject} from '@angular/core';
import {ConfText, month} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PermanentForm} from '../../../../model/form/permanent-form';
import {moment} from '../../../../services/GlobalFunctions';

@Component({
  selector: 'app-modal-permanent-form',
  templateUrl: './modal-permanent-form.component.html',
  styleUrls: ['./modal-permanent-form.component.css']
})
export class ModalPermanentFormComponent {
  text = ConfText;
  formData: PermanentForm;
  form: PermanentForm;
  private date = new Date();
  year = this.date.getFullYear();
  month = this.date.getMonth();
  months = month;

  constructor(
    public dialogRef: MatDialogRef<ModalPermanentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.data.form;

  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.formData = this.form;
      this.formData.periodicalPermanentForm = moment(new Date(this.year, this.month, 1));
      this.dialogRef.close();
    }
  }


  validData() {
    return this.year == null || this.year <= 0 || this.month == null || this.month < 0;
  }
}
