import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PermanenteFormConsolidateComponent} from './permanente-form-consolidate.component';

describe('PermanenteFormConsolidateComponent', () => {
  let component: PermanenteFormConsolidateComponent;
  let fixture: ComponentFixture<PermanenteFormConsolidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PermanenteFormConsolidateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermanenteFormConsolidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
