import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../../services/print-service.service';
import {PermanentFormService} from '../../../../services/form/permanent-form.service';
import {ViewPermanentForm} from '../../../../model/form/view-permanent-form';
import {TotalFormWork} from '../../../../model/total-form-work';
import {ModalDeleteComponent} from '../../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../../snack-msg/snack-delete/snack-delete.component';
import {ConfText} from '../../../../model/configuration/conf-text';
import {SnackAlertComponent} from '../../../snack-msg/snack-alert/snack-alert.component';
import {Router} from '@angular/router';
import {SnackEmptyComponent} from '../../../snack-msg/snack-empty/snack-empty.component';
import {compareNumber, compareString} from '../../../../services/GlobalFunctions';
import {_permission} from '../../../../services/permissions';

@Component({
  selector: 'app-permanente-form-consolidate',
  templateUrl: './permanente-form-consolidate.component.html',
  styleUrls: ['./permanente-form-consolidate.component.css']
})
export class PermanenteFormConsolidateComponent implements OnInit {
  @Input() height;
  @Input() idPermanentForm;
  @Output() getForm = new EventEmitter();
  titulo = 'Planillas';
  viewPermanentForm: ViewPermanentForm[];
  sortedData: ViewPermanentForm[];
  select: ViewPermanentForm;
  total: TotalFormWork = new TotalFormWork(0,
    0,
    0,
    0,
    0,
    0,
    0,
    0);
  form;
  displayedColumns = [
    'position',
    'name',
    'days',
    'month',
    'basic',
    'discount',
    'bonus',
    'total',
    'advance',
    'liquid',
    'options'
  ];
  msg = {msg: 'Planilla Vacia'};
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private permanentFormService: PermanentFormService,
              private router: Router) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.progress = true;
    this.permanentFormService.getViewConsolidate(type).subscribe(
      res => {
        this.viewPermanentForm = res['forms'];
        this.total = res['total'];
        this.form = res['form'];
        this.getForm.emit(this.form);
        this.progress = false;
      });
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.getFilter(this.idPermanentForm);
  }

  search(text) {
    if (text !== '') {
      this.progress = true;
      this.permanentFormService.searchPermanent(this.idPermanentForm, text).subscribe(
        response => {
          this.viewPermanentForm = response['permanentView'];
          this.total = response['total'];
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  deleted(pos: number) {
    this.select = <any>this.viewPermanentForm[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.select.namePermanent}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.permanentFormService.deletePermenentOfForm(this.select.idPermanent, this.select.idPermanentForm).subscribe(
          res => {
            if (res === 'deleted_all') {
              this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: this.msg});
              this.redirect();
            }
            if (res === 'deleted') {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            }
          }, error => {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            this.progress = false;
          });
      } else {
        this.resetSelect();
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'DIAS', 'MENSUAL', 'MONTO\nBASICO', 'DESCUENTO', 'BONO', 'MONTO\nTOTAL', 'ANTICIPO', 'LIQ.\nPAGABLE'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PLANILLA CONSOLIDADA PERMANENTE \n PERIODO: ' +
      this.form.periodicalPermanentForm.toUpperCase(), head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
        5: {
          halign: 'right',
        },
        6: {
          halign: 'right',
        },
        7: {
          halign: 'right',
        },
        8: {
          halign: 'right',
        },
        9: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.viewPermanentForm.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.namePermanent.toUpperCase());
      d.push(c.days);
      d.push(this.printService.moneyData(c.salaryMount));
      d.push(this.printService.moneyData(c.basicMount));
      d.push(this.printService.moneyData(c.discount));
      d.push(this.printService.moneyData(c.bonus));
      d.push(this.printService.moneyData(c.mountTotal));
      d.push(this.printService.moneyData(c.advance));
      d.push(this.printService.moneyData(c.liquidPay));
      data.push(d);
    });
    data.push(
      ['',
        '',
        'TOTAL',
        this.printService.moneyData(this.total.salaryMount),
        this.printService.moneyData(this.total.basicMount),
        this.printService.moneyData(this.total.discount),
        this.printService.moneyData(this.total.bonus),
        this.printService.moneyData(this.total.mountTotal),
        this.printService.moneyData(this.total.advance),
        this.printService.moneyData(this.total.liquidPay),
      ]);
    return data;
  }

  resetSelect() {
    this.select = null;
  }

  isDisabled() {
    return this.idDisabledPlan() || this.form.statusPermanentForm == 1 || this.form.mountPayAux > 0;
  }

  idDisabledPlan() {
    return _permission().f_permanent_plan != 1;
  }

  sortData(sort: Sort): any {
    const data = this.viewPermanentForm.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.viewPermanentForm = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'namePermanent':
          return compareString(a.namePermanent, b.namePermanent, isAsc);
        case 'salaryMount':
          return compareNumber(a.salaryMount, b.salaryMount, isAsc);
        case 'mountTotal':
          return compareNumber(a.mountTotal, b.mountTotal, isAsc);
        case 'days':
          return compareNumber(a.days, b.days, isAsc);
        case 'advance':
          return compareNumber(a.advance, b.advance, isAsc);
        case 'basicMount':
          return compareNumber(a.basicMount, b.basicMount, isAsc);
        case 'bonus':
          return compareNumber(a.bonus, b.bonus, isAsc);
        case 'discount':
          return compareNumber(a.discount, b.discount, isAsc);
        case 'liquidPay':
          return compareNumber(a.liquidPay, b.liquidPay, isAsc);
        default:
          return 0;
      }
    });
  }

  redirect() {
    setTimeout(() => {
      console.log('Test');
      this.router.navigate(['/forms/permanents']);
    }, 2000);
  }
}
