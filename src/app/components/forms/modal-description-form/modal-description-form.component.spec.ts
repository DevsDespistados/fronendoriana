import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalDescriptionFormComponent} from './modal-description-form.component';

describe('ModalDescriptionFormComponent', () => {
  let component: ModalDescriptionFormComponent;
  let fixture: ComponentFixture<ModalDescriptionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalDescriptionFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDescriptionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
