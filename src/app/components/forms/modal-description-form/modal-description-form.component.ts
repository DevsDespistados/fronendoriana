import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-description-form',
  templateUrl: './modal-description-form.component.html',
  styleUrls: ['./modal-description-form.component.css']
})
export class ModalDescriptionFormComponent {
  text = ConfText;
  description = '';
  edited = true;

  constructor(
    public dialogRef: MatDialogRef<ModalDescriptionFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.description = this.data.description;
  }

  closeDialog() {
    this.description = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.edited) {
      this.dialogRef.close();
    }
  }

  setEdited() {
    this.edited = false;
  }
}

export class DataDescription {
  constructor(public description: string) {
  }
}
