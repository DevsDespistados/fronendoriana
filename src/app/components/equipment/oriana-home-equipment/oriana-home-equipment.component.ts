import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-home-equipment',
  templateUrl: './oriana-home-equipment.component.html',
  styleUrls: ['./oriana-home-equipment.component.css']
})
export class OrianaHomeEquipmentComponent implements OnInit, AfterViewChecked {
  titles = {
    module: 'EQUIPO PROPIO'
  };

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }

}
