import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeEquipmentComponent} from './oriana-home-equipment.component';

describe('OrianaHomeEquipmentComponent', () => {
  let component: OrianaHomeEquipmentComponent;
  let fixture: ComponentFixture<OrianaHomeEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeEquipmentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
