import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaEquipmentComponent} from './oriana-equipment.component';

describe('OrianaEquipmentComponent', () => {
  let component: OrianaEquipmentComponent;
  let fixture: ComponentFixture<OrianaEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaEquipmentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
