import {Component, Input, OnInit} from '@angular/core';
import {PersonalInterface} from '../../personal/personal-interface';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {OwnerEquipmentService} from '../../../services/ownerMahine/owner-equipment.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ModalEquipmentComponent} from '../modal-equipment/modal-equipment.component';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {OwnerEquipment} from '../../../model/Equipment/owner-equipment';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {HttpErrorResponse} from '@angular/common/http';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {DialogService} from '../../../services/dialog.service';

@Component({
  selector: 'app-oriana-equipment',
  templateUrl: './oriana-equipment.component.html',
  styleUrls: ['./oriana-equipment.component.css']
})
export class OrianaEquipmentComponent implements OnInit, PersonalInterface {
  @Input() height: number;
  ownerEquipments: OwnerEquipment[];
  ownerEquipmentelect = new OwnerEquipment(
    0,
    '',
    '',
    null,
    'Hora',
    '',
    true);
  orderAsc = false;
  titulo = 'Disponibles';
  displayedColumns = ['position', 'codigo', 'nombre', 'costo', 'unidad', 'opciones'];
  sortedData: OwnerEquipment[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: DialogService,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private ownerEquipmentervice: OwnerEquipmentService) {
  }

  ngOnInit() {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalEquipmentComponent, new DataEquipment(this.ownerEquipmentelect))
      .afterClosed().subscribe(result => {
      if (result) {
        if (!this.ownerEquipmentelect.idOwnerEquipment) {
          this.ownerEquipmentervice.addOwnerEquipment(result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.progress = false;
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          this.ownerEquipmentervice.editOwnerEquipment(this.ownerEquipmentelect.idOwnerEquipment, result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
                this.progress = false;
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.ownerEquipmentelect = this.ownerEquipments[pos];
    this.dialog.dialogDelete(this.ownerEquipmentelect.nameOwnerEquipment).afterClosed().subscribe(result => {
      if (result) {
        this.ownerEquipmentervice.deleteOwnerEquipment(this.ownerEquipmentelect.idOwnerEquipment)
          .subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
              this.progress = false;
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const ownerEquipment = this.ownerEquipments[pos];
    this.ownerEquipmentelect = new OwnerEquipment(
      ownerEquipment.idOwnerEquipment,
      ownerEquipment.nameOwnerEquipment,
      ownerEquipment.codeOwnerEquipment,
      ownerEquipment.costOwnerEquipment,
      ownerEquipment.unityCost,
      ownerEquipment.fileOwnerEquipment,
      ownerEquipment.availableOwnerEquipment);
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.progress = true;
    this.ownerEquipmentervice.getActives(type).subscribe(
      res => {
        this.ownerEquipments = <OwnerEquipment[]>res;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.ownerEquipments) {
      if (this.ownerEquipments.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  loadPersonal() {
    this.resetSelect();
    this.getFilterPersonal(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'CODIGO', 'NOMBRE', 'COSTO', 'UNIDAD', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE EQUIPOS', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.ownerEquipments.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.codeOwnerEquipment.toUpperCase());
      d.push(c.nameOwnerEquipment.toUpperCase());
      d.push(this.printService.moneyData(c.costOwnerEquipment));
      d.push(c.unityCost.toUpperCase());
      d.push(c.availableOwnerEquipment == 2 ? 'NO DISPONIBLE' : c.availableOwnerEquipment == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.ownerEquipmentervice.searchOwnerEquipment(text).subscribe(
        response => {
          this.ownerEquipments = <any>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.ownerEquipments.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.ownerEquipments = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameOwnerEquipment':
          return compareString(a.nameOwnerEquipment, b.nameOwnerEquipment, isAsc);
        case 'codeOwnerEquipment':
          return compareString(a.codeOwnerEquipment, b.codeOwnerEquipment, isAsc);
        case 'costOwnerEquipment':
          return compareNumber(a.costOwnerEquipment, b.costOwnerEquipment, isAsc);
        case 'unityCost':
          return compareString(a.unityCost, b.unityCost, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    this.ownerEquipmentervice.changeState(pos, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.ownerEquipmentelect = new OwnerEquipment(
      0,
      null,
      null,
      null,
      'Hora',
      null,
      1);
  }

  isDisabled() {
    return _permission().o_ownerEquipment != 1;
  }
}

export class DataEquipment {
  constructor(public equipment: OwnerEquipment) {
  }

}
