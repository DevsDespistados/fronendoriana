import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OwnerEquipment} from '../../../model/Equipment/owner-equipment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-equipment',
  templateUrl: './modal-equipment.component.html',
  styleUrls: ['./modal-equipment.component.css']
})
export class ModalEquipmentComponent {

  text = ConfText;
  formData: FormGroup;
  equipment: OwnerEquipment;
  unity = [
    'Hora',
    'Dia',
    'Mes'
  ];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ModalEquipmentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.equipment = this.data.equipment;
    this.formData = this.fb.group({
      nameOwnerEquipment: [this.equipment.nameOwnerEquipment ? this.equipment.nameOwnerEquipment : null, [Validators.required]],
      codeOwnerEquipment: [this.equipment.codeOwnerEquipment ? this.equipment.codeOwnerEquipment : null, [Validators.required]],
      costOwnerEquipment: [this.equipment.costOwnerEquipment ? this.equipment.costOwnerEquipment : null, [Validators.required]],
      availableOwnerEquipment: [this.equipment.availableOwnerEquipment ? this.equipment.availableOwnerEquipment : null],
      fileOwnerEquipment: [this.equipment.fileOwnerEquipment ? this.equipment.fileOwnerEquipment : null],
      unityCost: [this.equipment.unityCost ? this.equipment.unityCost : null, [Validators.required]],
    });
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (this.formData.valid) {
      this.dialogRef.close(this.formData.value);
    }
  }
}
