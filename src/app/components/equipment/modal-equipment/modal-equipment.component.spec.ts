import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalEquipmentComponent} from './modal-equipment.component';

describe('ModalEquipmentComponent', () => {
  let component: ModalEquipmentComponent;
  let fixture: ComponentFixture<ModalEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalEquipmentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
