export interface ModalPersonalInterface {
  closeDialog();

  sendFormData(file: any);

  validData();
}
