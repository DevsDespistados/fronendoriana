import {Component, Input, OnInit} from '@angular/core';
import {Provider} from '../../../model/personal/provider';
import {ProviderService} from '../../../services/personal/provider.service';
import {HttpErrorResponse} from '@angular/common/http';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalProviderComponent} from '../modal-provider/modal-provider.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {PrintServiceService} from '../../../services/print-service.service';
import {PersonalInterface} from '../personal-interface';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {DialogService} from '../../../services/dialog.service';

@Component({
  selector: 'app-oriana-provider',
  templateUrl: './oriana-provider.component.html',
  styleUrls: ['./oriana-provider.component.css']
})
export class OrianaProviderComponent implements OnInit, PersonalInterface {
  @Input() height: number;
  providers: Provider[];
  titulo = 'Disponibles';
  formData: any;
  providerSelect = new Provider(null, '', 1, '');
  fileAux = '';
  sortedData: Provider[];
  displayedColumns = ['position', 'nombre', 'op'];

  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: DialogService,
              private providerService: ProviderService,
              private snack: MatSnackBar, private printService: PrintServiceService) {
  }

  ngOnInit() {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalProviderComponent, {provider: this.providerSelect})
      .afterClosed().subscribe(result => {
      if (result) {
        if (!this.providerSelect.idProvider) {
          this.providerService.addProvider(result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.providerService.editProvider(this.providerSelect.idProvider, result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.providerSelect = this.providers[pos];
    this.dialog.dialogDelete(this.providerSelect.nameProvider).afterClosed().subscribe(result => {
      if (result) {
        this.providerService.deleteProvider(this.providerSelect.idProvider)
          .subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const provider: Provider = this.providers[pos];
    this.providerSelect = new Provider(
      provider.idProvider,
      provider.nameProvider,
      provider.activeProvider,
      provider.fileProvider
    );
    if (this.providerSelect.fileProvider !== 'sin archivo') {
    } else {
      this.fileAux = this.providerSelect.fileProvider;
    }
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.providerService.getActives(type).subscribe(
      res => {
        this.providers = <Provider[]>res;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.providers) {
      return this.providers.length <= 0;
    }
  }

  loadPersonal() {
    this.progress = true;
    this.resetSelect();
    if (this.currentOption == 3) {
      this.providerService.getProviders().subscribe(
        res => {
          this.providers = <Provider[]>res;
          this.progress = false;
        }
      );
    } else {
      this.getFilterPersonal(this.currentOption);
    }
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PROVEEDOR', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.providers.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameProvider.toUpperCase());
      d.push(c.activeProvider == 2 ? 'NO DISPONIBLE' : c.activeProvider == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.providerService.searchProvider(text).subscribe(
        response => {
          this.providers = <Provider[]>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.providers.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.providers = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameProvider':
          return compareString(a.nameProvider, b.nameProvider, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    const provider = this.providers[pos];
    this.providerService.changeState(provider.idProvider, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption != 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.providers[pos] = (<Provider>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.providerSelect = new Provider(null, null, 1, null);
  }

  isDisabled() {
    return _permission().p_provider != 1;
  }
}

export interface DataProvider {
  provider: Provider;
}
