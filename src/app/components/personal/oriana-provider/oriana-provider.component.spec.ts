import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaProviderComponent} from './oriana-provider.component';

describe('OrianaProviderComponent', () => {
  let component: OrianaProviderComponent;
  let fixture: ComponentFixture<OrianaProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaProviderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
