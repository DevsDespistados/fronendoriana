import {Component, Input, OnInit} from '@angular/core';
import {TransitService} from '../../../services/personal/transit.service';
import {Transit} from '../../../model/personal/transit';
import {PrintServiceService} from '../../../services/print-service.service';
import {HttpErrorResponse} from '@angular/common/http';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {ModalTransitComponent} from '../modal-transit/modal-transit.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {PersonalInterface} from '../personal-interface';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {DialogService} from '../../../services/dialog.service';

@Component({
  selector: 'app-oriana-transit',
  templateUrl: './oriana-transit.component.html',
  styleUrls: ['./oriana-transit.component.css']
})
export class OrianaTransitComponent implements OnInit, PersonalInterface {
  @Input() height: number;
  titulo = 'Disponibles';
  transits: Transit[];
  transitSelect = new Transit(0, '', 1, '');
  displayedColumns = ['position', 'nombre', 'op'];
  sortedData: Transit[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(private transitService: TransitService,
              public dialog: DialogService,
              private snack: MatSnackBar,
              private printService: PrintServiceService) {
  }

  ngOnInit() {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalTransitComponent, {transit: this.transitSelect})
      .afterClosed().subscribe(result => {
      if (result) {
        if (!this.transitSelect.idTransit) {
          this.transitService.addTransit(result)
            .subscribe(
              formData => {
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
                this.loadPersonal();
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.transitService.editTransit(this.transitSelect.idTransit, result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.transitSelect = this.transits[pos];
    this.dialog.dialogDelete(this.transitSelect.nameTransit).afterClosed().subscribe(result => {
      if (result) {
        this.transitService.deleteTransit(this.transitSelect.idTransit)
          .subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const transit: Transit = this.transits[pos];
    this.transitSelect = new Transit(
      transit.idTransit,
      transit.nameTransit,
      transit.activeTransit,
      transit.fileTransit
    );
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.transitService.getActives(type).subscribe(
      res => {
        this.transits = <Transit[]>res;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.transits) {
      if (this.transits.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  loadPersonal() {
    this.progress = true;
    this.resetSelect();
    if (this.currentOption === 3) {
      this.transitService.getTransit().subscribe(
        res => {
          this.transits = <Transit[]>res;
          this.progress = false;
        }
      );
    } else {
      this.getFilterPersonal(this.currentOption);
    }
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE TRANSITO', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.transits.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameTransit.toUpperCase());
      d.push(c.activeTransit == 2 ? 'NO DISPONIBLE' : c.activeTransit == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.transitService.searchTransit(text).subscribe(
        response => {
          this.transits = <Transit[]>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.transits.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.transits = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameEventual':
          return compareString(a.nameTransit, b.nameTransit, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    const transit = this.transits[pos];
    this.transitService.changeState(transit.idTransit, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.transits[pos] = (<Transit>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.transitSelect = new Transit(0, null, 1, null);
  }

  isDisabled() {
    return _permission().p_transit != 1;
  }
}

export interface DataTransit {
  transit: Transit;
}
