import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Client} from '../../../model/personal/client';
import {ConfText} from '../../../model/configuration/conf-text';
import {FileInput} from 'ngx-material-file-input';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  text = ConfText;
  file: FileInput;
  client: Client;
  formClient: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.client = this.data.client;
    this.formClient = this.fb.group({
      nameClient: [this.client.nameClient ? this.client.nameClient : '', [Validators.required]],
      activeClient: [this.client.activeClient ? this.client.activeClient : null, [Validators.required]],
      fileClient: [this.client.fileClient ? this.client.fileClient : null],
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  accept() {
    if (this.formClient.valid) {
      this.dialogRef.close(this.formClient.value);
    }
  }
}
