import {Sort} from '@angular/material/sort';

export interface PersonalInterface {
  loadPersonal();

  searchPersonal(text: string);

  getFilterPersonal(type: any);

  printReport();

  createPersonal();

  editPersonal(pos: number);

  deletePersonal(pos: number);

  updateStatePersonal(state: any, pos: number);

  isEmpty(): any;

  sortData(sort: Sort): any;

  switchPersonal(type: any);

  resetSelect();

}
