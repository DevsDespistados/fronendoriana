import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Eventual} from '../../../model/personal/eventual';
import {FileInput} from 'ngx-material-file-input';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-eventual',
  templateUrl: './modal-eventual.component.html',
  styleUrls: ['./modal-eventual.component.css']
})
export class ModalEventualComponent {
  text = ConfText;
  formData: FormGroup;
  eventual: Eventual;
  file: FileInput;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.eventual = this.data.eventual;
    this.formData = this.fb.group({
      nameEventual: [this.eventual.nameEventual ? this.eventual.nameEventual : null, [Validators.required]],
      salaryJornal: [this.eventual.salaryJornal ? this.eventual.salaryJornal : null, [Validators.required]],
      activeEventual: [this.eventual.activeEventual ? this.eventual.activeEventual : null],
      fileEventual: [this.eventual.fileEventual ? this.eventual.fileEventual : null],
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  sendFormData() {
    if (this.formData.valid) {
      this.dialogRef.close(this.formData.value);
    }
  }

}
