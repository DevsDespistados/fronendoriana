import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Transit} from '../../../model/personal/transit';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-transit',
  templateUrl: './modal-transit.component.html',
  styleUrls: ['./modal-transit.component.css']
})
export class ModalTransitComponent {
  text = ConfText;
  formData: FormGroup;
  transit: Transit;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.transit = this.data.transit;
    this.formData = this.fb.group({
      nameTransit: [this.transit.nameTransit ? this.transit.nameTransit : null, [Validators.required]],
      activeTransit: [this.transit.activeTransit ? this.transit.activeTransit : null],
      fileTransit: [this.transit.fileTransit ? this.transit.fileTransit : null]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  datart() {
    if (this.formData.valid) {
      this.dialogRef.close(this.formData.value);
    }
  }
}
