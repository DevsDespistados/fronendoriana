import {Component, Input, OnInit} from '@angular/core';
import {PersonalInterface} from '../personal-interface';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {Eventual} from '../../../model/personal/eventual';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {EventualService} from '../../../services/personal/eventual.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ModalEventualComponent} from '../modal-eventual/modal-eventual.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {DialogService} from '../../../services/dialog.service';

@Component({
  selector: 'app-oriana-eventual',
  templateUrl: './oriana-eventual.component.html',
  styleUrls: ['./oriana-eventual.component.css']
})
export class OrianaEventualComponent implements OnInit, PersonalInterface {
  @Input() height: number;
  eventuals: Eventual[];
  eventualSelect: Eventual;
  titulo = 'Disponibles';
  displayedColumns = ['position', 'nombre', 'salario', 'op'];
  sortedData: Eventual[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: DialogService,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private eventualService: EventualService) {
  }

  ngOnInit() {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalEventualComponent, {eventual: this.eventualSelect})
      .afterClosed().subscribe(result => {
      if (result) {
        if (!this.eventualSelect.idEventual) {
          this.eventualService.addEventual(result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          this.eventualService.editEventual(this.eventualSelect.idEventual, result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.eventualSelect = this.eventuals[pos];
    this.dialog.dialogDelete(this.eventualSelect.nameEventual).afterClosed().subscribe(result => {
      if (result) {
        this.eventualService.deleteEventual(this.eventualSelect.idEventual)
          .subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const eventual = this.eventuals[pos];
    this.eventualSelect = new Eventual(eventual.idEventual, eventual.nameEventual,
      eventual.activeEventual, eventual.fileEventual, eventual.salaryJornal
    );
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.eventualService.getActives(type).subscribe(
      res => {
        this.eventuals = <Eventual[]>res;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.eventuals) {
      if (this.eventuals.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  loadPersonal() {
    this.resetSelect();
    this.progress = true;
    if (this.currentOption == 3) {
      this.eventualService.getEventuals().subscribe(
        res => {
          this.eventuals = <Eventual[]>res;
          this.progress = false;
        }
      );
    } else {
      this.getFilterPersonal(this.currentOption);
    }
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'SALARIO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE EVENTUAL', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.eventuals.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameEventual.toUpperCase());
      d.push(this.printService.moneyData(c.salaryJornal));
      d.push(c.activeEventual == 2 ? 'NO DISPONIBLE' : c.activeEventual == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.eventualService.searchEventual(text).subscribe(
        response => {
          this.eventuals = <Eventual[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.eventuals.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.eventuals = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameEventual':
          return compareString(a.nameEventual, b.nameEventual, isAsc);
        case 'salaryJornal':
          return compareNumber(a.salaryJornal, b.salaryJornal, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    this.eventualService.changeState(pos, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.eventualSelect = new Eventual(null, null, 1, null, null);
  }

  isDisabled() {
    return _permission().p_eventual != 1;
  }
}

export interface DataEventual {
  eventual: Eventual;
}
