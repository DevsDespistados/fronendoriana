import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaEventualComponent} from './oriana-eventual.component';

describe('OrianaEventualComponent', () => {
  let component: OrianaEventualComponent;
  let fixture: ComponentFixture<OrianaEventualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaEventualComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaEventualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
