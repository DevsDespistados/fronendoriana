import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Provider} from '../../../model/personal/provider';
import {FileInput} from 'ngx-material-file-input';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-provider',
  templateUrl: './modal-provider.component.html',
  styleUrls: ['./modal-provider.component.css']
})
export class ModalProviderComponent {
  text = ConfText;
  formData: FormGroup;
  provider: Provider;
  file: FileInput;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.provider = this.data.provider;
    this.formData = this.fb.group({
      nameProvider: [this.provider.nameProvider ? this.provider.nameProvider : null, [Validators.required]],
      activeProvider: [this.provider.activeProvider ? this.provider.activeProvider : null],
      fileProvider: [this.provider.fileProvider ? this.provider.fileProvider : null],
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  datart() {
    if (this.formData.valid) {
      this.dialogRef.close(this.formData.value);
    }
  }
}
