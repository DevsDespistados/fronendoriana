import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Permanent} from '../../../model/personal/permanent';
import {FileInput} from 'ngx-material-file-input';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-permanent',
  templateUrl: './modal-permanent.component.html',
  styleUrls: ['./modal-permanent.component.css']
})
export class ModalPermanentComponent {
  text = ConfText;
  formData: FormGroup;
  permanent: Permanent;
  file: FileInput;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.permanent = this.data.permanent;
    this.formData = this.fb.group({
      namePermanent: [this.permanent.namePermanent ? this.permanent.namePermanent : null, [Validators.required]],
      salaryPermanent: [this.permanent.salaryPermanent ? this.permanent.salaryPermanent : null, [Validators.required]],
      filePermanent: [this.permanent.filePermanent ? this.permanent.filePermanent : null],
      activePermanent: [this.permanent.activePermanent ? this.permanent.activePermanent : null],
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  sendFormData() {
    if (this.formData.valid) {
      this.dialogRef.close(this.formData.value);
    }
  }

}
