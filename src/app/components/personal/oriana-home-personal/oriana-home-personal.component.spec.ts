import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomePersonalComponent} from './oriana-home-personal.component';

describe('OrianaHomePersonalComponent', () => {
  let component: OrianaHomePersonalComponent;
  let fixture: ComponentFixture<OrianaHomePersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomePersonalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
