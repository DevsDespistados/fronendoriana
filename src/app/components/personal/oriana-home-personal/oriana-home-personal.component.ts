import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  __permissionClient,
  __permissionCompany,
  __permissionEventual,
  __permissionPermanent,
  __permissionProvider,
  __permissionTransit
} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-personal',
  templateUrl: './oriana-home-personal.component.html',
  styleUrls: ['./oriana-home-personal.component.css']
})
export class OrianaHomePersonalComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'PERSONAL',
    // client: 'CLIENTES',
    // eventual: 'EVENTUAL',
    // permanent: 'PERMANENTE',
    // provider: 'PROVEEDOR',
    // transit: 'TRANSITO',
    // company: 'EMPRESA'
  };
  private links = [
    {label: 'CLIENTES', path: '/personal/clients', permission: __permissionClient()},
    {label: 'PROVEEDOR', path: '/personal/providers', permission: __permissionProvider()},
    {label: 'TRANSITO', path: '/personal/transits', permission: __permissionTransit()},
    {label: 'EVENTUAL', path: '/personal/eventual', permission: __permissionEventual()},
    {label: 'PERMANENTE', path: '/personal/permanents', permission: __permissionPermanent()},
    {label: 'EMPRESA', path: '/personal/companies', permission: __permissionCompany()},
  ];
  navLinks = [];

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }

  getHeight(tab) {
    try {
      return tab._elementRef.nativeElement.offsetHeight;
    } catch (e) {
      console.log('error');
    }
  }
}
