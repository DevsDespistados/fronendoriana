import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OtherCompany} from '../../../model/personal/other-company';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-company',
  templateUrl: './modal-company.component.html',
  styleUrls: ['./modal-company.component.css']
})
export class ModalCompanyComponent {
  text = ConfText;
  formData: FormGroup;
  company: OtherCompany;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ModalCompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.company = this.data.company;
    this.formData = this.fb.group({
      nameAnotherCompany: [this.company.nameAnotherCompany ? this.company.nameAnotherCompany : null, [Validators.required]],
      activeAnotherCompany: [this.company.activeAnotherCompany ?
        this.company.activeAnotherCompany : null],
      fileAnotherCompany: [this.company.fileAnotherCompany ? this.company.fileAnotherCompany : null],
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    if (this.formData.valid) {
      this.dialogRef.close(this.formData.value);
    }
  }
}
