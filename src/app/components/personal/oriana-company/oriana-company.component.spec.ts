import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaCompanyComponent} from './oriana-company.component';

describe('OrianaCompanyComponent', () => {
  let component: OrianaCompanyComponent;
  let fixture: ComponentFixture<OrianaCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaCompanyComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
