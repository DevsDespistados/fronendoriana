import {Component, Input, OnInit} from '@angular/core';
import {PersonalInterface} from '../personal-interface';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {PrintServiceService} from '../../../services/print-service.service';
import {OtherCompanyService} from '../../../services/personal/other-company.service';
import {OtherCompany} from '../../../model/personal/other-company';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalCompanyComponent} from '../modal-company/modal-company.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {DialogService} from '../../../services/dialog.service';

@Component({
  selector: 'app-oriana-company',
  templateUrl: './oriana-company.component.html',
  styleUrls: ['./oriana-company.component.css']
})
export class OrianaCompanyComponent implements OnInit, PersonalInterface {
  @Input() height: number;
  titulo = 'Disponibles';
  otherCompanySelect = new OtherCompany(null, '', null, 1);
  otherCompanies: OtherCompany[];
  displayedColumns = ['position', 'nombre', 'op'];
  sortedData: OtherCompany[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: DialogService,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private otherCompanyService: OtherCompanyService) {
  }

  ngOnInit() {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalCompanyComponent, {company: this.otherCompanySelect}).afterClosed().subscribe(result => {
      if (result) {
        if (!this.otherCompanySelect.idAnotherCompany) {
          this.otherCompanyService.addOtherCompany(result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.otherCompanyService.editOtherCompany
          (this.otherCompanySelect.idAnotherCompany, result).subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.otherCompanySelect = this.otherCompanies[pos];
    this.dialog.dialogDelete(this.otherCompanySelect.nameAnotherCompany).afterClosed().subscribe(result => {
      if (result) {
        this.otherCompanyService.deleteOtherCompany(this.otherCompanySelect.idAnotherCompany)
          .subscribe(
            formData => {
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
              this.loadPersonal();
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const otherCompanies = this.otherCompanies[pos];
    this.otherCompanySelect = new OtherCompany(otherCompanies.idAnotherCompany,
      otherCompanies.nameAnotherCompany,
      otherCompanies.fileAnotherCompany,
      otherCompanies.activeAnotherCompany
    );
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.otherCompanyService.getActives(type).subscribe(
      ok => {
        this.otherCompanies = <OtherCompany[]>ok;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.otherCompanies) {
      if (this.otherCompanies.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  loadPersonal() {
    this.resetSelect();
    this.progress = true;
    if (this.currentOption == 3) {
      this.otherCompanyService.getOthersCompanies().subscribe(
        ok => {
          this.otherCompanies = <OtherCompany[]>ok;
          this.progress = false;
        }
      );
    } else {
      this.getFilterPersonal(this.currentOption);
    }
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE OTRAS EMPRESAS', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.otherCompanies.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameAnotherCompany.toUpperCase());
      d.push(c.activeAnotherCompany == 2 ? 'NO DISPONIBLE' : c.activeAnotherCompany == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.otherCompanyService.searchOtherCompany(text).subscribe(
        response => {
          this.otherCompanies = <OtherCompany[]>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.otherCompanies.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.otherCompanies = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameAnotherCompany':
          return compareString(a.nameAnotherCompany, b.nameAnotherCompany, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    const otherCompanies = this.otherCompanies[pos];
    this.otherCompanyService.changeState(otherCompanies.idAnotherCompany, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.otherCompanies[pos] = (<OtherCompany>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.otherCompanySelect = new OtherCompany(null, null, null, 1);
  }

  isDisabled() {
    return _permission().p_otherCompany != 1;
  }
}

export interface DataCompany {
  company: OtherCompany;
}
