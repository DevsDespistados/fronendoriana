import {Component, Input, OnInit} from '@angular/core';
import {PersonalInterface} from '../personal-interface';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {PermanentService} from '../../../services/personal/permanent.service';
import {PrintServiceService} from '../../../services/print-service.service';
import {Permanent} from '../../../model/personal/permanent';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ModalPermanentComponent} from '../modal-permanent/modal-permanent.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {DialogService} from '../../../services/dialog.service';

@Component({
  selector: 'app-oriana-permanent',
  templateUrl: './oriana-permanent.component.html',
  styleUrls: ['./oriana-permanent.component.css']
})
export class OrianaPermanentComponent implements OnInit, PersonalInterface {
  @Input() height: number;
  permanents: Permanent[];
  permanentSelect: Permanent;
  titulo = 'Disponibles';
  displayedColumns = ['position', 'nombre', 'salario', 'op'];
  sortedData: Permanent[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: DialogService,
              private snack: MatSnackBar,
              private permanentService: PermanentService,
              private printService: PrintServiceService) {
  }

  ngOnInit() {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalPermanentComponent, {permanent: this.permanentSelect})
      .afterClosed().subscribe(result => {
      if (result) {
        if (!this.permanentSelect.idPermanent) {
          this.permanentService.addPermanent(result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        } else {
          this.permanentService.editPermanent(this.permanentSelect.idPermanent, result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.permanentSelect = this.permanents[pos];
    this.dialog.dialogDelete(this.permanentSelect.namePermanent).afterClosed().subscribe(result => {
      if (result) {
        this.permanentService.deletePermanent(this.permanentSelect.idPermanent)
          .subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const permanent = this.permanents[pos];
    this.permanentSelect = new Permanent(
      permanent.idPermanent,
      permanent.namePermanent,
      permanent.salaryPermanent,
      permanent.activePermanent,
      permanent.filePermanent
    );
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.permanentService.getActives(type).subscribe(
      res => {
        this.permanents = <Permanent[]>res;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.permanents) {
      if (this.permanents.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  loadPersonal() {
    this.resetSelect();
    this.progress = true;
    if (this.currentOption === 3) {
      this.permanentService.getPermanents().subscribe(
        res => {
          this.permanents = <Permanent[]>res;
          this.progress = false;
        }
      );
    } else {
      this.getFilterPersonal(this.currentOption);
    }
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'SALARIO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE PERMANENTE', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        }
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.permanents.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.namePermanent.toUpperCase());
      d.push(this.printService.moneyData(c.salaryPermanent));
      d.push(c.activePermanent == 2 ? 'NO DISPONIBLE' : c.activePermanent == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }
  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.permanentService.searchPermanent(text).subscribe(
        response => {
          this.permanents = <Permanent[]>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.permanents.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.permanents = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'namePermanent':
          return compareString(a.namePermanent, b.namePermanent, isAsc);
        case 'salaryPermanent':
          return compareNumber(a.salaryPermanent, b.salaryPermanent, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    const permanent = this.permanents[pos];
    this.permanentService.changeState(permanent.idPermanent, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.permanents[pos] = (<Permanent>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.permanentSelect = new Permanent(null, null, null, 1, null);
  }

  isDisabled() {
    return _permission().p_permanent != 1;
  }

}

export interface DataPermanent {
  permanent: Permanent;
}
