import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaPermanentComponent} from './oriana-permanent.component';

describe('OrianaPermanentComponent', () => {
  let component: OrianaPermanentComponent;
  let fixture: ComponentFixture<OrianaPermanentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaPermanentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaPermanentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
