import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {ModalComponent} from '../modal-client/modal.component';
import {ClientService} from '../../../services/personal/client.service';
import {Client} from '../../../model/personal/client';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {PrintServiceService} from '../../../services/print-service.service';
import {PersonalInterface} from '../personal-interface';
import {_permission} from '../../../services/permissions';
import {compareString} from '../../../services/GlobalFunctions';
import {DialogService} from '../../../services/dialog.service';
import {ReportService} from '../../../services/report.service';

@Component({
  selector: 'app-oriana-client',
  templateUrl: './oriana-client.component.html',
  styleUrls: ['./oriana-client.component.css']
})
export class OrianaClientComponent implements OnInit, PersonalInterface {
  titulo = 'Disponibles';
  clientSelect: Client;
  clients: Client[];
  displayedColumns = ['position', 'nombre', 'op'];
  sortedData: Client[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 2),
    new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 3)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: DialogService,
              private snack: MatSnackBar,
              private clientService: ClientService,
              private reportService: ReportService) {
  }

  ngOnInit(): void {
    this.loadPersonal();
  }

  createPersonal() {
    this.dialog.dialogCreate(ModalComponent, {client: this.clientSelect, status: 0})
      .afterClosed().subscribe(result => {
      if (result) {
        if (!this.clientSelect.idClient) {
          this.clientService.addClient(result)
            .subscribe(
              formData => {
                this.loadPersonal();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.clientService.editClient(this.clientSelect.idClient, result).subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deletePersonal(pos: number) {
    this.clientSelect = this.clients[pos];
    this.dialog.dialogDelete(this.clientSelect.nameClient).afterClosed().subscribe(result => {
      if (result) {
        this.clientService.deleteClient(this.clientSelect.idClient)
          .subscribe(
            formData => {
              this.loadPersonal();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  editPersonal(pos: number) {
    const client = this.clients[pos];
    this.clientSelect = new Client(client.idClient, client.nameClient,
      client.activeClient, client.fileClient);
    this.createPersonal();
  }

  getFilterPersonal(type: any) {
    this.progress = true;
    this.clientService.getActives(type)
      .subscribe(
        response => {
          this.clients = <Client[]>response;
          this.progress = false;
          console.log(this.clients);
        }
      );
  }

  isEmpty(): any {
    if (this.clients) {
      if (this.clients.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  isDisabled() {
    return _permission().p_client != 1;
  }

  loadPersonal() {
    this.resetSelect();
    this.getFilterPersonal(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {

    const options = this.currentOption != 3 ? {LIST_ACTITE: this.currentOption} : {};
    this.reportService.printReport('Clients', options);
  }

  getData() {
    const data = [];
    this.clients.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameClient.toUpperCase());
      d.push(c.activeClient == 2 ? 'NO DISPONIBLE' : c.activeClient == 1 ? 'DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.clientService.searchClient(text).subscribe(
        response => {
          this.clients = <Client[]>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.loadPersonal();
    }
  }

  sortData(sort: Sort): any {
    const data = this.clients.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.clients = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameClient':
          return compareString(a.nameClient, b.nameClient, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.loadPersonal();
  }

  updateStatePersonal(state: any, pos: number) {
    const client = this.clients[pos];
    this.clientService.changeState(client.idClient, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.loadPersonal();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.clients[pos] = (<Client>errorResponse.error);
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.clientSelect = new Client(null, null, 1, null);
  }
}

export interface DataClient {
  client: Client;
  status: any;
}
