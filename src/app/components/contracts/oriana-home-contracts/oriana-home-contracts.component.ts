import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-home-contracts',
  templateUrl: './oriana-home-contracts.component.html',
  styleUrls: ['./oriana-home-contracts.component.css']
})
export class OrianaHomeContractsComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'CONTRATOS'
  };

  constructor(private cdRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
  }

  getHeight(tab) {
    try {
      if (window.innerWidth < 600) {
        return tab.offsetHeight + 16;
      } else {
        return tab.offsetHeight;
      }
    } catch (e) {
      console.log('error');
    }
  }
}
