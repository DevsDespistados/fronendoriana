import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeContractsComponent} from './oriana-home-contracts.component';

describe('OrianaHomeContractsComponent', () => {
  let component: OrianaHomeContractsComponent;
  let fixture: ComponentFixture<OrianaHomeContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeContractsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
