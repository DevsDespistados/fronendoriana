import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ContractItem} from '../../../model/contract/contract-item';

@Component({
  selector: 'app-modal-contract-item',
  templateUrl: './modal-contract-item.component.html',
  styleUrls: ['./modal-contract-item.component.css']
})
export class ModalContractItemComponent {
  text = ConfText;
  formData;
  item: ContractItem;

  constructor(
    public dialogRef: MatDialogRef<ModalContractItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.item = this.data.item;
  }

  onNoClick(): void {
    this.formData = null;
    this.dialogRef.close();
  }

  datart() {
    if (!this.valid()) {
      this.formData = this.item;
      this.dialogRef.close();
    }
  }

  valid() {
    return this.item.item_name === null || this.item.item_name === '' ||
      this.item.item_unity === null || this.item.item_unity === '' ||
      this.item.item_price === null || this.item.item_price < 0;
  }
}
