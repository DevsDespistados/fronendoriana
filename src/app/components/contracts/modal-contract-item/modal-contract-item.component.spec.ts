import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalContractItemComponent} from './modal-contract-item.component';

describe('ModalContractItemComponent', () => {
  let component: ModalContractItemComponent;
  let fixture: ComponentFixture<ModalContractItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalContractItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContractItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
