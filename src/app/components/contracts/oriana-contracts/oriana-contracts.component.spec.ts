import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaContractsComponent} from './oriana-contracts.component';

describe('OrianaFormsContractsComponent', () => {
  let component: OrianaContractsComponent;
  let fixture: ComponentFixture<OrianaContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaContractsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
