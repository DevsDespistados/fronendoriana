import {Component, Input, OnInit} from '@angular/core';
import {OrianaInterface} from '../../projects/oriana-interface';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {ViewContract} from '../../../model/contract/view-contract';
import {ContractProviderService} from '../../../services/contract/contract-provider.service';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ContractProvider} from '../../../model/contract/contract-provider';
import {ModalContractsComponent} from '../modal-contracts/modal-contracts.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {Category} from '../../../model/accounts/category';
import {HttpErrorResponse} from '@angular/common/http';
import {compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-contracts',
  templateUrl: './oriana-contracts.component.html',
  styleUrls: ['./oriana-contracts.component.css']
})
export class OrianaContractsComponent implements OnInit, OrianaInterface {
  @Input() height: number;
  contracts: ViewContract[];
  categories: Category[];
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'contract',
    'project',
    'provider',
    'category',
    'options'
  ];
  contractSelect: ContractProvider;
  sortedData: ViewContract[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    new OptionOrianaTableToolbar('TODOS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private contractProviderService: ContractProviderService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalContractsComponent, {
      width: '400px',
      data: new DataContract(this.contractSelect, this.categories)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.contractSelect.idContract) {
          this.progress = true;
          this.contractProviderService.addContractProvider(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        } else {
          this.contractProviderService.editContractProvider(this.contractSelect.idContract, dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.contractSelect = <any>this.contracts[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.contractSelect.numberContract}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.contractProviderService.deleteContract(this.contractSelect.idContract)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            });
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const c: ViewContract = this.contracts[pos];
    this.contractSelect = new ContractProvider(
      c.idContract, c.numberContract, c.idCategory, c.activeContract, c.idProject, c.idProvider);
    this.create();
  }

  getFilter(type: any) {
    this.progress = true;
    this.contractProviderService.getContractProviderFilter(type).subscribe(
      res => {
        this.contracts = res['contracts'];
        this.categories = res['categories'];
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NRO CONTRATO', 'PROYECTO', 'PROVEEDOR', 'CATEGORIA', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE CONTRATOS', head, body, null,
      {
        0: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.contracts.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.numberContract.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      d.push(c.nameProvider.toUpperCase());
      d.push(c.categoryContract.toUpperCase());
      d.push(c.activeContract == 1 ? 'DISPONIBLE' : 'NO DISPONIBLE');
      data.push(d);
    });
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.contractProviderService.searchContract(text).subscribe(
        response => {
          this.contracts = <ViewContract[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.contracts.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.contracts = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'nameProvider':
          return compareString(a.nameProvider, b.nameProvider, isAsc);
        case 'categoryContract':
          return compareString(a.categoryContract, b.categoryContract, isAsc);
        case 'numberContract':
          return compareString(a.numberContract, b.numberContract, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    this.contractProviderService.changeState(this.contracts[pos].idContract, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 2) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  resetSelect() {
    this.contractSelect = new ContractProvider(
      null,
      null,
      null,
      true,
      null,
      null
    );
  }

  isDisabled() {
    return _permission().contracts != 1;
  }

  isDisabledItem() {
    return _permission().contracts_item == 0;
  }
}

export class DataContract {
  constructor(public contract: ContractProvider, public categories: Category[]) {
  }

}
