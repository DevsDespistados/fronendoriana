import {Component, OnInit} from '@angular/core';
import {OrianaInterface} from '../../projects/oriana-interface';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {ContractItemService} from '../../../services/contract/contract-item.service';
import {ContractItem} from '../../../model/contract/contract-item';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalContractItemComponent} from '../modal-contract-item/modal-contract-item.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ActivatedRoute} from '@angular/router';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-items-contracts',
  templateUrl: './oriana-items-contracts.component.html',
  styleUrls: ['./oriana-items-contracts.component.css']
})
export class OrianaItemsContractsComponent implements OnInit, OrianaInterface {
  current: number;
  idContract: number;
  items: ContractItem[];
  contractItemS: ContractItem;
  displayedColumns = ['position', 'item', 'unity', 'price', 'op'];
  sortedData: ContractItem[];
  routerBack = '/contracts';

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private contractItemService: ContractItemService,
              private activeRouter: ActivatedRoute) {
  }

  ngOnInit() {
    this.activeRouter.parent.params.subscribe(
      params => {
        this.idContract = params['id'];
      }
    );
    this.activeRouter.params.subscribe(params => {
      this.current = params['type'];
      this.load();
    });
  }

  create() {
    const dialogRef = this.dialog.open(ModalContractItemComponent, {
      width: '400px',
      data: new DataContractItem(this.contractItemS)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.formData) {
        if (!this.contractItemS.id_item) {
          this.contractItemService.addContractItem(dialogRef.componentInstance.formData)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.contractItemService.editContractItem(this.contractItemS.id_item, dialogRef.componentInstance.formData).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.contractItemS = this.items[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.contractItemS.item_name}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contractItemService.deleteContractItem(this.contractItemS.id_item)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const item = this.items[pos];
    this.contractItemS = new ContractItem(
      item.id_item,
      item.item_name,
      item.item_unity,
      item.item_price,
      item.item_Type,
      item.item_previousAmount,
      item.item_previousQuantity,
      item.idContract_contract);
    this.create();
  }

  getFilter(type: any) {
    if (type == 1) {
      this.contractItemService.getContractAdvance(this.idContract).subscribe(
        res => {
          this.items = <ContractItem[]>res;
        }
      );
    } else {
      this.contractItemService.getContractDiscount(this.idContract).subscribe(
        res => {
          this.items = <ContractItem[]>res;
        }
      );
    }
  }

  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.getFilter(this.current);
  }

  printReport() {
  }

  resetSelect() {
    this.contractItemS = new ContractItem(
      null,
      null,
      null,
      null,
      this.current,
      null,
      null,
      this.idContract
    );
  }

  search(text: string) {
  }

  setCurrent(type: any) {
  }

  sortData(sort: Sort): any {
  }

  isDisabled() {
    return _permission().contracts_item != 1;
  }
}

export class DataContractItem {
  constructor(public item: ContractItem) {
  }
}
