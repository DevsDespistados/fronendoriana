import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaItemsContractsComponent} from './oriana-items-contracts.component';

describe('OrianaItemsContractsComponent', () => {
  let component: OrianaItemsContractsComponent;
  let fixture: ComponentFixture<OrianaItemsContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaItemsContractsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaItemsContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
