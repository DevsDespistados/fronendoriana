import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeItemsContractsComponent} from './oriana-home-items-contracts.component';

describe('OrianaHomeItemsContractsComponent', () => {
  let component: OrianaHomeItemsContractsComponent;
  let fixture: ComponentFixture<OrianaHomeItemsContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeItemsContractsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeItemsContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
