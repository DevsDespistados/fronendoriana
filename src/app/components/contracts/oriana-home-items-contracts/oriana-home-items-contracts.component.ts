import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ContractProviderService} from '../../../services/contract/contract-provider.service';
import {ViewContract} from '../../../model/contract/view-contract';

@Component({
  selector: 'app-oriana-home-items-contracts',
  templateUrl: './oriana-home-items-contracts.component.html',
  styleUrls: ['./oriana-home-items-contracts.component.css']
})
export class OrianaHomeItemsContractsComponent implements OnInit, AfterViewChecked {

  titles = {
    module: 'ITEMS',
    advance: 'AVANCE',
    discount: 'DESCUENTO',
  };
  idContract;
  contract: ViewContract;
  navLinks = [];
  constructor(private cdRef: ChangeDetectorRef,
              private activateRoute: ActivatedRoute,
              private contratService: ContractProviderService) {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.idContract = params['id'];
      this.navLinks = [
        {label: 'Avance', path: '/contracts/' + this.idContract + '/item/1'},
        {label: 'Descuento', path: '/contracts/' + this.idContract + '/item/0'},
      ];
      this.getContract();
    });
  }

  getHeight(tab) {
    try {
      return tab._elementRef.nativeElement.offsetHeight;
    } catch (e) {
      console.log('error');
    }
  }

  getNumberContract() {
    if (this.contract) {
      return 'N\u00b0 Contrato: ' + this.contract.numberContract;
    } else {
      return 'N\u00b0 Contrato: ';
    }
  }

  getContract() {
    this.contratService.showViewContract(this.idContract).subscribe(
      res => {
        this.contract = <ViewContract>res;
        this.titles.module.concat(this.contract.numberContract);
      }
    );
  }
}
