import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalContractsComponent} from './modal-contracts.component';

describe('ModalContractsComponent', () => {
  let component: ModalContractsComponent;
  let fixture: ComponentFixture<ModalContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalContractsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
