import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Category} from '../../../model/accounts/category';
import {ContractProvider} from '../../../model/contract/contract-provider';
import {Provider} from '../../../model/personal/provider';
import {ViewProjects} from '../../../model/project/view-projects';
import {ProjectService} from '../../../services/project/project.service';
import {ProviderService} from '../../../services/personal/provider.service';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';

@Component({
  selector: 'app-modal-contracts',
  templateUrl: './modal-contracts.component.html',
  styleUrls: ['./modal-contracts.component.css']
})
export class ModalContractsComponent {
  text = ConfText;
  formData;
  contract: ContractProvider;
  categories: Category[] = [];
  providers: Provider[] = [];
  projects: ViewProjects[] = [];

  constructor(
    public dialogRef: MatDialogRef<ModalContractsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private projectService: ProjectService,
    private providerService: ProviderService,
    private snack: MatSnackBar) {
    this.contract = this.data.contract;
    this.categories = this.data.categories;
    this.getProjects();
    this.getProviders();
  }

  closeDialog() {
    this.formData = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.formData = this.contract;
      this.dialogRef.close();
    }
  }

  getProjects() {
    this.projectService.getOpen().subscribe(
      res => {
        this.projects = <ViewProjects[]>res;
      },
      error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  getProviders() {
    this.providerService.getActives(1).subscribe(
      res => {
        this.providers = <Provider[]>res;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  validData() {
    return this.contract.idCategoryContract == 0 || this.contract.idCategoryContract == null
      || this.contract.projects_idProject == 0 || this.contract.projects_idProject == null
      || this.contract.provider_idProvider == 0 || this.contract.provider_idProvider == null
      || this.contract.numberContract == '' || this.contract.numberContract == null;
  }
}
