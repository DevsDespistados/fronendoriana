import {Component, OnInit} from '@angular/core';
import {_permission} from '../../services/permissions';
import {Router} from '@angular/router';
import {UserRoutesService} from '../navigation/user-routes.service';
import {DashboardService} from '../../services/dashboard.service';
import {ToggleService} from '../../toggle.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'Oriana Servicios de Ingenieria SRL';
  data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'My First Dataset',
        data: [65, 59, 80, 81, 56, 55, 40],
        fill: true,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)',
        ],
        borderColor: [
          'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
          'rgb(75, 192, 192)',
          'rgb(54, 162, 235)',
          'rgb(153, 102, 255)',
          'rgb(201, 203, 207)',
        ],
        borderWidth: 1,
      },
    ],
  };

  constructor(private router: Router,
              public userRoutesService: UserRoutesService,
              public dashboardService: DashboardService,
              private toggleService: ToggleService) {
  }

  ngOnInit() {
    this.toggleService.setOpen(false);
    this.userRoutesService.loadRoutes();
    this.dashboardService.getPersonal().subscribe(r => {
      console.log(r);
    });
    this.dashboardService.getOwnerEquipment().subscribe(r => {
      console.log(r);
    });
    this.dashboardService.getProjects().subscribe(r => {
      console.log(r);
    });
  }

  isModule(name) {
    if (name == 'Personal') {
      this.toggleService.setOpen(true);
      this.getRoutePersonal();
    } else if (name == 'Planillas') {
      this.toggleService.setOpen(true);
      this.getRouterForms();
    } else if (name == 'Cuentas') {
      this.toggleService.setOpen(true);
      this.getRouterAccounts();
    // } else if (name == 'Reportes') {
    //   this.toggleService.setOpen(true);
    //   this.getRouteReportsAccount();
    } else if (name == 'Cartas') {
      this.toggleService.setOpen(true);
      this.getRouteLetters();
    } else if (name == 'Garantias') {
      this.toggleService.setOpen(true);
      this.getRouteGuarantes();
    } else if (name == 'Pedidos') {
      this.toggleService.setOpen(true);
      this.getRouteOrders();
    } else {
      this.toggleService.setOpen(true);
    }
  }

  getRoutePersonal(): void {
    if (_permission().p_client > 0) {
      this.router.navigate(['/personal/clients/']);
    } else if (_permission().p_provider > 0) {
      this.router.navigate(['/personal/providers']);
    } else if (_permission().p_transit > 0) {
      this.router.navigate(['/personal/transits']);
    } else if (_permission().p_permanent > 0) {
      this.router.navigate(['/personal/permanents']);
    } else if (_permission().p_eventual > 0) {
      this.router.navigate(['/personal/eventual']);
    } else if (_permission().p_otherCompany > 0) {
      this.router.navigate(['/personal/companies']);
    }
  }

  getRouterForms(): void {
    if (_permission().f_provider > 0) {
      this.router.navigate(['/forms/provider']);
    } else if (_permission().f_permanent > 0) {
      this.router.navigate(['/forms/permanent']);
    } else if (_permission().f_eventual > 0) {
      this.router.navigate(['/forms/eventual']);
    } else if (_permission().f_ownerEquipment > 0) {
      this.router.navigate(['/forms/equipment']);
    } else if (_permission().f_projects > 0) {
      this.router.navigate(['/forms/projects']);
    }
  }

  getRouterAccounts() {
    if (_permission().ac_b_banks > 0) {
      this.router.navigate(['/accounts/banks']);
    } else if (_permission().ac_pto_pettyCashOffice > 0) {
      this.router.navigate(['/accounts/pettyCashOffice']);
    } else if (_permission().ac_pt_pettyCash > 0) {
      this.router.navigate(['/accounts/pettyCash']);
    } else if (_permission().ac_pj_projects > 0) {
      this.router.navigate(['/accounts/projects']);
    }
  }

  getRouteReportsAccount() {
    // if (_permission().f_provider > 0) {
    this.router.navigate(['/reports/accounts']);
    // }
  }

  getRouteLetters() {
    if (_permission().l_s_Letters > 0) {
      this.router.navigate(['/letters/send']);
    } else if (_permission().l_r_Letters > 0) {
      this.router.navigate(['/letters/received']);
    } else if (_permission().l_p_Letters > 0) {
      this.router.navigate(['/letters/projects']);
    }
  }

  getRouteGuarantes() {
    if (_permission().ti_financial > 0) {
      this.router.navigate(['/tickets/creditLine']);
    } else if (_permission().ti_warranties > 0) {
      this.router.navigate(['/tickets/guarantees']);
    } else if (_permission().ti_projects > 0) {
      this.router.navigate(['/tickets/projects']);
    }
  }

  getRouteOrders() {
    if (_permission().o_order_p > 0) {
      this.router.navigate(['/order/petition']);
    } else if (_permission().o_order_q > 0) {
      this.router.navigate(['/order/quotation']);
    } else if (_permission().o_order_item > 0) {
      this.router.navigate(['/order/items']);
    }
  }
}
