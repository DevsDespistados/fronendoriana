import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LettersSendComponent} from './letters-send.component';

describe('LettersSendComponent', () => {
  let component: LettersSendComponent;
  let fixture: ComponentFixture<LettersSendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LettersSendComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LettersSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
