import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {LettersService} from '../../../services/letters/letters.service';
import {ViewLetter} from '../../../model/letters/view-letter';
import {ModalLettersComponent} from '../modal-letters/modal-letters.component';
import {compareDate, compareNumber, compareString, getYearShort} from '../../../services/GlobalFunctions';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {_permission, _user} from '../../../services/permissions';

@Component({
  selector: 'app-letters-send',
  templateUrl: './letters-send.component.html',
  styleUrls: ['./letters-send.component.css']
})
export class LettersSendComponent implements OnInit {
  letters: ViewLetter[] = [];
  titulo = 'vigente';
  displayedColumns = [
    'position',
    'date',
    'registry',
    'destination',
    'project',
    'reference',
    'options'
  ];
  textTooltip = ['Cambiar a Vigente', 'Dar de Baja'];
  sortedData: ViewLetter[];
  options = [
    new OptionOrianaTableToolbar('PENDIENTES', 'PENDIENTE'),
    new OptionOrianaTableToolbar('FINALIZADAS', 'FINALIZADA'),
    new OptionOrianaTableToolbar('TODOS', 'ALL')
  ];
  currentOption = 'PENDIENTE';
  idActualUser = _user().id;
  viewLetterSelect: ViewLetter;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private lettersService: LettersService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalLettersComponent, {
      width: '500px',
      data: new DataLetters(this.viewLetterSelect, 'new')
    });
    this.openDialog(dialogRef);
  }

  openDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result: ViewLetter) => {
      // const d = dialogRef.componentInstance.data;
      if (result) {
        if (!this.viewLetterSelect.idLetter) {
          this.lettersService.addLetter(result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, error => {
              this.load();
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            }
          );
        } else {
          this.lettersService.editLetter(this.viewLetterSelect.idLetter, result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            }, error => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.viewLetterSelect = <any>this.letters[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.viewLetterSelect.numberLetterL}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.lettersService.deleteLetter(this.viewLetterSelect.idLetter)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});

            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const f = this.letters[pos];
    this.viewLetterSelect = new ViewLetter(
      f.idLetter,
      f.numberLetterL,
      f.yearLetter,
      f.numberLetter,
      f.typeLetter,
      f.citeLetter,
      f.referenceLetter,
      f.dateLetter,
      f.fileLetter,
      f.stateLetter,
      f.attachedLetter,
      f.idProject,
      f.nameProject,
      f.codeLetterProject,
      f.idUser,
      f.file_letter,
      f.dateTracingLetter,
      f.from_or_to,
      f.last_user
    );
    const dialogRef = this.dialog.open(ModalLettersComponent, {
      width: '500px',
      data: new DataLetters(this.viewLetterSelect, 'update')
    });
    this.openDialog(dialogRef);
  }

  getFilter(type: any) {
    this.progress = true;
    this.lettersService.getAllLetters('E', type).subscribe(
      response => {
        this.letters = <ViewLetter[]>response;
        this.progress = false;
      }
    );
  }


  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'FECHA', 'NRO REGISTRO', 'PROYECTO', 'REFERENCIA'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE CARTAS ENVIADAS', head, body, 'wrap',
      {});
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.letters.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(this.printService.dateData(c.dateLetter).toUpperCase());
      d.push(c.numberLetterL.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      d.push(c.referenceLetter.toUpperCase());
      data.push(d);
    });
    return data;
  }
  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.lettersService.searchLetter(text, 'E').subscribe(
        (response: any) => {
          this.letters = <ViewLetter[]>response.letters;
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.letters.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.letters = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'dateLetter':
          return compareDate(a.dateLetter, b.dateLetter, isAsc);
        case 'numberLetterL':
          return compareNumber(Number(a.numberLetter), Number(b.numberLetter), isAsc);
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'from_or_to':
          return compareString(a.from_or_to, b.from_or_to, isAsc);
        case 'referenceLetter':
          return compareString(a.referenceLetter, b.referenceLetter, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  optionsDisabled() {
  }

  resetSelect() {
    this.viewLetterSelect = new ViewLetter(
      null,
      null,
      getYearShort(),
      null,
      'E',
      null,
      null,
      null,
      null,
      'PENDIENTE',
      null,
      null,
      null,
      null,
      _user().id,
      null,
      null,
      null,
    );
  }

  isThisUser(l: ViewLetter) {
    if (l.last_user == this.idActualUser && l.stateLetter == 'PENDIENTE') {
      return {background: '#ffeece'};
    }
  }

  isDisabled(l?) {
    if (l) {
      return (_permission().l_s_Letters != 1 || _permission().idUser != l?.user_creator) && _user().username.toUpperCase() != 'MGARCIA';
    }
    return _permission().l_s_Letters != 1;
  }

  isDisabledT(l?) {
    if (l) {
      return (_permission().l_ss_Letters == 0 || _permission().idUser != l?.user_creator) && _user().username.toUpperCase() != 'MGARCIA';
    } else {
      return _permission().l_ss_Letters == 0;
    }
  }
}

export class DataLetters {
  constructor(public form: ViewLetter, public type: string) {
  }
}
