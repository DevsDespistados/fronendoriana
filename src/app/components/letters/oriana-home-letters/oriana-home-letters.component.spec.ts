import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeLettersComponent} from './oriana-home-letters.component';

describe('OrianaHomeLettersComponent', () => {
  let component: OrianaHomeLettersComponent;
  let fixture: ComponentFixture<OrianaHomeLettersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeLettersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
