import {Component, OnInit} from '@angular/core';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-letters',
  templateUrl: './oriana-home-letters.component.html',
  styleUrls: ['./oriana-home-letters.component.css']
})
export class OrianaHomeLettersComponent implements OnInit {

  titles = {
    module: 'CARTAS',
    creditLine: 'ENVIADAS',
    guarantees: 'RECIBIDAS',
    projects: 'PROYECTOS'
  };

  links = [
    {label: 'ENVIADAS', path: '/letters/send', permission: _permission().l_s_Letters},
    {label: 'RECIBIDAS', path: '/letters/received', permission: _permission().l_r_Letters},
    {label: 'PROYECTOS', path: '/letters/projects', permission: _permission().l_p_Letters},
  ];

  navLinks = [];

  constructor() {
  }

  ngOnInit() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }
}
