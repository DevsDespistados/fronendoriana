import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {moment, zeroPad} from '../../../services/GlobalFunctions';
import {LettersService} from '../../../services/letters/letters.service';
import {ViewLetter} from '../../../model/letters/view-letter';
import {Project} from '../../../model/project/project';

@Component({
  selector: 'app-modal-letters',
  templateUrl: './modal-letters.component.html',
  styleUrls: ['./modal-letters.component.css']
})
export class ModalLettersComponent implements OnInit {
  text = ConfText;
  form: ViewLetter;
  projects: Project[] = [];
  lastNumberLetter;
  data: FormData;
  // file: FileInput;
  projectSelect: Project;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data_: any,
    private letterService: LettersService) {
    this.form = this.data_.form;
  }

  ngOnInit(): void {
    this.loadProjects();
    this.loadLastNumberLetter();
  }

  loadProjects() {
    this.letterService.allProjectForLetter(1).subscribe(
      response => {
        this.projects = <Project[]>response;
      }
    );
  }

  async loadLastNumberLetter() {
    this.letterService.lastLetter(this.form.typeLetter).subscribe(
      response => {
        this.lastNumberLetter = zeroPad(Number(response) + 1, 4);
      }, error => {
        console.log(error);
      }
    );
  }

  closeDialog() {
    this.data = null;
    this.dialogRef.close();
  }

  sendFormData() {
    this.letterService.lastLetter(this.form.typeLetter).subscribe(
      response => {
        this.form.numberLetter = zeroPad(Number(response) + 1, 4);
        this.form.dateTracingLetter = moment(this.form.dateLetter);
        this.dialogRef.close(this.form);
      }, error => {
        console.log(error);
      }
    );
  }


  validData() {
    if (this.form.typeLetter == 'E') {
      return this.form.idProject == null
        || this.form.dateLetter == null
        || this.form.attachedLetter == null
        || this.form.referenceLetter == null;
    } else {
      return this.form.idProject == null
        || this.form.dateLetter == null
        || this.form.attachedLetter == null
        || this.form.referenceLetter == null
        || this.form.citeLetter == null;
    }
  }

  showCode(evt) {
    this.projectSelect = this.projects.find(p => {
      return p.idProject == evt;
    });
    if (this.projectSelect) {
      this.generateNumberLetter();
    }
  }

  generateNumberLetter() {
    if (!this.form.idLetter) {
      this.form.numberLetterL =
        `${this.projectSelect.codeLetterProject}_${this.form.yearLetter}${this.form.typeLetter}${this.lastNumberLetter}`;
    } else {
      this.form.numberLetterL =
        `${this.projectSelect.codeLetterProject}_${this.form.yearLetter}${this.form.typeLetter}${this.form.numberLetter}`;
    }
  }

  isDisabled() {
    if (this.form) {
      if (this.form.idLetter) {
        return true;
      }
    }
  }
}
