import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalLettersComponent} from './modal-letters.component';

describe('ModalLettersComponent', () => {
  let component: ModalLettersComponent;
  let fixture: ComponentFixture<ModalLettersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalLettersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
