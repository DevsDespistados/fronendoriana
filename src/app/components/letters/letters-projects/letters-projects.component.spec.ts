import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LettersProjectsComponent} from './letters-projects.component';

describe('LettersProjectsComponent', () => {
  let component: LettersProjectsComponent;
  let fixture: ComponentFixture<LettersProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LettersProjectsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LettersProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
