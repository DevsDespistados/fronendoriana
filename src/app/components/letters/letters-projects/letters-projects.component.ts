import {Component, OnInit} from '@angular/core';
import {ViewProjects} from '../../../model/project/view-projects';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {LettersService} from '../../../services/letters/letters.service';
import {Project} from '../../../model/project/project';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {ModalLetterProjectComponent} from '../modal-letter-project/modal-letter-project.component';
import {_permission} from '../../../services/permissions';
import {LetterUsersProjectService} from '../../../services/letters/letter-users-project.service';
import {ModalLetterUsersComponent} from '../modal-letter-users/modal-letter-users.component';
import {UserModuleProject} from '../../../model/project/user-module-project';

@Component({
  selector: 'app-letters-projects',
  templateUrl: './letters-projects.component.html',
  styleUrls: ['./letters-projects.component.css']
})
export class LettersProjectsComponent implements OnInit {

  projects: ViewProjects[] = [];
  titulo = 'Disponibles';
  displayedColumns = [
    'position',
    'contract',
    'name',
    'status',
  ];
  sortedData: ViewProjects[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLE', 0),
    new OptionOrianaTableToolbar('TODOS', 2),
  ];
  tooltip = ['No Disponible', 'Disponible'];
  currentOption = 1;
  progress = false;
  projectSelect: Project;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private letterService: LettersService,
              private letterProjectUserService: LetterUsersProjectService) {
  }

  ngOnInit() {
    this.load();
  }

  getFilter(type: any) {
    this.letterService.allProjectForLetter(type).subscribe(
      res => {
        this.progress = false;
        this.projects = <ViewProjects[]>res;
      },
      error => {
        this.progress = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  load() {
    this.progress = true;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  update(id, state) {
    this.letterService.updateActiveLetterForProject(id, state).subscribe(
      response => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
        if (this.currentOption !== 2) {
          this.load();
        }
      }, (errorResponse: HttpErrorResponse) => {
        this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
      }
    );
  }

  search(text: string) {
    if (text !== '') {
      this.letterService.searchProjectLetter(text).subscribe(
        response => {
          this.projects = <ViewProjects[]>response;
          this.titulo = ConfText.resultSearch;
        }
      );
    } else {
      this.load();
    }
  }

  create() {
    const dialogRef = this.dialog.open(ModalLetterProjectComponent, {
      width: '300px',
      data: {
        code: this.projectSelect.codeLetterProject,
        nameProject: this.projectSelect.nameProject
      }
    });
    dialogRef.afterClosed().subscribe(d => {
      if (d) {
        this.letterService.putCodeProject(this.projectSelect.idProject, d).subscribe(
          data => {
            this.load();
            this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
          }, error => {
            this.load();
            if (error.status == 406) {
              this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
            } else {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          }
        );
      } else {
        this.projectSelect = null;
      }
    });
  }

  edit(k) {
    const p = this.projects[k];
    this.projectSelect = new Project(
      p.idProject,
      p.nroContractProject,
      p.nameProject,
      p.amountInitProject,
      p.amountProject,
      p.limitProject,
      p.limitUpdateProject,
      p.stateProject,
      p.fileProject,
      null,
      p.daysPassed,
      p.physicalAdvanceProject,
      p.activeProject,
      p.activeProjectForForms,
      null,
      p.startDateOP,
      p.dateRP,
      p.limitRPtoRDRP,
      p.dateRP,
      p.idClient,
      p.activeProjectAccount,
      p.descriptionRD,
      p.codeLetterProject,
      p.activeLetterForProject,
      p.activeProjectForTicket
    );
    this.create();
  }

  sortData(sort: Sort): any {
    const data = this.projects.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.projects = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'codeLetterProject':
          return compareString(a.codeLetterProject, b.codeLetterProject, isAsc);
        case 'activeProjectForForms':
          return compareNumber(a.activeProjectForForms, b.activeProjectForForms, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  isDisabled() {
    return _permission().l_p_Letters != 1;
  }

  showUsers(idProject: any) {
    this.letterProjectUserService.getUsers(idProject).subscribe(
      res => {
        this.dialog.open(ModalLetterUsersComponent, {
          width: '600px',
          data: {
            users: res,
            idProject: idProject
          }
        }).afterClosed().subscribe( (users: UserModuleProject[]) => {
          if (users) {
            this.saveUserModule(users, idProject);
          }
        });
      },
      error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }

  private saveUserModule(users: UserModuleProject[], idProject: number) {
    this.letterProjectUserService.updateUserModule(idProject, users).subscribe(
      res => {
        this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
      },
      error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      }
    );
  }
}
