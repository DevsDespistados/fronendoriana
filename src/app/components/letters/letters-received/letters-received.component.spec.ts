import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LettersReceivedComponent} from './letters-received.component';

describe('LettersReceivedComponent', () => {
  let component: LettersReceivedComponent;
  let fixture: ComponentFixture<LettersReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LettersReceivedComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LettersReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
