import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalLetterUsersComponent } from './modal-letter-users.component';

describe('ModalLetterUsersComponent', () => {
  let component: ModalLetterUsersComponent;
  let fixture: ComponentFixture<ModalLetterUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalLetterUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLetterUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
