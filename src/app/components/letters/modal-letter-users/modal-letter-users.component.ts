import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {UserModuleProject} from '../../../model/project/user-module-project';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-letter-users',
  templateUrl: './modal-letter-users.component.html',
  styleUrls: ['./modal-letter-users.component.css']
})
export class ModalLetterUsersComponent implements OnInit {
  text = ConfText;
  users: UserModuleProject[] = [];
  displayedColumns: string[] = ['select', 'username', 'name'];
  dataSource = new MatTableDataSource<UserModuleProject>([]);
  selection = new SelectionModel<UserModuleProject>(true, []);

  constructor(@Inject(MAT_DIALOG_DATA) private data: {users: UserModuleProject[], projectId: number},
              private dialogRef: MatDialogRef<ModalLetterUsersComponent>) {
    this.users = data.users;
    this.dataSource = new MatTableDataSource<UserModuleProject>(this.users);
  }

  ngOnInit() {
    this.users.filter(user => user.project_id !== null).forEach(user => {
      this.selection.select(user);
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: UserModuleProject): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id_user + 1}`;
  }


  saveUsers() {
    this.dialogRef.close(this.selection.selected);
  }
}
