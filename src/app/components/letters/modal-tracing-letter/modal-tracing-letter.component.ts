import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {moment} from '../../../services/GlobalFunctions';
import {UserService} from '../../../services/user/user.service';
import {User} from '../../../model/user/user';
import {ViewTracingLetter} from '../../../model/letters/view-tracing-letter';
import {ViewLetter} from '../../../model/letters/view-letter';

@Component({
  selector: 'app-modal-tracing-letter',
  templateUrl: './modal-tracing-letter.component.html',
  styleUrls: ['./modal-tracing-letter.component.css']
})
export class ModalTracingLetterComponent implements OnInit {
  text = ConfText;
  form: ViewTracingLetter;
  users: User[] = [];
  data;
  letter: ViewLetter;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data_: any,
    private userService: UserService) {
    this.form = this.data_.form;
    this.letter = this.data_.letter;
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.userService.getUserActives().subscribe(
      response => {
        this.users = <User[]>response;
      }
    );
  }

  closeDialog() {
    this.data = null;
    this.dialogRef.close();
  }

  sendFormData() {
    this.data = {
      date_tracingLetter: moment(this.form.dateTracingLetter),
      state_tracingLetter: this.form.stateLetter,
      instruction_tracingLetter: this.form.instructionTracingLetter,
      idLetter: this.form.idLetter,
      id: this.form.id
    };
    this.dialogRef.close(this.data);
  }


  validData() {
    return this.form.idLetter == null
      || this.form.dateTracingLetter == null
      || this.form.stateLetter == null
      || this.form.instructionTracingLetter == null;
  }

  isDisabled() {
    if (this.form) {
      if (this.form.idTracingLetter) {
        return true;
      }
    }
  }
}
