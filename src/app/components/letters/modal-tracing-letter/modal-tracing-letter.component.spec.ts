import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTracingLetterComponent} from './modal-tracing-letter.component';

describe('ModalTracingLetterComponent', () => {
  let component: ModalTracingLetterComponent;
  let fixture: ComponentFixture<ModalTracingLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTracingLetterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTracingLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
