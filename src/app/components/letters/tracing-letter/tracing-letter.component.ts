import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {LettersService} from '../../../services/letters/letters.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {ViewTracingLetter} from '../../../model/letters/view-tracing-letter';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalTracingLetterComponent} from '../modal-tracing-letter/modal-tracing-letter.component';
import {ViewLetter} from '../../../model/letters/view-letter';
import {compareDate, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';
import {ReportService} from '../../../services/report.service';

@Component({
  selector: 'app-tracing-letter',
  templateUrl: './tracing-letter.component.html',
  styleUrls: ['./tracing-letter.component.css']
})
export class TracingLetterComponent implements OnInit {
  @Output() getLetter = new EventEmitter();
  viewTracingLetter: ViewTracingLetter[] = [];
  titulo = 'vigente';
  displayedColumns = [
    'position',
    'date',
    'responsible',
    'instruction',
    'options'
  ];
  textTooltip = ['Cambiar a Vigente', 'Dar de Baja'];
  sortedData: ViewTracingLetter[];
  viewTracingLetterSelect: ViewTracingLetter;
  letterSelect: ViewLetter;
  progress = false;
  idLetter;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private lettersService: LettersService,
              private activeRoute: ActivatedRoute,
              private router: Router,
              private reportService: ReportService) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(p => {
      this.idLetter = p['idLetter'];
      this.load();
    });
  }

  create() {
    const dialogRef = this.dialog.open(ModalTracingLetterComponent, {
      width: '300px',
      data: new DataTracingLetters(this.viewTracingLetterSelect, this.letterSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      const d = result;
      if (d) {
        if (!this.viewTracingLetterSelect.idTracingLetter) {
          this.lettersService.createTracingLetter(d).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, error => {
              this.load();
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            }
          );
        } else {
          this.lettersService.editLetter(this.viewTracingLetterSelect.idTracingLetter, d).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            }, error => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.viewTracingLetterSelect = <any>this.viewTracingLetter[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.viewTracingLetterSelect.numberLetterL}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.lettersService.deleterTracingLetter(this.viewTracingLetterSelect.idTracingLetter)
          .subscribe(
            (formData: any ) => {
              if (formData.delete) {
                this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
                this.router.navigate(['../'], {relativeTo: this.activeRoute});
              }
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const f = this.viewTracingLetter[pos];
    this.viewTracingLetterSelect = new ViewTracingLetter(
      f.idTracingLetter,
      f.numberLetterL,
      f.stateLetter,
      f.id,
      f.name,
      f.lastName,
      f.active,
      f.idTracingLetter,
      f.instructionTracingLetter,
      f.dateTracingLetter,
      f.stateTracingLetter
    );
    this.create();
  }

  isEmpty(): any {
  }

  load() {
    this.progress = true;
    this.lettersService.getViewTracingLetter(this.idLetter).subscribe(
      response => {
        this.progress = false;
        this.viewTracingLetter = <ViewTracingLetter[]>response['tracing'];
        this.letterSelect = response['letter'];
        this.getLetter.emit(this.letterSelect);
        this.resetSelect();
      }
    );
  }

  printReport() {
    this.reportService.printReport('Letter', {ID_LETTER: this.idLetter});
  }

  sortData(sort: Sort): any {
    const data = this.viewTracingLetter.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.viewTracingLetter = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'dateTracingLetter':
          return compareDate(a.dateTracingLetter, b.dateTracingLetter, isAsc);
        case 'name':
          return compareString(a.name, b.name, isAsc);
        default:
          return 0;
      }
    });
  }

  resetSelect() {
    this.viewTracingLetterSelect = new ViewTracingLetter(
      this.idLetter,
      null,
      this.letterSelect.stateLetter,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  isDisabled(tracing?: ViewTracingLetter) {
    if (tracing && tracing.from_user_id != _permission().idUser) {
      return true;
    }

    if (this.letterSelect?.typeLetter == 'E') {
      return _permission().l_ss_Letters != 1;
    } else {
      return _permission().l_rs_Letters != 1;
    }
  }
}

export class DataTracingLetters {
  constructor(public form: ViewTracingLetter, public letter: ViewLetter) {
  }
}
