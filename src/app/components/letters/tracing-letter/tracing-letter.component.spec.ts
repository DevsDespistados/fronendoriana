import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TracingLetterComponent} from './tracing-letter.component';

describe('TracingLetterComponent', () => {
  let component: TracingLetterComponent;
  let fixture: ComponentFixture<TracingLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TracingLetterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
