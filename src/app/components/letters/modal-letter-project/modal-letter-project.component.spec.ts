import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalLetterProjectComponent} from './modal-letter-project.component';

describe('ModalLetterProjectComponent', () => {
  let component: ModalLetterProjectComponent;
  let fixture: ComponentFixture<ModalLetterProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalLetterProjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalLetterProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
