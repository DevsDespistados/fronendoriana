import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-letter-project',
  templateUrl: './modal-letter-project.component.html',
  styleUrls: ['./modal-letter-project.component.css']
})
export class ModalLetterProjectComponent implements OnInit {
  text = ConfText;
  code: string;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data_: any) {
    this.code = this.data_.code;
  }

  ngOnInit(): void {
  }

  closeDialog() {
    this.code = null;
    this.dialogRef.close();
  }

  sendFormData() {
    this.dialogRef.close(this.code);
  }


  validData() {
    return this.code == null || this.code == '';
  }
}
