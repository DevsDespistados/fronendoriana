import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeTracingLettersComponent} from './oriana-home-tracing-letters.component';

describe('OrianaHomeTracingLettersComponent', () => {
  let component: OrianaHomeTracingLettersComponent;
  let fixture: ComponentFixture<OrianaHomeTracingLettersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeTracingLettersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeTracingLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
