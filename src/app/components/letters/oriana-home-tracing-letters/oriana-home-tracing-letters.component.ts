import {Component, OnInit} from '@angular/core';
import {ViewLetter} from '../../../model/letters/view-letter';

@Component({
  selector: 'app-oriana-home-tracing-letters',
  templateUrl: './oriana-home-tracing-letters.component.html',
  styleUrls: ['./oriana-home-tracing-letters.component.css']
})
export class OrianaHomeTracingLettersComponent implements OnInit {

  titles = {
    module: 'SEGUIMIENTO',
  };
  letterSelect: ViewLetter;

  constructor() {
  }

  navLinks = [
    {label: 'ENVIADAS', path: '/letters/send'},
    {label: 'RECIBIDAS', path: '/letters/received'},
    {label: 'PROYECTOS', path: '/letters/projects'},
  ];

  ngOnInit() {
  }

  setLetter(select) {
    this.letterSelect = select;
  }

  getSubtitle() {
    if (this.letterSelect) {
      return 'Carta: ' + this.letterSelect.numberLetterL;
    } else {
      return 'Carta: ';
    }
  }
}
