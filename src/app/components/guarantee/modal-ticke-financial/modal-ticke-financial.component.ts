import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FinancialService} from '../../../services/ticket/financial.service';
import {TicketFinancialService} from '../../../services/ticket/ticket-financial.service';
import {ViewTicket} from '../../../model/ticket/view-ticket';
import {Financial} from '../../../model/ticket/financial';
import {ViewProjects} from '../../../model/project/view-projects';
import {compareDateTime, momentDateTime} from '../../../services/GlobalFunctions';
import {FileInput} from 'ngx-material-file-input';

@Component({
  selector: 'app-modal-ticke-financial',
  templateUrl: './modal-ticke-financial.component.html',
  styleUrls: ['./modal-ticke-financial.component.css']
})
export class ModalTickeFinancialComponent implements OnInit {
  text = ConfText;
  form: ViewTicket;
  projects: ViewProjects[] = [];
  financials: Financial[] = [];
  types = [
    {value: 'SP', name: 'Seriedad de Propuesta(SP)'},
    {value: 'CC', name: 'Cumplimiento de Contrato(CC)'},
    {value: 'CIA', name: 'Correcta Inversion de Anticipo(CIA)'},
    {value: 'OTRA', name: 'Otro tipo de Garantia(OTRO)'},
  ];
  initTime;
  endTime;
  states = [
    'vigente',
    'baja'
  ];
  data: FormData;
  file: FileInput;
  financialSelect: Financial;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data_: any,
    private ticketService: TicketFinancialService,
    private financialService: FinancialService) {
    this.form = this.data_.form;
    this.initTime = this.form ?
      this.form.startDateTicket.split(' ')[1]
      : null;
    this.endTime = this.form ?
      this.form.endDateTicket.split(' ')[1]
      : null;

  }

  ngOnInit(): void {
    this.loadFinancials();
    this.loadProjects();
  }

  loadProjects() {
    this.ticketService.filterProjectOfTicket(1).subscribe(
      response => {
        this.projects = <ViewProjects[]>response;
      }
    );
  }

  loadFinancials() {
    this.financialService.filterFinancial('vigente').subscribe(
      response => {
        this.financials = response['financials'];
      }
    );
  }

  closeDialog() {
    this.data = null;
    this.dialogRef.close();
  }

  sendFormData() {

    this.data = new FormData();
    this.data.append('type_ticket', this.form.typeTicket);
    this.data.append('amountWarranty_ticket', String(this.form.amountWarrantyTicket));
    this.data.append('state_ticket', this.form.stateTicket);
    this.data.append('startDate_ticket', momentDateTime(this.form.startDateTicket, this.initTime));
    this.data.append('endDate_ticket', momentDateTime(this.form.endDateTicket, this.endTime));
    if (this.form.idTicket && this.data_.type == 'reNew') {
      this.data.append('id_ticketR', String(this.form.idTicketRenovated));
      this.data.append('idTicket', String(this.form.idTicket));
    }
    if (this.form.typeTicket != 'SP') {
      this.data.append('idProject', String(this.form.idProject));
    }
    this.data.append('idFinancial', String(this.form.idFinancial));
    if (this.file) {
      this.data.append('file_ticket', this.file.files[0], this.file.fileNames[0]);
    } else {
      this.data.append('file_ticket', null);
    }
    this.dialogRef.close();
  }


  validData() {
    return this.form.idFinancial == null
      || this.form.typeTicket == null
      || (this.form.typeTicket != 'SP' && this.form.idProject == null)
      || this.form.startDateTicket == null
      || this.form.endDateTicket == null
      || this.form.amountWarrantyTicket <= 0
      || this.initTime == null
      || this.endTime == null
      || this.financialSelect == null
      || this.isReNew()
      || this.minDate() == false;
  }

  minDate() {
    return compareDateTime(this.form.startDateTicket, this.initTime,
      this.form.endDateTicket, this.endTime);
  }

  isDisabledInputs() {
    return this.data_.type == 'reNew';
  }

  asignFinancial(evt) {
    this.financialSelect = this.financials.find(f => {
      return f.idFinancial == evt;
    });
  }

  isReNew() {
    if (this.data_.type == 'reNew') {
      return false;
    } else {
      return this.form.amountWarrantyTicket > this.financialSelect.availableBalanceFinancial;
    }
  }

  getAmountAvailable() {
    if (this.form.idFinancial) {
      this.asignFinancial(this.form.idFinancial);
    }
    return this.financialSelect ? this.financialSelect.availableBalanceFinancial - this.form.amountWarrantyTicket : 0;
  }
}
