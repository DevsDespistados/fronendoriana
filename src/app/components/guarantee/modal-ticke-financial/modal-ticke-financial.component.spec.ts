import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalTickeFinancialComponent} from './modal-ticke-financial.component';

describe('ModalTickeFinancialComponent', () => {
  let component: ModalTickeFinancialComponent;
  let fixture: ComponentFixture<ModalTickeFinancialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTickeFinancialComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTickeFinancialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
