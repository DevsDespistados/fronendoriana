import {Component, OnInit} from '@angular/core';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-oriana-home-guarantees',
  templateUrl: './oriana-home-guarantees.component.html',
  styleUrls: ['./oriana-home-guarantees.component.css']
})
export class OrianaHomeGuaranteesComponent implements OnInit {

  titles = {
    module: 'GARANTIAS',
    creditLine: 'LINEA DE CREDITO',
    guarantees: 'GARANTIAS',
    projects: 'PROYECTOS'
  };


  private links = [
    {label: 'LINEAS DE CREDITO', path: '/tickets/creditLine', permission: _permission().ti_financial},
    {label: 'GARANTIAS', path: '/tickets/guarantees', permission: _permission().ti_warranties},
    {label: 'PROYECTOS', path: '/tickets/projects', permission: _permission().ti_projects},
  ];

  navLinks = [];

  constructor() {
  }

  ngOnInit() {
    this.links.forEach(link => {
      if (link.permission > 0) {
        this.navLinks.push(link);
      }
    });
  }

}
