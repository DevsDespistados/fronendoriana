import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeGuaranteesComponent} from './oriana-home-guarantees.component';

describe('OrianaHomeGuaranteesComponent', () => {
  let component: OrianaHomeGuaranteesComponent;
  let fixture: ComponentFixture<OrianaHomeGuaranteesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeGuaranteesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeGuaranteesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
