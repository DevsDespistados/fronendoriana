import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {compareDate, compareNumber, compareString, currentTime, timeInTwoDate} from '../../../services/GlobalFunctions';
import {TicketFinancialService} from '../../../services/ticket/ticket-financial.service';
import {ViewTicket} from '../../../model/ticket/view-ticket';
import {ModalTickeFinancialComponent} from '../modal-ticke-financial/modal-ticke-financial.component';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-ticket-financial',
  templateUrl: './ticket-financial.component.html',
  styleUrls: ['./ticket-financial.component.css']
})
export class TicketFinancialComponent implements OnInit {
  tickets: ViewTicket[] = [];
  totals = {
    amountWarrantyTicket: 0,
  };
  titulo = 'vigente';
  displayedColumns = [
    'position',
    'line',
    'type',
    'project',
    'amount',
    'start',
    'end',
    'state',
    'options'
  ];
  textTooltip = ['Cambiar a Vigente', 'Dar de Baja'];
  sortedData: ViewTicket[];
  options = [
    new OptionOrianaTableToolbar('VIGENTE', 'vigente'),
    new OptionOrianaTableToolbar('VENCIDAS', 'vencida'),
    new OptionOrianaTableToolbar('BAJA', 'baja'),
    new OptionOrianaTableToolbar('RENOVADA', 'renovadas'),
    new OptionOrianaTableToolbar('VIGENTES Y VENCIDAS', 'vv'),
    new OptionOrianaTableToolbar('TODOS', 'all')
  ];
  currentOption = 'vigente';

  viewTicketSelect: ViewTicket;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private ticketService: TicketFinancialService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalTickeFinancialComponent, {
      width: '300px',
      data: new DataTicketFinancial(this.viewTicketSelect, 'new')
    });
    this.openDialog(dialogRef);
  }

  openDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result: FormData) => {
      const d = dialogRef.componentInstance.data;
      if (d) {
        if (!this.viewTicketSelect.idTicket || dialogRef.componentInstance.data_.type == 'reNew') {
          this.ticketService.addTicket(d).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            }, error => {
              this.load();
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            }
          );
        } else {
          this.ticketService.editTicket(this.viewTicketSelect.idTicket, d).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            }, error => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.viewTicketSelect = <any>this.tickets[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.viewTicketSelect.typeTicket}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.ticketService.deleteTicket(this.viewTicketSelect.idTicket)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});

            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const f = this.tickets[pos];
    this.viewTicketSelect = new ViewTicket(
      f.idTicket,
      f.typeTicket,
      f.amountWarrantyTicket,
      f.stateTicket,
      f.startDateTicket,
      f.endDateTicket,
      f.fileTicket,
      f.idFinancial,
      f.nameFinancial,
      f.stateFinancial,
      f.amountLimitFinancial,
      f.amountAvailableFinancial,
      f.idProject,
      f.nameProject,
      f.idTicketRenovated,
      f.dependencyFinancial,
      f.childTicket
    );
    const dialogRef = this.dialog.open(ModalTickeFinancialComponent, {
      width: '300px',
      data: new DataTicketFinancial(this.viewTicketSelect, 'update')
    });
    this.openDialog(dialogRef);
  }

  reNew(pos: number) {
    const f = this.tickets[pos];
    this.viewTicketSelect = new ViewTicket(
      f.idTicket,
      f.typeTicket,
      f.amountWarrantyTicket,
      f.stateTicket,
      f.startDateTicket,
      f.endDateTicket,
      f.fileTicket,
      f.idFinancial,
      f.nameFinancial,
      f.stateFinancial,
      f.amountLimitFinancial,
      f.amountAvailableFinancial,
      f.idProject,
      f.nameProject,
      f.idTicketRenovated,
      f.dependencyFinancial,
      f.childTicket
    );
    const dialogRef = this.dialog.open(ModalTickeFinancialComponent, {
      width: '300px',
      data: new DataTicketFinancial(this.viewTicketSelect, 'reNew')
    });
    this.openDialog(dialogRef);
  }

  getFilter(type: any) {
    this.progress = true;
    this.ticketService.getFilterTicket(type).subscribe(
      response => {
        this.tickets = response['tickets'];
        this.progress = false;
        this.setTotal();
      }
    );
  }


  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'L. CREDITO', 'TIPO', 'PROYECTO', 'MONTO', 'DESDE', 'HASTA', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE GARANTIAS', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.tickets.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameFinancial.toUpperCase());
      d.push(c.typeTicket.toUpperCase());
      d.push(c.nameProject.toUpperCase());
      d.push(this.printService.moneyData(c.amountWarrantyTicket));
      d.push(this.printService.dateData(c.startDateTicket).toUpperCase());
      d.push(this.printService.dateData(c.endDateTicket).toUpperCase());
      d.push(c.stateTicket.toUpperCase());
      data.push(d);
    });
    data.push([
      '',
      '',
      '',
      'TOTAL',
      this.printService.moneyData(this.totals.amountWarrantyTicket),
      '',
      '',
      ''
    ]);
    return data;
  }


  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.ticketService.searchProjectOfTicket(text).subscribe(
        response => {
          this.tickets = <ViewTicket[]>response;
          this.setTotal();
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.tickets.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.tickets = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameFinancial':
          return compareString(a.nameFinancial, b.nameFinancial, isAsc);
        case 'typeTicket':
          return compareString(a.typeTicket, b.typeTicket, isAsc);
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'amountWarrantyTicket':
          return compareNumber(a.amountWarrantyTicket, b.amountWarrantyTicket, isAsc);
        case 'startDateTicket':
          return compareDate(a.startDateTicket, b.startDateTicket, isAsc);
        case 'endDateTicket':
          return compareDate(a.endDateTicket, b.endDateTicket, isAsc);
        case 'stateTicket':
          return compareString(a.stateTicket, b.stateTicket, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    const r = this.tickets[pos];
    r.stateTicket = state == 1 ? 'vigente' : 'baja';
    this.ticketService.updateStateTicket(r.idTicket, r).subscribe(
      formData => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      },
      error => {
        this.load();
        if (error.status == 406) {
          this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
        } else {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        }
      });
  }

  resetSelect() {
    this.viewTicketSelect = new ViewTicket(
      null,
      null,
      0,
      'vigente',
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  isColor(row: ViewTicket) {
    const total = timeInTwoDate(row.endDateTicket, row.startDateTicket);
    const tr = currentTime(row.startDateTicket);
    if (row.stateTicket != 'baja') {
      if (tr >= total) {
        return {background: 'rgb(255,155, 155)'};
      } else {
        const p = (tr * 100) / total;
        if (p > 0) {
          const r = Math.round(p);
          const g = 255 - r;
          const b = 255 - r;
          return {background: `rgb(255,${g},${b})`};
        }
      }
    }
  }

  optionsDisabled(row: ViewTicket): boolean {
    return this.isDisabled() || row.childTicket > 0;
  }

  setTotal() {
    const t = {
      amountWarrantyTicket: 0,
    };
    this.tickets.forEach(f => {
      t.amountWarrantyTicket += Number(f.amountWarrantyTicket);
    });
    this.totals = t;
  }

  isDisabled() {
    return _permission().ti_warranties != 1;
  }
}

export class DataTicketFinancial {
  constructor(public form: ViewTicket, public type: string) {
  }
}
