import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TicketFinancialComponent} from './ticket-financial.component';

describe('TicketFinancialComponent', () => {
  let component: TicketFinancialComponent;
  let fixture: ComponentFixture<TicketFinancialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TicketFinancialComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketFinancialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
