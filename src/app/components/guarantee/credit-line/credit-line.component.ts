import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {FinancialService} from '../../../services/ticket/financial.service';
import {Financial} from '../../../model/ticket/financial';
import {ModalFianacialComponent} from '../modal-fianacial/modal-fianacial.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {compareNumber, compareString} from '../../../services/GlobalFunctions';
import {_permission} from '../../../services/permissions';

@Component({
  selector: 'app-credit-line',
  templateUrl: './credit-line.component.html',
  styleUrls: ['./credit-line.component.css']
})
export class CreditLineComponent implements OnInit {
  financials: Financial[] = [];
  totals = {
    amountLimitFinancial: 0,
    amountUsedFinancial: 0,
    availableBalanceFinancial: 0
  };
  titulo = 'vigente';
  displayedColumns = [
    'position',
    'identity',
    'line',
    'useMount',
    'availableMount',
    'options'
  ];
  textTooltip = ['Baja', 'Vigente'];
  sortedData: Financial[];
  options = [
    new OptionOrianaTableToolbar('VIGENTE', 'vigente'),
    new OptionOrianaTableToolbar('BAJA', 'baja'),
    new OptionOrianaTableToolbar('TODAS', 'all')
  ];
  currentOption = 'vigente';

  financialSelect: Financial;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private printService: PrintServiceService,
              private financialService: FinancialService) {
  }

  ngOnInit() {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalFianacialComponent, {
      width: '300px',
      data: new DataFinancial(this.financialSelect)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.financialSelect.idFinancial) {
          this.financialService.addFinancial(result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
            },
            error => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            }
          );
        } else {
          this.financialService.editFinancial(this.financialSelect.idFinancial, result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              if (error.status == 406) {
                this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
              } else {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            });
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.financialSelect = <any>this.financials[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.financialSelect.nameFinancial}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.progress = true;
        this.financialService.deleteFinancial(this.financialSelect.idFinancial)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});

            }, error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
              this.progress = false;
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const f = this.financials[pos];
    this.financialSelect = new Financial(
      f.idFinancial,
      f.nameFinancial,
      f.stateFinancial,
      f.amountLimitFinancial,
      f.amountUsedFinancial,
      f.availableBalanceFinancial,
      f.dependencyFinancial
    );
    this.create();
  }

  getFilter(type: any) {
    this.progress = true;
    this.financialService.getFilterFinancial(type).subscribe(
      response => {
        this.financials = response['financials'];
        this.totals = response['totals'];
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
  }

  load() {
    this.resetSelect();
    this.progress = false;
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'IDENTIFICADOR', 'MONTO LINEA', 'MONTO UTILIZADO', 'MONTO DISPONIBLE', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE LINEAS DE CREDITO', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
        2: {
          halign: 'right',
        },
        3: {
          halign: 'right',
        },
        4: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.financials.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.nameFinancial.toUpperCase());
      d.push(this.printService.moneyData(c.amountLimitFinancial));
      d.push(this.printService.moneyData(c.amountUsedFinancial));
      d.push(this.printService.moneyData(c.availableBalanceFinancial));
      d.push(c.stateFinancial.toUpperCase());
      data.push(d);
    });
    data.push([
      '',
      'TOTAL',
      this.printService.moneyData(this.totals.amountLimitFinancial),
      this.printService.moneyData(this.totals.amountUsedFinancial),
      this.printService.moneyData(this.totals.availableBalanceFinancial),
      ''
    ]);
    return data;
  }

  search(text: string) {
    if (text !== '') {
      this.progress = true;
      this.financialService.searchFinancial(text).subscribe(
        response => {
          this.financials = <Financial[]>response;
          this.setTotal();
          this.progress = false;
        }, error1 => {
          this.progress = false;
          this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.financials.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.financials = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nameFinancial':
          return compareString(a.nameFinancial, b.nameFinancial, isAsc);
        case 'amountLimitFinancial':
          return compareNumber(a.amountLimitFinancial, b.amountLimitFinancial, isAsc);
        case 'amountUsedFinancial':
          return compareNumber(a.amountUsedFinancial, b.amountUsedFinancial, isAsc);
        case 'availableBalanceFinancial':
          return compareNumber(a.availableBalanceFinancial, b.availableBalanceFinancial, isAsc);
        default:
          return 0;
      }
    });
  }

  setCurrent(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state, pos) {
    const r = this.financials[pos];
    r.stateFinancial = state == 1 ? 'vigente' : 'baja';
    this.financialService.editFinancial(r.idFinancial, r).subscribe(
      formData => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      },
      error => {
        if (error.status == 406) {
          this.snack.openFromComponent(SnackEmptyComponent, {duration: ConfText.timer, data: {msg: error.error}});
        } else {
          this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        }
      });
  }

  resetSelect() {
    this.financialSelect = new Financial(
      null,
      null,
      'vigente',
      0,
      null,
      null,
      null
    );
  }

  setTotal() {
    const t = {
      amountLimitFinancial: 0,
      amountUsedFinancial: 0,
      availableBalanceFinancial: 0
    };
    this.financials.forEach(f => {
      t.amountLimitFinancial += f.amountUsedFinancial;
      t.amountUsedFinancial += f.amountUsedFinancial;
      t.availableBalanceFinancial += f.availableBalanceFinancial;
    });
    this.totals = t;
  }

  isDisabled() {
    return _permission().ti_financial != 1;
  }

  isDisabledBtn(ele: Financial) {
    return this.isDisabled() || ele.amountUsedFinancial > 0;
  }
}

export class DataFinancial {
  constructor(public form: Financial) {
  }
}
