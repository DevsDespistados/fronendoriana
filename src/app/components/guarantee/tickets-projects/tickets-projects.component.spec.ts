import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TicketsProjectsComponent} from './tickets-projects.component';

describe('TicketsProjectsComponent', () => {
  let component: TicketsProjectsComponent;
  let fixture: ComponentFixture<TicketsProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TicketsProjectsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
