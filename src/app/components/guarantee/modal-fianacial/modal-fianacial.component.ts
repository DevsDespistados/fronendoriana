import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Financial} from '../../../model/ticket/financial';

@Component({
  selector: 'app-modal-fianacial',
  templateUrl: './modal-fianacial.component.html',
  styleUrls: ['./modal-fianacial.component.css']
})
export class ModalFianacialComponent {
  text = ConfText;
  form: Financial;
  states = [
    'vigente',
    'baja'
  ];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.data.form;

  }

  closeDialog() {
    this.form = null;
    this.dialogRef.close();
  }

  sendFormData() {
    if (!this.validData()) {
      this.form.amountUsedFinancial = 0;
      this.form.availableBalanceFinancial = 0;
      this.dialogRef.close(this.form);
    }
  }


  validData() {
    return this.form.nameFinancial == null || this.form.nameFinancial == ''
      || this.form.amountLimitFinancial == null || this.form.amountLimitFinancial == 0;
  }
}
