import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalFianacialComponent} from './modal-fianacial.component';

describe('ModalFianacialComponent', () => {
  let component: ModalFianacialComponent;
  let fixture: ComponentFixture<ModalFianacialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalFianacialComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFianacialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
