import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {User} from '../../../model/user/user';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-modal-user',
  templateUrl: './modal-user.component.html',
  styleUrls: ['./modal-user.component.css']
})
export class ModalUserComponent {
  text = ConfText;
  user: User;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService) {
    this.user = this.data.user;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  accept() {
    if (!this.valid()) {
      this.dialogRef.close({
        name_user: this.user.name,
        lastName: this.user.lastName,
        userName_user: this.user.username,
        active_user: 1,
        password_user: 1234
      });
    }
  }

  setLastName() {
    if (this.user.name && this.user.lastName) {
      if (this.user.name.length > 0 && this.user.lastName.length > 0) {
        const ini = this.user.name.split(' ');
        this.user.username = ini[0].charAt(0) + this.user.lastName.split(' ')[0];
        if (this.serach(this.user.username)) {
          if (ini.length > 1) {
            this.user.username = ini[0].charAt(0) + ini[1].charAt(1) + this.user.lastName.split(' ')[0];
          } else {
            this.user.username = ini[0].charAt(0) + ini[0].charAt(1) + this.user.lastName.split(' ')[0];
          }
        }
      }
    }
  }

  searchUser() {
    return this.data.listUsers.find(u => {
      return u.name.toLowerCase() == this.user.name.toLowerCase()
        && u.lastName.toLowerCase() == this.user.lastName.toLowerCase();
    });
  }

  serach(text) {
    return this.data.listUsers.find(u => {
        return u.username.toUpperCase() == text.toUpperCase();
      }
    );
  }

  valid() {
    return this.user.name == null
      || this.user.name == ''
      || this.user.lastName == null
      || this.user.lastName === ''
      || this.searchUser();
  }
}
