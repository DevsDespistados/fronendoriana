import {Component, OnInit} from '@angular/core';
import {User} from '../../../model/user/user';

@Component({
  selector: 'app-home-user-permision',
  templateUrl: './home-user-permision.component.html',
  styleUrls: ['./home-user-permision.component.css']
})
export class HomeUserPermisionComponent implements OnInit {

  titles = {
    module: 'Permisos Usuario'
  };
  userSelect: User;

  constructor() {
  }

  ngOnInit() {
  }

  setUser(user: User) {
    this.userSelect = user;
  }

  getUser() {
    if (this.userSelect) {
      return 'Permisos: ' + this.userSelect.name + ' ' + this.userSelect.lastName;
    } else {
      return 'Permisos: ';
    }
  }
}
