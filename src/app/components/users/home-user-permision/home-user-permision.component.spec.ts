import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeUserPermisionComponent} from './home-user-permision.component';

describe('HomeUserPermisionComponent', () => {
  let component: HomeUserPermisionComponent;
  let fixture: ComponentFixture<HomeUserPermisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeUserPermisionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUserPermisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
