import {Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {User} from '../../../model/user/user';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-user-perfil',
  templateUrl: './user-perfil.component.html',
  styleUrls: ['./user-perfil.component.css']
})
export class UserPerfilComponent {

  text = ConfText;
  user: User;
  password: string;
  repeatPassword: string;
  editable = true;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService) {
    this.user = this.data.user;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  accept() {
    if (!this.valid()) {
      if (this.password == this.repeatPassword) {
        const d = {
          name_user: this.user.name,
          lastName: this.user.lastName,
          userName_user: this.user.username,
          active_user: true
        };
        if (this.password != null) {
          d['password'] = this.password;
        }
        this.userService.editUser(this.user.id, d).subscribe(r => {
          this.dialogRef.close({res: true});
        });
      }
    }
  }

  edit() {
    this.editable = !this.editable;
    this.user = this.data.user;
  }

  valid() {
    return this.user.name == null
      || this.user.name == ''
      || this.user.lastName == null
      || this.user.lastName === '';
  }
}
