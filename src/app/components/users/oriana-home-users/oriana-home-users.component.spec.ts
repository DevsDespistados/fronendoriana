import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeUsersComponent} from './oriana-home-users.component';

describe('OrianaHomeUsersComponent', () => {
  let component: OrianaHomeUsersComponent;
  let fixture: ComponentFixture<OrianaHomeUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeUsersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
