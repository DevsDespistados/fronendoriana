import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-home-users',
  templateUrl: './oriana-home-users.component.html',
  styleUrls: ['./oriana-home-users.component.css']
})
export class OrianaHomeUsersComponent implements OnInit {
  titles = {
    module: 'Usuarios'
  };

  constructor() {
  }

  ngOnInit() {
  }
}
