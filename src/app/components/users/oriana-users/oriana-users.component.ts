import {Component, OnInit} from '@angular/core';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PrintServiceService} from '../../../services/print-service.service';
import {SnackSuccessComponent} from '../../snack-msg/snack-success/snack-success.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ModalDeleteComponent} from '../../navigation/modal-delete/modal-delete.component';
import {SnackDeleteComponent} from '../../snack-msg/snack-delete/snack-delete.component';
import {SnackAlertComponent} from '../../snack-msg/snack-alert/snack-alert.component';
import {_permission} from '../../../services/permissions';
import {compareString} from '../../../services/GlobalFunctions';
import {HttpErrorResponse} from '@angular/common/http';
import {User} from '../../../model/user/user';
import {UserService} from '../../../services/user/user.service';
import {ModalUserComponent} from '../modal-user/modal-user.component';

@Component({
  selector: 'app-oriana-users',
  templateUrl: './oriana-users.component.html',
  styleUrls: ['./oriana-users.component.css']
})
export class OrianaUsersComponent implements OnInit {
  titulo = 'Disponibles';
  userSelect: User;
  users: User[];
  displayedColumns = [
    'position',
    'name',
    'username',
    'op'];
  sortedData: User[];
  options = [
    new OptionOrianaTableToolbar('DISPONIBLES', 1),
    new OptionOrianaTableToolbar('NO DISPONIBLES', 0),
    // new OptionOrianaTableToolbar('BAJAS', 0),
    new OptionOrianaTableToolbar('TODOS', 2)
  ];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private userService: UserService,
              private printService: PrintServiceService) {
  }

  ngOnInit(): void {
    this.load();
  }

  create() {
    const dialogRef = this.dialog.open(ModalUserComponent, {
      width: '300px',
      data: new DataUser(this.userSelect, this.users)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.userSelect.id) {
          this.userService.addUser(result)
            .subscribe(
              formData => {
                this.load();
                this.snack.openFromComponent(SnackSuccessComponent, {duration: ConfText.timer});
              },
              error => {
                this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
              }
            );
        } else {
          this.userService.editUser(this.userSelect.id, result).subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
            }
          );
        }
      } else {
        this.resetSelect();
      }
    });
  }

  deleted(pos: number) {
    this.userSelect = this.users[pos];
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      width: '400px',
      data: {deleted: this.userSelect.name}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userService.deleteUser(this.userSelect.id)
          .subscribe(
            formData => {
              this.load();
              this.snack.openFromComponent(SnackDeleteComponent, {duration: ConfText.timer});
            },
            error => {
              this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
            }
          );
      } else {
        this.resetSelect();
      }
    });
  }

  edit(pos: number) {
    const user = this.users[pos];
    this.userSelect = new User(user.id,
      user.name,
      user.lastName,
      user.username,
      user.password,
      user.active,
      user.iconImg);
    this.create();
  }

  getFilter(type: any) {
    this.progress = true;
    switch (type) {
      case 1: {
        this.getUserActive();
        break;
      }
      case 0: {
        this.getUserDisable();
        break;
      }
      case 2: {
        this.getAllUsers();
        break;
      }
    }
  }

  getUserActive() {
    this.userService.getUserActives().subscribe(
      response => {
        this.users = <User[]>response;
        this.progress = false;
      }
    );
  }

  getUserDisable() {
    this.userService.getUserDisables().subscribe(
      response => {
        this.users = <User[]>response;
        this.progress = false;
      }
    );
  }

  getAllUsers() {
    this.userService.getUsers().subscribe(
      response => {
        this.users = <User[]>response;
        this.progress = false;
      }
    );
  }

  isEmpty(): any {
    if (this.users) {
      if (this.users.length > 0) {
        return false;
      } else {
        return true;
      }
    }
  }

  isDisabled() {
    return _permission().u_users != 1;
  }

  isRoot(id) {
    return id == 1;
  }

  load() {
    this.resetSelect();
    this.getFilter(this.currentOption);
    this.options.forEach(op => {
      if (op.url == this.currentOption) {
        this.titulo = op.option;
      }
    });
  }

  printReport() {
    this.progress = true;
    const head = ['NRO', 'NOMBRE', 'USUARIO', 'ESTADO'];
    const body = this.getData();
    const report = this.printService.createSingleTable('REPORTE USUARIOS', head, body, 'wrap',
      {
        0: {
          halign: 'right',
        },
      });
    this.printService.setDataPrint(report);
  }

  getData() {
    const data = [];
    this.users.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push((c.name + ' ' + c.lastName).toUpperCase());
      d.push(c.username.toUpperCase());
      d.push(c.active == 1 ? 'DISPONIBLE' : c.active == 2 ? 'NO DISPONIBLE' : 'BAJA');
      data.push(d);
    });
    return data;
  }

  searchPersonal(text: string) {
    if (text !== '') {
      this.progress = true;
      this.userService.searchUsers(text).subscribe(
        response => {
          this.users = <User[]>response;
          this.titulo = ConfText.resultSearch;
          this.progress = false;
        }
      );
    } else {
      this.load();
    }
  }

  sortData(sort: Sort): any {
    const data = this.users.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.users = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return compareString(a.name, b.name, isAsc);
        case 'username':
          return compareString(a.username, b.username, isAsc);
        default:
          return 0;
      }
    });
  }

  switchPersonal(type: any) {
    this.currentOption = type;
    this.load();
  }

  updateState(state: any, pos: number) {
    const user = this.users[pos];
    this.userService.changeState(user.id, state)
      .subscribe(
        res => {
          this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
          if (this.currentOption !== 3) {
            this.load();
          }
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 406) {
            this.snack.openFromComponent(SnackAlertComponent, {duration: ConfText.timer});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
          }
        }
      );
  }

  passwordReset(id) {
    this.userSelect.password = '1234';
    this.userService.resetPassword(id, this.userSelect.password).subscribe(
      response => {
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      }
    );
  }

  resetSelect() {
    this.userSelect = new User(
      null,
      null,
      null,
      null,
      null,
      true,
      null);
  }
}

export class DataUser {
  constructor(public user: User, public listUsers: User[]) {
  }
}
