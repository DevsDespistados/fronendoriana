import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaUsersComponent} from './oriana-users.component';

describe('OrianaUsersComponent', () => {
  let component: OrianaUsersComponent;
  let fixture: ComponentFixture<OrianaUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaUsersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
