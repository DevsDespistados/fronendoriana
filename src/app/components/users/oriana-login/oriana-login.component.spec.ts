import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaLoginComponent} from './oriana-login.component';

describe('OrianaLoginComponent', () => {
  let component: OrianaLoginComponent;
  let fixture: ComponentFixture<OrianaLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaLoginComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
