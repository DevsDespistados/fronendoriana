import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/user/authentication.service';
import {HttpErrorResponse} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackEmptyComponent} from '../../snack-msg/snack-empty/snack-empty.component';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ToggleService} from '../../../toggle.service';
import {UserRoutesService} from '../../navigation/user-routes.service';

@Component({
  selector: 'app-oriana-login',
  templateUrl: './oriana-login.component.html',
  styleUrls: ['./oriana-login.component.css'],
  providers: [UserRoutesService]
})
export class OrianaLoginComponent implements OnInit {
  hide = true;
  f: FormGroup;
  visible = false;
  title = 'Oriana Servicios de ingenieria SRL';

  constructor(private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private router: Router,
              private snack: MatSnackBar,
              private toggle: ToggleService) {
  }

  ngOnInit() {
    this.toggle.setOpen(false);
    this.f = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  onSubmit() {
    if (this.f.valid) {
      this.visible = true;
      this.authService.login(this.f.value).subscribe(
        (resp) => {
          this.visible = false;
          this.router.navigate(['/']);
          this.toggle.setOpen(true);
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse.status === 401) {
            this.snack.openFromComponent(SnackEmptyComponent, {duration: 2000, data: {msg: 'Usuario o Contraseña incorrectas'}});
          } else {
            this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
          }
          this.visible = false;
        }
      );
    }
  }

  isValid() {
    const user = this.f.controls['username'].value;
    const pass = this.f.controls['password'].value;
    return user == null || user == '' || pass == null || pass == '';
  }
}
