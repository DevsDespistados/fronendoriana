import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {UserPermissions} from '../../../model/user/user-permissions';
import {User} from '../../../model/user/user';
import {SnackUpdateComponent} from '../../snack-msg/snack-update/snack-update.component';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-permissions',
  templateUrl: './user-permissions.component.html',
  styleUrls: ['./user-permissions.component.css']
})
export class UserPermissionsComponent implements OnInit {
  @Output() getUser = new EventEmitter();
  saved = true;
  progress = false;
  idUser: number;
  userSelect: User;
  userPermissions: UserPermissions;
  tooltips = [
    'sin permisos',
    'escritura',
    'lectura'
  ];

  constructor(private routerActive: ActivatedRoute,
              private userService: UserService,
              private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.routerActive.params.subscribe(params => {
      this.idUser = params['id'];
      this.givePermissionUser(params['id']);
    });
  }

  savePermission() {
    this.userService.setPermissions(this.userPermissions, this.userPermissions.idUserPermission).subscribe(
      res => {
        this.load();
        this.snack.openFromComponent(SnackUpdateComponent, {duration: ConfText.timer});
      },
      error => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
      });
  }

  load() {
    this.givePermissionUser(this.idUser);
  }

  givePermissionUser(idUser) {
    this.progress = true;
    this.userService.getUserPermission(idUser).subscribe(
      res => {
        this.userPermissions = res['perm'];
        this.userSelect = res['user'];
        this.getUser.emit(res['user']);
        this.progress = false;
      }, error1 => {
        this.progress = false;
      }
    );
  }
}
