import {Injectable} from '@angular/core';
import {
  __permissionFacturation,
  __permissionPersonal,
  _permissionAccount,
  _permissionContract,
  _permissionEquipment,
  _permissionForms,
  _permissionLetters,
  _permissionOrders,
  _permissionProjects,
  _permissionTickets,
  _permissionUsers
} from '../../services/permissions';

@Injectable({
  providedIn: 'root'
})
export class UserRoutesService {
  routes = [];
  // routes = [
  //   new LinksHome('/dashboard', '/assets/icons/Control-Panel.png', 'DASHBOARD'),
  //   new LinksHome('/personal', '/assets/icon_modules/personal.svg', 'Personal'),
  //   new LinksHome('/ownerEquipment', '/assets/icon_modules/owner_equipment.svg', 'Equipo Propio'),
  //   new LinksHome('/projects', '/assets/icon_modules/projects.svg', 'Proyectos'),
  //   new LinksHome('/contracts', '/assets/icon_modules/contracts.svg', 'Contratos'),
  //   new LinksHome('/users', '/assets/icon_modules/users.svg', 'Usuarios'),
  //   new LinksHome('/forms', '/assets/icon_modules/forms.svg', 'Planillas'),
  //   new LinksHome('/tickets', '/assets/icon_modules/tickets.svg', 'Garantias'),
  //   new LinksHome('/letters', '/assets/icon_modules/letters.svg', 'Cartas'),
  //   new LinksHome('/order', '/assets/icon_modules/order.svg', 'Pedidos'),
  //   new LinksHome('/invoice', '/assets/icon_modules/invoices.svg', 'Facturas'),
  //   new LinksHome('/accounts', '/assets/icon_modules/accounts.svg', 'Cuentas'),
  //   new LinksHome('/reports', '/assets/icon_modules/reports.svg', 'Reportes'),
  // ];

  constructor() {
  }
  loadRoutes() {
    setTimeout(r => {
      this.loadRout();
    }, 1000);
  }

  loadRout() {
    this.routes = [];
    this.routes.push(new LinksHome('/dashboard', '/assets/icons/Control-Panel.png', 'DASHBOARD'));
    // while (_user()) {
    if (__permissionPersonal()) {
      this.routes.push(new LinksHome('/personal', '/assets/icon_modules/personal.svg', 'Personal'));
    }
    if (_permissionEquipment()) {
      this.routes.push(new LinksHome('/ownerEquipment', '/assets/icon_modules/owner_equipment.svg', 'Equipo Propio'));
    }
    if (_permissionProjects()) {
      this.routes.push(new LinksHome('/projects', '/assets/icon_modules/projects.svg', 'Proyectos'));
    }
    if (_permissionContract()) {
      this.routes.push(new LinksHome('/contracts', '/assets/icon_modules/contracts.svg', 'Contratos'));
    }
    if (_permissionUsers()) {
      this.routes.push(new LinksHome('/users', '/assets/icon_modules/users.svg', 'Usuarios'));
    }
    if (_permissionForms()) {
      this.routes.push(new LinksHome('/forms', '/assets/icon_modules/forms.svg', 'Planillas'));
    }
    if (_permissionTickets()) {
      this.routes.push(new LinksHome('/tickets', '/assets/icon_modules/tickets.svg', 'Garantias'));
    }
    if (_permissionLetters()) {
      this.routes.push(new LinksHome('/letters', '/assets/icon_modules/letters.svg', 'Cartas'));
    }
    if (_permissionOrders()) {
      this.routes.push(new LinksHome('/order', '/assets/icon_modules/order.svg', 'Pedidos'));
    }
    if (__permissionFacturation()) {
      this.routes.push(new LinksHome('/invoice', '/assets/icon_modules/invoices.svg', 'Facturas'));
    }
    if (_permissionAccount()) {
      this.routes.push(new LinksHome('/accounts', '/assets/icon_modules/accounts.svg', 'Cuentas'));
    }
    // this.routes.push(new LinksHome('/reports', '/assets/icon_modules/reports.svg', 'Reportes'));
    //  }
    return this.routes;
  }
}

export class LinksHome {
  constructor(public link: string, public img: string, public name: string) {
  }
}
