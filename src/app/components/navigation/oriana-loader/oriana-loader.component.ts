import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-loader',
  templateUrl: './oriana-loader.component.html',
  styleUrls: ['./oriana-loader.component.css']
})
export class OrianaLoaderComponent implements OnInit {
  isRateLimitReached = true;
  isLoadingResults = true;

  constructor() {
  }

  ngOnInit() {
  }

}
