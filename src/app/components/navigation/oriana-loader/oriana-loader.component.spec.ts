import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaLoaderComponent} from './oriana-loader.component';

describe('OrianaLoaderComponent', () => {
  let component: OrianaLoaderComponent;
  let fixture: ComponentFixture<OrianaLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaLoaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
