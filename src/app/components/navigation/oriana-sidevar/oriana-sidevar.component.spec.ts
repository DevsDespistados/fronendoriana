import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaSidevarComponent} from './oriana-sidevar.component';

describe('OrianaSidevarComponent', () => {
  let component: OrianaSidevarComponent;
  let fixture: ComponentFixture<OrianaSidevarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaSidevarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaSidevarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
