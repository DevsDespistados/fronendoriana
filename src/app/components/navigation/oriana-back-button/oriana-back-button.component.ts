import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-oriana-back-button',
  templateUrl: './oriana-back-button.component.html',
  styleUrls: ['./oriana-back-button.component.css']
})
export class OrianaBackButtonComponent implements OnInit {
  @Input() routeBack: string;

  constructor(private router: Router, private location: Location) {
  }

  ngOnInit() {
  }

  back() {
    if (this.routeBack) {
      this.router.navigate([this.routeBack]);
    } else {
      this.location.back();
    }
  }

}
