import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaBackButtonComponent} from './oriana-back-button.component';

describe('OrianaBackButtonComponent', () => {
  let component: OrianaBackButtonComponent;
  let fixture: ComponentFixture<OrianaBackButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaBackButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaBackButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
