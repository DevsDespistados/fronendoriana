import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaContentComponent} from './oriana-content.component';

describe('OrianaContentComponent', () => {
  let component: OrianaContentComponent;
  let fixture: ComponentFixture<OrianaContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaContentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
