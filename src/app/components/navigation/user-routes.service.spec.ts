import {TestBed} from '@angular/core/testing';

import {UserRoutesService} from './user-routes.service';

describe('UserRoutesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserRoutesService = TestBed.get(UserRoutesService);
    expect(service).toBeTruthy();
  });
});
