import {AfterViewInit, Component} from '@angular/core';
import {Location} from '@angular/common';
import {PrintServiceService} from '../../../services/print-service.service';

@Component({
  selector: 'app-view-print',
  templateUrl: './view-print.component.html',
  styleUrls: ['./view-print.component.css']
})
export class ViewPrintComponent implements AfterViewInit {
  data;
  title = 'RESULTADO REPORTE';

  constructor(private location: Location, private printService: PrintServiceService) {
    this.data = this.printService.getDataPrint();
  }

  ngAfterViewInit() {
    if (this.data) {
      const ss = document.getElementById('contentPDF');
      ss.setAttribute('data', this.data);
    } else {
      this.location.back();
    }
  }
}

