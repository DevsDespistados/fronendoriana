import {Component, Input, OnInit} from '@angular/core';
import {ToggleService} from '../../../toggle.service';
import {AuthenticationService} from '../../../services/user/authentication.service';
import {UserRoutesService} from '../user-routes.service';
import {UserPerfilComponent} from '../../users/user-perfil/user-perfil.component';
import {MatDialog} from '@angular/material/dialog';
import {_user} from '../../../services/permissions';
import {User} from '../../../model/user/user';

@Component({
  selector: 'app-oriana-toolbar',
  templateUrl: './oriana-toolbar.component.html',
  styleUrls: ['./oriana-toolbar.component.css'],
})
export class OrianaToolbarComponent implements OnInit {
  @Input() title = '';
  @Input() subTitle: string;
  user: User;

  constructor(private toggle: ToggleService,
              private authService: AuthenticationService,
              private userRoutesService: UserRoutesService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.user = _user();
  }

  buttonDraw() {
    if (this.toggle.isOpen()) {
      this.toggle.setOpen(false);
    } else {
      this.toggle.setOpen(true);
    }
  }

  logout() {
    this.authService.logout();
  }

  refreshPermision() {
    this.authService.refreshPermission();
    // setTimeout(o => {
    // this.userRoutesService.loadRoutes();
    // }, 1000);
  }

  openPerfil() {
    const dialogRef = this.dialog.open(UserPerfilComponent, {
      width: '400px',
      data: {user: this.authService.getUser()},
    });
    dialogRef.afterClosed().subscribe(r => {
      if (r) {
        if (r.res == true) {
          this.authService.logout();
        }
      }
    });
  }

}
