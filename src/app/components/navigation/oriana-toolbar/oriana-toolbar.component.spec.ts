import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaToolbarComponent} from './oriana-toolbar.component';

describe('OrianaToolbarComponent', () => {
  let component: OrianaToolbarComponent;
  let fixture: ComponentFixture<OrianaToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaToolbarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
