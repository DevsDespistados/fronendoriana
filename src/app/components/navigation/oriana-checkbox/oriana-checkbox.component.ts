import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-oriana-checkbox',
  templateUrl: './oriana-checkbox.component.html',
  styleUrls: ['./oriana-checkbox.component.css']
})
export class OrianaCheckboxComponent implements OnInit {
  @Input() checked: any;
  @Input() disabled: boolean;
  @Input() textTooltip: string[] = ['Baja', 'Disponible', 'No Disponible'];
  @Output() changed = new EventEmitter();
  state;

  constructor() {
    this.state = this.checked;
  }

  ngOnInit() {
  }

  clickCheck() {
    if (this.checked == 1) {
      this.checked = 2;
    } else if (this.checked == 2) {
      this.checked = 0;
    } else if (this.checked == 0) {
      this.checked = 1;
    }
    this.changed.emit(this.checked);
  }

  isIndeterminate() {
    return this.checked == 2;
  }

  tooltip() {
    // if (this.checked == 1) {
    //   return 'Disponible';
    // } else if (this.checked == 2) {
    //   return 'No disponible';
    // } else if (this.checked == 0) {
    //   return 'Baja';
    // }
    this.state = <number>this.checked;
    return this.textTooltip[this.state];
  }
}
