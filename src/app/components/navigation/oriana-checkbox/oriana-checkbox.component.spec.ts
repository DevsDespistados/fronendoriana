import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaCheckboxComponent} from './oriana-checkbox.component';

describe('OrianaCheckboxComponent', () => {
  let component: OrianaCheckboxComponent;
  let fixture: ComponentFixture<OrianaCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaCheckboxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
