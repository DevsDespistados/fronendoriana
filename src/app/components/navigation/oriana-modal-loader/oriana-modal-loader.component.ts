import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-modal-loader',
  templateUrl: './oriana-modal-loader.component.html',
  styleUrls: ['./oriana-modal-loader.component.css']
})
export class OrianaModalLoaderComponent implements OnInit {
  @Input() visible = false;

  constructor() {
  }

  ngOnInit() {
  }

}
