import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaModalLoaderComponent} from './oriana-modal-loader.component';

describe('OrianaModalLoaderComponent', () => {
  let component: OrianaModalLoaderComponent;
  let fixture: ComponentFixture<OrianaModalLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaModalLoaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaModalLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
