import {AfterViewInit, Component, Inject} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.css']
})
export class ModalDeleteComponent implements AfterViewInit {
  deleted: string;
  text = ConfText;
  constructor(
    public dialogRef: MatDialogRef<ModalDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.deleted = this.data.deleted;
  }

  ngAfterViewInit(): void {
    // this.button.nativeElement.focus();
  }

  onNoClick(): void {
    this.deleted = null;
    this.dialogRef.close();
  }

  action() {
    this.dialogRef.close(1);
  }
}

export class DataDelete {
  constructor(public deleted: string) {
  }
}
