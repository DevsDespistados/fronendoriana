import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaButtonModulesComponent} from './oriana-button-modules.component';

describe('OrianaButtonModulesComponent', () => {
  let component: OrianaButtonModulesComponent;
  let fixture: ComponentFixture<OrianaButtonModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaButtonModulesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaButtonModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
