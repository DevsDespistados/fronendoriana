import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {_permission} from '../../../services/permissions';
import {UserRoutesService} from '../user-routes.service';

@Component({
  selector: 'app-oriana-button-modules',
  templateUrl: './oriana-button-modules.component.html',
  styleUrls: ['./oriana-button-modules.component.css'],
})
export class OrianaButtonModulesComponent implements OnInit {


  constructor(private router: Router,
              public userRoutesService: UserRoutesService) {
  }

  ngOnInit() {
    this.userRoutesService.loadRoutes();
  }

  isModule(name) {
    if (name == 'Personal') {
      this.getRoutePersonal();
    } else if (name == 'Planillas') {
      this.getRouterForms();
    } else if (name == 'Cuentas') {
      this.getRouterAccounts();
    // } else if (name == 'Reportes') {
    //   this.getRouteReportsAccount();
    } else if (name == 'Cartas') {
      this.getRouteLetters();
    } else if (name == 'Garantias') {
      this.getRouteGuarantes();
    } else if (name == 'Pedidos') {
      this.getRouteOrders();
    }
  }

  getRoutePersonal(): void {
    if (_permission().p_client > 0) {
      this.router.navigate(['/personal/clients/']);
    } else if (_permission().p_provider > 0) {
      this.router.navigate(['/personal/providers']);
    } else if (_permission().p_transit > 0) {
      this.router.navigate(['/personal/transits']);
    } else if (_permission().p_permanent > 0) {
      this.router.navigate(['/personal/permanents']);
    } else if (_permission().p_eventual > 0) {
      this.router.navigate(['/personal/eventual']);
    } else if (_permission().p_otherCompany > 0) {
      this.router.navigate(['/personal/companies']);
    }
  }

  getRouterForms(): void {
    if (_permission().f_provider > 0) {
      this.router.navigate(['/forms/provider']);
    } else if (_permission().f_permanent > 0) {
      this.router.navigate(['/forms/permanent']);
    } else if (_permission().f_eventual > 0) {
      this.router.navigate(['/forms/eventual']);
    } else if (_permission().f_ownerEquipment > 0) {
      this.router.navigate(['/forms/equipment']);
    } else if (_permission().f_projects > 0) {
      this.router.navigate(['/forms/projects']);
    }
  }

  getRouterAccounts() {
    if (_permission().ac_b_banks > 0) {
      this.router.navigate(['/accounts/banks']);
    } else if (_permission().ac_pto_pettyCashOffice > 0) {
      this.router.navigate(['/accounts/pettyCashOffice']);
    } else if (_permission().ac_pt_pettyCash > 0) {
      this.router.navigate(['/accounts/pettyCash']);
    } else if (_permission().ac_pj_projects > 0) {
      this.router.navigate(['/accounts/projects']);
    }
  }

  // getRouteReportsAccount() {
  //   // if (_permission().f_provider > 0) {
  //   this.router.navigate(['/reports/accounts']);
  //   // }
  // }

  getRouteLetters() {
    if (_permission().l_s_Letters > 0) {
      this.router.navigate(['/letters/send']);
    } else if (_permission().l_r_Letters > 0) {
      this.router.navigate(['/letters/received']);
    } else if (_permission().l_p_Letters > 0) {
      this.router.navigate(['/letters/projects']);
    }
  }

  getRouteGuarantes() {
    if (_permission().ti_financial > 0) {
      this.router.navigate(['/tickets/creditLine']);
    } else if (_permission().ti_warranties > 0) {
      this.router.navigate(['/tickets/guarantees']);
    } else if (_permission().ti_projects > 0) {
      this.router.navigate(['/tickets/projects']);
    }
  }

  getRouteOrders() {
    if (_permission().o_order_p > 0) {
      this.router.navigate(['/order/petition']);
    } else if (_permission().o_order_q > 0) {
      this.router.navigate(['/order/quotation']);
    } else if (_permission().o_order_item > 0) {
      this.router.navigate(['/order/items']);
    }
  }


}

export class LinksHome {
  constructor(public link: string, public img: string, public name: string) {
  }
}
