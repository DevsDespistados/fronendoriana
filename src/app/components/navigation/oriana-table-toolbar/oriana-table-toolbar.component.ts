import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OptionOrianaTableToolbar} from './option-oriana-table-toolbar';
import {Tooltips} from '../../../model/configuration/Tooltips';

@Component({
  selector: 'app-oriana-table-toolbar',
  templateUrl: './oriana-table-toolbar.component.html',
  styleUrls: ['./oriana-table-toolbar.component.css']
})
export class OrianaTableToolbarComponent implements OnInit {
  @Input() title: string;
  @Input() options: OptionOrianaTableToolbar[];
  @Input() searchTittle: string;

  @Input() onSearch = true;
  @Input() onPrint = true;
  @Input() onNew = true;
  @Input() onRefresh = true;
  @Input() onMore = true;
  @Input() onSave = false;
  @Input() onFilter = false;
  @Input() onProgress = false;
  @Input() onBack = true;
  @Input() routeBack: string;
  @Input() optionFilters: OptionOrianaTableToolbar[];
  value;
  searchVisible = false;
  tooltip = Tooltips;
  @Output() new = new EventEmitter();
  @Output() refresh = new EventEmitter();
  @Output() print = new EventEmitter();
  @Output() searching = new EventEmitter();
  @Output() filterOption = new EventEmitter();
  @Output() save = new EventEmitter();
  @Output() _filter = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  setSearchVisible() {
    if (this.searchVisible) {
      this.searchVisible = false;
    } else {
      this.searchVisible = true;
    }
  }

  resetSearch() {
    this.value = '';
    this.searchVisible = false;
    this.searching.emit(this.value);
  }

  clickNew() {
    this.new.emit(true);
  }

  clickRefresh() {
    this.refresh.emit(true);
  }

  clickPrint() {
    this.print.emit(true);
  }

  clickSave() {
    this.save.emit(true);
  }

  search() {
    this.searching.emit(this.value);
  }

  isVisible() {
    if (this.value) {
      return this.value.length < 1 ? 'hidden' : 'visible';
    } else {
      return 'hidden';
    }
  }

  filter(option) {
    this.filterOption.emit(option);
  }

  clickFilter(e) {
    this._filter.emit(e);
  }
}
