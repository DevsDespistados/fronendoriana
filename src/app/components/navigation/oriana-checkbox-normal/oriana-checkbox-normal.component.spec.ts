import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaCheckboxNormalComponent} from './oriana-checkbox-normal.component';

describe('OrianaCheckboxNormalComponent', () => {
  let component: OrianaCheckboxNormalComponent;
  let fixture: ComponentFixture<OrianaCheckboxNormalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaCheckboxNormalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaCheckboxNormalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
