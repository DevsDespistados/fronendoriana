import {Component, Input, OnInit} from '@angular/core';
import {OrianaCheckboxComponent} from '../oriana-checkbox/oriana-checkbox.component';

@Component({
  selector: 'app-oriana-checkbox-normal',
  templateUrl: './oriana-checkbox-normal.component.html',
  styleUrls: ['./oriana-checkbox-normal.component.css']
})
export class OrianaCheckboxNormalComponent extends OrianaCheckboxComponent implements OnInit {
  @Input() textTooltip = ['No disponible', 'Disponible'];
  constructor() {
    super();
  }

  ngOnInit() {
    this.state = this.checked;
  }
  clickCheck() {
    this.changed.emit(this.checked ? 1 : 0);
  }

  tooltip(): string {
    return this.textTooltip[this.checked ? 1 : 0];
  }
}
