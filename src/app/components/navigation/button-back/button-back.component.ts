import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-button-back',
  templateUrl: './button-back.component.html',
  styleUrls: ['./button-back.component.css']
})
export class ButtonBackComponent implements OnInit {
  @Input() route;
  @Input() isBack: boolean;

  constructor(private router: Router, private routeActive: ActivatedRoute, private location: Location) {
  }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }
}
