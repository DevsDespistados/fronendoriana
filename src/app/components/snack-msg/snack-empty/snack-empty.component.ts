import {Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';

@Component({
  selector: 'app-snack-empty',
  templateUrl: './snack-empty.component.html',
  styleUrls: ['./snack-empty.component.css']
})
export class SnackEmptyComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: DataMsg) {
  }
}

export interface DataMsg {
  // constructor(msg: string) {
  // }
  msg: string;
}
