import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalReportdiagComponent} from './modal-reportdiag.component';

describe('ModalReportdiagComponent', () => {
  let component: ModalReportdiagComponent;
  let fixture: ComponentFixture<ModalReportdiagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalReportdiagComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReportdiagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
