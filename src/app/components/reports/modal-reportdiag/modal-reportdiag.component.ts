import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-modal-reportdiag',
  templateUrl: './modal-reportdiag.component.html',
  styleUrls: ['./modal-reportdiag.component.css']
})
export class ModalReportdiagComponent implements OnInit {
  text = ConfText;
  unity = [
    'Hora',
    'Dia',
    'Mes'
  ];
  private years = [];
  private months = [];
  private personal = [];
  private projects = [];
  private accounts = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {

  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close({
      year: this.years,
      month: this.months,
      project: this.projects,
      account: this.accounts,
      personal: this.personal
    });
  }

  setProjects(arg: SelectionModel<any>) {
    this.projects = arg.selected;
  }

  setPersonal(arg: SelectionModel<any>) {
    this.personal = arg.selected;
  }

  setAccount(arg: SelectionModel<any>) {
    this.accounts = arg.selected;
  }

  setYears(arg: SelectionModel<any>) {
    this.years = arg.selected;
  }

  setMonths(arg: SelectionModel<any>) {
    this.months = arg.selected;
  }

  valid(): boolean {
    return this.years.length == 0
      || this.months.length == 0;
  }
}

