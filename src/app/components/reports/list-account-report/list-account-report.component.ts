import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Bank} from '../../../model/accounts/bank';
import {AccountService} from '../../../services/accounts/account.service';

@Component({
  selector: 'app-list-account-report',
  templateUrl: './list-account-report.component.html',
  styleUrls: ['./list-account-report.component.css']
})
export class ListAccountReportComponent implements OnInit {
  @Output() getSelectAccount = new EventEmitter();
  displayedColumns: string[] = ['select', 'position', 'name'];
  accounts = new MatTableDataSource<Bank>([]);
  selection = new SelectionModel<Bank>(true, []);

  constructor(private  accountService: AccountService) {
  }

  ngOnInit() {
    this.accountService.getBankActives(1).subscribe(res => {
      this.accounts = new MatTableDataSource<Bank>(<Bank[]>res['banks']);
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.accounts.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.accounts.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: Bank): string {
    if (!row) {
      return this.isAllSelected() ? 'select' : 'deselect';
    }
    this.getSelect();
    return this.selection.isSelected(row) ? 'deselect' : 'select';
  }

  getSelect() {
    this.getSelectAccount.emit(this.selection);
  }
}
