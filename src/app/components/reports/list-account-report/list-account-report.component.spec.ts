import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListAccountReportComponent} from './list-account-report.component';

describe('ListAccountReportComponent', () => {
  let component: ListAccountReportComponent;
  let fixture: ComponentFixture<ListAccountReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListAccountReportComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAccountReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
