import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalPrintGroupComponent} from './modal-print-group.component';

describe('ModalPrintGroupComponent', () => {
  let component: ModalPrintGroupComponent;
  let fixture: ComponentFixture<ModalPrintGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPrintGroupComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPrintGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
