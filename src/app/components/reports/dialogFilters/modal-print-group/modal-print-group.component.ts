import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-print-group',
  templateUrl: './modal-print-group.component.html',
  styleUrls: ['./modal-print-group.component.css']
})
export class ModalPrintGroupComponent implements OnInit {
  text = ConfText;
  labelPosition = 'any';
  options = [
    new OptionGroup('any', 'Sin Agrupar'),
    new OptionGroup('account', 'Cuenta'),
    new OptionGroup('operation', 'Operacion'),
    new OptionGroup('project', 'Proyecto'),
    new OptionGroup('category', 'Categoria'),
    new OptionGroup('period', 'Periodo')
  ];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {

  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close(this.labelPosition);
  }

  valid(): boolean {
    return !this.labelPosition;
  }
}

export class OptionGroup {
  constructor(public value: string, public option: string) {
  }
}
