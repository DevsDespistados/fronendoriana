import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {Category} from '../../../../model/accounts/category';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-modal-filter-category',
  templateUrl: './modal-filter-category.component.html',
  styleUrls: ['./modal-filter-category.component.css']
})
export class ModalFilterCategoryComponent implements OnInit {
  text = ConfText;
  displayedColumns: string[] = ['select', 'position', 'name'];
  categories = new MatTableDataSource<Category>([]);
  selection = new SelectionModel<Category>(true, []);

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }


  ngOnInit() {
    this.categories = new MatTableDataSource<Category>(<Category[]>this.data.categories);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.categories.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.categories.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: Category): string {
    if (!row) {
      return this.isAllSelected() ? 'select' : 'deselect';
    }
    return this.selection.isSelected(row) ? 'deselect' : 'select';
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close(this.selection);
  }

  valid(): boolean {
    return false;
  }
}
