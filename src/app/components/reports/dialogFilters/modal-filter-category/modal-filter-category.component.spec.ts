import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalFilterCategoryComponent} from './modal-filter-category.component';

describe('ModalFilterCategoryComponent', () => {
  let component: ModalFilterCategoryComponent;
  let fixture: ComponentFixture<ModalFilterCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalFilterCategoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFilterCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
