import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-filter-date',
  templateUrl: './modal-filter-date.component.html',
  styleUrls: ['./modal-filter-date.component.css']
})
export class ModalFilterDateComponent implements OnInit {
  text = ConfText;
  now = new Date();
  date1;
  date2;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {

  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    this.dialogRef.close({date1: this.date1, date2: this.date2});
  }

  valid(): boolean {
    return !this.date1 || !this.date2;
  }
}
