import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalFilterDateComponent} from './modal-filter-date.component';

describe('ModalFilterDateComponent', () => {
  let component: ModalFilterDateComponent;
  let fixture: ComponentFixture<ModalFilterDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalFilterDateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFilterDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
