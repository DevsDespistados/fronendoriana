import {Component, Inject, OnInit} from '@angular/core';
import {ConfText} from '../../../../model/configuration/conf-text';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-filter-amount',
  templateUrl: './modal-filter-amount.component.html',
  styleUrls: ['./modal-filter-amount.component.css']
})
export class ModalFilterAmountComponent implements OnInit {
  text = ConfText;
  now = new Date();
  option1 = 1;
  option2 = 3;
  value1;
  value2;
  active = false;
  types = [
    {value: 1, text: 'Igual'},
    {value: 2, text: 'Mayor o Igual'},
    {value: 3, text: 'Menor o Igual'}
  ];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {

  }

  setValues() {
    if (this.active == true) {
      this.option1 = 2;
      this.option2 = 3;
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

  accept() {
    if (this.active) {
      this.dialogRef.close({
        operator1: this.option1,
        operator2: this.option2,
        value1: this.value1,
        value2: this.value2
      });
    } else {
      this.dialogRef.close({
        operator1: this.option1,
        operator2: null,
        value1: this.value1,
        value2: null
      });
    }
  }

  valid(): boolean {
    if (!this.active) {
      return this.value1 == null;
    } else if (this.active) {
      return this.value1 == null || this.value2 == null;
    }
  }
}
