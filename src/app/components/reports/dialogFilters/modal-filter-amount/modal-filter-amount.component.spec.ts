import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalFilterAmountComponent} from './modal-filter-amount.component';

describe('ModalFilterAmountComponent', () => {
  let component: ModalFilterAmountComponent;
  let fixture: ComponentFixture<ModalFilterAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalFilterAmountComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFilterAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
