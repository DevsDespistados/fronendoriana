import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {allYears, getMonths} from '../../../services/GlobalFunctions';

@Component({
  selector: 'app-list-periods',
  templateUrl: './list-periods.component.html',
  styleUrls: ['./list-periods.component.css']
})
export class ListPeriodsComponent implements OnInit {
  @Output() getSelectYears = new EventEmitter();
  @Output() getSelectMonths = new EventEmitter();
  displayedColumnsYears: string[] = ['select', 'nameYear'];
  displayedColumnsMonth: string[] = ['select', 'month'];
  months = new MatTableDataSource<any>(getMonths());
  years = new MatTableDataSource<any>(allYears());
  selectionMonths = new SelectionModel<any>(true, []);
  selectionYears = new SelectionModel<any>(true, []);

  constructor() {
  }

  ngOnInit() {
  }

  isAllSelectedMonth() {
    const numSelected = this.selectionMonths.selected.length;
    const numRows = this.months.data.length;
    return numSelected === numRows;
  }

  masterToggleMonth() {
    this.isAllSelectedMonth() ?
      this.selectionMonths.clear() :
      this.months.data.forEach(row => this.selectionMonths.select(row));
  }

  checkboxLabelMonth(row?: any): string {
    if (!row) {
      // return `${this.isAllSelectedMonth() ? 'select' : 'deselect'} all`;
      return this.isAllSelectedMonth() ? 'select' : 'deselect';
    }
    this.getSelectM();
    // return `${this.selectionMonths.isSelected(row) ? 'deselect' : 'select'} row ${row.index + 1}`;
    return this.selectionMonths.isSelected(row) ? 'deselect' : 'select';
  }

  isAllSelectedYear() {
    const numSelected = this.selectionYears.selected.length;
    const numRows = this.years.data.length;
    return numSelected === numRows;
  }

  masterToggleYear() {
    this.isAllSelectedYear() ?
      this.selectionYears.clear() :
      this.years.data.forEach(row => this.selectionYears.select(row));
  }

  checkboxLabelYear(row?: any): string {
    if (!row) {
      // return `${this.isAllSelectedYear() ? 'select' : 'deselect'} all`;
      return this.isAllSelectedYear() ? 'select' : 'deselect';
    }
    this.getSelectY();
    // return `${this.selectionYears.isSelected(row) ? 'deselect' : 'select'} row ${row.index + 1}`;
    return this.selectionYears.isSelected(row) ? 'deselect' : 'select';
  }

  getSelectY() {
    this.getSelectYears.emit(this.selectionYears);
  }

  getSelectM() {
    this.getSelectMonths.emit(this.selectionMonths);
  }
}
