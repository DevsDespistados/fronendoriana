import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListProyectsReportComponent} from './list-proyects-report.component';

describe('ListProyectsReportComponent', () => {
  let component: ListProyectsReportComponent;
  let fixture: ComponentFixture<ListProyectsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListProyectsReportComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProyectsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
