import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {ProjectService} from '../../../services/project/project.service';
import {ViewProjects} from '../../../model/project/view-projects';

@Component({
  selector: 'app-list-proyects-report',
  templateUrl: './list-proyects-report.component.html',
  styleUrls: ['./list-proyects-report.component.css']
})
export class ListProyectsReportComponent implements OnInit {
  @Output() getSelectProjects = new EventEmitter();
  displayedColumns: string[] = ['select', 'position', 'name'];
  projects = new MatTableDataSource<ViewProjects>([]);
  selection = new SelectionModel<ViewProjects>(true, []);

  constructor(private  projectService: ProjectService) {
  }

  ngOnInit() {
    this.projectService.getProjects().subscribe(res => {
      this.projects = new MatTableDataSource<ViewProjects>(<ViewProjects[]>res);
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.projects.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.projects.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: ViewProjects): string {
    if (!row) {
      // return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      return this.isAllSelected() ? 'select' : 'deselect';
    }
    this.getSelection();
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idProject + 1}`;
    return this.selection.isSelected(row) ? 'deselect' : 'select'; // row row.idProject + 1;
  }

  getSelection() {
    this.getSelectProjects.emit(this.selection);
  }

}
