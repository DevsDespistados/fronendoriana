import {Component, OnInit} from '@angular/core';
import {Permanent} from '../../../model/personal/permanent';
import {Tooltips} from '../../../model/configuration/Tooltips';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Sort} from '@angular/material/sort';
import {PermanentService} from '../../../services/personal/permanent.service';
import {PrintServiceService} from '../../../services/print-service.service';
import {ConfText} from '../../../model/configuration/conf-text';
import {SnackErrorComponent} from '../../snack-msg/snack-error/snack-error.component';
import {ModalReportdiagComponent} from '../modal-reportdiag/modal-reportdiag.component';
import {ReportServiceService} from '../../../services/accountsReports/report-service.service';
import {_user} from '../../../services/permissions';
import {TransactionService} from '../../../services/accounts/transaction.service';
import {compareDate, compareNumber, compareString} from '../../../services/GlobalFunctions';
import {ModalFilterDateComponent} from '../dialogFilters/modal-filter-date/modal-filter-date.component';
import {OptionOrianaTableToolbar} from '../../navigation/oriana-table-toolbar/option-oriana-table-toolbar';
import {ModalFilterAmountComponent} from '../dialogFilters/modal-filter-amount/modal-filter-amount.component';
import * as moment from 'moment';
import {ModalPrintGroupComponent} from '../dialogFilters/modal-print-group/modal-print-group.component';
import {DateOrianaPipe} from '../../../pipes/date-oriana.pipe';
import * as jsonQuery from 'json-groupby';
import {MoneyPipe} from '../../../pipes/money.pipe';
import {StorageService} from '../../../services/user/storage.service';

@Component({
  selector: 'app-template-acconts-reports',
  templateUrl: './template-acconts-reports.component.html',
  styleUrls: ['./template-acconts-reports.component.css'],
  providers: [DateOrianaPipe, MoneyPipe]
})
export class TemplateAccontsReportsComponent implements OnInit {
  titulo = ' ';
  displayedColumns = [
    'position',
    'date',
    'account',
    // 'check',
    'project',
    'operation',
    'accountD',
    'category',
    'respaldo',
    'description',
    'import',
    'options'
  ];
  sortedData: ViewReport[];
  auxTransactions: ViewReport[] = [];
  tooltip = Tooltips;
  currentOption = 1;
  progress = false;
  transactions: ViewReport[] = [];
  total = 0;
  optionFilters = [
    new OptionOrianaTableToolbar('Filtrar Por Fecha', 1),
    new OptionOrianaTableToolbar('Filtrar Por Importe', 2),
  ];
  paramFilters;

  constructor(public dialog: MatDialog,
              private snack: MatSnackBar,
              private permanentService: PermanentService,
              private printService: PrintServiceService,
              private reportService: ReportServiceService,
              private transactionService: TransactionService,
              private datePipe: DateOrianaPipe,
              private moneyPipe: MoneyPipe,
              private storageService: StorageService) {
  }

  ngOnInit() {
    this.load();
    if (this.storageService.getStorage('report')) {
      this.transactions = <ViewReport[]>this.storageService.getStorage('report');
      this.auxTransactions = <ViewReport[]>this.storageService.getStorage('report');
      this.setTotal();
    }
  }

  create() {
    const dialogRef = this.dialog.open(ModalReportdiagComponent, {
      width: '600px',
      data: new DataReport(null)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.paramFilters = result;
        this.storageService.setStorage('params', this.paramFilters);
        this.load();
      }
    });
  }

  printFacturation(id) {
    this.progress = true;
    this.transactionService.printFact({idTransaction: id, user: _user()}).subscribe(
      res => {
        this.printService.setDataPrint(res['data']);
        this.progress = false;
      }, error1 => {
        this.snack.openFromComponent(SnackErrorComponent, {duration: ConfText.timer});
        this.progress = false;
      }
    );
  }

  filter(e) {
    switch (e) {
      case 1: {
        const dialogRef = this.dialog.open(ModalFilterDateComponent, {
          width: '254px',
          data: new DataReport(null)
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.filterADate(result.date1, result.date2);
            this.setTotal();
          }
        });
        break;
      }
      case 2: {
        const dialogRef = this.dialog.open(ModalFilterAmountComponent, {
          width: '350px',
          data: new DataReport(null)
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.filterMount(result);
          }
        });
        break;
      }
    }
  }

  load() {
    if (this.paramFilters || this.storageService.getStorage('params')) {
      this.paramFilters = this.storageService.getStorage('params');
      this.progress = true;
      this.reportService.reportFiltering(this.paramFilters).subscribe(res => {
        this.transactions = <ViewReport[]>res['transactions'];
        this.auxTransactions = <ViewReport[]>res['transactions'];
        this.storageService.setStorage('report', this.transactions);
        this.storageService.setStorage('params', this.paramFilters);
        this.setTotal();
        this.progress = false;
      });
    }
  }

  printReport() {
    const dialogRef = this.dialog.open(ModalPrintGroupComponent, {
      width: '250px',
      data: new DataReport(null)
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const d = this.groupBy(result);
        const groupsKey = this.transactionGroup(d);
        const dataTotal = this.listForPrint(groupsKey.groups, groupsKey.keys);
        const data = dataTotal.groups;
        const keys = groupsKey.keys;
        this.progress = true;
        const pdf = this.printService.createMultipleTablePdf(keys, this.getTittle(), data);
        this.printService.setDataPrint(pdf);
      }
    });
  }

  transactionGroup(listMap) {
    const keys = [];
    // tslint:disable-next-line
    for (const k  in listMap) {
      const ids: Array<any> = listMap[k].idTransaction;
      listMap[k] = this.transactions.filter(r => {
        return ids.includes(r.idTransaction);
      });
      keys.push(k);
    }
    return {
      keys: keys,
      groups: listMap
    };
  }

  groupBy(params: string): Array<any> {
    if (params == 'account') {
      return jsonQuery(this.transactions, ['nameBank'], ['idTransaction']);
    } else if (params == 'operation') {
      return jsonQuery(this.transactions, ['typeTransaction'], ['idTransaction']);
    } else if (params == 'project') {
      return jsonQuery(this.transactions, ['nameProject'], ['idTransaction']);
    } else if (params == 'category') {
      return jsonQuery(this.transactions, ['nameCategory'], ['idTransaction']);
    } else if (params == 'period') {
      return jsonQuery(this.transactions, ['idBankPeriod'], ['idTransaction']);
    } else if (params == 'any') {
      return jsonQuery(this.transactions, ['all'], ['idTransaction']);
    }
  }

  listForPrint(list: Array<any>, keys: Array<any>) {
    const totals = {};
    keys.forEach(k => {
      let t_t = 0;
      list[k].forEach((t, index) => {
        t_t += t.amountTransaction;
        const row = [];
        row.push(index + 1);
        row.push(this.datePipe.transform(t.dateTransaction).toUpperCase());
        // row.push( t.numberCheck,)
        row.push(t.nameBank.toUpperCase());
        row.push(t.nameProject.toUpperCase());
        row.push(t.typeTransaction.toUpperCase());
        row.push(t.nameAccountTransaction.toUpperCase());
        row.push(t.nameCategory.toUpperCase());
        row.push(t.nroReceiptTransaction);
        row.push(t.descriptionTransaction.toUpperCase());
        row.push(this.moneyPipe.transform(t.amountTransaction));
        list[k][index] = row;
      });
      list[k].push(
        [
          '',
          // '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          'Total',
          this.moneyPipe.transform(t_t)
        ]
      );
      totals[k] = this.moneyPipe.transform(t_t);
    });
    return {
      groups: list,
      totals: totals
    };
  }

  getTittle() {
    return [
      'NRO',
      'FECHA',
      // 'No CHEQUE',
      'CUENTA',
      'PROYECTO',
      'OPERACION',
      'CUENTA/DESC',
      'CATEGORIA',
      'No RESPALDO',
      'DESCRIPCION',
      'IMPORTE',
    ];
  }

  search(text: string) {
    if (text !== '') {
      text = text.toLowerCase();
      this.sortedData = [];
      this.progress = true;
      if (this.transactions) {
        this.transactions.forEach(t => {
          if (searchText(t.nameProject, text)
            || searchText(t.nameAccountTransaction, text)
            || searchText(t.typeTransaction, text)
            || searchText(t.nroReceiptTransaction, text)
            || searchText(t.descriptionTransaction, text)
            || searchText(t.nameCategory, text)) {
            this.sortedData.push(t);
          }
        });
      }
      this.transactions = this.sortedData;
      this.setTotal();
      this.progress = false;
    } else {
      this.transactions = this.auxTransactions;
      this.progress = false;
      this.setTotal();
    }
  }

  filterADate(date1, date2?) {
    this.sortedData = [];
    if (date1 && date2) {
      this.transactions.forEach(t => {
        const d = moment(t.dateTransaction);
        if (d >= moment(date1) && d <= moment(date2)) {
          this.sortedData.push(t);
        }
      });
      this.transactions = this.sortedData;
    }
  }

  filterMount(params) {
    this.sortedData = [];
    if (params.value1 && params.value2) {
      this.transactions.forEach(t => {
        if (this.numberInTwoRange(t.amountTransaction, params.value1, params.value2, params.operator1, params.operator2)) {
          this.sortedData.push(t);
        }
      });
    } else {
      this.transactions.forEach(t => {
        if (this.numberInRange(t.amountTransaction, params.value1, params.operator1)) {
          this.sortedData.push(t);
        }
      });
    }
    this.transactions = this.sortedData;
    this.setTotal();
  }

  private numberInTwoRange(n, v1, v2, op1, op2): boolean {
    if (op1 == 2 && op2 == 3) {
      return n >= v1 && n <= v2;
    } else if (op1 == 3 && op2 == 2) {
      return n <= v1 && n >= v2;
    }
  }

  private numberInRange(n, v1, op1): boolean {
    if (op1 == 1) {
      return n == v1;
    } else if (op1 == 2) {
      return n >= v1;
    } else if (op1 == 3) {
      return n <= v1;
    }
  }

  sortData(sort: Sort): any {
    const data = this.transactions.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.transactions = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'dateTransaction':
          return compareDate(a.dateTransaction, b.dateTransaction, isAsc);
        case 'numberCheck':
          return compareString(a.numberCheck, b.numberCheck, isAsc);
        case 'nameProject':
          return compareString(a.nameProject, b.nameProject, isAsc);
        case 'typeTransaction':
          return compareString(a.typeTransaction, b.typeTransaction, isAsc);
        case 'nameAccountTransaction':
          return compareString(a.nameAccountTransaction, b.nameAccountTransaction, isAsc);
        case 'nameBank':
          return compareString(a.nameBank, b.nameBank, isAsc);
        case 'nameCategory':
          return compareString(a.nameCategory, b.nameCategory, isAsc);
        case 'nroReceiptTransaction':
          return compareString(a.nroReceiptTransaction, b.nroReceiptTransaction, isAsc);
        case 'descriptionTransaction':
          return compareString(a.descriptionTransaction, b.descriptionTransaction, isAsc);
        case 'amountTransaction':
          return compareNumber(a.amountTransaction, b.amountTransaction, isAsc);
        default:
          return 0;
      }
    });
  }

  setTotal() {
    this.total = 0;
    this.transactions.forEach(t => {
      this.total = this.total += t.amountTransaction;
    });
  }
}

export function searchText(a: string, b: string) {
  return a.toLowerCase().includes(b);
}
export class DataReport {
  constructor(public permanent: Permanent) {
  }
}

export class ViewReport {
  all = 'Transacciones';

  constructor(
    public idTransaction: number,
    public typeTransaction: string,
    public dateTransaction: any,
    public nroReceiptTransaction: string,
    public descriptionTransaction: string,
    public amountTransaction: number,
    public idCategory_transaction: number,
    public nameCategory: string,
    public nameAccountTransaction: string,
    public idBankPeriod: number,
    public idBankExecutor: number,
    public nameBank: string,
    public idProject: number,
    public nameProject: string,
    public idCheck: number,
    public numberCheck: string,
    public idActor: number,
    public code_transaction: number,
  ) {
  }
}
