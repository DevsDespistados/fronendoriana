import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TemplateAccontsReportsComponent} from './template-acconts-reports.component';

describe('TemplateAccontsReportsComponent', () => {
  let component: TemplateAccontsReportsComponent;
  let fixture: ComponentFixture<TemplateAccontsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TemplateAccontsReportsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateAccontsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
