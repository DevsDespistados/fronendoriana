import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrianaHomeReportsComponent} from './oriana-home-reports.component';

describe('OrianaHomeReportsComponent', () => {
  let component: OrianaHomeReportsComponent;
  let fixture: ComponentFixture<OrianaHomeReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrianaHomeReportsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrianaHomeReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
