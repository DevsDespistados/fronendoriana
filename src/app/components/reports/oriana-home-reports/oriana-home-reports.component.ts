import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-oriana-home-reports',
  templateUrl: './oriana-home-reports.component.html',
  styleUrls: ['./oriana-home-reports.component.css']
})
export class OrianaHomeReportsComponent implements OnInit {
  titles = {
    module: 'REPORTES',
  };

  constructor() {
  }

  navLinks = [
    {label: 'CUENTAS', path: '/reports/accounts'},
  ];

  ngOnInit() {
  }

}
