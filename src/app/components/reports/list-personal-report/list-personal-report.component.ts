import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Provider} from '../../../model/personal/provider';
import {ProviderService} from '../../../services/personal/provider.service';
import {ClientService} from '../../../services/personal/client.service';
import {EventualService} from '../../../services/personal/eventual.service';
import {PermanentService} from '../../../services/personal/permanent.service';
import {TransitService} from '../../../services/personal/transit.service';
import {OtherCompanyService} from '../../../services/personal/other-company.service';
import {Client} from '../../../model/personal/client';
import {Eventual} from '../../../model/personal/eventual';
import {Permanent} from '../../../model/personal/permanent';
import {Transit} from '../../../model/personal/transit';
import {OtherCompany} from '../../../model/personal/other-company';

@Component({
  selector: 'app-list-personal-report',
  templateUrl: './list-personal-report.component.html',
  styleUrls: ['./list-personal-report.component.css']
})
export class ListPersonalReportComponent implements OnInit {
  @Output() getSelectPersonal = new EventEmitter();
  displayedColumns: string[] = ['select', 'position', 'name', 'state'];
  providers = new MatTableDataSource<Provider>([]);
  clients = new MatTableDataSource<Client>([]);
  eventuals = new MatTableDataSource<Eventual>([]);
  permanents = new MatTableDataSource<Permanent>([]);
  transit = new MatTableDataSource<Transit>([]);
  companies = new MatTableDataSource<OtherCompany>([]);
  selection = new SelectionModel<Provider>(true, []);
  disabled = true;

  providerAux;

  constructor(private clientService: ClientService,
              private providerService: ProviderService,
              private eventualService: EventualService,
              private transitService: TransitService,
              private companyService: OtherCompanyService,
              private permanentService: PermanentService) {
  }

  ngOnInit() {
    this.providerService.getProviders().subscribe(res => {
      this.providers = new MatTableDataSource<Provider>(<Provider[]>res);
    });
    // this.clientService.getClients().subscribe(res => {
    //   this.clients = new MatTableDataSource<Client>(<Client[]>res);
    // });
    // this.eventualService.getEventuals().subscribe(res => {
    //   this.eventuals = new MatTableDataSource<Eventual>(<Eventual[]>res);
    // });
    // this.permanentService.getPermanents().subscribe(res => {
    //   this.permanents = new MatTableDataSource<Permanent>(<Permanent[]> res);
    // });
    // this.transitService.getTransit().subscribe(res => {
    //   this.transit = new MatTableDataSource<Transit>(<Transit[]>res);
    // });
    // this.companyService.getOthersCompanies().subscribe( res => {
    //   this.companies = new MatTableDataSource<OtherCompany>(<OtherCompany[]> res);
    // });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.providers.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.providers.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: Provider): string {
    if (!row) {
      return this.isAllSelected() ? 'select' : 'deselect';
      // return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    this.getSelection();
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.idProvider + 1}`;
    return this.selection.isSelected(row) ? 'deselect' : 'select';
  }

  getSelection() {
    this.getSelectPersonal.emit(this.selection);
  }

  search(personal: string) {
    if (personal == '') {
      this.providers = this.providerAux;
    } else {
      this.providers = new MatTableDataSource<Provider>(this.providers.filteredData.filter(x =>
        x.nameProvider.includes(personal.toUpperCase()) || x.nameProvider.includes(personal.toLowerCase())));
    }
  }

  // backUp() {
  //   this.providerAux = this.providers;
  // }
}
