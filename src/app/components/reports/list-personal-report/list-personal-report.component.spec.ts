import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListPersonalReportComponent} from './list-personal-report.component';

describe('ListPersonalReportComponent', () => {
  let component: ListPersonalReportComponent;
  let fixture: ComponentFixture<ListPersonalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListPersonalReportComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPersonalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
