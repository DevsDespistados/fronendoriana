import {Injectable} from '@angular/core';
import {AppService} from './principalService/app.service';
import {env} from './Env';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends AppService {
  getPersonal() {
    return this.get(env.SERVERNAME + '/dashboard/personal');
  }

  getOwnerEquipment() {
    return this.get(env.SERVERNAME + '/dashboard/ownerEquipment');
  }

  getProjects() {
    return this.get(env.SERVERNAME + '/dashboard/projects');
  }
}
