import {UserPermissions} from '../model/user/user-permissions';
import {User} from '../model/user/user';

export function _permission(): UserPermissions {
  // let res = null;
  if (sessionStorage.getItem('permission')) {
    return <UserPermissions>JSON.parse(atob(sessionStorage.getItem('permission')));
  } else {
    return null;
  }
}

export function _user(): User {
  return sessionStorage.getItem('user') ? JSON.parse(atob(sessionStorage.getItem('user'))) : null;
}

export function __permissionPersonal(): boolean {
  return _permission().p_client > 0 ||
    _permission().p_provider > 0 ||
    _permission().p_transit > 0 ||
    _permission().p_eventual > 0 ||
    _permission().p_permanent > 0 ||
    _permission().p_otherCompany > 0;
}

export function _permissionEquipment(): boolean {
  return _permission().o_ownerEquipment > 0;
}

export function _permissionContract() {
  return _permission().contracts > 0 ||
    _permission().contracts_item > 0;
}

export function _permissionForms() {
  return _permission().f_eventual > 0 ||
    _permission().f_ownerEquipment > 0 ||
    _permission().f_permanent > 0 ||
    _permission().f_provider;
}

export function _permissionProjects() {
  return _permission().pj_project > 0;
}

export function _permissionTickets() {
  return _permission().ti_financial > 0
    || _permission().ti_projects > 0
    || _permission().ti_warranties > 0;
}

export function _permissionLetters() {
  return _permission().l_p_Letters > 0
    || _permission().l_r_Letters > 0
    || _permission().l_s_Letters > 0;
}

export function _permissionOrders() {
  return _permission().o_order_item > 0
    || _permission().o_order_p > 0
    || _permission().o_order_q > 0;
}

export function __permissionFacturation() {
  return _permission().f_fac_check > 0
    || _permission().f_fac_invoice > 0
    || _permission().f_fac_period > 0
    || _permission().f_fac_render > 0;
}

export function _permissionAccount() {
  return _permission().ac_b_banks > 0
    || _permission().ac_pto_pettyCashOffice > 0
    || _permission().ac_pt_pettyCash > 0
    || _permission().ac_pj_projects > 0;
}

export function _permissionUsers() {
  return _permission().u_users > 0;
}

export function __permissionClient(): number {
  return _permission().p_client;
}

export function __permissionProvider(): number {
  return _permission().p_provider;
}

export function __permissionTransit(): number {
  return _permission().p_transit;
}

export function __permissionPermanent(): number {
  return _permission().p_permanent;
}

export function __permissionEventual(): number {
  return _permission().p_eventual;
}

export function __permissionCompany(): number {
  return _permission().p_otherCompany;
}

export function __checkUser(): boolean {
  return !!sessionStorage.getItem('user');
}

