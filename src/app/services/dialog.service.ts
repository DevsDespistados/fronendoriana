import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {modal} from '../model/configuration/conf-text';
import {ModalDeleteComponent} from '../components/navigation/modal-delete/modal-delete.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) {
  }

  dialogCreate(component, data) {
    return this.dialog.open(component, {
      width: modal.widthCreate,
      data: data
    });
  }

  dialogDelete(textInfo) {
    return this.dialog.open(ModalDeleteComponent, {
      width: modal.widthCreate,
      data: {deleted: textInfo}
    });
  }
}
