// export const Msg = {
//   MSG_SUCCESS : 'Actualizado',
//   MSG_UPDATE : 'Actualizado',
//   MSG_ADD : 'agregado',
//   MSG_DELETE : 'eliminado',
//   MSG_ALERT : 'imposible',
//   MSG_ERROR : 'error',
// };
export const Msg = {
  MSG_SUCCESS: '<span><i class="mdi-navigation-check green-text small"></i>Actualizado</span>',
  MSG_UPDATE: '<span><i class="mdi-navigation-check green-text small"></i>Actualizado</span>',
  MSG_ADD: '<span><i class="mdi-navigation-check green-text small"></i>agregado</span>',
  MSG_DELETE: '<span><i class="mdi-navigation-check green-text small"></i>eliminado</span>',
  MSG_ALERT: '<span><i class="mdi-alert-warning small yellow-text"></i>imposible</span>',
  MSG_ERROR: '<span><i class="mdi-alert-error small red-text"></i>error</span>',
};
// export const Msg = {
//   MSG_SUCCESS : 1,
//   MSG_UPDATE : 2,
//   MSG_ADD : 3,
//   MSG_DELETE : 4,
//   MSG_ALERT : 5,
//   MSG_ERROR : 6,
// };
