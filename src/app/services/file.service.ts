import { Injectable } from '@angular/core';
import {AppService} from './principalService/app.service';
import {env} from './Env';
import {RequestOptions} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FileService extends AppService {

  saveFile(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('name', file.name);
    const headers = new Headers();
    headers.append('enctype', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    // @ts-ignore
    const options = new RequestOptions({headers});
    return this.post( env.SERVERNAME + '/files', formData, options);
  }

  updateFile(file: File, name: string) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('name', file.name);
    const headers = new Headers();
    headers.append('enctype', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    // @ts-ignore
    const options = new RequestOptions({headers});
    return this.put3( env.SERVERNAME + '/files/' + name, formData, options);
  }

  getFile(name: string) {
    return this.get( env.SERVERNAME + '/files/' + name);
  }
}
