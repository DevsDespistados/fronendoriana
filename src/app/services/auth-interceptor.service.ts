import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {parseObject} from '../common/common';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = sessionStorage.getItem('tokenAuth');

    let request = req;

    if (token) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${token}`
        },
        // body: parseObject(req.body)
      });
    }
    // console.log(request);

    return next.handle(request);
    //   .pipe(
    //   tap((ev: HttpEvent<any>) => {
    //     if (!(ev instanceof HttpResponse)) {
    //       sessionStorage.removeItem('tokenAuth');
    //       this.router.navigate(['/login']);
    //     } else {
    //       return request;
    //     }
    //   })
    // );
  }
}
