import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {RequestOptions} from '@angular/http';
import {env} from '../Env';

@Injectable()
export class LettersService extends AppService {
  headers: any;
  options: any;

  getAllLetters(type: any, section: string) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/' + type + '/' + section);
  }

  getLetter(id) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/letter/show/' + id);
  }

  addLetter(formData) {
    // this.headers = new Headers();
    // this.headers.append('enctype', 'multipart/form-data');
    // this.headers.append('Accept', 'application/json');
    // this.options = new RequestOptions({headers: this.headers});
    return this.postI(env.SERVERNAME + this.letters, formData);
  }

  editLetter(id, formData) {
    // this.headers = new Headers();
    // this.headers.append('enctype', 'multipart/form-data');
    // this.headers.append('Accept', 'application/json');
    // this.options = new RequestOptions({headers: this.headers});
    return this.postI(env.SERVERNAME + this.letters + '/edit/' + id, formData);
  }

  deleteLetter(id: number) {
    return this.delete(env.SERVERNAME + this.letters + '/' + id);
  }

  lastLetter(type: string): any {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/last/number/' + type);
  }

  searchLetter(some: string, type: any) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/search/' + some + '/' + type);
  }

  putCodeProject(id, code) {
    const data = {'codeLetterProject': code};
    return this.httpClient.put(env.SERVERNAME + this.letters + '/addCodeLetter/' + id, data);
  }

  updateActiveLetterForProject(id, state) {
    const data = {'activeLetterForProject': state};
    return this.httpClient.put(env.SERVERNAME + this.letters + '/project/' + id, data);
  }

  allProjectForLetter(type) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/projects/type/' + type);
  }

  searchProjectLetter(some) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/projects/search/' + some);
  }

  /**
   * add TracingLetter
   * */
  getViewTracingLetter(idLetter: number) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/tracingLetter/show/' + idLetter);
  }

  createTracingLetter(data) {
    return this.httpClient
      .post(env.SERVERNAME + this.letters + '/tracingLetter', data);
  }

  deleterTracingLetter(idLetter: number) {
    return this.httpClient.delete(env.SERVERNAME + this.letters + '/tracingLetter/' + idLetter);
  }

  printTracingLetter(id) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/tracingLetter/report/' + id);
  }

  letterHasTraingLetter(idLetter) {
    return this.httpClient.get(env.SERVERNAME + this.letters + '/tracingLetter/' + idLetter);
  }
}
