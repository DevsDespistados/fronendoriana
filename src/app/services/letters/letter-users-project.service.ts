import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';
import {UserModuleProject} from '../../model/project/user-module-project';

@Injectable({
  providedIn: 'root'
})
export class LetterUsersProjectService extends AppService {
  getUsers(projectId: number) {
    return this.httpClient.get(env.SERVERNAME + this.letterUsers + '/' + projectId + '/letter');
  }

  updateUserModule(idProject: number, users: UserModuleProject[]) {
    return this.httpClient.post(env.SERVERNAME + this.letterUsers + '/' + idProject + '/letter', users);
  }
}
