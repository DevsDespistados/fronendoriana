import {Injectable} from '@angular/core';
// import { Permission } from '../../components/back/model/security/permission';
// import { UserPermission } from '../../components/back/model/security/user-permission';
import 'rxjs';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class PermissionService extends AppService {
  getAllPermission(description: string): any {
    return this.http.get(env.SERVERNAME + '/all/' + description);
  }

  readPermissionUser(idUser: number, idPermission: number, read: number, write: number): any {
    const data = {idUser: idUser, idPermission: idPermission, readUserPermission: read, writeUserPermission: write};
    return this.http.post(env.SERVERNAME + '/updateUserPermission', data);
  }

  getAllPermissions(id) {
    return this.httpClient.get(env.SERVERNAME + '/user/permissions');
  }
}
