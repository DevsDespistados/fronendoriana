import {environment} from '../../environments/environment';

export const env = {
  // url: 'http://181.188.135.21:8081' + '/api';
  // SERVERNAME: window.location.origin + '/api',
  SERVERNAME: environment.SERVERNAME,
};

// build with
// node --max_old_space_size=8192 node_modules/@angular/cli/bin/ng build --prod
