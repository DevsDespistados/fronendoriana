import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class ProviderService extends AppService {
  headers: any;
  options: any;

  getProviders() {
    return this.get(env.SERVERNAME + this.provider);
  }

  addProvider(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.provider, formData, this.options);
  }

  editProvider(id, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.provider + '/' + id, formData, this.options);
  }

  deleteProvider(id: number) {
    return this.delete(env.SERVERNAME + this.provider + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activeProvider: state};
    return this.put(env.SERVERNAME + this.provider + '/' + id, newState);
  }

  searchProvider(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.provider + '/search/' + nameOrLastName);
  }

  getActives(type) {
    return this.get(env.SERVERNAME + this.provider + '/active/' + type);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.provider + '/report/provider', data);
  }
}
