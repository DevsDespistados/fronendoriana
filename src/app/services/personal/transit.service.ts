import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class TransitService extends AppService {
  headers: any;
  options: any;

  getTransit() {
    return this.get(env.SERVERNAME + this.transit);
  }

  addTransit(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.transit, formData, this.options);
  }

  editTransit(id, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.transit + '/' + id, formData, this.options);
  }

  deleteTransit(id: number) {
    return this.delete(env.SERVERNAME + this.transit + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activeTransit: state};
    return this.put(env.SERVERNAME + this.transit + '/' + id, newState);
  }

  searchTransit(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.transit + '/search/' + nameOrLastName);
  }

  getActives(type) {
    return this.get(env.SERVERNAME + this.transit + '/active/' + type);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.transit + '/report/transit', data);
  }
}
