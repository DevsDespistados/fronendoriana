import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class EventualService extends AppService {
  headers: any;
  options: any;

  getEventuals() {
    return this.get(env.SERVERNAME + this.eventual);
  }

  addEventual(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.eventual, formData, this.options);
  }

  editEventual(id, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.eventual + '/' + id, formData, this.options);
  }

  deleteEventual(id: number) {
    return this.delete(env.SERVERNAME + this.eventual + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activeEventual: state};
    return this.put(env.SERVERNAME + this.eventual + '/' + id, newState);
  }

  searchEventual(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.eventual + '/searchEventual/' + nameOrLastName);
  }

  getActives(type) {
    return this.get(env.SERVERNAME + this.eventual + '/active/' + type);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.eventual + '/report/eventual', data);
  }
}
