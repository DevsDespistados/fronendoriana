import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class PermanentService extends AppService {
  headers: any;
  options: any;

  getPermanents() {
    return this.get(env.SERVERNAME + this.permanent);
  }

  addPermanent(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.permanent, formData, this.options);
  }

  editPermanent(id, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.permanent + '/' + id, formData, this.options);
  }

  deletePermanent(id: number) {
    return this.delete(env.SERVERNAME + this.permanent + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activePermanent: state};
    return this.put(env.SERVERNAME + this.permanent + '/' + id, newState);
  }

  searchPermanent(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.permanent + '/search/' + nameOrLastName);
  }

  getActives(type) {
    return this.get(env.SERVERNAME + this.permanent + '/active/' + type);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.permanent + '/report/permanent', data);
  }
}
