import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class OtherCompanyService extends AppService {
  headers: any;
  options: any;

  getOthersCompanies() {
    return this.get(env.SERVERNAME + this.anotherCompany);
  }

  addOtherCompany(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.anotherCompany, formData, this.options);
  }

  editOtherCompany(id, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.anotherCompany + '/' + id, formData, this.options);
  }

  deleteOtherCompany(id: number) {
    return this.delete(env.SERVERNAME + this.anotherCompany + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activeAnotherCompany: state};
    return this.put(env.SERVERNAME + this.anotherCompany + '/' + id, newState);
  }

  searchOtherCompany(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.anotherCompany + '/search/' + nameOrLastName);
  }

  getActives(type) {
    return this.get(env.SERVERNAME + this.anotherCompany + '/active/' + type);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.anotherCompany + '/report/anotherCompany', data);
  }
}
