import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
// import { saveAs } from 'file-saver';
import {env} from '../Env';

@Injectable()
export class ClientService extends AppService {
  headers: any;
  options: any;

  getClients() {
    return this.get(env.SERVERNAME + this.client);
  }

  addClient(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.client, formData, this.options);
  }

  deleteClient(id: number) {
    return this.delete(env.SERVERNAME + this.client + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activeClient: state};
    return this.put(env.SERVERNAME + this.client + '/' + id, newState);
  }

  searchClient(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.client + '/search/' + nameOrLastName);
  }

  getActives(type) {
    return this.get(env.SERVERNAME + this.client + '/active/' + type);
  }

  // getDisables() {
  //     return this.get(env.SERVERNAME + this.client + '/active/0');
  // }
  getClientFilter(param: string, type, acd) {
    return this.get(env.SERVERNAME + this.client + '/allOrderClient/' + param + '/' + type + '/' + acd);
  }

  getClient(id) {
    return this.get(env.SERVERNAME + this.client + '/' + id);
  }

  editClient(idClient, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.client + '/' + idClient, formData, this.options);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.client + '/report/clients', data);
  }

  downloadSampleCSV(file) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    // hhh.append( 'Content-Type', 'application/json' );
    // const optio = new RequestOptions({headers: hhh});
    return this.httpClient.post(env.SERVERNAME + this.client + '/download/file', file, this.options);
  }
}
