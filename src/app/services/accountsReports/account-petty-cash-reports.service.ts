import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable({
  providedIn: 'root'
})
export class AccountPettyCashReportsService extends AppService {
  allPettyCash(type: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountPettyCash/pettyCash/' + type);
  }

  allProjectExpenses() {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountPettyCash/projects');
  }

  async getPeriods(idPettyCash: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountPettyCash/periodOfPettyCash/' + idPettyCash).toPromise();
  }

  async getTransactions(idPeriod: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountPettyCash/expensesOfPeriod/' + idPeriod).toPromise();
  }

  async getTransactionsProject(idProject: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountPettyCash/expenses/' + idProject).toPromise();
  }
}
