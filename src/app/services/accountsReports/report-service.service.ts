import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {env} from '../Env';

@Injectable({
  providedIn: 'root'
})
export class ReportServiceService {

  constructor(private http: HttpClient) {
  }
  reportFiltering(params) {
    return this.http.post(env.SERVERNAME + '/report/account', params);
  }

  reportPrintFiltering(params) {
    return this.http.post(env.SERVERNAME + '/report/account/print', params);
  }
}
