import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class AccountsBankReportsService extends AppService {

  allBanks(type: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/banks/' + type);
  }

  allPeriodBank(idBank: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/periodBank/all/' + idBank);
  }

  allPayCheck(idPeriod: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/payCheck/' + idPeriod);
  }

  allProjects() {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/projects');
  }

  // asyn
  async getBankAndPeriods(idBank: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/periodOfBank/' + idBank).toPromise();
  }

  async getPayCheck(idPeriod: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/payCheckOfPeriod/' + idPeriod).toPromise();
  }

  async allTransactionsBank(idPayCheck: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/transactionsPayCheck/all/' + idPayCheck).toPromise();
  }

  async allTransactionProject(idProject: number) {
    return this.httpClient.get(env.SERVERNAME + '/reportsAccountBank/transactionsPayCheck/all/' + idProject).toPromise();
  }
}
