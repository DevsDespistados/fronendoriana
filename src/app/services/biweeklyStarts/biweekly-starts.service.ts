import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class BiweeklyStartsService extends AppService {
  getYears() {
    return this.httpClient.get(env.SERVERNAME + this.biweeklyStarts);
  }

  getDateYear(year: number) {
    return this.httpClient.get(env.SERVERNAME + this.biweeklyStarts + '/' + year);
  }
}
