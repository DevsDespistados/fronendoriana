import {Injectable, Injector} from '@angular/core';
import {Http} from '@angular/http';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
// import * as moment from 'moment';
// import {forEach} from '@angular/router/src/utils/collection';
declare var $: any;

// declare var jsPDF: any;
@Injectable()
export class AppService {

  // loading$: Observable<boolean> = Observable.of(false);
  header: string;
  public actualModuleTxt: string[] = [];
  public subTittle = '';
  public with = 276;
  public px_mov = 'show';
  public px_mov_accounts = 'show';
  public printData: string;
  loading = true;
  public optionsAll = [];
  protected accounts = '/account';
  protected payCheck = '/payCheck';
  protected contract = '/contract';
  protected eventualForm = '/eventualForm';
  protected ownerEquipmentForm = '/ownerEquipmentForm';
  protected permanentForm = '/permanentForm';
  protected providerForm = '/providerForm';
  protected ownerEquipment = '/ownerEquipment';
  protected heavyEquipment = '/heavyEquipment';
  protected heavyVehicle = '/heavyVehicle';
  protected lightEquipment = '/lightEquipment';
  protected lightVehicle = '/lightVehicle';
  protected client = '/client';
  protected eventual = '/eventual';
  protected anotherCompany = '/anotherCompany';
  protected permanent = '/permanent';
  protected provider = '/provider';
  protected transit = '/transit';
  protected project = '/project';
  protected modification = '/modification';
  protected projectForm = '/projectForm';
  protected financial = '/financial';
  protected ticket = '/ticket';
  protected letters = '/letters';
  protected letterUsers = '/letterUsers';
  protected login = '/login';
  protected user = '/user';
  protected biweeklyStarts = '/biweeklyStart';
  protected order = '/order';
  protected item = '/items';
  protected orderItems = '/orderItem';
  protected facturacion = '/facturacion';
  public colorBlue = '#00245c';
  public colorOrangeLight = '#ffb74d';
  public COLOR = 'colorOrangeLight';
  public COLORBlue = 'colorBlue';
  // public optionsClients = [
  //   {op: '/personal/clients/actives', text: 'disponibles'},
  //   {op: '/personal/clients/disables', text: 'no disponibles'},
  //   {op: '/personal/clients/low', text: 'bajas'},
  //   {op: '/personal/clients/all', text: 'todos'},
  // ];
  // public optionsEventual = [
  //   {op: '/personal/eventual/actives', text: 'disponibles'},
  //   {op: '/personal/eventual/disables', text: 'no disponibles'},
  //   {op: '/personal/eventual/low', text: 'bajas'},
  //   {op: '/personal/eventual/all', text: 'todos'},
  // ];
  // public optionsOtherCompany = [
  //   {op: '/personal/otherCompany/actives', text: 'disponibles'},
  //   {op: '/personal/otherCompany/disables', text: 'no disponibles'},
  //   {op: '/personal/otherCompany/low', text: 'bajas'},
  //   {op: '/personal/otherCompany/all', text: 'todos'},
  // ];
  // public optionsPermanent = [
  //   {op: '/personal/permanent/actives', text: 'disponibles'},
  //   {op: '/personal/permanent/disables', text: 'no disponibles'},
  //   {op: '/personal/permanent/low', text: 'bajas'},
  //   {op: '/personal/permanent/all', text: 'todos'},
  // ];
  // public optionsTransit = [
  //   {op: '/personal/transit/actives', text: 'disponibles'},
  //   {op: '/personal/transit/disables', text: 'no disponibles'},
  //   {op: '/personal/transit/low', text: 'bajas'},
  //   {op: '/personal/transit/all', text: 'todos'},
  // ];
  // public optionsProvider = [
  //   {op: '/personal/provider/actives', text: 'disponibles'},
  //   {op: '/personal/provider/disables', text: 'no disponibles'},
  //   {op: '/personal/provider/low', text: 'bajas'},
  //   {op: '/personal/provider/all', text: 'todos'},
  // ];
  // public optionsEq = [
  //   {op: '/ownerEquipment/actives', text: 'disponibles'},
  //   {op: '/ownerEquipment/disables', text: 'no disponibles'},
  //   {op: '/ownerEquipment/low', text: 'bajas'},
  //   {op: '/ownerEquipment/all', text: 'todos'},
  // ];
  // public optionsLettersSend = [
  //   {op: '/letters/send/pendient', text: 'pendientes'},
  //   {op: '/letters/send/finalized', text: 'finalizadas'},
  //   {op: '/letters/send/all', text: 'todas'},
  // ];
  // public optionsLettersReceived = [
  //   {op: '/letters/received/pendient', text: 'pendientes'},
  //   {op: '/letters/received/finalized', text: 'finalizadas'},
  //   {op: '/letters/received/all', text: 'todas'},
  // ];
  // public optionsLettersProject = [
  //   {op: '/letters/projects/actives', text: 'disponibles'},
  //   {op: '/letters/projects/disables', text: 'no disponibles'},
  //   {op: '/letters/projects/all', text: 'todos'},
  // ];
  // public optionsFinancial = [
  //   {op: '/tickets/financials/available', text: 'vigentes'},
  //   {op: '/tickets/financials/low', text: 'bajas'},
  //   {op: '/tickets/financials/all', text: 'todas'},
  // ];
  // public optionsProjectTicket = [
  //   {op: '/tickets/projects/actives', text: 'disponibles'},
  //   {op: '/tickets/projects/disables', text: 'no disponibles'},
  //   {op: '/tickets/projects/all', text: 'todos'},
  // ];
  //
  // print = false;
  constructor(protected httpClient: HttpClient, protected http: Http,
              protected injector: Injector, protected router: Router) {
    //   // this.actualModuleTxt = ' _ ';
    //   this.subTittle = '';
    //   this.optionsAll = [];
    //   this.px_mov = 'show';
  }

  public setOpen() {
    this.with = 0;
  }

  public setClose() {
    this.with = 276;
  }

  fireLoader() {
    // this.loading$ = Observable.of(true);
    this.loading = true;
  }

  stopLoader() {
    // this.loading$ = Observable.of(false);
    this.loading = false;
    // this.clearInputFile();
  }

  setDefaultColor() {
    // this.COLOR = 'colorBlue';
  }

  public post(url, data, options) {
    return this.httpClient.post(url, data, options);
  }

  public postI(url, data) {
    return this.httpClient.post(url, data);
  }

  public get(url) {
    return this.httpClient.get(url);
  }

  public getOptions(url, options) {
    return this.httpClient.get(url, {
      params: options
    });
  }

  public put3(url, data, options) {
    return this.httpClient.put(url, data, options);
  }

  public put(url, data) {
    return this.httpClient.put(url, data);
  }

  public delete(url) {
    return this.httpClient.delete(url);
  }

  // generateToPdf( title: string , columns: any , rows, columnStyle: any) {
  //   const doc = new jsPDF('p', 'pt');
  //   const base64Img = '../assets/images/oriana-logo5.jpg' ;
  //   doc.addImage(base64Img, 'JPEG',  40, 35, 120, 35);
  //   console.log('style' + columnStyle);
  //   doc.autoTable(columns, rows , columnStyle);
  //   doc.setFontSize(12);
  //   doc.text(title, 230, 90);
  //   const date =  new Date();
  //   doc.text('FECHA: ' +  moment().format('DD-MMM-YY').toUpperCase(), 460, 40);
  //   const a = doc.output('dataurlstring');
  //   this.printData = a;
  //   // document.getElementsByTagName('object');
  //   // const ss = document.getElementById('contentPDF');
  //   // ss.setAttribute('data', a);
  //   // const rou = Router.prototype;
  //   console.log(a);
  //   this.router.navigate(['/print']);
  // }
  // showFile(code: string) {
  //   this.printData = code;
  //   this.router.navigate(['/print']);
  // }
  // public clearInputFile() {
  //   const  files =  $('input[type="file"]').val(null);
  //   const  inputDate =  $('.ngx-datepicker-container>input[type="text"]').val(null);
  //   // if (files) {
  //   //   files.forEach(file => {
  //   //     file.value = null;
  //   //   });
  //   // }
  // }
}
