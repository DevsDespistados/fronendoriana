import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
// import {LightVehicle} from '../../components/back/model/ownerEquipment/lightVehicle';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class LightVehicleService extends AppService {
  headers: any;
  options: any;

  getLightVehicle(): any {
    return this.httpClient.get(env.SERVERNAME + this.lightVehicle);
  }

  addLightVehicle(formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.lightVehicle, formData, this.options);
  }

  editLightVehicle(id, formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.lightVehicle + '/' + id, formData, this.options);
  }

  deleteLightVehicle(id: number): any {
    return this.httpClient.delete(env.SERVERNAME + this.lightVehicle + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {availableLightVehicle: state};
    return this.httpClient
      .put(env.SERVERNAME + this.lightVehicle + '/' + id, newState);
  }

  searchLightVehicle(nameOrLastName: string) {
    return this.httpClient.get(env.SERVERNAME + this.lightVehicle + '/search/' + nameOrLastName);
  }

  getActives(param: any): any {
    return this.httpClient.get(env.SERVERNAME + this.lightVehicle + '/available/' + param);
  }

  getLowAndNotLow(a, b): any {
    return this.httpClient.get(env.SERVERNAME + this.lightVehicle + '/notLow/' + a + '/' + b);
  }

  getLightVehicleFilter(type, name, asc): any {
    return this.httpClient.get(env.SERVERNAME + this.lightVehicle +
      '/allOrderLightVehicle/' + type + '/' + name + '/' + asc);
  }

  deleteFile(id) {
    return this.httpClient
      .put(env.SERVERNAME + this.lightVehicle + '/updateFile/' + id, {fileLightVehicle: 'sin archivo'});
  }
}
