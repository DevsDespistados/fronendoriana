import {Injectable} from '@angular/core';
// import {any} from '../../components/back/model/ownerEquipment/heavy-equipment';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class HeavyEquipmentService extends AppService {
  headers: any;
  options: any;

  getHeavyEquipment(): any {
    return this.get(env.SERVERNAME + this.heavyEquipment);
  }

  addHeavyEquipment(formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.heavyEquipment, formData, this.options);
  }

  editHeavyEquipment(id, formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.heavyEquipment + '/' + id, formData, this.options);
  }

  deleteHeavyEquipment(id: number): any {
    return this.httpClient.delete(env.SERVERNAME + this.heavyEquipment + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {availableHeavyEquipment: state};
    return this
      .put(env.SERVERNAME + this.heavyEquipment + '/' + id, newState);
  }

  searchHeavyEquipment(nameOrLastName: string) {
    return this.httpClient.get(env.SERVERNAME + this.heavyEquipment + '/search/' + nameOrLastName);
  }

  getActives(param: any): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyEquipment + '/available/' + param);
  }

  getLowAndNotLow(a, b): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyEquipment + '/notLow/' + a + '/' + b);
  }

  getHeavyEquipmentFilter(type, order, asc): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyEquipment +
      '/allData/' + type + '/' + order + '/' + asc);
  }
}
