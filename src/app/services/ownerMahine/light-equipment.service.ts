import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
// import {LightEquipment} from '../../components/back/model/ownerEquipment/light-equipment';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class LightEquipmentService extends AppService {
  headers: any;
  options: any;

  getLightEquipment(): any {
    return this.httpClient.get(env.SERVERNAME + this.lightEquipment);
  }

  addLightEquipment(formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.lightEquipment, formData, this.options);
  }

  editLightEquipment(id, formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.lightEquipment + '/' + id, formData, this.options);
  }

  deleteLightEquipment(id: number): any {
    return this.httpClient.delete(env.SERVERNAME + this.lightEquipment + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {availableLightEquipment: state};
    return this.httpClient
      .put(env.SERVERNAME + this.lightEquipment + '/' + id, newState);
  }

  searchLightEquipment(nameOrLastName: string) {
    return this.httpClient.get(env.SERVERNAME + this.lightEquipment + '/search/' + nameOrLastName);
  }

  getActives(param: any): any {
    return this.httpClient.get(env.SERVERNAME + this.lightEquipment + '/available/' + param);
  }

  getLowAndNotLow(a, b): any {
    return this.httpClient.get(env.SERVERNAME + this.lightEquipment + '/notLow/' + a + '/' + b);
  }

  getLightEquipmentFilter(type, name, asc): any {
    return this.httpClient.get(env.SERVERNAME + this.lightEquipment +
      '/allOrderLightEquipment/' + type + '/' + name + '/' + asc);
  }
}
