import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class OwnerEquipmentService extends AppService {

  headers: any;
  options: any;

  getOwnerEquipment() {
    return this.get(env.SERVERNAME + this.ownerEquipment);
  }

  addOwnerEquipment(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.ownerEquipment, formData, this.options);
  }

  editOwnerEquipment(id, formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this
      .post(env.SERVERNAME + this.ownerEquipment + '/' + id, formData, this.options);
  }

  deleteOwnerEquipment(id: number) {
    return this.delete(env.SERVERNAME + this.ownerEquipment + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {availableOwnerEquipment: state};
    return this.put(env.SERVERNAME + this.ownerEquipment + '/' + id, newState);
  }

  searchOwnerEquipment(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.ownerEquipment + '/search/' + nameOrLastName);
  }

  getActives(param: any) {
    return this.get(env.SERVERNAME + this.ownerEquipment + '/available/' + param);
  }

  getAllOwnerEquipment(a) {
    return this.get(env.SERVERNAME + this.ownerEquipment + '/allData/' + a);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.ownerEquipment + '/report/ownerEquipment', data);
  }
}
