import {Injectable} from '@angular/core';
// import {HeavyVehicle} from '../../components/back/model/ownerEquipment/heavy-vehicle';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class HeavyVehicleService extends AppService {
  headers: any;
  options: any;

  getHeavyVehicle(): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyVehicle);
  }

  addHeavyVehicle(formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.heavyVehicle, formData, this.options);
  }

  editHeavyVehicle(id, formData: FormData): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.heavyVehicle + '/' + id, formData, this.options);
  }

  deleteHeavyVehicle(id: number): any {
    return this.httpClient.delete(env.SERVERNAME + this.heavyVehicle + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {availableHeavyVehicle: state};
    return this.httpClient
      .put(env.SERVERNAME + this.heavyVehicle + '/' + id, newState);
  }

  searchHeavyVehicle(nameOrLastName: string) {
    return this.httpClient.get(env.SERVERNAME + this.heavyVehicle + '/search/' + nameOrLastName);
  }

  getActives(param: any): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyVehicle + '/available/' + param);
  }

  getLowAndNotLow(a, b): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyVehicle + '/notLow/' + a + '/' + b);
  }

  getHeavyVehicleFilter(type, name, asc): any {
    return this.httpClient.get(env.SERVERNAME + this.heavyVehicle + '/allOrderHeavyVehicle/' + type + '/' + name + '/' + asc);
  }
}
