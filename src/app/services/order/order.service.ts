import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class OrderService extends AppService {
  optionPetition = 1;
  optionQuotation = 2;
  addOrder(formData: FormData) {
    return this.httpClient.post(env.SERVERNAME + this.order, formData);
  }

  deleteOrder(idOrder: number) {
    return this.delete(env.SERVERNAME + this.order + '/' + idOrder);
  }

  updateOrder(idOrder: number, formData: FormData) {
    return this.httpClient.post(env.SERVERNAME + this.order + '/' + idOrder, formData);
  }

  updateState(idOrder: number, stateOrder: boolean) {
    const data = {stateOrderPetition: stateOrder};
    return this.httpClient.put(env.SERVERNAME + this.order + '/updateState/' + idOrder, data);
  }

  updateStates(idOrder: number, form) {
    return this.httpClient.post(env.SERVERNAME + this.order + '/update/states/' + idOrder, form);
  }

  searchOrder(text: string) {
    return this.httpClient.get(env.SERVERNAME + this.order + '/search/' + text);
  }

  filterOrder(state: number, typeState) {
    return this.httpClient.get(env.SERVERNAME + this.order + '/' + state + '/' + typeState);
  }

  getOrder(id) {
    return this.httpClient.get(env.SERVERNAME + this.order + '/show/' + id);
  }

  getItems(idOrder: number) {
    return this.httpClient.get(env.SERVERNAME + this.order + '/filter/item/' + idOrder);
  }
}
