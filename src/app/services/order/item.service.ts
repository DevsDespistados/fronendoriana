import {AppService} from '../principalService/app.service';
import {env} from '../Env';
import {Injectable} from '@angular/core';

@Injectable()
export class ItemService extends AppService {
  allItem() {
    return this.httpClient.get(env.SERVERNAME + this.item);
  }

  getOrderItems(idOrder) {
    return this.httpClient.get(env.SERVERNAME + this.orderItems + '/items/' + idOrder);
  }

  addItem(formData: FormData) {
    return this.httpClient.post(env.SERVERNAME + this.item, formData);
  }

  deleteItem(idOrder: number) {
    return this.delete(env.SERVERNAME + this.item + '/' + idOrder);
  }

  updateItem(idOrder: number, formData: FormData) {
    return this.httpClient.post(env.SERVERNAME + this.item + '/' + idOrder, formData);
  }

  searchItem(text: string) {
    return this.httpClient.get(env.SERVERNAME + this.item + '/search/' + text);
  }
}
