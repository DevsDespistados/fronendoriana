import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class OrderItemService extends AppService {
  addRelationOrderItem(idOrder: number, selected: any) {
    const data = {
      data: selected,
      idOrder: idOrder
    };
    return this.httpClient.post(env.SERVERNAME + '/orderItem', data);
  }

  deleteRelationOrderItem(idOrderItem: number) {
    return this.httpClient.delete(env.SERVERNAME + '/orderItem/delete/' + idOrderItem);
  }

  allItemOfOrder(idOrder: number) {
    return this.httpClient.get(env.SERVERNAME + '/orderItem/items/' + idOrder);
  }

  updateOrderItem(data: any, section: string) {
    const datas = {orderItems: data, type: section};
    return this.httpClient.post(env.SERVERNAME + '/orderItem/update', datas);
  }

  searchItems(idOrderItem: number, text: string) {
    return this.httpClient.get(env.SERVERNAME + '/orderItem/search/items/' + idOrderItem + '/' + text);
  }
}
