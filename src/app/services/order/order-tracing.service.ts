import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
// import {ViewItemOrder} from '../../components/back/model/order/view-item-order';
// import {ViewTracingOrder} from '../../components/back/model/order/view-tracing-order';
import {env} from '../Env';

@Injectable()
export class OrderTracingService extends AppService {
  allOrderTracingItem(id: number) {
    return this.httpClient.get(env.SERVERNAME + '/orderTracing/' + id);
  }

  addOrderTracingItem(data: any, date: any, idOrder: number) {
    const datas = {
      datas: data,
      dateOrderBuy: date,
      idOrder: idOrder
    };
    return this.httpClient.post(env.SERVERNAME + '/orderTracing', datas);
  }

  allOrderGroupTable(idOrder: number, nro: number) {
    return this.httpClient.get(env.SERVERNAME + '/orderTracing/' + idOrder + '/' + nro);
  }

  allOrderBuy(idOrder: number, type: number) {
    return this.httpClient.get(env.SERVERNAME + '/orderTracing/buys/' + idOrder + '/' + type);
  }

  deleteOrderBuy(idOrderBuy: number, idOrder) {
    return this.httpClient.delete(env.SERVERNAME + '/orderTracing/' + idOrderBuy + '/' + idOrder);
  }

  updateOrderBuy(idOrderBuy: number, date: any, data: any) {
    const datas = {
      datas: data,
      dateOrderBuy: date,
      idOrderBuy: idOrderBuy
    };
    return this.httpClient.put(env.SERVERNAME + '/orderTracing/' + idOrderBuy, datas);
  }

  updateStateOrderBuy(idOrderBuy: number, idOrder, state: number) {
    return this.httpClient.post(env.SERVERNAME + '/orderTracing/updateState/' + idOrderBuy, {stateOrderBuy: state, idOrder: idOrder});
  }

  editOrderTracing(idOrder: number, idOrderBuy: number) {
    return this.httpClient.get(env.SERVERNAME + '/orderTracing/edit/' + idOrder + '/' + idOrderBuy);
  }

  async getAsyncOrderTracing(idOrder: number, idOrderBuy: number) {
    return this.httpClient.get<any[]>(env.SERVERNAME + '/orderTracing/edit/' + idOrder + '/' + idOrderBuy).toPromise();
  }
}
