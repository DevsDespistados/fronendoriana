import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class CategoryService extends AppService {
  category() {
    return this.httpClient.get(env.SERVERNAME + '/category');
  }
}


