import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class TransactionService extends AppService {
  headers: any;
  options: any;

  getTransactionPeriod(idPeriod, idCheck) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankTransaction/all/' + idPeriod + '/' + idCheck);
  }

  getTransactionPeriodCheck(idPeriod, idCheck) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankTransaction/' + idPeriod + '/' + idCheck);
  }

  getAllAmortization(idBank, idPeriod, idCheck, idTransit) {
    return this.httpClient
      .get(env.SERVERNAME + this.accounts + '/bankTransaction/getAllAmortization/'
        + idBank + '/'
        + idPeriod + '/'
        + idCheck + '/'
        + idTransit);
  }

  allAmotization() {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankTransaction/transitTransactions');
  }

  searchTransaction(param) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankTransaction/search/' + param + '/transaction');
  }

  searchAmortization(param) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankTransaction/transitTransactions/search/' + param);
  }

  newTransaction(type, data) {
    return this.httpClient.post(env.SERVERNAME + this.accounts + '/bankTransaction/' + type, data);
  }

  addTransaction(actor, type, idBank, formData: any) {
    return this.httpClient
      .post(env.SERVERNAME + this.accounts + '/bankTransaction/add/' + actor + '/' + type + '/' + idBank, formData);
  }

  editTransaction(idTransaction, type, formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.accounts + '/bankTransaction/edit/' + type + '/' + idTransaction, formData);
  }

  addAccountPettyCash(formData: any): any {
    return this.httpClient
      .post(env.SERVERNAME + this.accounts + '/pettyCash', formData);
  }

  editAccountBank(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/editBank/' + id, formData);
  }

  editAccountPettyCash(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/editPettyCash/' + id, formData);
  }

  deleteTransaction(id) {
    return this.httpClient.delete(env.SERVERNAME + this.accounts + '/bankTransaction/' + id);
  }

  deletePettyCash(id): any {
    return this.httpClient
      .delete(env.SERVERNAME + this.accounts + '/deletePettyCash/' + id);
  }

  getTransactionPartForm(idForm, code) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/BankTransaction/getParts/' + idForm + '/' + code);
  }

  deleteTransactionPartForm(id, idBank): any {
    return this.httpClient
      .delete(env.SERVERNAME + this.accounts + '/BankTransaction/delete/' + id + '/' + idBank);
  }

  getTransactionPettyCashPeriod(idPettyCash, idPettyCashPeriod) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashExpense/' + idPettyCash + '/' + idPettyCashPeriod);
  }

  getAllTransactionPeriodPettyCash(idPeriod) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashExpense/all/' + idPeriod);
  }

  getPeriodSelect(idP, year, month): any {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashExpense/getPeriod/'
      + idP + '/' + year + '/' + month);
  }

  addTransactionPettyCash(formData) {
    return this.httpClient.post(env.SERVERNAME + this.accounts + '/pettyCashExpense', formData);
  }

  editTransactionPettyCash(id, formData) {
    return this.httpClient.put(env.SERVERNAME + this.accounts + '/pettyCashExpense/' + id, formData);
  }

  searchPettyCashTransacction(text, idPeriod) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashExpense/search/' + text + '/' + idPeriod);
  }

  importFile(formData: FormData, idPeriod): any {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient
      .post(env.SERVERNAME + this.accounts + '/pettyCashExpense/import/' + idPeriod, formData, this.options);
  }

  getAnticipations(idActor, idCode) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/BankTransaction/getAnticipations/' + idActor + '/' + idCode);
  }

  setAnticipation(idTransaction, body): any {
    return this.httpClient.put(env.SERVERNAME + this.accounts + '/BankTransaction/setAnticipations/' + idTransaction, body);
  }

  /**amortization
   * id
   */
  getTransactionAmortization(id) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/amortization/getAmortizationParts/' + id);
  }

  deletePettyCashExpense(id) {
    return this.httpClient.delete(env.SERVERNAME + this.accounts + '/pettyCashExpense/' + id);
  }

  printFact(formData) {
    return this.httpClient.post(env.SERVERNAME + this.accounts + '/bankTransaction/printFact/transaction', formData);
  }
}
