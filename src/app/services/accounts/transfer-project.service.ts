import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class TransferProjectService extends AppService {

  getProjects(type) {
    return this.httpClient.get(env.SERVERNAME + '/transferProject/projects/' + type);
  }

  setState(project, id) {
    return this.httpClient.put(env.SERVERNAME + '/transferProject/update/stateProject/' + id, project);
  }

  addTransfer(data) {
    return this.httpClient.post(env.SERVERNAME + '/transferProject/receiptTransferProject', data);
  }

  searchTransfer(text) {
    return this.httpClient.get(env.SERVERNAME + '/transferProject/search/' + text);
  }
}
