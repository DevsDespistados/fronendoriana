import {Injectable} from '@angular/core';
// import {Bank} from '../../components/back/model/accounts/bank';
// import {PettyCash} from '../../components/back/model/accounts/petty-cash';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class AccountService extends AppService {
  addAccountBank(formData: any): any {
    return this.httpClient
      .post(env.SERVERNAME + this.accounts + '/bank', formData);
  }

  editAccountBank(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/editBank/' + id, formData);
  }

  deleteBank(id): any {
    return this.httpClient
      .delete(env.SERVERNAME + this.accounts + '/deleteBank/' + id);
  }

  setStateBank(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/stateBank/' + id, formData);
  }

  getBankActives(type) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/activeBank/' + type);
  }

  showBank(id) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bank/' + id);
  }

  getBanksNot(id) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/allListBank/' + id);
  }

  searchBanks(param) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/searchBank/' + param);
  }

  /*pettyCash Office*/
  getPettyCashOfficeActives(type) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/activePettyCashOffice/' + type);
  }

  searchPettyCashOffice(param) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/searchPettyCashOffice/' + param);
  }

  /*petty cash*/
  addAccountPettyCash(formData: any): any {
    return this.httpClient.post(env.SERVERNAME + this.accounts + '/pettyCash', formData);
  }

  editAccountPettyCash(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/editPettyCash/' + id, formData);
  }

  searchPettyCash(param) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/searchPettyCash/' + param);
  }

  getPettyCash(type) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCash/filter/' + type);
  }

  deletePettyCash(id): any {
    return this.httpClient
      .delete(env.SERVERNAME + this.accounts + '/deletePettyCash/' + id);
  }

  setStatePettyCash(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/statePettyCash/' + id, formData);
  }
}
