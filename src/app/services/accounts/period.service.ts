import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class PeriodService extends AppService {
  getPeriods(id, type) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankPeriod/' + type + '/' + id);
  }

  getPeriod(idBank) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankPeriodGet/' + idBank);
  }

  addPeriod(formData: any): any {
    return this.httpClient
      .post(env.SERVERNAME + this.accounts + '/bankPeriod', formData);
  }

  searchPeriod(param) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankPeriod/search/' + param);
  }

  deletePeriod(id): any {
    return this.httpClient
      .delete(env.SERVERNAME + this.accounts + '/bankPeriod/' + id);
  }

  setPeriod(id: number, state: number) {
    return this.httpClient.put(env.SERVERNAME + this.accounts + '/bankPeriod/update/' + id, {stateBankPeriod: state});
  }

  editAccountPeriod(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/bankPeriod/' + id, formData);
  }

  getPeriodLimit(id, limit, not) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/bankPeriodLimit/' + id + '/' + limit + '/' + not);
  }

  /*petty cash period*/
  getPettyCashPeriods(id, type) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashPeriod/' + type + '/' + id);
  }

  getAllPeriodPettyCash(id) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashPeriod/all/' + id);
  }

  addPettyCashPeriod(formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.accounts + '/pettyCashPeriod', formData);
  }

  editPettyCashPeriod(id, formData: any): any {
    return this.httpClient
      .put(env.SERVERNAME + this.accounts + '/pettyCashPeriod/' + id, formData);
  }

  deletePettyCash(id): any {
    return this.httpClient
      .delete(env.SERVERNAME + this.accounts + '/pettyCashPeriod/' + id);
  }

  getPeriodPettyCash(idPettyCash, year, month) {
    return this.httpClient.get(env.SERVERNAME + this.accounts + '/pettyCashPeriod/' + idPettyCash + '/' + year + '/' + month);
  }

  setPeriodPettyCash(id: number, state: number) {
    return this.httpClient.put(env.SERVERNAME + this.accounts + '/pettyCashPeriod/update/' + id, {statePettyCashPeriod: state});
  }
}
