import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {RequestOptions} from '@angular/http';
import {env} from '../Env';

@Injectable()
export class PayCheckService extends AppService {
  headers;
  options;

  getPayCheck(type, idPeriod) {
    return this.get(env.SERVERNAME + this.payCheck + '/period/' + type + '/' + idPeriod);
  }

  addPayCheck(data) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.payCheck + '/period', data, this.options);
  }

  editCheck(id, data) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.payCheck + '/period/' + id, data, this.options);
  }

  allCheck(id) {
    return this.get(env.SERVERNAME + this.payCheck + '/period/' + id);

  }

  delete(id) {
    return this.httpClient.delete(env.SERVERNAME + this.payCheck + '/period/' + id);
  }

  // render
  getPayCheckRender(idPeriod) {
    return this.get(env.SERVERNAME + this.payCheck + '/render/' + idPeriod);
  }

  getTotalPettyCash(idPeriod) {
    return this.get(env.SERVERNAME + this.payCheck + '/renderPettyCash/' + idPeriod);
  }

  saveRenderInvoice(data) {
    return this.httpClient.post(env.SERVERNAME + this.payCheck + '/render', data);
  }

  deleteInvoiceOnRender(id) {
    return this.httpClient.delete(env.SERVERNAME + this.payCheck + '/render/' + id);
  }
}
