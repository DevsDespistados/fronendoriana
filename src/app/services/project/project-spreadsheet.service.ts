import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class ProjectSpreadsheetService extends AppService {
  headers;
  options;

  getProjectSpSheet(idP) {
    return this.get(env.SERVERNAME + this.projectForm + '/allOrderProjectForm/' + idP + '/dateForm');
  }

  deleteProjectSpSheet(id: number) {
    return this.delete(env.SERVERNAME + this.projectForm + '/' + id);
  }

  addSpSheet(data: any) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.projectForm, data, this.options);
  }

  editSpSheet(id, data: any) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.projectForm + '/edit/' + id, data, this.options);
  }

  getSpSheetAccount(idP, idBank, idCheck, idPeriod) {
    return this.get(env.SERVERNAME + this.projectForm + '/allOrderProjectForm/account/' + idP + '/' + idBank + '/' + idCheck + '/' + idPeriod);
  }
}
