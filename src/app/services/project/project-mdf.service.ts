import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class ProjectMdfService extends AppService {
  // serverName + this.modification: any = 'http://localhost:8000/api/v1/modification';
  // constructor(private http: Http) { }
  headers;
  options;

  getProjectMdf(idP) {
    return this.get(env.SERVERNAME + this.modification + '/allOrderModification/' + idP + '/dateModification');
  }

  deleteProjectMdf(id: number) {
    return this.delete(env.SERVERNAME + this.modification + '/' + id);
  }

  addModification(data: any) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.modification, data, this.options);
  }

  editModification(id, data: any) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.post(env.SERVERNAME + this.modification + '/edit/' + id, data, this.headers);
  }
}
