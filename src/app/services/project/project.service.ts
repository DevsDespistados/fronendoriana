import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';
import {Project} from '../../model/project/project';

declare var $: any;

@Injectable()
export class ProjectService extends AppService {
  headers: any;
  options: any;

  getProjects(options?: any) {
    if (options) {
      return this.getOptions(env.SERVERNAME + this.project, options);
    }
    return this.get(env.SERVERNAME + this.project);
  }

  getOneProject(id) {
    return this.get(env.SERVERNAME + this.project + '/' + id);
  }

  addProject(formData: Project, idClient: any) {
    // this.headers = new Headers();
    // this.headers.append('enctype', 'multipart/form-data');
    // this.headers.append('Accept', 'application/json');
    // this.options = new RequestOptions({headers: this.headers});
    return this.postI(env.SERVERNAME + this.project + '/' + idClient, formData);
  }

  editProject(idProject, formData: FormData, idClient: any) {
    // this.headers = new Headers();
    // this.headers.append('enctype', 'multipart/form-data');
    // this.headers.append('Accept', 'application/json');
    // this.options = new RequestOptions({headers: this.headers});
    return this.postI(env.SERVERNAME + this.project + '/update/' + idProject + '/with/' + idClient, formData);
  }

  deleteProject(id: number) {
    return this.delete(env.SERVERNAME + this.project + '/' + id);
  }

  changeState(id: number, state: number) {
    const newState = {activeProject: state};
    return this.put(env.SERVERNAME + this.project + '/' + id, newState);
  }

  searchProject(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.project + '/searchProject/' + nameOrLastName);
  }

  filterProjectStates(nameOrLastName: string) {
    return this.get(env.SERVERNAME + this.project + '/states/' + nameOrLastName);
  }

  addDateFollowUpProject(idProject, state, data) {
    return this.httpClient.post(env.SERVERNAME + this.project + '/addDateFollowUp/' + idProject + '/' + state, data);
  }

  deleteState(idProject, state) {
    return this.httpClient.delete(env.SERVERNAME + this.project + '/delete/' + idProject + '/' + state);
  }

  getOpen() {
    return this.get(env.SERVERNAME + this.project + '/open/1');
  }

  getOpeForm(type: number) {
    return this.get(env.SERVERNAME + this.project + '/openForm/' + type);
  }

  getOpeAccount(type) {
    return this.get(env.SERVERNAME + this.project + '/activeAccount/' + type);
  }

  updateDescription(data) {
    return this.put(env.SERVERNAME + this.project + '/updateDescription', data);
  }

  updateStateForFormProject(id, state) {
    return this.httpClient.put(env.SERVERNAME + this.project + '/openFormActive/' + id, {state: state});
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.project + '/report/project', data);
  }

  getProjectOfClient(idClient: number) {
    return this.get(env.SERVERNAME + this.project + '/projectClient/' + idClient);
  }
}
