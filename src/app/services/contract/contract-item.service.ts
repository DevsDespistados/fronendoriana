import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

declare var $: any;

@Injectable()
export class ContractItemService extends AppService {
  getAllItems(id: number) {
    return this.get(env.SERVERNAME + this.contract + '/item/' + id + '/all/2');
  }

  getContractAdvance(idContract) {
    return this.get(env.SERVERNAME + this.contract + '/item/' + idContract + '/all/1');
  }

  getContractDiscount(idContract) {
    return this.get(env.SERVERNAME + this.contract + '/item/' + idContract + '/all/0');
  }

  addContractItem(formData: any) {
    return this.postI(env.SERVERNAME + this.contract + '/item/save', formData);
  }

  editContractItem(id, formData: any) {
    return this.put(env.SERVERNAME + this.contract + '/item/saveEdit/' + id, formData);
  }

  deleteContractItem(id) {
    return this.delete(env.SERVERNAME + this.contract + '/item/delete/' + id);
  }
}
