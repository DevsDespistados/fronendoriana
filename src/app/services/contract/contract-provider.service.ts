import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

declare var $: any;

@Injectable()
export class ContractProviderService extends AppService {
  getContractProvider() {
    return this.get(env.SERVERNAME + this.contract + '/getViewContracts/nameProvider');
  }

  getContractProviderFilter(active) {
    return this.get(env.SERVERNAME + this.contract + '/getFilterContracts/nameProvider/' + active);
  }

  getContractProviderAccount(idBank, idPeriod, idCheck) {
    return this.get(env.SERVERNAME + this.contract + '/getAccountContract/' + idBank + '/' + idPeriod + '/' + idCheck);
  }

  addContractProvider(formData: any) {
    return this.postI(env.SERVERNAME + this.contract, formData);
  }

  editContractProvider(idContract, formData) {
    return this.put(env.SERVERNAME + this.contract + '/edit/' + idContract, formData);
  }

  changeState(idContract, state) {
    const newState = {activeContract: state};
    return this.put(env.SERVERNAME + this.contract + '/' + idContract, newState);
  }

  deleteContractProvider(id: number) {
    return this.delete(env.SERVERNAME + this.contract + '/' + id);
  }

  getContract(id) {
    return this.get(env.SERVERNAME + this.contract + '/edit/' + id);
  }

  deleteContract(id) {
    return this.delete(env.SERVERNAME + this.contract + '/' + id);
  }

  searchContract(param) {
    return this.get(env.SERVERNAME + this.contract + '/search/' + param);
  }

  searchContractActives(param) {
    return this.get(env.SERVERNAME + this.contract + '/searchOnlyActives/' + param);
  }

  showViewContract(id) {
    return this.get(env.SERVERNAME + this.contract + '/showViewContract/' + id);
  }
}
