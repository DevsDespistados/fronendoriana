import * as _moment from 'moment';

export function moment(date): string {
  return _moment(date).format('YYYY-MM-DD');
}

export function momentDateTime(date, time): string {
  return _moment(date).format('YYYY-MM-DD') + ' ' + time + ':00';
}

export function actualBiwekly(): number {
  const date = _moment();
  const year = date.year();
  let ab = 1;
  for (let pos = 1; pos < 27; pos++) {
    const dateIni = _moment('2017-12-31').add((((year - 2018) * 26 + (pos - 1)) * 14), 'days');
    const dateEnd = _moment(dateIni.format('YYYY-MM-DD')).add(13, 'days');
    if (date >= dateIni && date <= dateEnd) {
      ab = pos;
      break;
    }
  }
  return ab;
}

export function endDate(date: any) {
  return _moment(date).endOf('month').format('YYYY-MM-DD');
}

export function compareString(a: string, b: string, isAsc: boolean) {
  if (a && b) {
    return (a.toLocaleUpperCase() < b.toLocaleUpperCase() ? -1 : 1) * (isAsc ? 1 : -1);
  } else if (a && !b) {
    return (a.toLocaleUpperCase() < '' ? -1 : 1) * (isAsc ? 1 : -1);
  } else if (!a && b) {
    return ('' < b.toLocaleUpperCase() ? -1 : 1) * (isAsc ? 1 : -1);
  }
}

export function compareNumber(a: number, b: number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

export function compareDate(a: string, b: string, isAsc: boolean): number {
  return (_moment(a) < _moment(b) ? -1 : 1) * (isAsc ? 1 : -1);
}

export function compareDateTime(a: any, iTime, b: any, eTime): boolean {
  const ini = _moment(_moment(a).format('YYYY-MM-DD') + ' ' + iTime + ':00');
  const end = _moment(_moment(b).format('YYYY-MM-DD') + ' ' + eTime + ':00');
  return ini < end;
}

export function compareBiweekly(a1: number, y1: number, a2: number, y2: number, isAsc: boolean) {
  return (dateForBiweebly(y1, a1) < dateForBiweebly(y1, a2) ? -1 : 1) * (isAsc ? 1 : -1);
}

function dateForBiweebly(year, pos) {
  return _moment('2017-12-31').add((((year - 2018) * 26 + (pos - 1)) * 14), 'days');
}

export function timeInTwoDate(d1, d2) {
  const a = _moment(d1);
  const b = _moment(d2);
  return a.diff(b);
}

export function currentTime(end) {
  const b = _moment(end);
  const a = _moment(new Date());
  return a.diff(b);
}

export function getYearShort(): number {
  return Number(_moment().format('YY'));
}

export function allYears() {
  const years = [];
  const ini = 1990;
  const actualYear = new Date().getFullYear();
  for (let y = ini; y <= actualYear; y++) {
    years.push(y);
  }
  return years;
}

export function getMonths(): any[] {
  const months = [
    'ENERO',
    'FEBRERO',
    'MARZO',
    'ABRIL',
    'MAYO',
    'JUNIO',
    'JULIO',
    'AGOSTO',
    'SEPTIEMBRE',
    'OCTUBRE',
    'NOVIEMBRE',
    'DICIEMBRE'
  ];
  return months;
}

export function convertToNormatDate(date: string) {
  const months = {
    Ene: 1,
    Feb: 2,
    Mar: 3,
    Abr: 4,
    May: 5,
    Jun: 6,
    Jul: 7,
    Ago: 8,
    Sep: 9,
    Oct: 10,
    Nov: 11,
    Dic: 12
  };
  const dat = date.split('-');
  return new Date((2000 + Number(dat[1])), (months[dat[0]]) - 1, 1);
}

export function zeroPad(num, places) {
  const zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join('0') + num;
}

export function zeroFill(number, width) {
  width -= number.toString().length;
  if (width > 0) {
    return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
  }
  return number + ''; // siempre devuelve tipo cadena
}
