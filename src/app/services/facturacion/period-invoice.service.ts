import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {RequestOptions} from '@angular/http';
import {env} from '../Env';

@Injectable({
  providedIn: 'root'
})
export class PeriodInvoiceService extends AppService {
  headers: any;
  options: any;

  importFile(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient.post(env.SERVERNAME + this.facturacion, formData, this.options);
  }

  getPeriodInvoice(type) {
    return this.httpClient.get(env.SERVERNAME + '/period' + this.facturacion + '/' + type);
  }

  searchPeriods(text: string) {
    return this.httpClient.get(env.SERVERNAME + '/period/search' + this.facturacion + '/' + text);
  }

  setStatePeriod(id, state) {
    return this.httpClient.post(env.SERVERNAME + '/period/state' + this.facturacion + '/' + id, {periodState: state});
  }

  deletePeriod(id) {
    return this.httpClient.delete(env.SERVERNAME + '/period' + this.facturacion + '/' + id);
  }

  savePeriod(data) {
    return this.postI(env.SERVERNAME + '/period' + this.facturacion, data);
  }
}
