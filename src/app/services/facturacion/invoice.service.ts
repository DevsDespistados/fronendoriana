import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class InvoiceService extends AppService {
  headers: any;
  options: any;

  importFile(formData: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient.post(env.SERVERNAME + this.facturacion, formData, this.options);
  }

  allInvoice(id) {
    return this.httpClient.get(env.SERVERNAME + this.facturacion + '/' + id);
  }

  getInvoiceFormRender(id) {
    return this.httpClient.get(env.SERVERNAME + this.facturacion + '/render/' + id);
  }
}
