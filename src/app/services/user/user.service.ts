import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class UserService extends AppService {
  getUsers() {
    return this.httpClient.get(env.SERVERNAME + this.user);
  }

  addUser(formData: FormData) {
    return this.httpClient.post(env.SERVERNAME + this.user, formData);
  }

  resetPassword(id: number, data: any) {
    const newPassword = {password: data};
    return this.httpClient.put(env.SERVERNAME + this.user + '/resetPassword/' + id, newPassword);
  }

  deleteUser(idUser: number) {
    return this.httpClient.delete(env.SERVERNAME + this.user + '/' + idUser);
  }

  editUser(id, formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.user + '/edit/' + id, formData);
  }

  changeState(id: number, state: number) {
    const newState = {active: state};
    return this.httpClient.put(env.SERVERNAME + this.user + '/' + id, newState);
  }

  getUser(id: number) {
    return this.httpClient.get(env.SERVERNAME + this.user + '/' + id);
  }

  getUserActives() {
    return this.httpClient.get(env.SERVERNAME + this.user + '/active/1');
  }

  getUserDisables() {
    return this.httpClient.get(env.SERVERNAME + this.user + '/active/0');
  }

  searchUsers(any: string) {
    return this.httpClient.get(env.SERVERNAME + this.user + '/search/' + any);
  }

  searchUser(any: string) {
    return this.httpClient.get(env.SERVERNAME + this.user + '/search/' + any);
  }

  getUserPermission(id) {
    return this.httpClient.get(env.SERVERNAME + this.user + '/permission/' + id);
  }

  setPermissions(perm, id) {
    return this.httpClient.put(env.SERVERNAME + this.user + '/permission/' + id, perm);
  }

  isValidPass(data) {
    return this.httpClient.post(env.SERVERNAME + this.user + '/isValid', data);
  }

  setImg(id, data) {
    return this.httpClient.post(env.SERVERNAME + this.user + '/setImg/' + id, data);
  }
}


