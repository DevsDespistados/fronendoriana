import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {User} from '../../model/user/user';
import {env} from '../Env';
import {UserPermissions} from '../../model/user/user-permissions';
import {UserRoutesService} from '../../components/navigation/user-routes.service';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router, private userP: UserRoutesService) {
  }

  check(): boolean {
    return !!sessionStorage.getItem('user');
  }

  login(credentials: { username: string, password: string }) {
    return this.http.post<any>(env.SERVERNAME + '/auth/login', credentials)
      .pipe(map(data => {
        console.log(data);
        sessionStorage.setItem('tokenAuth', data.token);
        sessionStorage.setItem('user', btoa(JSON.stringify(data['user'])));
        sessionStorage.setItem('permission', btoa(JSON.stringify(data['permission'])));
        // setTimeout(o => {
        // this.userP.loadRoutes();
        // }, 1000);
      }));
  }

  logout(): void {
    this.http.post(env.SERVERNAME + '/auth/logout', localStorage.getItem('tokenAuth')).subscribe(
      resp => {
        console.log(resp);
      });
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  getUser(): User {
    return sessionStorage.getItem('user') ? JSON.parse(atob(sessionStorage.getItem('user'))) : null;
  }

  getPermission(): UserPermissions {
    let res = null;
    if (sessionStorage.getItem('permission')) {
      const r = <UserPermissions>JSON.parse(atob(sessionStorage.getItem('permission')));
      res = new UserPermissions(
        Number(r.idUserPermission),
        Number(r.idUser),
        Number(r.p_personal),
        Number(r.p_client),
        Number(r.p_provider),
        Number(r.p_transit),
        Number(r.p_permanent),
        Number(r.p_eventual),
        Number(r.p_otherCompany),
        Number(r.o_ownerEquipment),
        Number(r.p_project),
        Number(r.pj_project),
        Number(r.pj_operation),
        Number(r.pj_tracing),
        Number(r.pj_modifications),
        Number(r.pj_forms),
        Number(r.contracts),
        Number(r.contracts_item),
        Number(r.f_forms),
        Number(r.f_provider),
        Number(r.f_provider_plan),
        Number(r.f_permanent),
        Number(r.f_permanent_plan),
        Number(r.f_eventual),
        Number(r.f_eventual_plan),
        Number(r.f_ownerEquipment),
        Number(r.f_ownerEquipment_plan),
        Number(r.f_projects),
        Number(r.ac_accounts),
        Number(r.ac_b_banks),
        Number(r.ac_b_periods),
        Number(r.ac_b_transactions),
        Number(r.ac_check),
        Number(r.ac_pto_pettyCashOffice),
        Number(r.ac_pto_period),
        Number(r.ac_pto_transaction),
        Number(r.ac_pt_pettyCash),
        Number(r.ac_pt_periods),
        Number(r.ac_pt_transactions),
        Number(r.ac_pj_projects),
        Number(r.ti_tickets),
        Number(r.ti_financial),
        Number(r.ti_warranties),
        Number(r.ti_projects),
        Number(r.l_letters),
        Number(r.l_s_Letters),
        Number(r.l_ss_Letters),
        Number(r.l_r_Letters),
        Number(r.l_rs_Letters),
        Number(r.l_p_Letters),
        Number(r.o_order),
        Number(r.o_order_item),
        Number(r.o_order_p),
        Number(r.o_order_p_item),
        Number(r.o_order_q),
        Number(r.o_order_q_item),
        Number(r.o_order_t),
        Number(r.f_fac),
        Number(r.f_fac_period),
        Number(r.f_fac_check),
        Number(r.f_fac_invoice),
        Number(r.f_fac_render),
        Number(r.u_users));
      return res;
    } else {
      return null;
    }
  }

  refreshPermission() {
    this.http.get(env.SERVERNAME + '/user/permission/' + this.getUser().id).subscribe(
      res => {
        sessionStorage.setItem('permission', btoa(JSON.stringify(res['perm'])));
      });
  }

  setUser(): Promise<boolean> {
    return this.http.get<any>(env.SERVERNAME + '/refresh').toPromise()
      .then(data => {
        if (data.user) {
          sessionStorage.setItem('user', btoa(JSON.stringify(data.user)));
          // localStorage.setItem('user',  btoa(JSON.stringify(data.user)));
          return true;
        }
        return false;
      });
  }
}
