import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Session} from '../../model/user/session';
import {User} from '../../model/user/user';

@Injectable()
export class StorageService {

  private localStorageService;
  private currentSession: Session = null;

  constructor(private router: Router) {
    this.localStorageService = sessionStorage;
    this.currentSession = this.loadSessionData();
  }

  setCurrentSession(session: Session): void {
    this.currentSession = session;
    this.localStorageService
      .setItem('current_user', JSON.stringify(session));
  }

  loadSessionData(): Session {
    const sessionStr = this.localStorageService.getItem('current_user');
    return (sessionStr) ? <Session>JSON.parse(sessionStr) : null;
  }

  getCurrentSession(): Session {
    return this.currentSession;
  }

  removeCurrentSession(): void {
    this.localStorageService.removeItem('current_user');
    this.currentSession = null;
  }

  getCurrentUser(): User {
    const session: Session = this.getCurrentSession();
    return (session && session.user) ? session.user : null;
  }

  isAuthenticated(): boolean {
    return this.getCurrentSession() != null ? true : false;
  }

  getCurrentToken(): string {
    const session = this.getCurrentSession();
    return (session && session.token) ? session.token : null;
  }

  setStorage(key, data): void {
    sessionStorage.setItem(key, JSON.stringify(data));
  }

  getStorage(key) {
    return JSON.parse(sessionStorage.getItem(key));
  }
}
