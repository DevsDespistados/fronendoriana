import {Injectable} from '@angular/core';
// import {ViewProviderFormItem} from '../../components/back/model/form/view-provider-form-item';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

declare var $: any;

@Injectable()
export class ProviderFormService extends AppService {

  getProviderForms(idProvider, idContract, type) {
    return this.get(env.SERVERNAME + this.providerForm + '/allOrderProviderForm/' +
      idProvider + '/' + idContract + '/periodProviderForm/' + type);
  }

  getProviderFormAccounts(idProvider, idContract, idProject, idBank, idPeriod, idCheck) {
    return this.get(env.SERVERNAME + this.providerForm + '/providerFormAccounts/' +
      idProvider + '/' + idContract + '/' + idProject + '/' + idBank + '/' + idPeriod + '/' + idCheck);
  }

  addProviderForm(formData: any, idContract) {
    return this.postI(env.SERVERNAME + this.providerForm + '/' + idContract, formData);
  }

  addDataItem(formData: any) {
    return this.put(env.SERVERNAME + this.providerForm + '/addDate/items', formData);
  }

  updateState(id, formData: any) {
    return this.put(env.SERVERNAME + this.providerForm + '/' + id, formData);
  }

  editForm(id, formData: any) {
    return this.put(env.SERVERNAME + this.providerForm + '/edit/' + id, formData);
  }

  getContractAdvance(idProvider): any {
    return this.get(env.SERVERNAME + this.providerForm + '/allOrderProviderForm/' +
      idProvider + '/periodProviderForm');
  }

  getContractDiscount(idContract) {
    return this.get(env.SERVERNAME + this.providerForm + '/item/' + idContract + '/all/0');
  }

  getItemsForm(type, idProviderForm) {
    return this.get(env.SERVERNAME + this.providerForm + '/getProviderFormItem/' + type + '/' + idProviderForm);
  }

  deleteProviderForm(id) {
    return this.delete(env.SERVERNAME + this.providerForm + '/' + id);
  }

  searchProviderForm(text) {
    return this.get(env.SERVERNAME + this.providerForm + '/search/' + text);
  }

  returnAdvance(id) {
    return null;
  }

  getPayProviderForm(id) {
    return this.get(env.SERVERNAME + this.providerForm + '/getPeriodsPayForm/' + id);
  }

  getPartPayProviderForm(id) {
    return this.get(env.SERVERNAME + this.providerForm + '/getPartPayProvideForm/' + id);
  }

  payPartProviderForm(data, id) {
    return this.postI(env.SERVERNAME + this.providerForm + '/payPartPayProvideForm/' + id, data);
  }

  // forms items
  getProviderFormAdvance(idProvider) {
    return this.httpClient.get(env.SERVERNAME + this.providerForm + '/getAdvance/' + idProvider + '/1');
  }

  getProviderFormDiscount(idProvider) {
    return this.httpClient.get(env.SERVERNAME + this.providerForm + '/getAdvance/' + idProvider + '/0');
  }
}
