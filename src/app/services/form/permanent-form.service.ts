import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class PermanentFormService extends AppService {
  getPermanentForms(state) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/getAll/periodicalPermanentForm/' + state);
  }

  getPermanentFormAccounts(idBank, idPeriod, idCheck) {
    return this.get(env.SERVERNAME + this.permanentForm + '/permanentFormAccounts/' + idBank + '/' + idPeriod + '/' + idCheck);
  }

  addPermanentForm(formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.permanentForm + '/createForm', formData);
  }

  editPermanentForm(id, formData: any) {
    return this.httpClient
      .put(env.SERVERNAME + this.permanentForm + '/edit/' + id, formData);
  }

  getViewConsolidate(idForm) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/getPermanentInFormView/namePermanent/' +
      idForm);
  }

  getViewPermanentOnProjects(idPermanent, idForm) {
    return this.get(env.SERVERNAME + this.permanentForm + '/getPermanentForm/' + idPermanent + '/' + idForm);
  }

  getPermanentFormForProjects(id: number) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/getFormsForProjects/' + id);
  }


  getAllProjectOfPermanentForm(id) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/allProjectOfPermanentForm/' + id);
  }

  deletePermanentForm(id) {
    return this.httpClient.delete(env.SERVERNAME + this.permanentForm + '/delete/' + id);
  }

  saveFormWorkPermanent(form) {
    return this.httpClient.put(env.SERVERNAME + this.permanentForm + '/savePermanentWorkForm', form);
  }

  saveTotalForms(id, form) {
    return this.httpClient
      .put(env.SERVERNAME + this.permanentForm + '/saveTotalForm/' + id, form);
  }

  saveDetail(id, data) {
    return this.httpClient
      .put(env.SERVERNAME + this.permanentForm + '/saveDetails/' + id, data);
  }

  getDetail(id, idForm) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/getDetails/' + id + '/' + idForm);
  }

  setState(id, state) {
    return this.httpClient
      .put(env.SERVERNAME + this.permanentForm + '/setState/' + id, state);
  }

  search(crit) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/search/' + crit);
  }

  searchPermanent(id, crit) {
    return this.httpClient.get(env.SERVERNAME + this.permanentForm + '/searchPermanent/namePermanent/' + id + '/' + crit);
  }

  deletePermenentOfForm(idPermanent, idForm) {
    return this.httpClient.delete(env.SERVERNAME + this.permanentForm + '/' + idPermanent + '/' + idForm);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.permanentForm + '/report/permanentForm', data);
  }

}
