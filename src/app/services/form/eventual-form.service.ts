import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class EventualFormService extends AppService {
  getEventualForms(type) {
    return this.get(env.SERVERNAME + this.eventualForm + '/getAll/biweekly/' + type);
  }

  getEventualFormAccounts(idBank, idPeriod, idCheck) {
    return this.get(env.SERVERNAME + this.eventualForm + '/eventualFormAccounts/' + idBank + '/' + idPeriod + '/' + idCheck);
  }

  addEventualForm(formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.eventualForm + '/createForm', formData);
  }

  editEventualForm(id, formData: any) {
    return this.httpClient.put(env.SERVERNAME + this.eventualForm + '/edit/' + id, formData);
  }

  getViewConsolidate(idForm) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/getEventualInFormView/nameEventual/' +
      idForm);
  }

  getViewEventualOnProjects(idEventual, idForm) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/getEventualForm/' + idEventual + '/' + idForm);
  }

  deleteEventualForm(id) {
    return this.httpClient
      .delete(env.SERVERNAME + this.eventualForm + '/delete/' + id);
  }

  deleteEventualOfForm(idEventual, idForm) {
    return this.httpClient.delete(env.SERVERNAME + this.eventualForm + '/' + idEventual + '/' + idForm);
  }

  saveFormWorkEventual(form) {
    return this.httpClient
      .put(env.SERVERNAME + this.eventualForm + '/saveEventualWorkForm', form);
  }

  saveTotalForms(id, form) {
    return this.httpClient.put(env.SERVERNAME + this.eventualForm + '/saveTotalForm/' + id, form);
  }

  saveDetail(id, data) {
    return this.httpClient.put(env.SERVERNAME + this.eventualForm + '/saveDetails/' + id, data);
  }

  getDetail(id, idForm) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/getDetails/' + id + '/' + idForm);
  }

  setState(id, state) {
    return this.httpClient.put(env.SERVERNAME + this.eventualForm + '/setState/' + id, state);
  }

  search(crit) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/search/' + crit);
  }

  searchEventual(id, crit) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/searchEventual/nameEventual/' + id + '/' + crit);
  }

  getEventualFormByProject(id, idP) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/getEventualFormByProject/' + id + '/' + idP);
  }

  getEventualFormForProjects(id: number) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/getFormsForProjects/' + id);
  }

  getAllProjectOfEventualForm(id) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/allProjectOfEventualForm/' + id);
  }

  getEventualsWorkAtProject(idEF, idE) {
    return this.httpClient.get(env.SERVERNAME + this.eventualForm + '/getEventualWorkAtProject/eventual/' + idEF + '/' + idE);
  }

  report(data) {
    return this.httpClient.post(env.SERVERNAME + this.eventualForm + '/report/oneEventualForm', data);
  }

}
