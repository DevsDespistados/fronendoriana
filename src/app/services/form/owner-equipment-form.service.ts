import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class OwnerEquipmentFormService extends AppService {
  getHeavyEquipmentForms(state) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/allOrderOwnerEquipmentForm/periodOwnerEquipmentForm/' + state);
  }

  getOwnerEquipmentFormAccounts(idBank, idPeriod, idCheck) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm +
      '/ownerEquipmentFormAccount/' + idBank + '/' + idPeriod + '/' + idCheck);
  }

  allProjectOfOwnerEquipmentForm(id) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/allProjectOfOwnerEquipmentForm/' + id);
  }

  addHeavyEquipmentForm(formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.ownerEquipmentForm, formData);
  }

  editHeavyEquipmentForm(id, formData: any) {
    return this.httpClient.put(env.SERVERNAME + this.ownerEquipmentForm + '/edit/' + id, formData);
  }

  getViewConsolidate(idForm) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/getViewOwnerEquipmentForm/' + idForm);
  }

  getViewHeavyEquipmentOnProjects(idHeavyEquipment, idForm) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/getProject/' + idForm + '/' + idHeavyEquipment);
  }

  deleteHeavyEquipmentForm(id) {
    return this.httpClient.delete(env.SERVERNAME + this.ownerEquipmentForm + '/' + id);
  }

  deleteHeavyEquipmentOnForm(idOwE, idForm) {
    return this.httpClient.delete(env.SERVERNAME + this.ownerEquipmentForm + '/' + idOwE + '/' + idForm);
  }

  saveFormWorkHeavyEquipment(form) {
    return this.httpClient.put(env.SERVERNAME + this.ownerEquipmentForm + '/update/dateForm', form);
  }

  saveTotalForms(id, form) {
    return this.httpClient.put(env.SERVERNAME + this.ownerEquipmentForm + '/addDateForm/' + id, form);
  }

  saveDetail(id, data) {
    return this.httpClient.put(env.SERVERNAME + this.ownerEquipmentForm + '/saveDetails/' + id, data);
  }

  getDetail(id, idForm) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/getDetails/' + id + '/' + idForm);
  }

  setState(id, state) {
    return this.httpClient.put(env.SERVERNAME + this.ownerEquipmentForm + '/' + id, state);
  }

  search(crit) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/search/' + crit);
  }

  searchOwnerEquipment(id, crit) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/searchEquipment/' + id + '/' + crit);
  }

  getEquipmentFormForProjects(id) {
    return this.httpClient.get(env.SERVERNAME + this.ownerEquipmentForm + '/getEquipmentFormForProjects/' + id);
  }

  addListForm(data, idPeriod) {
    return this.httpClient.post(env.SERVERNAME + this.ownerEquipmentForm + '/addListForm/' + idPeriod, data);
  }
}
