import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {ENCODE_DATA_QUERY} from '../common/common';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  url = environment.SERVERNAME;

  tracingLetter(idLetter) {
    window.open(`${this.url}&p_ID_LETTER=${idLetter}&id=13006`, '_blank');
  }

  printReport(report: string, params: {[key: string]: any}) {
    window.open(`${this.url}/report?report=${report}&ext=pdf&${ENCODE_DATA_QUERY(params)}`, '_blank');
  }

}
