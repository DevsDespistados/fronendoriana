import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {RequestOptions} from '@angular/http';
import {env} from '../Env';

@Injectable()
export class TicketFinancialService extends AppService {
  headers: any;
  options: any;

  addTicket(form: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient.post(env.SERVERNAME + this.ticket, form, this.options);
  }

  editTicket(idTicket, form: FormData) {
    this.headers = new Headers();
    this.headers.append('enctype', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    return this.httpClient.post(env.SERVERNAME + this.ticket + /update/ + idTicket, form, this.options);
  }

  updateStateTicket(idTicket: number, data) {
    return this.httpClient.put(env.SERVERNAME + this.ticket + `/${idTicket}`, data);
  }

  deleteTicket(id: number) {
    return this.httpClient.delete(env.SERVERNAME + this.ticket + '/delete/' + id);
  }

  searchTicket(any) {
    return this.httpClient.get(env.SERVERNAME + this.ticket + '/search/' + any);
  }

  getFilterTicket(type: string) {
    return this.httpClient.get(env.SERVERNAME + this.ticket + '/filter/by/type/' + type);
  }

  updateStateProjectForTicket(idProject: number, state: any) {
    const data = {activeProjectForTicket: state};
    return this.httpClient.put(env.SERVERNAME + this.ticket + '/projects/updateState/' + idProject, data);
  }

  filterProjectOfTicket(type: number) {
    return this.httpClient.get(env.SERVERNAME + this.ticket + '/projects/filter/' + type);
  }

  searchProjectOfTicket(type: string) {
    return this.httpClient.get(env.SERVERNAME + this.ticket + '/projects/search/' + type);
  }
}
