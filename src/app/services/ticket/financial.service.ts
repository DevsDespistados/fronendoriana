import {Injectable} from '@angular/core';
import {AppService} from '../principalService/app.service';
import {env} from '../Env';

@Injectable()
export class FinancialService extends AppService {
  headers: any;
  options: any;

  getFinancial() {
    return this.httpClient.get(env.SERVERNAME + this.financial + '/all');
  }

  addFinancial(formData: any) {
    console.log('aqui' + formData);
    return this.httpClient.post(env.SERVERNAME + this.financial, formData);
  }

  deleteFinancial(idFinancial: number) {
    return this.httpClient.delete(env.SERVERNAME + this.financial + '/' + idFinancial);
  }

  searchFinancial(nameOrCode: string) {
    return this.httpClient.get(env.SERVERNAME + this.financial + '/search/' + nameOrCode);
  }

  editFinancial(idFinancial, formData: any) {
    return this.httpClient.post(env.SERVERNAME + this.financial + '/' + idFinancial, formData, this.options);
  }

  getFilterFinancial(param: string) {
    return this.httpClient.get(env.SERVERNAME + this.financial + '/filters/state/' + param);
  }

  updateAmount(idFinancial, amountPrevious, amount: number, type: number) {
    const values = {
      amountActual: amount,
      amountPrevious: amountPrevious,
      type: type
    };
    return this.httpClient.put(env.SERVERNAME + this.financial + '/updateAmount/' + idFinancial, values);
  }

  updateState(idFinancial, state: string) {
    console.log('idFinancial put' + idFinancial);
    const value = {stateFinancial: state};
    return this.httpClient.put(env.SERVERNAME + this.financial + '/' + idFinancial, value);
  }

  filterFinancial(param: string) {
    return this.httpClient.get(env.SERVERNAME + this.financial + '/filters/state/' + param);
  }
}
