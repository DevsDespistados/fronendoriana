import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './material/material.module';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {OrianaToolbarComponent} from './components/navigation/oriana-toolbar/oriana-toolbar.component';
import {OrianaSidevarComponent} from './components/navigation/oriana-sidevar/oriana-sidevar.component';
import {OrianaContentComponent} from './components/navigation/oriana-content/oriana-content.component';
import {ToggleService} from './toggle.service';
import {OrianaHomePersonalComponent} from './components/personal/oriana-home-personal/oriana-home-personal.component';
import {OrianaClientComponent} from './components/personal/oriana-client/oriana-client.component';
import {OrianaTableToolbarComponent} from './components/navigation/oriana-table-toolbar/oriana-table-toolbar.component';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import {ModalComponent} from './components/personal/modal-client/modal.component';
import {ClientService} from './services/personal/client.service';
import {HttpModule} from '@angular/http';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {OrianaLoaderComponent} from './components/navigation/oriana-loader/oriana-loader.component';
import {ModalDeleteComponent} from './components/navigation/modal-delete/modal-delete.component';
import {SnackSuccessComponent} from './components/snack-msg/snack-success/snack-success.component';
import {SnackErrorComponent} from './components/snack-msg/snack-error/snack-error.component';
import {SnackAlertComponent} from './components/snack-msg/snack-alert/snack-alert.component';
import {SnackDeleteComponent} from './components/snack-msg/snack-delete/snack-delete.component';
import {SnackUpdateComponent} from './components/snack-msg/snack-update/snack-update.component';
import {ButtonBackComponent} from './components/navigation/button-back/button-back.component';
import {ViewPrintComponent} from './components/navigation/view-print/view-print.component';
import {PrintServiceService} from './services/print-service.service';
import {OrianaCheckboxComponent} from './components/navigation/oriana-checkbox/oriana-checkbox.component';
import {OrianaProviderComponent} from './components/personal/oriana-provider/oriana-provider.component';
import {ProviderService} from './services/personal/provider.service';
import {ModalProviderComponent} from './components/personal/modal-provider/modal-provider.component';
import {OrianaTransitComponent} from './components/personal/oriana-transit/oriana-transit.component';
import {ModalTransitComponent} from './components/personal/modal-transit/modal-transit.component';
import {TransitService} from './services/personal/transit.service';
import {OrianaPermanentComponent} from './components/personal/oriana-permanent/oriana-permanent.component';
import {ModalPermanentComponent} from './components/personal/modal-permanent/modal-permanent.component';
import {PermanentService} from './services/personal/permanent.service';
import {MoneyPipe} from './pipes/money.pipe';
import {OrianaEventualComponent} from './components/personal/oriana-eventual/oriana-eventual.component';
import {ModalEventualComponent} from './components/personal/modal-eventual/modal-eventual.component';
import {EventualService} from './services/personal/eventual.service';
import {OrianaCompanyComponent} from './components/personal/oriana-company/oriana-company.component';
import {ModalCompanyComponent} from './components/personal/modal-company/modal-company.component';
import {OtherCompanyService} from './services/personal/other-company.service';
import {OrianaEquipmentComponent} from './components/equipment/oriana-equipment/oriana-equipment.component';
import {OrianaButtonModulesComponent} from './components/navigation/oriana-button-modules/oriana-button-modules.component';
import {OrianaHomeEquipmentComponent} from './components/equipment/oriana-home-equipment/oriana-home-equipment.component';
import {ModalEquipmentComponent} from './components/equipment/modal-equipment/modal-equipment.component';
import {OwnerEquipmentService} from './services/ownerMahine/owner-equipment.service';
import {OrianaHomeProjectComponent} from './components/projects/oriana-home-project/oriana-home-project.component';
import {OrianaProjectsComponent} from './components/projects/oriana-projects/oriana-projects.component';
import {ModalProjectComponent} from './components/projects/modal-project/modal-project.component';
import {ProjectService} from './services/project/project.service';
import {DateOrianaPipe} from './pipes/date-oriana.pipe';
import {TracingProjectComponent} from './components/projects/tracing-project/tracing-project.component';
import {FormsProjectComponent} from './components/projects/forms-project/forms-project.component';
import {ModificationsProjectComponent} from './components/projects/modifications-project/modifications-project.component';
import {OperationsHomeProjectComponent} from './components/projects/operations-home-project/operations-home-project.component';
import {ModalModificationComponent} from './components/projects/modal-modification/modal-modification.component';
import {ProjectMdfService} from './services/project/project-mdf.service';
import {DATE_CONSTANTS} from './model/date-constants';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {ToolbarOrianaProjectOperationComponent} from './components/projects/toolbar-oriana-project-operation/toolbar-oriana-project-operation.component';
import {ModalFormComponent} from './components/projects/modal-form/modal-form.component';
import {ProjectSpreadsheetService} from './services/project/project-spreadsheet.service';
import {ModalTracingComponent} from './components/projects/modal-tracing/modal-tracing.component';
import {ShortDatePipe} from './pipes/short-date.pipe';
import {OrianaHomeContractsComponent} from './components/contracts/oriana-home-contracts/oriana-home-contracts.component';
import {OrianaContractsComponent} from './components/contracts/oriana-contracts/oriana-contracts.component';
import {ModalContractsComponent} from './components/contracts/modal-contracts/modal-contracts.component';
import {OrianaCheckboxNormalComponent} from './components/navigation/oriana-checkbox-normal/oriana-checkbox-normal.component';
import {ContractProviderService} from './services/contract/contract-provider.service';
import {OrianaHomeItemsContractsComponent} from './components/contracts/oriana-home-items-contracts/oriana-home-items-contracts.component';
import {OrianaItemsContractsComponent} from './components/contracts/oriana-items-contracts/oriana-items-contracts.component';
import {ModalContractItemComponent} from './components/contracts/modal-contract-item/modal-contract-item.component';
import {ContractItemService} from './services/contract/contract-item.service';
import {OrianaBackButtonComponent} from './components/navigation/oriana-back-button/oriana-back-button.component';
import {OrianaHomeFormsComponent} from './components/forms/oriana-home-forms/oriana-home-forms.component';
import {OrianaFormsContractsComponent} from './components/forms/provider/oriana-forms-contracts/oriana-forms-contracts.component';
import {ProviderFormsComponent} from './components/forms/provider/provider-forms/provider-forms.component';
import {ModalProviderFormComponent} from './components/forms/provider/modal-provider-form/modal-provider-form.component';
import {ProviderFormService} from './services/form/provider-form.service';
import {TransactionService} from './services/accounts/transaction.service';
// tslint:disable-next-line:max-line-length
import {OrianaHomeProviderFormsComponent} from './components/forms/provider/oriana-home-provider-forms/oriana-home-provider-forms.component';
import {ProviderFormPlanerComponent} from './components/forms/provider/provider-form-planer/provider-form-planer.component';
import {OrianaHomeProviderFormPlanerComponent} from './components/forms/provider/oriana-home-provider-form-planer/oriana-home-provider-form-planer.component';
import {ProviderFormAnticipationComponent} from './components/forms/provider/provider-form-anticipation/provider-form-anticipation.component';
import {PermanentFormsComponent} from './components/forms/permanent/permanent-forms/permanent-forms.component';
import {PermanentFormService} from './services/form/permanent-form.service';
import {ModalPermanentFormComponent} from './components/forms/permanent/modal-permanent-form/modal-permanent-form.component';
import {PermanentFormPlanerComponent} from './components/forms/permanent/permanent-form-planer/permanent-form-planer.component';
import {OrianaHomePermanentFormPlanerComponent} from './components/forms/permanent/oriana-home-permanent-form-planer/oriana-home-permanent-form-planer.component';
import {OrianaHomePermanenteFormConsolidateComponent} from './components/forms/permanent/oriana-home-permanente-form-consolidate/oriana-home-permanente-form-consolidate.component';
import {PermanenteFormConsolidateComponent} from './components/forms/permanent/permanente-form-consolidate/permanente-form-consolidate.component';
import {ModalPermanentAdvancesComponent} from './components/forms/permanent/modal-permanent-advances/modal-permanent-advances.component';
import {SnackEmptyComponent} from './components/snack-msg/snack-empty/snack-empty.component';
import {ModalDescriptionFormComponent} from './components/forms/modal-description-form/modal-description-form.component';
import {EventualFormsComponent} from './components/forms/eventual/eventual-forms/eventual-forms.component';
import {PeriodEventualPipe} from './pipes/period-eventual.pipe';
import {EventualFormService} from './services/form/eventual-form.service';
import {ModalEventualFormComponent} from './components/forms/eventual/modal-eventual-form/modal-eventual-form.component';
import {EventualFormConsolidateComponent} from './components/forms/eventual/eventual-form-consolidate/eventual-form-consolidate.component';
import {OrianaHomeEventualFormConsolidateComponent} from './components/forms/eventual/oriana-home-eventual-form-consolidate/oriana-home-eventual-form-consolidate.component';
// tslint:disable-next-line:max-line-length
import {OrianaHomeEvetualPlanerComponent} from './components/forms/eventual/oriana-home-evetual-planer/oriana-home-evetual-planer.component';
import {EvetualFormPlanerComponent} from './components/forms/eventual/evetual-form-planer/evetual-form-planer.component';
import {ModalEventualAdvancesComponent} from './components/forms/eventual/modal-eventual-advances/modal-eventual-advances.component';
import {EquipmentFormsComponent} from './components/forms/equipment/equipment-forms/equipment-forms.component';
import {ModalEquipmentFormComponent} from './components/forms/equipment/modal-equipment-form/modal-equipment-form.component';
import {OwnerEquipmentFormService} from './services/form/owner-equipment-form.service';
import {OrianaHomeEquipmentFormConsolidateComponent} from './components/forms/equipment/oriana-home-equipment-form-consolidate/oriana-home-equipment-form-consolidate.component';
import {EquipmentFormConsolidateComponent} from './components/forms/equipment/equipment-form-consolidate/equipment-form-consolidate.component';
import {OrianaHomeEquipmentFormPlanerComponent} from './components/forms/equipment/oriana-home-equipment-form-planer/oriana-home-equipment-form-planer.component';
import {EquipmentFormPlanerComponent} from './components/forms/equipment/equipment-form-planer/equipment-form-planer.component';
import {ModalEquipmentAdvancesComponent} from './components/forms/equipment/modal-equipment-advances/modal-equipment-advances.component';
import {ProjectForFormsComponent} from './components/forms/project-for-forms/project-for-forms.component';
import {OrianaHomeUsersComponent} from './components/users/oriana-home-users/oriana-home-users.component';
import {OrianaLoginComponent} from './components/users/oriana-login/oriana-login.component';
import {AuthenticationService} from './services/user/authentication.service';
import {AppService} from './services/principalService/app.service';
import {LoginGuard} from './guards/login.guard';
import {OrianaHomeAccountsComponent} from './components/accounts/oriana-home-accounts/oriana-home-accounts.component';
import {AccountBanksComponent} from './components/accounts/bank/account-banks/account-banks.component';
import {ModalBankComponent} from './components/accounts/bank/modal-bank/modal-bank.component';
import {AccountService} from './services/accounts/account.service';
import {OrianaHomeBankPeriodComponent} from './components/accounts/bankPeriod/oriana-home-bank-period/oriana-home-bank-period.component';
import {BankPeriodComponent} from './components/accounts/bankPeriod/bank-period/bank-period.component';
import {ModalBankPeriodComponent} from './components/accounts/bankPeriod/modal-bank-period/modal-bank-period.component';
import {PeriodService} from './services/accounts/period.service';
import {BankCheckComponent} from './components/accounts/check/bank-check/bank-check.component';
import {ModalCheckComponent} from './components/accounts/check/modal-check/modal-check.component';
import {OrianaHomeCheckTransacctionComponent} from './components/accounts/oriana-home-check-transacction/oriana-home-check-transacction.component';
import {PayCheckService} from './services/accounts/pay-check.service';
import {TransactionsCheckComponent} from './components/accounts/transactions/transactions-check/transactions-check.component';
import {OrianaHomeTransactionComponent} from './components/accounts/transactions/oriana-home-transaction/oriana-home-transaction.component';
import {ModalClientIncomeComponent} from './components/accounts/transactions/modalsTransactions/clientIncome/modal-client-income/modal-client-income.component';
import {ToolbarTransactionIncomesComponent} from './components/accounts/transactions/toolbar-transaction-incomes/toolbar-transaction-incomes.component';
import {ClientIncomeComponent} from './components/accounts/transactions/modalsTransactions/clientIncome/client-income/client-income.component';
import {PartFormsProjectComponent} from './components/accounts/transactions/modalsTransactions/part-forms-project/part-forms-project.component';
import {ClientSelectComponent} from './components/accounts/transactions/modalsTransactions/clientIncome/client-select/client-select.component';
import {ClientFormsComponent} from './components/accounts/transactions/modalsTransactions/clientIncome/client-forms/client-forms.component';
import {DirectIncomeComponent} from './components/accounts/transactions/modalsTransactions/direct-income/direct-income.component';
import {CompanyIncomeComponent} from './components/accounts/transactions/modalsTransactions/company-income/company-income.component';
import {DirectExpenseComponent} from './components/accounts/transactions/modalsTransactions/direct-expense/direct-expense.component';
import {CategoryService} from './services/accounts/category.service';
import {ProviderAdvanceExpenseComponent} from './components/accounts/transactions/modalsTransactions/provider-advance-expense/provider-advance-expense.component';
import {PermanentAdvanceExpenseComponent} from './components/accounts/transactions/modalsTransactions/permanent-advance-expense/permanent-advance-expense.component';
import {EventualAdvanceExpenseComponent} from './components/accounts/transactions/modalsTransactions/eventual-advance-expense/eventual-advance-expense.component';
import {EquipmentAdvanceExpenseComponent} from './components/accounts/transactions/modalsTransactions/equipment-advance-expense/equipment-advance-expense.component';
import {CompanyExpenseComponent} from './components/accounts/transactions/modalsTransactions/company-expense/company-expense.component';
import {CotractSelectComponent} from './components/accounts/transactions/modalsTransactions/providerFormExpense/cotract-select/cotract-select.component';
import {ProviderFormSelectComponent} from './components/accounts/transactions/modalsTransactions/providerFormExpense/provider-form-select/provider-form-select.component';
import {ProviderFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/providerFormExpense/provider-form-advance/provider-form-advance.component';
import {ModalProviderFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/providerFormExpense/modal-provider-form-advance/modal-provider-form-advance.component';
import {ModalEventualFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/eventualFormExpense/modal-eventual-form-advance/modal-eventual-form-advance.component';
import {EventualFormSelectComponent} from './components/accounts/transactions/modalsTransactions/eventualFormExpense/eventual-form-select/eventual-form-select.component';
import {EventualFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/eventualFormExpense/eventual-form-advance/eventual-form-advance.component';
import {ModalPermanentFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/permanentFormExpense/modal-permanent-form-advance/modal-permanent-form-advance.component';
import {PermanentFormSelectComponent} from './components/accounts/transactions/modalsTransactions/permanentFormExpense/permanent-form-select/permanent-form-select.component';
import {PermanentFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/permanentFormExpense/permanent-form-advance/permanent-form-advance.component';
import {ModalEquipmentFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/equipmentFormExpense/modal-equipment-form-advance/modal-equipment-form-advance.component';
import {EquipmentFormAdvanceComponent} from './components/accounts/transactions/modalsTransactions/equipmentFormExpense/equipment-form-advance/equipment-form-advance.component';
import {EquipmentFormSelectComponent} from './components/accounts/transactions/modalsTransactions/equipmentFormExpense/equipment-form-select/equipment-form-select.component';
import {ModalTransferAccountsComponent} from './components/accounts/transactions/modalsTransactions/transerAccount/modal-transfer-accounts/modal-transfer-accounts.component';
import {TransferBankComponent} from './components/accounts/transactions/modalsTransactions/transerAccount/transfer-bank/transfer-bank.component';
import {TransferPettyCashComponent} from './components/accounts/transactions/modalsTransactions/transerAccount/transfer-petty-cash/transfer-petty-cash.component';
import {TransferPettyCashOfficeComponent} from './components/accounts/transactions/modalsTransactions/transerAccount/transfer-petty-cash-office/transfer-petty-cash-office.component';
import {AllTransactionsComponent} from './components/accounts/transactions/all-transactions/all-transactions.component';
import {ModalPartsFormsDetailsComponent} from './components/accounts/transactions/modalsTransactions/detailsforms/modal-parts-forms-details/modal-parts-forms-details.component';
import {TableProviderFormsComponent} from './components/accounts/transactions/modalsTransactions/detailsforms/table-provider-forms/table-provider-forms.component';
import {TableEventualFormsComponent} from './components/accounts/transactions/modalsTransactions/detailsforms/table-eventual-forms/table-eventual-forms.component';
import {TableProjectsFormsComponent} from './components/accounts/transactions/modalsTransactions/detailsforms/table-projects-forms/table-projects-forms.component';
import {TablePermanentFormsComponent} from './components/accounts/transactions/modalsTransactions/detailsforms/table-permanent-forms/table-permanent-forms.component';
import {PettyCashOfficeComponent} from './components/accounts/pettyCashOffice/petty-cash-office/petty-cash-office.component';
import {ModalPettyCashOfficeComponent} from './components/accounts/pettyCashOffice/modal-petty-cash-office/modal-petty-cash-office.component';
import {OrianaHomePettyCashOfficePeriodComponent} from './components/accounts/pettyCashOfficePeriod/oriana-home-petty-cash-office-period/oriana-home-petty-cash-office-period.component';
import {OrianaHomePettyCashOfficeTransactionComponent} from './components/accounts/oriana-home-petty-cash-office-transaction/oriana-home-petty-cash-office-transaction.component';
import {TransactionsPettyCashOfficeComponent} from './components/accounts/transactions/transactions-petty-cash-office/transactions-petty-cash-office.component';
import {PettyCashAccountsComponent} from './components/accounts/pettyCash/petty-cash-accounts/petty-cash-accounts.component';
import {ModalPettyCashAccountsComponent} from './components/accounts/pettyCash/modal-petty-cash-accounts/modal-petty-cash-accounts.component';
import {UserService} from './services/user/user.service';
import {PettyCashPeriodComponent} from './components/accounts/pettyCashPeriod/petty-cash-period/petty-cash-period.component';
import {OrianaHomePettyCashPeriodComponent} from './components/accounts/pettyCashPeriod/oriana-home-petty-cash-period/oriana-home-petty-cash-period.component';
import {ModalPettyCashPeriodComponent} from './components/accounts/pettyCashPeriod/modal-petty-cash-period/modal-petty-cash-period.component';
import {OrianaHomePettyCashTransactionsComponent} from './components/accounts/pettyCashTransactions/oriana-home-petty-cash-transactions/oriana-home-petty-cash-transactions.component';
import {PettyCashTransactionsComponent} from './components/accounts/pettyCashTransactions/petty-cash-transactions/petty-cash-transactions.component';
import {ModalPettyCashTransactionComponent} from './components/accounts/pettyCashTransactions/modal-petty-cash-transaction/modal-petty-cash-transaction.component';
import {ToolbarTransactionPettyCashComponent} from './components/accounts/pettyCashTransactions/toolbar-transaction-petty-cash/toolbar-transaction-petty-cash.component';
import {ModalPettyCashTransactionFileComponent} from './components/accounts/pettyCashTransactions/modal-petty-cash-transaction-file/modal-petty-cash-transaction-file.component';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {ModalInspectImportFileComponent} from './components/accounts/pettyCashTransactions/modal-inspect-import-file/modal-inspect-import-file.component';
import {ModalTransitExpenseComponent} from './components/accounts/transactions/modalsTransactions/modal-transit-expense/modal-transit-expense.component';
import {ModalTransitIncomeComponent} from './components/accounts/transactions/modalsTransactions/modal-transit-income/modal-transit-income.component';
import {ModalAmortizacionComponent} from './components/accounts/transactions/modalsTransactions/amortizacion/modal-amortizacion/modal-amortizacion.component';
import {TransitSelectComponent} from './components/accounts/transactions/modalsTransactions/amortizacion/transit-select/transit-select.component';
import {TransitForAmortizacionSelectComponent} from './components/accounts/transactions/modalsTransactions/amortizacion/transit-for-amortizacion-select/transit-for-amortizacion-select.component';
import {TransitFormAmortizacionComponent} from './components/accounts/transactions/modalsTransactions/amortizacion/transit-form-amortizacion/transit-form-amortizacion.component';
import {TransferProjectComponent} from './components/accounts/accountProjects/transfer-project/transfer-project.component';
import {ModalTransferProjectComponent} from './components/accounts/accountProjects/modal-transfer-project/modal-transfer-project.component';
import {TransferProjectService} from './services/accounts/transfer-project.service';
import {OrianaHomeReportsComponent} from './components/reports/oriana-home-reports/oriana-home-reports.component';
import {AccountReportsComponent} from './components/reports/account-reports/account-reports.component';
import {TemplateAccontsReportsComponent} from './components/reports/template-acconts-reports/template-acconts-reports.component';
import {ModalReportdiagComponent} from './components/reports/modal-reportdiag/modal-reportdiag.component';
import {ListProyectsReportComponent} from './components/reports/list-proyects-report/list-proyects-report.component';
import {ListPersonalReportComponent} from './components/reports/list-personal-report/list-personal-report.component';
import {ListAccountReportComponent} from './components/reports/list-account-report/list-account-report.component';
import {ListPeriodsComponent} from './components/reports/list-periods/list-periods.component';
import {OrianaModalLoaderComponent} from './components/navigation/oriana-modal-loader/oriana-modal-loader.component';
import {ReportServiceService} from './services/accountsReports/report-service.service';
import {ModalFilterDateComponent} from './components/reports/dialogFilters/modal-filter-date/modal-filter-date.component';
import {ModalFilterAmountComponent} from './components/reports/dialogFilters/modal-filter-amount/modal-filter-amount.component';
import {ModalFilterCategoryComponent} from './components/reports/dialogFilters/modal-filter-category/modal-filter-category.component';
import {ModalPrintGroupComponent} from './components/reports/dialogFilters/modal-print-group/modal-print-group.component';
import {OrianaHomeGuaranteesComponent} from './components/guarantee/oriana-home-guarantees/oriana-home-guarantees.component';
import {CreditLineComponent} from './components/guarantee/credit-line/credit-line.component';
import {FinancialService} from './services/ticket/financial.service';
import {ModalFianacialComponent} from './components/guarantee/modal-fianacial/modal-fianacial.component';
import {TicketFinancialComponent} from './components/guarantee/ticket-financial/ticket-financial.component';
import {TicketFinancialService} from './services/ticket/ticket-financial.service';
import {ModalTickeFinancialComponent} from './components/guarantee/modal-ticke-financial/modal-ticke-financial.component';
import {TicketsProjectsComponent} from './components/guarantee/tickets-projects/tickets-projects.component';
import {OrianaHomeLettersComponent} from './components/letters/oriana-home-letters/oriana-home-letters.component';
import {LettersSendComponent} from './components/letters/letters-send/letters-send.component';
import {LettersReceivedComponent} from './components/letters/letters-received/letters-received.component';
import {LettersProjectsComponent} from './components/letters/letters-projects/letters-projects.component';
import {LettersService} from './services/letters/letters.service';
import {ModalLettersComponent} from './components/letters/modal-letters/modal-letters.component';
import {ModalTracingLetterComponent} from './components/letters/modal-tracing-letter/modal-tracing-letter.component';
import {TracingLetterComponent} from './components/letters/tracing-letter/tracing-letter.component';
import {OrianaHomeTracingLettersComponent} from './components/letters/oriana-home-tracing-letters/oriana-home-tracing-letters.component';
import {ModalLetterProjectComponent} from './components/letters/modal-letter-project/modal-letter-project.component';
import {OrianaHomeOrderComponent} from './components/order/oriana-home-order/oriana-home-order.component';
import {OrderItemsComponent} from './components/order/order-items/order-items.component';
import {ItemService} from './services/order/item.service';
import {ModalOrderItemComponent} from './components/order/modal-order-item/modal-order-item.component';
import {OrderPetitionComponent} from './components/order/order-petition/order-petition.component';
import {OrderService} from './services/order/order.service';
import {ModalOrderPetitionComponent} from './components/order/modal-order-petition/modal-order-petition.component';
import {OrderItemPetitionComponent} from './components/order/order-item-petition/order-item-petition.component';
import {ModalOrderPetitionItemComponent} from './components/order/modal-order-petition-item/modal-order-petition-item.component';
import {HomeOrderPetitionItemsComponent} from './components/order/home-order-petition-items/home-order-petition-items.component';
import {OrderItemService} from './services/order/order-item.service';
import {OrderQuotationComponent} from './components/order/order-quotation/order-quotation.component';
import {OrderQuotationItemsComponent} from './components/order/order-quotation-items/order-quotation-items.component';
import {HomeOrderQuotationItemComponent} from './components/order/home-order-quotation-item/home-order-quotation-item.component';
import {HomeOrderQuotationBuyComponent} from './components/order/home-order-quotation-buy/home-order-quotation-buy.component';
import {OrderBuyComponent} from './components/order/order-buy/order-buy.component';
import {OrderTracingService} from './services/order/order-tracing.service';
import {ModalOrderBuyComponent} from './components/order/modal-order-buy/modal-order-buy.component';
import {HomeOrderPetitionTracingComponent} from './components/order/home-order-petition-tracing/home-order-petition-tracing.component';
import {OrianaHomeInvoicesComponent} from './components/facturacion/oriana-home-invoices/oriana-home-invoices.component';
import {HomePeriodBillingComponent} from './components/facturacion/home-period-billing/home-period-billing.component';
import {PeriodBillingComponent} from './components/facturacion/period-billing/period-billing.component';
import {ModalPeriodBillingComponent} from './components/facturacion/modal-period-billing/modal-period-billing.component';
import {InvoiceService} from './services/facturacion/invoice.service';
import {PeriodInvoiceService} from './services/facturacion/period-invoice.service';
import {OrianaChecksComponent} from './components/facturacion/oriana-checks/oriana-checks.component';
import {OrianaInvoiceComponent} from './components/facturacion/oriana-invoice/oriana-invoice.component';
import {OrianaRenderComponent} from './components/facturacion/oriana-render/oriana-render.component';
import {ModalRenderCheckComponent} from './components/facturacion/modal-render-check/modal-render-check.component';
import {ModalRenderPettyCashInvoiceComponent} from './components/facturacion/modal-render-petty-cash-invoice/modal-render-petty-cash-invoice.component';
import {ModalRenderPettyCashCheckComponent} from './components/facturacion/modal-render-petty-cash-check/modal-render-petty-cash-check.component';
import {ModalViewRenderCheckComponent} from './components/facturacion/modal-view-render-check/modal-view-render-check.component';
import {ModalViewRenderPettyCashCheckComponent} from './components/facturacion/modal-view-render-petty-cash-check/modal-view-render-petty-cash-check.component';
import {ModalViewRenderPettyCashInvoiceComponent} from './components/facturacion/modal-view-render-petty-cash-invoice/modal-view-render-petty-cash-invoice.component';
import {OrianaUsersComponent} from './components/users/oriana-users/oriana-users.component';
import {ModalUserComponent} from './components/users/modal-user/modal-user.component';
import {HomeUserPermisionComponent} from './components/users/home-user-permision/home-user-permision.component';
import {UserPermissionsComponent} from './components/users/user-permissions/user-permissions.component';
import {UserPerfilComponent} from './components/users/user-perfil/user-perfil.component';
import {DialogService} from './services/dialog.service';
import {StorageService} from './services/user/storage.service';
import {UserRoutesService} from './components/navigation/user-routes.service';
import {AuthInterceptorService} from './services/auth-interceptor.service';
import {ChartjsModule} from '@ctrl/ngx-chartjs';
import {DashboardService} from './services/dashboard.service';
import { InputSelectDirective } from './directives/input-select.directive';
import { InputFileDirective } from './directives/input-file.directive';
import {FileService} from './services/file.service';
import { ShowFileDirective } from './directives/show-file.directive';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {InputNumberDirective} from './directives/input-number.directive';
import { NgxCurrencyMaskModule } from 'ngx-currency-input-mask';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {LetterUsersProjectService} from './services/letters/letter-users-project.service';
import { ModalLetterUsersComponent } from './components/letters/modal-letter-users/modal-letter-users.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    OrianaToolbarComponent,
    OrianaSidevarComponent,
    OrianaContentComponent,
    OrianaHomePersonalComponent,
    OrianaClientComponent,
    OrianaTableToolbarComponent,
    ModalComponent,
    OrianaLoaderComponent,
    ModalDeleteComponent,
    SnackSuccessComponent,
    SnackErrorComponent,
    SnackAlertComponent,
    SnackDeleteComponent,
    SnackUpdateComponent,
    ButtonBackComponent,
    ViewPrintComponent,
    OrianaCheckboxComponent,
    OrianaProviderComponent,
    ModalProviderComponent,
    OrianaTransitComponent,
    ModalTransitComponent,
    OrianaPermanentComponent,
    ModalPermanentComponent,
    MoneyPipe,
    DateOrianaPipe,
    ShortDatePipe,
    PeriodEventualPipe,
    OrianaEventualComponent,
    ModalEventualComponent,
    OrianaCompanyComponent,
    ModalCompanyComponent,
    OrianaEquipmentComponent,
    OrianaButtonModulesComponent,
    OrianaHomeEquipmentComponent,
    ModalEquipmentComponent,
    OrianaHomeProjectComponent,
    OrianaProjectsComponent,
    ModalProjectComponent,
    TracingProjectComponent,
    FormsProjectComponent,
    ModificationsProjectComponent,
    OperationsHomeProjectComponent,
    ModalModificationComponent,
    ToolbarOrianaProjectOperationComponent,
    ModalFormComponent,
    ModalTracingComponent,
    OrianaHomeContractsComponent,
    OrianaContractsComponent,
    ModalContractsComponent,
    OrianaCheckboxNormalComponent,
    OrianaHomeItemsContractsComponent,
    OrianaItemsContractsComponent,
    ModalContractItemComponent,
    OrianaBackButtonComponent,
    OrianaHomeFormsComponent,
    OrianaFormsContractsComponent,
    ProviderFormsComponent,
    ModalProviderFormComponent,
    OrianaHomeProviderFormsComponent,
    ProviderFormPlanerComponent,
    OrianaHomeProviderFormPlanerComponent,
    ProviderFormAnticipationComponent,
    PermanentFormsComponent,
    ModalPermanentFormComponent,
    PermanentFormPlanerComponent,
    OrianaHomePermanentFormPlanerComponent,
    OrianaHomePermanenteFormConsolidateComponent,
    PermanenteFormConsolidateComponent,
    ModalPermanentAdvancesComponent,
    SnackEmptyComponent,
    ModalDescriptionFormComponent,
    EventualFormsComponent,
    ModalEventualFormComponent,
    EventualFormConsolidateComponent,
    OrianaHomeEventualFormConsolidateComponent,
    OrianaHomeEvetualPlanerComponent,
    EvetualFormPlanerComponent,
    ModalEventualAdvancesComponent,
    EquipmentFormsComponent,
    ModalEquipmentFormComponent,
    OrianaHomeEquipmentFormConsolidateComponent,
    EquipmentFormConsolidateComponent,
    OrianaHomeEquipmentFormPlanerComponent,
    EquipmentFormPlanerComponent,
    ModalEquipmentAdvancesComponent,
    ProjectForFormsComponent,
    OrianaHomeUsersComponent,
    OrianaLoginComponent,
    OrianaHomeAccountsComponent,
    AccountBanksComponent,
    ModalBankComponent,
    OrianaHomeBankPeriodComponent,
    BankPeriodComponent,
    ModalBankPeriodComponent,
    BankCheckComponent,
    ModalCheckComponent,
    OrianaHomeCheckTransacctionComponent,
    TransactionsCheckComponent,
    OrianaHomeTransactionComponent,
    ModalClientIncomeComponent,
    ToolbarTransactionIncomesComponent,
    ClientIncomeComponent,
    PartFormsProjectComponent,
    ClientSelectComponent,
    ClientFormsComponent,
    DirectIncomeComponent,
    CompanyIncomeComponent,
    DirectExpenseComponent,
    ProviderAdvanceExpenseComponent,
    PermanentAdvanceExpenseComponent,
    EventualAdvanceExpenseComponent,
    EquipmentAdvanceExpenseComponent,
    CompanyExpenseComponent,
    CotractSelectComponent,
    ProviderFormSelectComponent,
    ProviderFormAdvanceComponent,
    ModalProviderFormAdvanceComponent,
    ModalEventualFormAdvanceComponent,
    EventualFormSelectComponent,
    EventualFormAdvanceComponent,
    ModalPermanentFormAdvanceComponent,
    PermanentFormSelectComponent,
    PermanentFormAdvanceComponent,
    ModalEquipmentFormAdvanceComponent,
    EquipmentFormAdvanceComponent,
    EquipmentFormSelectComponent,
    ModalTransferAccountsComponent,
    TransferBankComponent,
    TransferPettyCashComponent,
    TransferPettyCashOfficeComponent,
    AllTransactionsComponent,
    ModalPartsFormsDetailsComponent,
    TableProviderFormsComponent,
    TableEventualFormsComponent,
    TableProjectsFormsComponent,
    TablePermanentFormsComponent,
    PettyCashOfficeComponent,
    ModalPettyCashOfficeComponent,
    OrianaHomePettyCashOfficePeriodComponent,
    OrianaHomePettyCashOfficeTransactionComponent,
    TransactionsPettyCashOfficeComponent,
    PettyCashAccountsComponent,
    ModalPettyCashAccountsComponent,
    PettyCashPeriodComponent,
    OrianaHomePettyCashPeriodComponent,
    ModalPettyCashPeriodComponent,
    OrianaHomePettyCashTransactionsComponent,
    PettyCashTransactionsComponent,
    ModalPettyCashTransactionComponent,
    ToolbarTransactionPettyCashComponent,
    ModalPettyCashTransactionFileComponent,
    ModalInspectImportFileComponent,
    ModalTransitExpenseComponent,
    ModalTransitIncomeComponent,
    ModalAmortizacionComponent,
    TransitSelectComponent,
    TransitForAmortizacionSelectComponent,
    TransitFormAmortizacionComponent,
    TransferProjectComponent,
    ModalTransferProjectComponent,
    OrianaHomeReportsComponent,
    AccountReportsComponent,
    TemplateAccontsReportsComponent,
    ModalReportdiagComponent,
    ListProyectsReportComponent,
    ListPersonalReportComponent,
    ListAccountReportComponent,
    ListPeriodsComponent,
    OrianaModalLoaderComponent,
    ModalFilterDateComponent,
    ModalFilterAmountComponent,
    ModalFilterCategoryComponent,
    ModalPrintGroupComponent,
    OrianaHomeGuaranteesComponent,
    CreditLineComponent,
    ModalFianacialComponent,
    TicketFinancialComponent,
    ModalTickeFinancialComponent,
    TicketsProjectsComponent,
    OrianaHomeLettersComponent,
    LettersSendComponent,
    LettersReceivedComponent,
    LettersProjectsComponent,
    ModalLettersComponent,
    ModalTracingLetterComponent,
    TracingLetterComponent,
    OrianaHomeTracingLettersComponent,
    ModalLetterProjectComponent,
    OrianaHomeOrderComponent,
    OrderItemsComponent,
    ModalOrderItemComponent,
    OrderPetitionComponent,
    ModalOrderPetitionComponent,
    OrderItemPetitionComponent,
    ModalOrderPetitionItemComponent,
    HomeOrderPetitionItemsComponent,
    OrderQuotationComponent,
    OrderQuotationItemsComponent,
    HomeOrderQuotationItemComponent,
    HomeOrderQuotationBuyComponent,
    OrderBuyComponent,
    ModalOrderBuyComponent,
    HomeOrderPetitionTracingComponent,
    OrianaHomeInvoicesComponent,
    HomePeriodBillingComponent,
    PeriodBillingComponent,
    ModalPeriodBillingComponent,
    OrianaChecksComponent,
    OrianaInvoiceComponent,
    OrianaRenderComponent,
    ModalRenderCheckComponent,
    ModalRenderPettyCashCheckComponent,
    ModalRenderPettyCashInvoiceComponent,
    ModalViewRenderCheckComponent,
    ModalViewRenderPettyCashCheckComponent,
    ModalViewRenderPettyCashInvoiceComponent,
    OrianaUsersComponent,
    ModalUserComponent,
    HomeUserPermisionComponent,
    UserPermissionsComponent,
    UserPerfilComponent,
    InputSelectDirective,
    InputFileDirective,
    ShowFileDirective,
    InputNumberDirective,
    ModalLetterUsersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    MaterialFileInputModule,
    NgxCurrencyMaskModule,
    HttpClientModule,
    ChartjsModule,
    NgxCurrencyMaskModule,
    NgxCurrencyMaskModule,
    NgxCurrencyMaskModule
  ],
  providers: [

    ToggleService,
    UserRoutesService,
    DialogService,
    ClientService,
    ProviderService,
    TransitService,
    PermanentService,
    EventualService,
    OtherCompanyService,
    OwnerEquipmentService,
    ProjectService,
    ProjectMdfService,
    ProjectSpreadsheetService,
    ContractProviderService,
    ContractItemService,
    ProviderFormService,
    PermanentFormService,
    TransactionService,
    EventualFormService,
    OwnerEquipmentFormService,
    PrintServiceService,
    ReportServiceService,
    AccountService,
    PeriodService,
    PayCheckService,
    AppService,
    CategoryService,
    AuthenticationService,
    TransferProjectService,
    FinancialService,
    TicketFinancialService,
    LettersService,
    UserService,
    ItemService,
    OrderService,
    OrderItemService,
    OrderTracingService,
    InvoiceService,
    PeriodInvoiceService,
    StorageService,
    DashboardService,
    LetterUsersProjectService,
    FileService,
    MoneyPipe,
    DateOrianaPipe,
    LoginGuard,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true, disableClose: true},
    },
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: DATE_CONSTANTS},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}},
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'fill'}},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalComponent,
    ModalProviderComponent,
    ModalTransitComponent,
    ModalPermanentComponent,
    ModalEventualComponent,
    ModalCompanyComponent,
    ModalDeleteComponent,
    ModalEquipmentComponent,
    ModalProjectComponent,
    ModalModificationComponent,
    ModalFormComponent,
    ModalTracingComponent,
    ModalContractsComponent,
    ModalContractItemComponent,
    ModalProviderFormComponent,
    ModalPermanentFormComponent,
    ModalPermanentAdvancesComponent,
    ModalEventualFormComponent,
    ModalEventualAdvancesComponent,
    ModalEquipmentFormComponent,
    ModalEquipmentAdvancesComponent,
    ModalDescriptionFormComponent,
    ModalBankComponent,
    ModalPettyCashOfficeComponent,
    ModalPettyCashAccountsComponent,
    ModalBankPeriodComponent,
    ModalPettyCashPeriodComponent,
    ModalPettyCashTransactionComponent,
    ModalPettyCashTransactionFileComponent,
    ModalInspectImportFileComponent,
    ModalCheckComponent,
    ModalClientIncomeComponent,
    ModalTransitIncomeComponent,
    ModalTransitExpenseComponent,
    ModalReportdiagComponent,
    DirectIncomeComponent,
    ProviderAdvanceExpenseComponent,
    PermanentAdvanceExpenseComponent,
    EventualAdvanceExpenseComponent,
    EquipmentAdvanceExpenseComponent,
    CompanyExpenseComponent,
    CompanyIncomeComponent,
    DirectExpenseComponent,
    ModalProviderFormAdvanceComponent,
    ModalEventualFormAdvanceComponent,
    ModalPermanentFormAdvanceComponent,
    ModalEquipmentFormAdvanceComponent,
    ModalTransferAccountsComponent,
    ModalPartsFormsDetailsComponent,
    ModalAmortizacionComponent,
    ModalTransferProjectComponent,
    SnackSuccessComponent,
    SnackAlertComponent,
    SnackErrorComponent,
    SnackDeleteComponent,
    SnackEmptyComponent,
    SnackUpdateComponent,
    ModalFilterDateComponent,
    ModalFilterAmountComponent,
    ModalFilterCategoryComponent,
    ModalPrintGroupComponent,
    ModalFianacialComponent,
    ModalTickeFinancialComponent,
    ModalLettersComponent,
    ModalTracingLetterComponent,
    ModalLetterProjectComponent,
    ModalOrderItemComponent,
    ModalOrderPetitionComponent,
    ModalOrderPetitionItemComponent,
    ModalOrderBuyComponent,
    ModalPeriodBillingComponent,
    ModalRenderCheckComponent,
    ModalRenderPettyCashCheckComponent,
    ModalRenderPettyCashInvoiceComponent,
    ModalViewRenderCheckComponent,
    ModalViewRenderPettyCashCheckComponent,
    ModalViewRenderPettyCashInvoiceComponent,
    ModalUserComponent,
    UserPerfilComponent,
    ModalLetterUsersComponent,
    AppComponent
  ]
})
export class AppModule {
}
