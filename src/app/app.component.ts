import {Component, OnInit} from '@angular/core';
import {ToggleService} from './toggle.service';

// import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'FRONEND-ORIANA';
  // @ViewChild('sidenav') sidenav: MatSidenav;

  reason = '';
  constructor(public toggle: ToggleService) {
  }

  // close(reason: string) {
  //   this.reason = reason;
  //   this.sidenav.close();
  // }


  ngOnInit(): void {
  }

  getStyle() {
    if (this.toggle.isOpen()) {
      return {
        marginLeft: '318px',
        'transition': 'margin-left .5s',
        visibility: 'visible'
      };
    }
  }

  getStyleMenu() {
    if (this.toggle.isOpen()) {
      return {
        'transform': 'translate3d(0,0,0)',
        'transition': 'transform .5s'
      };
    }
  }
}
