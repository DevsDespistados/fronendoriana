import {Directive, ElementRef, HostListener, OnInit} from '@angular/core';
import {MatInput} from '@angular/material/input';
import {Validators} from '@angular/forms';

@Directive({
  selector: '[appInputNumber]'
})
export class InputNumberDirective implements OnInit {
  constructor(private elementRef: ElementRef,
              private matInput: MatInput) {
    this.elementRef.nativeElement.type = 'text';
    console.log(this.elementRef.nativeElement);
  }

  ngOnInit() {
    this.matInput.ngControl.control.setValidators([Validators.pattern(/^\d+(,\d+)*$/)]);
  }

  @HostListener('keyup', ['$event'])
  parseText($event: KeyboardEvent) {
    console.log($event);
    if ($event.keyCode >= 48  && $event.keyCode <= 57) {
      console.log(this.matInput.ngControl.control.value);

      const value = (this.matInput.ngControl.control.value + '').replace(/[^\d.]/g, '');
      this.matInput.ngControl.control.setValue(value);
    }
    // const value = this.matInput.ngControl.value.replace(/[^\d,]/g, '');

  }
}
