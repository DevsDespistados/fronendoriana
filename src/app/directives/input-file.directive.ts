import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {MatInput} from '@angular/material/input';
import {FileService} from '../services/file.service';

@Directive({
  selector: '[appInputFile]'
})
export class InputFileDirective {
  constructor(private matInput: MatInput,
              private elementRef: ElementRef,
              private fileService: FileService) {
    this.elementRef.nativeElement.style.cursor = 'pointer';
  }

  @HostListener('click')
  onClick() {
    const x = document.createElement('input');
    x.setAttribute('type', 'file');
    x.setAttribute('accept', '*/*');
    x.setAttribute('style', 'display:none');
    x.onchange = (event: any) => {
      const [file] = event.target.files;
      this.fileService.saveFile(file).subscribe((a: any) => {
        if (a?.name) {
          this.matInput.ngControl.control.setValue(a.name);
          x.remove();
        }
      });
    };
    x.click();
  }
}
