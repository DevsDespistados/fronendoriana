import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appInputSelect]'
})
export class InputSelectDirective {

  constructor(private elementRef: ElementRef) {
    this.elementRef.nativeElement.addEventListener('click', (event) => {
      this.elementRef.nativeElement.select();
    });
  }
}
