import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {env} from '../services/Env';
import {MatAnchor} from '@angular/material/button';

@Directive({
  selector: '[appShowFile]'
})
export class ShowFileDirective {
  file: string;
  constructor(private elementRef: ElementRef) {
  }

  @Input() set appShowFile(file) {
    if (file === null) {
      this.elementRef.nativeElement.disabled = true ; // .setAttribute('disabled', 'disabled');
    } else {
      this.file = file;
    }
  }

  @HostListener('click')
  onClick() {
    if (this.file) {
      const url = env.SERVERNAME + '/files/' + this.file;
      window.open(url, '_blank');
    }

  }

}
