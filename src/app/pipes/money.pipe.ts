import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dataMoney'
})
export class MoneyPipe implements PipeTransform {

  transform(value: number): string {
    if (value) {
      return Number(value).toLocaleString('us-US', {minimumFractionDigits: 2, maximumFractionDigits: 2});
    } else {
      return Number(0).toLocaleString('us-US', {minimumFractionDigits: 2, maximumFractionDigits: 2});
    }
  }

}
