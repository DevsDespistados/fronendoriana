import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shortDate'
})
export class ShortDatePipe implements PipeTransform {
  mount = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

  transform(value: any): string {
    if (value) {
      const _value = value.split('-');
      const month = this.mount[Number(_value[1] - 1)];
      const year = _value[0].substr(2, 2);
      return month + '-' + year;
    } else {
      return '';
    }

  }

}
