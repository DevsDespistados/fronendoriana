import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

moment.locale('es');

@Pipe({
  name: 'periodEventual'
})
export class PeriodEventualPipe implements PipeTransform {

  transform(period: number, year: number): any {
    return this.setPeriod(period, year);
  }

  private setPeriod(pos: number, year: number): any {
    let period = 'invalido';
    if (pos > 0 && pos < 27) {
      const dateIni = moment('2017-12-31').add((((year - 2018) * 26 + (pos - 1)) * 14), 'days');
      const dateEnd = moment(dateIni.format('YYYY-MM-DD')).add(13, 'days');
      period = dateIni.format('DD-MMM-YY') + ' / ' + dateEnd.format('DD-MMM-YY');
    }
    return period;
  }
}
