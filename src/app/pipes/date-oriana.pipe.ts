import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dateOriana'
})
export class DateOrianaPipe implements PipeTransform {
  private mount = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

  transform(value: any): string {
    if (value != null) {
      const _dates = value.split(' ');
      if (Number(_dates.length) === 1) {
        const _value = value.split('-');
        const month = this.mount[Number(_value[1] - 1)];
        const year = _value[0].substr(2, 2);
        return _value[2] + '-' + month + '-' + year;
      } else if (Number(_dates.length) === 2) {
        const _value = _dates[0].split('-');
        const month = this.mount[Number(_value[1] - 1)];
        const year = _value[0].substr(2, 2);
        return _value[2] + '-' + month + '-' + year + ' ' + _dates[1];
      }
    } else {
      return '';
    }
  }
}
