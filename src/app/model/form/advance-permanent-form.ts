export class AdvancePermanentForm {
  constructor(
    public idPermanent: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
