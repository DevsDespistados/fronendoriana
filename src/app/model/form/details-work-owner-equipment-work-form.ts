export class DetailsWorkOwnerEquipmentWorkForm {
  constructor(
    public idBonusDiscount: number,
    public idOwnerEquipmentForm: number,
    public idOwnerEquipment: number,
    public BonusDetails: string,
    public DiscountDetails: string
  ) {
  }
}
