export class ProviderFormBankTransaction {
  constructor(
    public idTransaction: number,
    public typeTransaction: string,
    public dateTransaction: any,
    public nroReceiptTransaction: string,
    public descriptionTransaction: string,
    public amountTransaction: number,
    public idCategory_transaction: number,
    public idBankPeriod: number,
    public idBankExecutor: number,
    public idProject: number,
    public idCheck: number,
    public idActor: number,
    public code_transaction: number,
    public id: number,
    public idProviderForm: number,
    public paidOut: number
  ) {
  }
}
