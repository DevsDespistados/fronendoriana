export class DetailsWorkPermanentWorkForm {
  constructor(
    public idBonusDiscount: number,
    public idPermanentForm: number,
    public idPermanent: number,
    public BonusDetails: string,
    public DiscountDetails: string
  ) {
  }
}
