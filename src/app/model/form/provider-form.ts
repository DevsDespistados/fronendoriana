export class ProviderForm {
  constructor(
    public idProviderForm: number,
    public periodProviderForm: any,
    public advanceProviderForm: number,
    public discountProviderForm: number,
    public anticipationProviderForm: number,
    public liquidPayableProviderForm: number,
    public activeProviderForm: any,
    public idProvider_providers: number,
    public balanceAux: number,
    public mountPayAux: number
  ) {
  }
}
