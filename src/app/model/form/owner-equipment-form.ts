export class OwnerEquipmentForm {
  constructor(
    public idOwnerEquipmentForm: number,
    public periodOwnerEquipmentForm: any,
    public activeOwnerEquipmentForm: any,
    public mountPayAux: number,
    public totalAmount: number,
    public bondOwnerEquipmentForm: number,
    public discountOwnerEquipmentForm: number,
    public anticipationOwnerEquipmentForm: number,
    public liquidPayableOwnerEquipmentForm: number,
    public balanceAux: number
  ) {
  }
}
