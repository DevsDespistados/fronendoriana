export class AdvanceHeavyVehicleForm {
  constructor(
    public idHeavyVehicle: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
