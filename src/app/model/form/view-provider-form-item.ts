export class ViewProviderFormItem {
  constructor(
    public idProviderFormItem: number,
    public id_item: number,
    public item_name: any,
    public item_unity: string,
    public item_price: number,
    public item_type: any,
    public item_previousAmount: number,
    public item_previousQuantity: number,
    public item_actualAmount: number,
    public item_actualQuantity: number,
    public item_accumulatedAmount: number,
    public item_accumulatedQuantity: number,
    public idProviderForm: number,
    public idProvider: number,
  ) {
  }
}
