export class DetailsWorkLightEquipmentWorkForm {
  constructor(
    public idBonusDiscount: number,
    public idLightEquipmentForm: number,
    public idLightEquipment: number,
    public BonusDetails: string,
    public DiscountDetails: string
  ) {
  }
}
