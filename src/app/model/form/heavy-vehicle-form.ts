export class HeavyVehicleForm {
  constructor(
    public idHeavyVehicleForm: number,
    public periodHeavyVehicleForm: string,
    public bondHeavyVehicleForm: number,
    public discountHeavyVehicleForm: number,
    public anticipationHeavyVehicleForm: number,
    public liquidPayableHeavyVehicleForm: number,
    public activeHeavyVehicleForm: number,
    public balanceAux: number,
    public mountPayAux: number,
  ) {
  }
}
