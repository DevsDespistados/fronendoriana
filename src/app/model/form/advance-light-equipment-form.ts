export class AdvanceLightEquipmentForm {
  constructor(
    public idLightEquipment: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
