export class AdvacedEventualForm {
  constructor(
    public idEventual: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
