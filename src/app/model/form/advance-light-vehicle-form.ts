export class AdvanceLightVehicleForm {
  constructor(
    public idLightVehicle: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
