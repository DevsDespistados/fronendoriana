export class DetailsWorkLightVehicleWorkForm {
  constructor(
    public idBonusDiscount: number,
    public idLightVehicleForm: number,
    public idLightVehicle: number,
    public BonusDetails: string,
    public DiscountDetails: string
  ) {
  }
}
