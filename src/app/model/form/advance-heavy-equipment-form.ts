export class AdvanceHeavyEquipmentForm {
  constructor(
    public idHeavyEquipment: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
