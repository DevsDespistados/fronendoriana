export class EventualForm {
  constructor(
    public idEventualForm: number,
    public year: number,
    public biweekly: number,
    public discountEventualForm: number,
    public bondEventualForm: number,
    public advanceEventualForm: number,
    public liquidEventualForm: number,
    public statusEventualForm: any,
    public balanceAux: number,
    public mountPayAux: number,
    public mountTotal: number
  ) {
  }
}
