export class ViewOwnerEquipmentForm {
  constructor(
    public idOwnerEquipment: number,
    public nameOwnerEquipment: string,
    public unityCost: any,
    public costOwnerEquipment: number,
    public idOwnerEquipmentForm: number,
    public availableOwnerEquipment: boolean,
    public timeMonth: number,
    public priceHour: number,
    public totalAmount: number,
    public bondsMonth: number,
    public discountMonth: number,
    public anticipationMonth: number,
    public liquidPayableMonth: number,
    public idProject,
    public nameProject: string,
    public idOwnerEquipmentProjectForm: number,
  ) {
  }

  // constructor(
  // public timeMonth: number,
  // public totalAmount: number,
  // public bondsMonth: number,
  // public discountMonth: number,
  // public anticipationMonth: number,
  // public liquidPayableMonth: number,
  // public idOwnerEquipment: number,
  // public idOwnerEquipmentForm: number,
  // public priceHour: number,
  // public idProject: number,
  // public nameProject: number,
  // ) {}

}
