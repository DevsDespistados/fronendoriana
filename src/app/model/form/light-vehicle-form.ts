export class LightVehicleForm {
  constructor(
    public idLightVehicleForm: number,
    public noLightVehicleForm: string,
    public periodLightVehicleForm: string,
    public bondLightVehicleForm: number,
    public discountLightVehicleForm: number,
    public anticipationLightVehicleForm: number,
    public liquidPayableLightVehicleForm: number,
    public activeLightVehicleForm: number,
    public balanceAux: number,
    public mountPayAux: number,
  ) {
  }
}
