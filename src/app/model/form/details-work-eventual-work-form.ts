export class DetailsWorkEventualWorkForm {
  constructor(
    public idBonusDiscount: number,
    public idEventualForm: number,
    public idEventual: number,
    public BonusDetails: string,
    public DiscountDetails: string
  ) {
  }
}
