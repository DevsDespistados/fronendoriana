export class EventualWorkProjects {
  constructor(
    public idEventualWorkProject: number,
    public idProject: number,
    public idEventualForm: number,
    public idEventual: number,
    public sun1: number,
    public mon1: number,
    public tue1: number,
    public wed1: number,
    public thu1: number,
    public fri1: number,
    public sat1: number,
    public sun2: number,
    public mon2: number,
    public tue2: number,
    public wed2: number,
    public thu2: number,
    public fri2: number,
    public sat2: number,
    public totalDays: number,
    public salaryJornal: number,
    public basicMount: number,
    public bonus: number,
    public discount: number,
    public mountTotal: number,
    public advance: number,
    public liquidPay: number
  ) {
  }
}
