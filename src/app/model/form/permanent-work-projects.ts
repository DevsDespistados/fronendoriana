export class PermanentWorkProjects {
  constructor(
    public idPermanentWorkProject: number,
    public idProject: number,
    public idPermanentForm: number,
    public idPermanent: number,
    public days: number,
    public salaryMount: number,
    public basicMount: number,
    public bonus: number,
    public discount: number,
    public mountTotal: number,
    public advance: number,
    public liquidPay: number
  ) {
  }
}
