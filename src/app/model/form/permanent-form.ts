export class PermanentForm {
  constructor(
    public idPermanentForm: number,
    public periodicalPermanentForm: any,
    public discountPermanentForm: number,
    public bondPermanentForm: number,
    public advancePermanentForm: number,
    public liquidPermanentForm: number,
    public statusPermanentForm: any,
    public balanceAux: number,
    public mountPayAux: number,
    public mountTotal: number,
  ) {
  }
}
