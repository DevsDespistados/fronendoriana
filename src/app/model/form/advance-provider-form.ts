export class AdvanceProviderForm {
  constructor(
    public idProvider: number,
    public idTransaction: number,
    public date: any,
    public canceled: boolean,
    public recib: string,
    public description: string
  ) {
  }
}
