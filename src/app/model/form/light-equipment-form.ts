export class LightEquipmentForm {
  constructor(
    public idLightEquipmentForm: number,
    public noLightEquipmentForm: string,
    public periodLightEquipmentForm: string,
    public bondLightEquipmentForm: number,
    public discountLightEquipmentForm: number,
    public anticipationLightEquipmentForm: number,
    public liquidPayableLightEquipmentForm: number,
    public activeLightEquipmentForm: number,
    public balanceAux: number,
    public mountPayAux: number,
  ) {
  }
}
