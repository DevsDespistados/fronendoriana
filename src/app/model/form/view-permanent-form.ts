export class ViewPermanentForm {
  constructor(
    public idPermanent: number,
    public namePermanent: string,
    public lastNamePermanent: string,
    public idPermanentForm: number,
    public idProject: number,
    public nameProject: string,
    // public idEventualForm: number,
    public days: number,
    public salaryMount: number,
    public basicMount: number,
    public discount: number,
    public bonus: number,
    public mountTotal: number,
    public advance: number,
    public liquidPay: number
  ) {
  }
}
