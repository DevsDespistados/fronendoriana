export class DetailsWorkHeavyVehicleWorkForm {
  constructor(
    public idBonusDiscount: number,
    public idHeavyVehicleForm: number,
    public idHeavyVehicle: number,
    public BonusDetails: string,
    public DiscountDetails: string
  ) {
  }
}
