export class ViewTracingLetter {
  constructor(
    public idLetter: number,
    public numberLetterL: string,
    public stateLetter: string,
    public id: number,
    public name: string,
    public lastName: string,
    public active: boolean,
    public idTracingLetter: number,
    public instructionTracingLetter: string,
    public dateTracingLetter: any,
    public stateTracingLetter: string,
    public from_user_id?: number,
  ) {
  }
}
