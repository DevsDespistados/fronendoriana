export class ViewLetter {
  constructor(
    public idLetter: number,
    public numberLetterL: string,
    public yearLetter: any,
    public numberLetter: string,
    public typeLetter: string,
    public citeLetter: string,
    public referenceLetter: string,
    public dateLetter: any,
    public fileLetter: string,
    public stateLetter: string,
    public attachedLetter: string,
    public idProject: number,
    public nameProject: string,
    public codeLetterProject: string,
    public idUser: number,
    public file_letter?: string,
    public dateTracingLetter?: string,
    public from_or_to?: string,
    public last_user?: number,
  ) {
  }
}
