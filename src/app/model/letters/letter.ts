export class Letter {
  constructor(
    public idLetter: number,
    public numberLetter: string,
    public typeLetter: string,
    public citeLetter: string,
    public referenceLetter: string,
    public dateLetter: any,
    public senderLetter: string,
    public stateLetter: string,
    public idProject: number,
    public numberSheetLetter: number,
    public fileLetter: string,
  ) {
  }
}
