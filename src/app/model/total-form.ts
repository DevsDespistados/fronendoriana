export class TotalForm {
  // public total: number;
  // public discount: number;
  // public bond: number;
  // public advance: number;
  // public liquid: number;
  constructor(
    public total: number,
    public discount: number,
    public bond: number,
    public advance: number,
    public liquid: number) {
  }
}
