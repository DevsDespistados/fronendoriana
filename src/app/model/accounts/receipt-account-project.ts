export class ReceiptAccountProject {
  constructor(
    public idTransferProject: number,
    public idProjectDestine: number,
    public idProjectOrigin: number,
    public amountReceiptProject: number,
    public dateReceiptProject: any,
    public numberReceiptProject: any,
    public descriptionReceiptProject: string,
  ) {
  }
}
