export class Bank {
  constructor(
    public idBank: number,
    public nameAccountBank: string,
    public balanceBank: number,
    public activeBank: any,
    public isPettyCash: boolean
  ) {
  }
}
