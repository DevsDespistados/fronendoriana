export class ViewBankPeriod {
  constructor(
    public idBankPeriod: number,
    public bankPeriod: any,
    public stateBankPeriod: any,
    public transactions: number,
    public idBank: number,
    public mount_period: number,
    public mountPay: number,
    public balance: number
  ) {
  }

}
