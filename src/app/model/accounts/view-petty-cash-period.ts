export class ViewPettyCashPeriod {
  constructor(
    public idPettyCash: number,
    public nameAccountPettyCash: string,
    public balancePettyCash: number,
    public activePettyCash: number,
    public idPettyCashPeriod: number,
    public datePeriod: any,
    public period: any,
    public statePettyCashPeriod: any,
  ) {
  }
}
