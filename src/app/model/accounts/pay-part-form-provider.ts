export class PayPartFormProvider {
  constructor(
    public idPayPartProviderForm: number,
    public idPayFormProvider: number,
    public idBank: number,
    public datePay: any,
    public nroReceipt: number,
    public nameBank: string,
    public mountPay: number,
    public balance: number,
    public idExecutor: number
  ) {
  }
}
