export class PayPartForm {
  constructor(
    public idPayPart: number,
    public idForm: number,
    public idTransaction: number,
    public idBank: number,
    public datePay: any,
    public nroReceipt: string,
    public nameBank: string,
    public mountPay: number,
    public balance: number,
    public idExecutor: number,
    public formOwner: string,
  ) {
  }
}
