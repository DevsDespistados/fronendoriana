export class PayPartPermanentForm {
  constructor(
    public idPayPart: number,
    public mountPay: number,
    public nameBank: string,
    public idBank: number,
    public idPermanentForm: number,
    public datePay: any,
    public nroReceipt: string,
    public balance: number,
    public idExecutor: number
  ) {
  }
}
