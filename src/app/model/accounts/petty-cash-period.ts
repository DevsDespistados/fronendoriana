export class PettyCashPeriod {
  constructor(
    public idPettyCashPeriod: number,
    // public yearPettyCashPeriod: number,
    // public monthPettyCashPeriod: string,
    public datePeriod: any,
    public statePettyCashPeriod: number,
    public idPettyCash: number
  ) {
  }
}
