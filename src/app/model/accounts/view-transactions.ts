export class ViewTransactions {
  constructor(
    public idTransaction: number,
    public typeTransaction: string,
    public dateTransaction: any,
    public nroReceiptTransaction: string,
    public descriptionTransaction: string,
    public amountTransaction: number,
    public idCategory_transaction: number,
    public nameCategory: string,
    public nameAccountTransaction: string,
    public idBankPeriod: number,
    public idBankExecutor: number,
    public idProject: number,
    public nameProject: string,
    public idCheck: number,
    public numberCheck: string,
    public idActor: number,
    public code_transaction: number,
  ) {
  }
}
