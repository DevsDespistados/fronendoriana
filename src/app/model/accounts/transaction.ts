export class Transaction {
  constructor(
    public idTransaction: number,
    public typeTransaction: string,
    public dateTransaction: any,
    public nroReceiptTransaction: string,
    public descriptionTransaction: string,
    public amountTransaction: number,
    public idCategory_transaction: number,
    public idBankPeriod: number,
    public idBankExecutor: number,
    // public idBankPeriod: number,
    public idProject: number,
    public idCheck: number,
    public idActor: number,
    public code_transaction: number,
    // public code: number
  ) {
  }
}

// 'idTransaction',
//   'typeTransaction',
//   'dateTransaction',
//   'nroReceiptTransaction',
//   'descriptionTransaction',
//   'amountTransaction',
//   'idCategory_transaction',
//   'idBankPeriod',
//   'idBankExecutor',
//   'idProject',
//   'idCheck',
//   'idActor',
//   'code_transaction',
