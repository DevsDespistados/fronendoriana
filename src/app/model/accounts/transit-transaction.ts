export class TransitTransaction {
  constructor(
    public idTransitTransaction: number,
    public typeTransaction: any,
    public dateTransaction: any,
    public nroReceiptTransaction: string,
    public descriptionTransaction: string,
    public amountTransaction: number,
    public idBankPeriod: number,
    public idExecutor: number,
    public actor: number,
    public code: number,
  ) {
  }
}
