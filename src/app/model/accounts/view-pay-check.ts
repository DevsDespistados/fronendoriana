export class ViewPayCheck {
  constructor(
    public idPayCheck: number,
    public numberCheck: string,
    public dateCheck: any,
    public mountCheck: number,
    public type: string,
    public fileCheck: any,
    public idBankPeriod_period: number,
    public mountPay: number,
    public balance: number
  ) {
  }
}
