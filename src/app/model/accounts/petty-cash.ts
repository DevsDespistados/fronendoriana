export class PettyCash {
  constructor(
    public idPettyCash: number,
    public nameAccountPettyCash: string,
    public balancePettyCash: number,
    public activePettyCash: any,
    public idUser: number
  ) {
  }
}
