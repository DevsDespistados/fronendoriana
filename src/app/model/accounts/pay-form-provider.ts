export class PayFormProvider {
  constructor(
    public idPayFormProvider: number,
    public idContract: number,
    public period: string,
    public totalPay: number,
    public amountPay: number,
    public balance: number,
    public completed: number,
  ) {
  }
}
