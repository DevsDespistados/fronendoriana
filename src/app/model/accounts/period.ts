export class Period {
  constructor(
    public idBankPeriod: number,
    public bankPeriod: any,
    public stateBankPeriod: any,
    public transactions: number,
    public idBank: number
  ) {
  }
}
