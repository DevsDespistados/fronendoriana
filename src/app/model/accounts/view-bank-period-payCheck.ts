export class ViewBankPeriodPayCheck {
  constructor(
    public nameAccountBank: string,
    public balanceBank: number,
    public activeBank: boolean,
    public isPettyCash: boolean,
    public idBankPeriod: number,
    public bankPeriod: any,
    public stateBankPeriod: any,
    public idBank: number,
    public idPayCheck: number,
    public numberCheck: string,
    public dateCheck: any,
    public mountCheck: number,
    public mountPay: number,
  ) {
  }
}
