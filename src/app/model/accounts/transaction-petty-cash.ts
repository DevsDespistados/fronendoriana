export class TransactionPettyCash {
  constructor(
    public idPettyCashExpense: number,
    public nameProject: string,
    public idProject: number,
    public datePettyCashExpense: any,
    public nroReceiptPettyCashExpense: string,
    public amountPettyCashExpense: number,
    public descriptionPettyCashExpense: string,
    public idPettyCashPeriod: number,
    public idPettyCash: number,
    public idTransaction: number
  ) {
  }
}
