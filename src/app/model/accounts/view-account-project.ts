export class ViewAccountProject {
  // constructor(
  //   public idProject: number,
  //   public nameProject: string,
  //   public activeProjectAccount: boolean,
  //   public idTransferProject: number,
  //   public incomeTransferProject: number,
  //   public expenseTransferProject: number,
  //   public balanceTransferProject: number,
  //   public stateTransferProject: boolean
  // ) {}
  constructor(
    public idProject: number,
    public activeProjectAccount: boolean,
    public nameProject: string,
    public income: number,
    public expense: number,
    public balance: number
  ) {
  }
}
