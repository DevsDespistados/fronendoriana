export class ViewTicket {
  constructor(
    public idTicket: number,
    public typeTicket: string,
    public amountWarrantyTicket: number,
    public stateTicket: string,
    public startDateTicket: any,
    public endDateTicket: any,
    public fileTicket: string,
    public idFinancial: number,
    public nameFinancial: string,
    public stateFinancial: string,
    public amountLimitFinancial: number,
    public amountAvailableFinancial: number,
    public idProject: number,
    public nameProject: string,
    public idTicketRenovated: number,
    public dependencyFinancial: number,
    public childTicket: number,
  ) {
  }
}
