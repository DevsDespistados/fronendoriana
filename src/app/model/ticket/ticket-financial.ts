export class TicketFinancial {
  constructor(
    public idTicket: number,
    public typeTicket: string,
    public amountWarrantyTicket: number,
    public stateTicket: string,
    public startDateTicket: any,
    public endDateTicket: any,
    public idProject: number,
    public idFinancial: number,
    public idTicketRenovated: number
  ) {
  }
}
