export class Financial {
  constructor(
    public idFinancial: number,
    public nameFinancial: string,
    public stateFinancial: string,
    public amountLimitFinancial: number,
    public amountUsedFinancial: number,
    public availableBalanceFinancial: number,
    public dependencyFinancial: number,
  ) {
  }
}
