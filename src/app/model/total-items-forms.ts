export class TotalItemsForms {
  constructor(
    public previous_amount: number,
    public actual_amount: number,
    public accumulated_amount: number
  ) {
  }
}
