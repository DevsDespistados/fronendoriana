export class TracingProject {
  constructor(
    public id_tracing: number,
    public stateProject: string,
    public idProject_project: number,
    public deleted_at: any,
    public created_at: any,
    public updated_at: any
  ) {
  }
}
