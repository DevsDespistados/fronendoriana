export interface UserModuleProject {
  name: string;
  created_at: string;
  id: number;
  id_user: number;
  lastName: string;
  module: string;
  project_id: number;
  updated_at: string;
  user_id: number;
  username: string;
}
