export class ProjectSpSheet {
  constructor(
    public idProjectForm: number,
    public TypeForm: string,
    public documentForm: string,
    public dateForm: any,
    public sinceDateForm: any,
    public tillDateForm: any,
    public amountForm: number,
    public advanceDiscountForm: any,
    public liquidPayableForm: number,
    public descriptionForm: string,
    public fileForm: string,
    public mountPayAux: number,
    public balanceAux: number,
    public projects_idProject: number
  ) {
  }
}
