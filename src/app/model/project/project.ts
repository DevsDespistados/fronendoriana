export class Project {
  constructor(
    public idProject: number,
    public nroContractProject: string,
    public nameProject: string,
    public amountInitProject: number,
    public amountProject: number,
    public limitProject: number,
    public limitUpdateProject: number,
    public statesProject: string,
    public fileProject: string,
    public finalDateProject: any,
    public daysPassedProject: number,
    public physicalAdvanceProject: number,
    public activeProject: boolean,
    public activeProjectForForms: boolean,
    public limitContractProject: number,
    public startDateOP: any,
    public dateRP: any,
    public limitRPtoRDRP: number,
    public dateRD: any,
    public idClient: number,
    public activeProjectAccount: any,
    public descriptionRD: string,
    public codeLetterProject: string,
    public activeLetterForProject: any,
    public activeProjectForTicket: any,
  ) {
  }
}
