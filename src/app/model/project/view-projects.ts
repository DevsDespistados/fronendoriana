export class ViewProjects {
  constructor(
    public idProject: number,
    public nroContractProject: string,
    public nameProject: string,
    public amountInitProject: number,
    public limitProject: number,
    public stateProject: string,
    public fileProject: string,
    public activeProject: boolean,
    public activeProjectForForms: any,
    public activeProjectAccount: boolean,
    public startDateOP: any,
    public dateRP: any,
    public limitRPtoRDRP: number,
    public dateRD: any,
    public idClient: number,
    public descriptionRD: string,
    public codeLetterProject: string,
    public activeLetterForProject: boolean,
    public activeProjectForTicket: boolean,
    public daysPassed: number,
    public physicalAdvanceProject: number,
    public limitUpdateProject: number,
    public endDate: any,
    public amountProject: number
  ) {
  }
}
