export class ProjectMdf {
  constructor(
    public idProjectModification: number,
    public TypeModification: string,
    public documentModification: string,
    public dateModification: any,
    public daysModification: number,
    public amountModification: number,
    public descriptionModification: string,
    public fileModification: string,
    public projects_idProject: number) {
  }
}
