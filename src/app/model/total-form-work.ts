export class TotalFormWork {
  constructor(
    public advance: number,
    public basicMount: number,
    public bonus: number,
    public days: number,
    public discount: number,
    public liquidPay: number,
    public mountTotal: number,
    public salaryMount: number) {
  }
}
