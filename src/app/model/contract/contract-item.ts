export class ContractItem {
  constructor(
    public id_item: number,
    public item_name: string,
    public item_unity: string,
    public item_price: number,
    public item_Type: any,
    public item_previousAmount: any,
    public item_previousQuantity: any,
    public idContract_contract: number
  ) {
  }
}
