export class ContractProvider {
  constructor(
    public idContract: number,
    public numberContract: string,
    public idCategoryContract: number,
    public activeContract: boolean,
    public projects_idProject: number,
    public provider_idProvider: number
  ) {
  }
}
