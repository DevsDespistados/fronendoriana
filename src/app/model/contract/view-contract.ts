export class ViewContract {
  constructor(
    public idContract: number,
    public numberContract: string,
    public categoryContract: string,
    public idProvider: number,
    public nameProvider: string,
    public lastNameProvider: string,
    public activeContract: any,
    public idProject: number,
    public nameProject: string,
    public idCategory: number
  ) {
  }
}
