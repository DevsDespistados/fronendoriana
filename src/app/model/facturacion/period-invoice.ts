export class PeriodInvoice {
  constructor(
    public idBillingPeriod: number,
    public billingPeriod: any,
    public stateBilling: boolean) {
  }
}
