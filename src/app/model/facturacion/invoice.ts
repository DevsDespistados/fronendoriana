export class Invoice {
  constructor(
    public idInvoice: number,
    public dateInvoice: any,
    public nitInvoice: number,
    public nameInvoice: string,
    public numberInvoice: number,
    public numberAutoInvoice: number,
    public amountTotalInvoice: number,
    public amountNotCreditInvoice: number,
    public subTotalInvoice: number,
    public discountInvoice: number,
    public amountCreditInvoice: number,
    public fiscalCreditInvoice: number,
    public codeControlInvoice: number,
    public typeBuyInvoice: number,
  ) {
  }
}
