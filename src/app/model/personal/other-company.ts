export class OtherCompany {
  constructor(public idAnotherCompany: number,
              public nameAnotherCompany: string,
              public fileAnotherCompany: string,
              public activeAnotherCompany: any) {
  }
}
