export class Permanent {
  constructor(
    public idPermanent: number,
    public namePermanent: string,
    public salaryPermanent: number,
    public activePermanent: any,
    public filePermanent: string
  ) {
  }
}
