export class Client {
  constructor(public idClient: number,
              public nameClient: string,
              public activeClient: any,
              public fileClient: string) {
  }
}
