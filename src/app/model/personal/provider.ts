export class Provider {
  constructor(
    public idProvider: number,
    public nameProvider: string,
    public activeProvider: any,
    public fileProvider: string
  ) {
  }
}
