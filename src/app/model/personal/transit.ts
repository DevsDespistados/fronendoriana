export class Transit {
  constructor(
    public idTransit: number,
    public nameTransit: string,
    public activeTransit: any,
    public fileTransit: string
  ) {
  }
}
