export const ConfText = {
  accept: 'aceptar',
  save: 'guardar',
  cancel: 'cancelar',
  file: 'archivo',
  timer: 2000,
  resultSearch: 'Resultado de Busqueda',
  messageDecimal: 'Utilizar . decimal'
};
export const modal = {
  widthCreate: '300px',
  widthDelete: '400px'
};

export const month = [
  'ENERO',
  'FEBRERO',
  'MARZO',
  'ABRIL',
  'MAYO',
  'JUNIO',
  'JULIO',
  'AGOSTO',
  'SEPTIEMBRE',
  'OCTUBRE',
  'NOVIEMBRE',
  'DICIEMBRE'
];
