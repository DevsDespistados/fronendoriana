export class User {

  constructor(
    public id: number,
    public name: string,
    public lastName: string,
    public username: string,
    public password: string,
    public active: any,
    public iconImg: string
  ) {
  }
}
