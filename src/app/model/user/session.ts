import {User} from './user';

export class Session {
  public token: string; /*para las peticiones al backend*/
  public user: User;
}
