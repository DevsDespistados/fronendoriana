export class ViewUser {
  constructor(
    public id: number,
    public name: string,
    public lastName: string,
    public username: string,
    public password: string,
    public active: boolean,
    public idPermission: number,
    public namePermission: string,
    public readPermission: boolean,
    public writePermission: boolean
  ) {
  }
}
