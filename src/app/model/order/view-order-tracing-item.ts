export class ViewOrderTracingItem {
  constructor(
    public idOrderTracing: number,
    public idOrderItem: number,
    public idItem: number,
    public idOrder: number,
    public amountOrderItem: number,
    public priceUnityItem: number,
    public materialItem: string,
    public unityItem: string,
    public previewSubmitItem: number,
    public submitOrderItem: number,
    public balanceOrderItem: number,
    public amountPayOrderTracing: number,
    public idOrderBuy: number) {
  }
}
