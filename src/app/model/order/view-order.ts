export class ViewOrder {
  constructor(
    public idOrder: number,
    public numberPeriodOrder: string,
    public dateOrder: string,
    public idProject: number,
    public nameProject: string,
    public stateOrderPetition: any,
    public stateText: string,
    // public stateOrderBuy: any,
    // public stateOrderTracing: any,
    public amountTracing: number,
  ) {
  }
}
