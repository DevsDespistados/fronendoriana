export class ViewTracingOrder {
  constructor(
    public idOrder: number,
    public idItem: boolean,
    public materialItem: number,
    public unityItem: number,
    public priceUnityItem: number,
    public idOrderItem: number,
    public submitOrderItem: number,
    public amountOrderItem: number,
    public balanceOrderItem: number,
    public idOrderTracing: number,
    public amountPayOrderTracing: number,
    public idOrderBuy: number,
    public dateOrderBuy: any,
    public amountOrderBuy: number,
    public stateOrderBuy: any,
  ) {
  }
}
