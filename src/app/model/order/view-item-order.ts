export class ViewItemOrder {
  constructor(
    public idOrderItem: number,
    public idItem: number,
    public materialItem: string,
    public unityItem: string,
    public priceUnityItem: number,
    public submitOrderItem: number,
    public amountOrderItem: number,
    public totalOrderItem: number,
    public balanceOrderItem: number,
    public idOrder: number,
    public updated_at: any
  ) {
  }
}
