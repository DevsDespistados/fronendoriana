export class OwnerEquipment {
  constructor(
    public idOwnerEquipment: number,
    public nameOwnerEquipment: string,
    public codeOwnerEquipment: string,
    public costOwnerEquipment: number,
    public unityCost: string,
    public fileOwnerEquipment: string,
    public availableOwnerEquipment: any,
  ) {
  }
}
