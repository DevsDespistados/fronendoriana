export const environment = {
  production: true,
  SERVERNAME: 'https://api.orianasrl.com/api',
  REPORT_SERVERNAME: 'https://report.orianasrl.com/reportserver/httpauthexport?user=oriana&apikey=eab57e94-751b-40a7-ad21-363d283fd0b0&format=PDF&download=false',
};
